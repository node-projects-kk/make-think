const mongoose = require('mongoose');
const User = require('user/models/user.model');
const Coins = require('coins/models/coins.model');
const role = require('role/models/role.model');
const rolePermission = require('permission/models/role_permission.model');
const perPage = config.PAGINATION_PERPAGE;

const userRepository = {
    fineOneWithRole: async (params) => {
        try {
            let user = await User.findOne({
                email: params.email,
                isDeleted: false,
                isActive: true
            }).populate('role').exec();

            if (!user) {
                throw {
                    "status": 500,
                    data: null,
                    "message": 'Authentication failed. User not found.'
                }
            }

            if (!user.validPassword(params.password, user.password)) {
                throw {
                    "status": 500,
                    data: null,
                    "message": 'Authentication failed. Wrong password.'
                }
            } else {
                throw {
                    "status": 200,
                    data: user,
                    "message": ""
                }
            }
        } catch (e) {
            return e;
        }

    },

    findIsAccess: async (user_role, permission_id) => {
        try {
            let roleInfo = await rolePermission.findOne({
                $and: [{
                    'role': user_role
                }, {
                    'permissionall': {
                        $in: [permission_id]
                    }
                }]
            }).exec();
            let is_access = (roleInfo != null) ? true : false;
            throw {
                "status": 200,
                is_access: is_access,
                "message": ""
            }
        } catch (e) {
            return e;
        }
    },

    getAllUsers: async (req) => {
        try {
            var conditions = {};
            var and_clauses = [];

            and_clauses.push({
                "isDeleted": false,
                "user_role.role": req.body.role
            });
            // and_clauses.push({
            //     "user_role.role": 'principal'
            // });

            if (_.isObject(req.body.query) && _.has(req.body.query, 'generalSearch')) {
                and_clauses.push({
                    $or: [{
                        'first_name': {
                            $regex: req.body.query.generalSearch,
                            $options: 'i'
                        }
                    }, {
                        'last_name': {
                            $regex: req.body.query.generalSearch,
                            $options: 'i'
                        }
                    }, {
                        'email': {
                            $regex: req.body.query.generalSearch,
                            $options: 'i'
                        }
                    }]
                });
            }
            if (_.isObject(req.body.query) && _.has(req.body.query, 'Status')) {
                (req.body.query.Status == 'Active') ? and_clauses.push({
                    "isActive": true
                }): and_clauses.push({
                    "isActive": false
                });
                //and_clauses.push({"isActive": req.body.query.Status});
            }

            conditions['$and'] = and_clauses;
            var sortOperator = {
                "$sort": {}
            };
            if (_.has(req.body, 'sort')) {
                var sortField = req.body.sort.field;
                if (req.body.sort.sort == 'desc') {
                    var sortOrder = -1;
                } else if (req.body.sort.sort == 'asc') {
                    var sortOrder = 1;
                }

                sortOperator["$sort"][sortField] = sortOrder;
            } else {
                sortOperator["$sort"]['_id'] = -1;
            }
            var aggregate = User.aggregate([{
                    $lookup: {
                        "from": "roles",
                        "localField": "role",
                        "foreignField": "_id",
                        "as": "user_role"
                    }
                },
                {
                    "$unwind": "$user_role"
                },
                {
                    $lookup: {
                        "from": "schools",
                        "localField": "school",
                        "foreignField": "_id",
                        "as": "school_info"
                    }
                },
                {
                    "$unwind": {
                        path: "$school_info",
                        preserveNullAndEmptyArrays: true
                    }
                },
                {
                    $match: conditions
                },
                sortOperator
            ]);

            var options = {
                page: req.body.pagination.page,
                limit: req.body.pagination.perpage
            };
            let allUsers = await User.aggregatePaginate(aggregate, options);
            return allUsers;
        } catch (e) {
            throw (e);
        }
    },


    getById: async (id) => {
        let user = await User.findById(id).lean().exec();
        try {
            if (!user) {
                return null;
            }
            return user;

        } catch (e) {
            return e;
        }
    },

    getByIdWithRoleInfo: async (id) => {
        let user = await User.findById(id).populate('role').lean().exec();
        try {
            if (!user) {
                return null;
            }
            return user;
        } catch (e) {
            return e;
        }
    },

	getByField: async (params) => {
		let user = await User.findOne(params).exec();
		try {
			if (!user) {
				return null;
			}
			return user;
		}
		catch (e) {
			return e;
		}
	},
	
	 getByIdStudent: async (id) => {
        let user = await User.findById(id).populate('parent_id').lean().exec();
        try {
            if (!user) {
                return null;
            }
            return user;

        } catch (e) {
            return e;
        }
    },
	

	getAllByField: async (params) => {
		let user = await User.find(params).exec();
		try {
			if (!user) {
				return null;
			}
			return user;
		}
		catch (e) {
			return e;
		}
	},

    getLimitUserByField: async (params) => {
        let user = await User.find(params).populate('role').limit(5).sort({
            _id: -1
        }).exec();
        try {
            if (!user) {
                return null;
            }
            return user;

        } catch (e) {
            return e;
        }
    },


    delete: async (id) => {
        try {
            let user = await User.findById(id);
            if (user) {
                let userDelete = await User.remove({
                    _id: id
                }).exec();
                if (!userDelete) {
                    return null;
                }
                return userDelete;
            }
        } catch (e) {
            return e;
        }
    },

    deleteByField: async (field, fieldValue) => {
        //todo: Implement delete by field
    },


    updateById: async (data, id) => {
        try {
            let user = await User.findByIdAndUpdate(id, data, {
                new: true
            });
            if (!user) {
                return null;
            }
            return user;
        } catch (e) {
            throw e;
        }
    },

    updateOneByField:async(id,data)=>{
       try{
            let update = await User.updateOne(
                {"_id" : id},
                {$set: { "user_balance" : data}}
            );
            if (!update) {
                return null;
            }
            return update;
       }catch(e){
           return e;
       }
    },

    updateByField: async (field, fieldValue, data) => {
        try {
            let user = await User.update(fieldValue, field, {
                new: true
            });
            if (!user) {
                return null;
            }
            return user;
        } catch (e) {
            return e;
        }
    },

     updateAllByField: async(id,data) => {
        try{
            let updateAll = await User.update({ role: id },{ $set: { user_balance: data } }, { multi: true });
            if(!updateAll){
                return null;
            }
            return updateAll;
        }catch(error){
            return error;
        }
    },

    save: async (data) => {
        try {
            let user = await User.create(data);
            if (!user) {
                return null;
            }
            return user;
        } catch (e) {
            console.log("280>>Repo>>", e.message)
            return e;
        }
    },

    forgotPassword: async (params) => {
        try {
            let user = await User.findOne({
                email: params.email_p_c
            }).exec();
            if (!user) {
                return null;
            } else if (user) {

                let random_pass = Math.random().toString(36).substr(2, 9);
                let readable_pass = random_pass;
                random_pass = user.generateHash(random_pass);

                let user_details = await User.findByIdAndUpdate(user._id, {
                    password: random_pass
                }).exec();
                if (!user_details) {
                    return null;
                }
                return readable_pass;


            }

        } catch (e) {
            return e;
        }
    },

    getUser: async (id) => {
        try {
            let user = await User.findOne({
                id
            }).exec();
            if (!user) {
                return null;
            }
            return user;
        } catch (e) {
            return e;
        }
    },

    getUserByField: async (data) => {
        try {
            let user = await User.findOne(data).populate('role').lean().exec();
            if (!user) {
                return null;
            }
            return user;
        } catch (e) {
            return e;
        }
    },

    getUsersCount: async (req) => {
        try {
            var conditions = {};
            var and_clauses = [];

            and_clauses.push({
                "isDeleted": false
            });
            and_clauses.push({
                "user_role.role": {
                    "$ne": "admin"
                }
            });

            conditions['$and'] = and_clauses;
            let users = await User.aggregate([{
                    "$lookup": {
                        "from": "roles",
                        "localField": "role",
                        "foreignField": "_id",
                        "as": "user_role"
                    },
                },
                {
                    $unwind: "$user_role"
                },
                {
                    $match: conditions
                },
                {
                    $group: {
                        _id: "$user_role._id",
                        name: {
                            $first: "$user_role.role"
                        },
                        count: {
                            $sum: 1
                        }
                    }
                },
                {
                    "$sort": {
                        _id: 1
                    }
                },
            ]).exec();
            return users;
        } catch (e) {
            throw (e);
        }
    },

    ParentDashboardDetails: async (id) => {
        try {
            let users = await User.aggregate([{
                    "$lookup": {
                        "from": "users",
                        "localField": "_id",
                        "foreignField": "parent_id",
                        "as": "child_details"
                    },
                },
                {
                    $unwind: "$child_details"
                },
                {
                    "$lookup": {
                        "from": "schools",
                        "localField": "child_details.school",
                        "foreignField": "_id",
                        "as": "child_details.school_details"
                    },
                },
                {
                    $unwind: {
                        path: "$child_details.school_details",
                        preserveNullAndEmptyArrays: true
                    }
                },
                {
                    "$lookup": {
                        "from": "avatars",
                        "localField": "child_details.avatar_id",
                        "foreignField": "_id",
                        "as": "child_details.avatar_details"
                    },
                },
                {
                    $unwind: {
                        path: "$child_details.avatar_details",
                        preserveNullAndEmptyArrays: true
                    }
                },
                {
                    $match: {
                        "isDeleted": false,
                        "child_details.isDeleted": false,
                        "isActive": true,
                        "child_details.isActive": true,
                        "_id": mongoose.Types.ObjectId(id)
                    }
                },
                {
                    $group: {
                        _id: "$_id",
                        first_name: {
                            $first: '$first_name'
                        },
                        last_name: {
                            $first: '$last_name'
                        },
                        email: {
                            $first: '$email'
                        },
                        teacher_id: {
                            $first: '$teacher_id'
                        },
                        school: {
                            $first: '$school'
                        },
                        parent_id: {
                            $first: '$parent_id'
                        },
                        principle_id: {
                            $first: '$principle_id'
                        },
                        child_details: {
                            $addToSet: "$child_details"
                        },
                    }
                },
                {
                    "$sort": {
                        _id: 1
                    }
                },
            ]).exec();
            return users;
        } catch (e) {
            throw (e);
        }
    },

    ParentMyChildDetails: async (id) => {
        try {
            let users = await User.aggregate([{
                    $match: {
                        "isDeleted": false,
                        "parent_id": mongoose.Types.ObjectId(id)
                    }
                },
                {
                    "$lookup": {
                        "from": "schools",
                        "localField": "school",
                        "foreignField": "_id",
                        "as": "school_details"
                    },
                },
                {
                    $unwind: {
                        path: '$school_details',
                        preserveNullAndEmptyArrays: true
                    }
                },
                {
                    "$lookup": {
                        "from": "users",
                        "localField": "teacher_id",
                        "foreignField": "_id",
                        "as": "teacher_details"
                    },
                },
                {
                    $unwind: {
                        path: '$teacher_details',
                        preserveNullAndEmptyArrays: true
                    }
                },
                {
                    "$sort": {
                        _id: 1
                    }
                },
            ]).exec();


            return users;
        } catch (e) {
            throw (e);
        }
    },

    getStudentCountByGrade: async (req) => {
        try {
            var conditions = {};
            var and_clauses = [];

            and_clauses.push({
                "isDeleted": false,
                "isActive": true
            });
            and_clauses.push({
                "user_role.role": {
                    $eq: "student"
                },
                "grade": {
                    $ne: null
                }
            });

            conditions['$and'] = and_clauses;
            let users = await User.aggregate([{
                    "$lookup": {
                        "from": "roles",
                        "localField": "role",
                        "foreignField": "_id",
                        "as": "user_role"
                    },
                },
                {
                    $unwind: "$user_role"
                },
                {
                    $match: conditions
                },
                {
                    $group: {
                        _id: {
                            "grade": "$grade"
                        },
                        role: {
                            $first: "$user_role.role"
                        },
                        count: {
                            $sum: 1
                        }
                    }
                },
                {
                    "$sort": {
                        _id: 1
                    }
                },
            ]).exec();
            return users;
        } catch (e) {
            throw (e);
        }
    },

    getStudentsBySchool: async (schoolId) => {
        try {
            var conditions = {};
            var and_clauses = [];
            and_clauses.push({
                "isDeleted": false,
                "isActive": true
            });
            //and_clauses.push({"user_role.role": {$eq: 'student'},"school": schoolId,});
            and_clauses.push({
                "role": {
                    $eq: 'student'
                },
                "class_info.school_id": mongoose.Types.ObjectId(schoolId),
            });

            conditions['$and'] = and_clauses;

            let users = await User.aggregate([{
                    "$lookup": {
                        "from": "roles",
                        "localField": "role",
                        "foreignField": "_id",
                        "as": "user_role"
                    },
                },
                {
                    $unwind: "$user_role"
                },
                {
                    "$lookup": {
                        "from": "avatars",
                        "localField": "avatar_id",
                        "foreignField": "_id",
                        "as": "avatar_info"
                    },
                },
                {
                    $unwind: {
                        path: "$avatar_info",
                        preserveNullAndEmptyArrays: true
                    }
                },
                {
                    "$lookup": {
                        "from": "schools",
                        "localField": "school",
                        "foreignField": "_id",
                        "as": "school_info"
                    },
                },
                {
                    $unwind: {
                        path: "$school_info",
                        preserveNullAndEmptyArrays: true
                    }
                },
                {
                    "$lookup": {
                        "from": "classes",
                        "localField": "_id",
                        "foreignField": "student_id",
                        "as": "class_info"
                    },
                },
                {
                    $unwind: {
                        path: "$class_info",
                        //preserveNullAndEmptyArrays: true
                    }
                },
                {
                    $group: {
                        _id: "$_id",
                        first_name: {
                            $first: "$first_name"
                        },
                        last_name: {
                            $first: "$last_name"
                        },
                        email: {
                            $first: "$email"
                        },
                        avatar_id: {
                            $first: "$avatar_info"
                        },
                        teacher_id: {
                            $first: "$teacher_id"
                        },
                        user_balance: {
                            $first: "$user_balance"
                        },
                        role: {
                            $first: "$user_role.role"
                        },
                        school_info: {
                            $first: "$school_info"
                        },
                        class_info: {
                            $first: "$class_info"
                        },
                        isDeleted: {
                            $first: "$isDeleted"
                        },
                        isActive: {
                            $first: "$isActive"
                        }
                    }
                },
                {
                    $match: conditions
                },
                {
                    "$sort": {
                        _id: 1
                    }
                },
            ]).exec();

            return users;
        } catch (e) {
            throw (e);
        }
    },

    getTeachersBySchool: async (schoolId) => {
        try {
            var conditions = {};
            var and_clauses = [];

            and_clauses.push({
                "isDeleted": false,
                "isActive": true
            });
            //and_clauses.push({"user_role.role": {$eq: 'teacher'},"school": schoolId,});
            and_clauses.push({
                "role": {
                    $eq: 'teacher'
                },
                "class_info.school_id": schoolId,
            });

            conditions['$and'] = and_clauses;
            //console.log("687>>", conditions); 
            let users = await User.aggregate([{
                    "$lookup": {
                        "from": "roles",
                        "localField": "role",
                        "foreignField": "_id",
                        "as": "user_role"
                    },
                },
                {
                    $unwind: "$user_role"
                },
                {
                    "$lookup": {
                        "from": "avatars",
                        "localField": "avatar_id",
                        "foreignField": "_id",
                        "as": "avatar_info"
                    },
                },
                {
                    $unwind: {
                        path: "$avatar_info",
                        preserveNullAndEmptyArrays: true
                    }
                },
                {
                    "$lookup": {
                        "from": "schools",
                        "localField": "school",
                        "foreignField": "_id",
                        "as": "school_info"
                    },
                },
                {
                    $unwind: {
                        path: "$school_info",
                        preserveNullAndEmptyArrays: true
                    }
                },
                {
                    "$lookup": {
                        "from": "classes",
                        "localField": "_id",
                        "foreignField": "teacher_id",
                        "as": "class_info"
                    },
                },
                {
                    $unwind: {
                        path: "$class_info",
                        //preserveNullAndEmptyArrays: true
                    }
                },
                {
                    $group: {
                        _id: "$_id",
                        first_name: {
                            $first: "$first_name"
                        },
                        last_name: {
                            $first: "$last_name"
                        },
                        email: {
                            $first: "$email"
                        },
                        avatar_id: {
                            $first: "$avatar_info"
                        },
                        user_balance: {
                            $first: "$user_balance"
                        },
                        role: {
                            $first: "$user_role.role"
                        },
                        school_info: {
                            $first: "$school_info"
                        },
                        class_info: {
                            $first: "$class_info"
                        },
                        isDeleted: {
                            $first: "$isDeleted"
                        },
                        isActive: {
                            $first: "$isActive"
                        }
                    },
                },
                {
                    $match: conditions
                },
                {
                    "$sort": {
                        _id: 1
                    }
                },
            ]).exec();

            //console.log("758>>", users); process.exit();

            return users;
        } catch (e) {
            throw (e);
        }
    },

    getStudentsByTeacher: async (teacherId) => {
        try {
            var conditions = {};
            var and_clauses = [];

            and_clauses.push({
                "isDeleted": false,
                "isActive": true
            });
            and_clauses.push({
                "user_role.role": {
                    $eq: 'student'
                },
                "teacher_id": teacherId,
            });

            conditions['$and'] = and_clauses;
            let users = await User.aggregate([{
                    "$lookup": {
                        "from": "schools",
                        "localField": "school",
                        "foreignField": "_id",
                        "as": "school_details"
                    },
                },
                {
                    $unwind: "$school_details"
                },
                {
                    "$lookup": {
                        "from": "roles",
                        "localField": "role",
                        "foreignField": "_id",
                        "as": "user_role"
                    },
                },
                {
                    $unwind: "$user_role"
                },
                {
                    "$lookup": {
                        "from": "avatars",
                        "localField": "avatar_id",
                        "foreignField": "_id",
                        "as": "avatar_info"
                    },
                },
                {
                    $unwind: {
                        path: "$avatar_info",
                        preserveNullAndEmptyArrays: true
                    }
                },
                {
                    $match: conditions
                },
                {
                    $group: {
                        _id: "$_id",
                        first_name: {
                            $first: "$first_name"
                        },
                        last_name: {
                            $first: "$last_name"
                        },
                        email: {
                            $first: "$email"
                        },
                        avatar_id: {
                            $first: "$avatar_info"
                        },
                        teacher_id: {
                            $first: "$teacher_id"
                        },
                        user_balance: {
                            $first: "$user_balance"
                        },
                        role: {
                            $first: "$user_role.role"
                        },
                        school_details: {
                            $first: "$school_details"
                        },
                    }
                },
                {
                    "$sort": {
                        _id: 1
                    }
                },
            ]).exec();
            return users;
        } catch (e) {
            throw (e);
        }
    },

    getUserRoleandDetails: async (id) => {
        let user = await User.findById(id).populate('role').lean().exec();
        try {
            if (!user) {
                return null;
            }
            return user;
        } catch (e) {
            return e;
        }
    },

    ParentSubscriptionInfo: async (id) => {
        try {
            var conditions = {};
            var and_clauses = [];

            and_clauses.push({
                "isDeleted": false,
                "subscription_info.payment_status": "Completed",
                "_id": mongoose.Types.ObjectId(id)
            });

            conditions['$and'] = and_clauses;
            let users = await User.aggregate([{
                    "$lookup": {
                        "from": "payments",
                        "localField": "_id",
                        "foreignField": "parent_id",
                        "as": "subscription_info"
                    },
                },
                {
                    $unwind: {
                        path: "$subscription_info",
                        preserveNullAndEmptyArrays: true
                    }
                },
                {
                    $match: conditions
                }
            ]).exec();
            return users;
        } catch (e) {
            throw (e);
        }
    },

    getUserAndSchoolById: async (id) => {
        let user = await User.findById(id).populate('school').exec();
        try {
            if (!user) {
                return null;
            }
            return user;

        } catch (e) {
            return e;
        }
    },

     coinsTotalDetails:async (id) => {
        try{

            let aggregate =   Coins.aggregate([
                {$match: { "coin_to_user_id": id }},
                {
                    $group:{
                        "_id":"$_id",
                        "provider_type":{$first:"$provider_type"},
                        "coins":{$first:"$no_of_coins"},
                        "total_coins":{ $sum: "$no_of_coins" }
                    }
                },
            ]);
            console.log("1051>>>",aggregate);
            return aggregate;
        }
        catch(e){
            return e;
        }
    }

};

module.exports = userRepository;