var mongoose = require('mongoose');
require('mongoose-double')(mongoose);
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt-nodejs');
var mongooseAggregatePaginate = require('mongoose-aggregate-paginate');

const deleted = [true, false];
const providerstatus = ["parent", "teacher","priciple","admin"];

var UserSchema = new Schema({
	first_name: {type: String,default: ''},
	last_name: {	type: String,default: ''},
	email: {	type: String,default: ''},
	registered_as: {	type: String,default: ''}, //enum: ["under_a_school", "personal_use"]
	password: {type: String,default: ''},
	teacher_id: {type: Schema.Types.ObjectId,ref: 'User',	default: null},
	parent_id: {	type: Schema.Types.ObjectId,	ref: 'User',	default: null},
	principle_id: {type: Schema.Types.ObjectId,ref: 'User',default: null},
	role: {type: Schema.Types.ObjectId,ref: 'Role'},
	school: {type: Schema.Types.ObjectId,ref: 'School',default: null},
	avatar_id: {	type: Schema.Types.ObjectId,	ref: 'Avatar',default: null},
	avatars: [{
		avatar_id: {	type: Schema.Types.ObjectId,	ref: 'Avatar',default: null},
		isCurrent: {	type: Boolean,default: true,enum: [true, false]},
		_id: false
	}],
	profile_image: {	type: String,default: ''},
	user_balance: {type: Schema.Types.Double,default: 0.00},
	phone: {type: String,default: ''},
	address: {type: String,default: ''},
	city: {type: String,default: ''},
	state: {	type: String,default: ''},
	country: {type: String,default: ''},
	isVerified: {type: String,default: "No",enum: ['Yes', 'No']},
	grade: {	type: Number,default: null},
	verifyToken: {type: Number,default: null},
	deviceToken: {type: String,default: ''},
	deviceType: {type: String,default: ''},
	isDeleted: {type: Boolean,default: false,	enum: deleted},
	isActive: {type: Boolean,default: true,enum: [true, false]	},
}, {	timestamp: true});

// generating a hash
UserSchema.methods.generateHash = function (password) {
	return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
UserSchema.methods.validPassword = function (password, checkPassword) {
	return bcrypt.compareSync(password, checkPassword);
	//bcrypt.compare(jsonData.password, result[0].pass
};


// For pagination
UserSchema.plugin(mongooseAggregatePaginate);

// create the model for users and expose it to our app
module.exports = mongoose.model('User', UserSchema);