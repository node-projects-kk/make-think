const mongoose = require('mongoose');
const User = require('user/models/user.model');
const userRepo = require('user/repositories/user.repository');
const coinsRepo = require('coins/repositories/coins.repository');
const authorRepo = require('author/repositories/author.repository');
const genreRepo = require('genre/repositories/genre.repository');
const roleRepo = require('role/repositories/role.repository');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const gm = require('gm').subClass({
    imageMagick: true
});
const fs = require('fs');
const jwt = require('jsonwebtoken');
//mail send 
const {
    join
} = require('path');
const ejs = require('ejs');
const {
    readFile
} = require('fs');
const {
    promisify
} = require('util');
const readFileAsync = promisify(readFile);


class UserController {
    constructor() {
        this.users = [];

    }

    /* @Method: login
    // @Description: user Login Render
    */
    async login(req, res) {
        res.render('user/views/login.ejs');
    };

    /* @Method: signin
    // @Description: user Login
    */
	async signin(req, res) {
		try {
			let userData = await userRepo.fineOneWithRole(req.body);
			//console.log("50>>",userData); process.exit();
			if (userData.status == 500) {
				req.flash('error', userData.message);
				return res.redirect(namedRouter.urlFor('user.login'));
			}
			let user = userData.data;
			if (!_.isEmpty(user.role)) {
				const payload = {id: user._id};
				let token = jwt.sign(payload, config.jwtSecret, {expiresIn: 86400 // expires in 24 hours
				});
				req.session.token = token;
				req.user = user;
				let user_details = {};
				user_details.id = user._id;
				user_details.name = user.name;
				user_details.email = user.email;
				// return the information including token as JSON
				req.flash('success', "You have successfully logged in");
				res.redirect(namedRouter.urlFor('user.dashboard'));
			}
			else {
				req.flash('error', 'Authentication failed. Wrong credentials.');
				res.redirect(namedRouter.urlFor('user.login'));
			}
		}
		catch (e) {
			return res.status(500).send({message: e.message});
		}
	};

    /* @Method: create
    // @Description: user create view render
    */
    async create(req, res) {
        try {
            let success = {};
            let role = await roleRepo.getAll({});
            success.data = role;
            let country = await countryRepo.getAllByField({
                isActive: true
            });
            success.country = country;

            res.render('user/views/add.ejs', {
                page_name: 'user-management',
                page_title: 'Create Vehicle Owner',
                user: req.user,
                response: success
            });
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    /* @Method: studentList
    // @Description: To get all the books from DB
    */
    async studentList(req, res) {
        try {
            res.render('user/views/studentList.ejs', {
                page_name: 'user-management',
                page_title: 'Student List',
                user: req.user
            });
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    /* @Method: getAllStudent
    // @Description: To get all the students from DB
    */
    async getAllStudent(req, res) {
        try {
            req.body.role = 'student';

            if (_.has(req.body, 'sort')) {
                var sortOrder = req.body.sort.sort;
                var sortField = req.body.sort.field;
            } else {
                var sortOrder = -1;
                var sortField = '_id';
            }

            if (!_.has(req.body, 'pagination')) {
                req.body.pagination.page = 1;
                eq.body.pagination.perpage = config.PAGINATION_PERPAGE
            }
            let student = await userRepo.getAllUsers(req);

            let meta = {
                "page": req.body.pagination.page,
                "pages": student.pageCount,
                "perpage": req.body.pagination.perpage,
                "total": student.totalCount,
                "sort": sortOrder,
                "field": sortField
            };

            return {
                status: 200,
                meta: meta,
                data: student.data,
                message: `Data fetched succesfully.`
            };
        } catch (e) {
            return {
                status: 500,
                data: [],
                message: e.message
            };
        }
    }

    /**
     * @Method: studentEdit
     * @Description: To edit student information
     */
    async studentEdit(req, res) {
        try {
            let result = {};
            let student = await userRepo.getById(req.params.id);
            if (!_.isEmpty(student)) {
                result.student_data = student;
                res.render('user/views/studentEdit.ejs', {
                    page_name: 'user-management',
                    page_title: 'Update Student',
                    user: req.user,
                    response: result
                });
            } else {
                req.flash('error', "Sorry book not found!");
                res.redirect(namedRouter.urlFor('book.listing'));
            }
        } catch (e) {
            return {
                status: 500,
                data: [],
                message: e.message
            };
        }
    };

    async studentUpdate(req, res) {
        try {
            if (req.body.isActive == 'Active') {
                req.body.isActive = true;
            } else {
                req.body.isActive = false;
            }
            let studentUpdate = userRepo.updateById(req.body, req.body.uid);
            req.flash('success', 'Student information updated succesfully.');
            res.redirect(namedRouter.urlFor('student.listing'));
        } catch (e) {
            return {
                status: 500,
                data: [],
                message: e.message
            };
        }
    };

    /* @Method: delete
    // @Description: Student Delete
    */
    async deleteStudent(req, res) {
        try {
            let studentDelete = await userRepo.updateById({
                "isDeleted": true
            }, req.params.id)
            if (!_.isEmpty(studentDelete)) {
                req.flash('success', 'Student Removed Successfully');
                res.redirect(namedRouter.urlFor('student.listing'));
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };



    /* @Method: Dashboard
    // @Description: User Dashboard
    */
    async dashboard(req, res) {

        try {
            let relsultall = {};
            let user = await userRepo.getLimitUserByField({
                'isDeleted': false,
                _id: {
                    $ne: mongoose.Types.ObjectId(req.user._id)
                }
            });
            relsultall.user = user;

            /* Html render here */
            res.render('user/views/dashboard.ejs', {
                page_name: 'user-dashboard',
                page_title: 'Dashboard',
                user: req.user,
                response: relsultall
            });
        } catch (e) {
            throw (e);
            //return res.status(500).send({message: e.message}); 
        }
    };

    /* @Method: Logout
    // @Description: User Logout
    */
    async logout(req, res) {
        req.session.destroy(function (err) {
            res.redirect('/' + process.env.ADMIN_FOLDER_NAME);
        });
        // req.session.token = "";
        // req.session.destroy();
        // return res.redirect('/');
    };

    /* @Method: viewmyprofile
    // @Description: To get Profile Info from db
    */
    async viewmyprofile(req, res) {
        try {
            const id = req.params.id;
            let user = await userRepo.getById(id)
            if (!_.isEmpty(user)) {
                res.render('user/views/myprofile.ejs', {
                    page_name: 'user-profile',
                    page_title: 'My Profile',
                    user: req.user,
                    response: user
                });

            }
        } catch (e) {

            return res.status(500).send({
                message: e.message
            });
        }
    }

    /* @Method: updateprofile
    // @Description: Update My Profile 
    */
    async updateprofile(req, res) {
        try {
            const id = req.body.id;
            let userUpdate = await userRepo.updateById(req.body, id)
            if (!_.isEmpty(userUpdate)) {
                req.flash('success', "Profile updated successfully.");
                res.redirect(namedRouter.urlFor('admin.profile', {
                    id: id
                }));
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    /*
    // @Method: status_change
    // @Description: User status change action
    */
    async statusChange(req, res) {

        try {
            let user = await userRepo.getById(req.body.id)
            if (!_.isEmpty(user)) {
                let userStatus = (user.isActive == true) ? false : true;
                let userUpdate = userRepo.updateById({
                    'isActive': userStatus
                }, req.body.id);
                req.flash('success', "User status has changed successfully.");
                res.send(userUpdate);
            } else {
                req.flash('error', "sorry user not found");
                res.redirect(namedRouter.urlFor('user.list'));
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    /* @Method: changepassword
    // @Description: user changepassword Render
    */
    async adminChangePassword(req, res) {
        var vehicleOwner = await userRepo.getById(req.user._id);
        if (vehicleOwner) {
            res.render('user/views/change_password.ejs', {
                page_name: 'user-changepassword',
                page_title: 'Change Password',
                response: vehicleOwner,
                user: req.user
            });
        } else {
            req.flash('error', "sorry vehicle owner not found.");
            res.redirect(namedRouter.urlFor('user.dashboard'));
        }

    };

    /*
    // @Method: updatepassword
    // @Description: User password change
    */

    async adminUpdatePassword(req, res) {
        try {
            let user = await userRepo.getById(req.user._id);
            if (!_.isEmpty(user)) {
                // check if password matches

                if (!user.validPassword(req.body.old_password, user.password)) {
                    req.flash('error', "Sorry old password mismatch!");
                    res.redirect(namedRouter.urlFor('admin.changepassword'));
                } else {
                    // if user is found and password is right, check if he is an admin

                    let new_password = req.user.generateHash(req.body.password);
                    let userUpdate = await userRepo.updateById({
                        "password": new_password
                    }, req.body.id)
                    if (!_.isEmpty(user)) {
                        req.flash('success', "Your password has been changed successfully.");
                        res.redirect(namedRouter.urlFor('user.dashboard'));
                    }
                }
            } else {
                req.flash('error', "Authentication failed. Wrong credentials.");
                res.redirect(namedRouter.urlFor('admin.changepassword'));
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    /*
    // @Method: forgotPassword
    // @Description: User forgotPassword
    */

    async forgotPassword(req, res) {
        try {
            let result = await userRepo.forgotPassword(req.body)
            if (!_.isEmpty(result)) {
                let locals = {
                    password: result
                };
                let isMailSend = await mailer.sendMail('Admin<smith.williams0910@gmail.com>', req.body.email_p_c, 'Node latest structure New Password', 'forgot-password', locals);
                if (isMailSend) {
                    req.flash('success', "Chechk Email For New Password");
                    res.redirect(namedRouter.urlFor('user.login'));
                } else {
                    req.flash('error', "Sorry unable to send mail");
                    res.redirect(namedRouter.urlFor('user.login'));
                }
            } else {
                req.flash('error', "Sorry user not found");
                res.redirect(namedRouter.urlFor('user.login'));
            }

        } catch (e) {
            req.flash('error', e.message);
            res.redirect(namedRouter.urlFor('user.login'));
        }
    };

    async getAllUserCount(req, res) {
        try {
            let userCount = await userRepo.getUsersCount(req);
            return userCount;
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    }

    async getAllAuthorCount(req, res) {
        try {
            let authorCount = await authorRepo.getAuthorCount(req);
            return authorCount;
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    }

    async getAllGenreCount(req, res) {
        try {
            let genreCount = await genreRepo.getGenreCount(req);
            return genreCount;
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    }

    async studentCoinsDetails(req,res){
        try{
            let studentId = req.params.id;
            let getCoinsBysrudentId = await coinsRepo.getAllByField({'coin_to_user_id': studentId});
            let coinTotal = 0;

            for(let i=0; i < getCoinsBysrudentId.length; i++){
                coinTotal += getCoinsBysrudentId[i].no_of_coins
            }

            res.render('coins/views/student_coins_details.ejs', {
                page_name: 'coins-details',
                page_title: 'Student coins Details',
                user: req.user,
                response: getCoinsBysrudentId,
                total : coinTotal,
                student : studentId
            });
        }catch(e){
            return res.status(500).send({
                message: e.message
            });
        }
    }

    async adminCoin(req, res){
        try{
            var role = "admin";
            var studentId = req.body.id;
            var data = parseFloat(req.body.user_balance);
            var usersBasedOnRole = await roleRepo.getByField({'role': role});
            var adminId = usersBasedOnRole._id;
            let matchStudent = await coinsRepo.getByField({"coin_to_user_id":studentId});
            if(matchStudent){
                let matcheAdmin = await coinsRepo.getAllByField({"coin_from_user_id":adminId})
                if(matcheAdmin){
                    let coinid = matcheAdmin[0]._id
                    let updatecoin = await coinsRepo.updateOneByField(coinid,data);
                }
            }
            else{
                let coinSave = await coinsRepo.save({
                    "coin_to_user_id" : studentId,
                    "coin_from_user_id" : adminId,
                    "coin_given_date" : Date.now(),
                    "provider_type" : role,
                     "no_of_coins" : data,
            });
            }
            let getStutent = await userRepo.getById(studentId);
            let studentBalance = getStutent.user_balance;
            data +=studentBalance;
            let studentUserBalanceUpdate = await userRepo.updateOneByField(studentId,data)
            req.flash('success', 'Coins Update Successfully');
            res.redirect(namedRouter.urlFor('student.listing'));
                
       }
           catch(e){
               console.log(e)
               return res.status(500).send({
                   message: e.message
               });
           }
       }
    }

module.exports = new UserController();