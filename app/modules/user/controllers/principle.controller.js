const mongoose = require('mongoose');
const User = require('user/models/user.model');
const userRepo = require('user/repositories/user.repository');
const roleRepo = require('role/repositories/role.repository');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const gm = require('gm').subClass({
    imageMagick: true
});
const fs = require('fs');




class principleController {
    constructor() {
        this.users = [];

    }


    /*
    // @Method: edit
    // @Description:  render principle update page
    */
    async edit(req, res) {
        try {
            let result = {};
            let user = await userRepo.getUserAndSchoolById(req.params.id);
            let role_list = await roleRepo.getAll({});
            if (!_.isEmpty(user)) {
                result.user_data = user;
                result.role_list = role_list;
                res.render('user/views/principleEdit.ejs', {
                    page_name: 'user-management',
                    page_title: 'Edit Principle',
                    user: req.user,
                    response: result
                });
            } else {
                req.flash('error', "Sorry user not found!");
                res.redirect(namedRouter.urlFor('principle.getall'));
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    /* @Method: update
    // @Description: user update action
    */
    async update(req, res) {
        try {
            const userId = req.body.principleId;
            let user = await userRepo.getByField({
                'email': req.body.email,
                'isDeleted': false,
                '_id': {
                    $ne: mongoose.Types.ObjectId(userId)
                }
            });

            if (_.isEmpty(user)) {
                if (req.body.isActive == 'Active') {
                    req.body.isActive = true;
                } else {
                    req.body.isActive = false;
                }
                let userUpdate = await userRepo.updateById(req.body, userId);
                if (userUpdate) {
                    req.flash('success', "Principle Updated Successfully");
                    res.redirect(namedRouter.urlFor('principle.list'));
                }

            } else {
                req.flash('error', "This email address is already exist!");
                res.redirect(namedRouter.urlFor('principle.edit', {
                    id: userId
                }));
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    /* @Method: list
    // @Description: To get all the users from DB
    */
    async list(req, res) {
        try {
            let success = {};
            res.render('user/views/principleList.ejs', {
                page_name: 'user-management',
                page_title: 'Principle List',
                user: req.user,
                response: success
            });
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    async delete(req, res) {

        try {
            let principleDelete = await userRepo.updateById({
                "isDeleted": true
            }, req.params.id)
            if (!_.isEmpty(principleDelete)) {
                req.flash('success', 'principle Removed Successfully');
                res.redirect(namedRouter.urlFor('principle.list'));
            }

        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };


    async getAllPrinciple(req, res) {
        try {
            req.body.role = 'principal';

            if (_.has(req.body, 'sort')) {
                var sortOrder = req.body.sort.sort;
                var sortField = req.body.sort.field;
            } else {
                var sortOrder = -1;
                var sortField = '_id';
            }

            if (!_.has(req.body, 'pagination')) {
                req.body.pagination.page = 1;
                eq.body.pagination.perpage = config.PAGINATION_PERPAGE
            }
            let principle = await userRepo.getAllUsers(req);
            let meta = {
                "page": req.body.pagination.page,
                "pages": principle.pageCount,
                "perpage": req.body.pagination.perpage,
                "total": principle.totalCount,
                "sort": sortOrder,
                "field": sortField
            };
            return {
                status: 200,
                meta: meta,
                data: principle.data,
                message: `Data fetched succesfully.`
            };
        } catch (e) {
            return {
                status: 500,
                data: [],
                message: e.message
            };
        }
    }



}

module.exports = new principleController();