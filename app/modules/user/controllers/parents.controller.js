const mongoose = require('mongoose');
const User = require('user/models/user.model');
const userRepo = require('user/repositories/user.repository');
const roleRepo = require('role/repositories/role.repository');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const gm = require('gm').subClass({ imageMagick: true });
const fs = require('fs');


class parentsController {
    constructor() {
        this.users = [];
    }

    
    /*
    // @Method: edit
    // @Description:  render principle update page
    */
    async edit(req, res) {
        try {
            let result = {};
            let user = await userRepo.getById(req.params.id);
            if (!_.isEmpty(user)) {
                result.user_data = user;
                res.render('user/views/parentEdit.ejs', {
                    page_name: 'user-management',
                    page_title: 'Edit Parent',
                    user: req.user,
                    response: result
                });
            } else {
                req.flash('error', "Sorry parent not found!");
                res.redirect(namedRouter.urlFor('parents.list'));
            }
        } catch (e) {
            return res.status(500).send({ message: e.message });
        }
    };

    /* @Method: update
    // @Description: user update action
    */
    async update(req, res) {
        try {
            const userId = req.body.parentId;
            let user = await userRepo.getByField({ 'email': req.body.email, 'isDeleted': false, '_id': { $ne: mongoose.Types.ObjectId(userId) } });

            if (_.isEmpty(user)) {
                if(req.body.isActive == 'Active'){
                    req.body.isActive = true;
                }else{
                    req.body.isActive = false;
                }
                let userUpdate = await userRepo.updateById(req.body,userId);
                if (userUpdate) {
                    req.flash('success', "Parent Updated Successfully");
                    res.redirect(namedRouter.urlFor('parents.list'));
                }

            } else {
                req.flash('error', "This email address is already exist!");
                res.redirect(namedRouter.urlFor('parents.edit', { id: userId }));
            }
        } catch (e) {
            return res.status(500).send({ message: e.message });
        }

    };

    /* @Method: list
    // @Description: To get all the users from DB
    */
    async list(req, res) {
        try {
            let success = {};
            res.render('user/views/parentList.ejs', {
                page_name: 'user-management',
                page_title: 'Parent List',
                user: req.user,
                response: success
            });
        } catch (e) {
            return res.status(500).send({ message: e.message });
        }
    };

    async delete(req, res) {

        try {
            let parentDelete = await userRepo.updateById({"isDeleted": true}, req.params.id)
            if(!_.isEmpty(parentDelete)){
                req.flash('success','Parent Removed Successfully');
                res.redirect(namedRouter.urlFor('parents.list'));
            } 
            
        } catch (e) {
            return res.status(500).send({ message: e.message });
        }
    };


    async getAllParents(req, res){
        try{

            req.body.role = 'parent';
            if(_.has(req.body, 'sort')){
                var sortOrder = req.body.sort.sort;
                var sortField = req.body.sort.field;
            }else{
                var sortOrder = -1;
                var sortField = '_id';
            }

            if(!_.has(req.body, 'pagination')){
                req.body.pagination.page = 1;
                eq.body.pagination.perpage = config.PAGINATION_PERPAGE
            }
            let teacher = await userRepo.getAllUsers(req);
            let meta = {"page": req.body.pagination.page, "pages": teacher.pageCount, "perpage": req.body.pagination.perpage, "total": teacher.totalCount, "sort": sortOrder, "field": sortField}; 

            return {status: 200, meta: meta, data:teacher.data, message: `Data fetched succesfully.`};
        } catch(e){
            return {status: 500,data: [],message: e.message};
        }
    }


    
}

module.exports = new parentsController();