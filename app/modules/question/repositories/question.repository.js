const mongoose = require('mongoose');
const Question = require('question/models/question.model');
const perPage = config.PAGINATION_PERPAGE;

const QuestionRepository = {
    getAll: async (req) => {
        try {
            var conditions = {};
            const and_clauses = [];

            and_clauses.push({
                "isDeleted": false
            });
            if (_.isObject(req.body.query) && _.has(req.body.query, 'generalSearch')) {
                and_clauses.push({
                    $or: [{
                            'question': {
                                $regex: req.body.query.generalSearch,
                                $options: 'i'
                            }
                        },
                        {
                            'book_name': {
                                $regex: req.body.query.generalSearch,
                                $options: 'i'
                            }
                        },
                        {
                            'points': parseInt(req.body.query.generalSearch)
                        },
                        {
                            'question_type': {
                                $regex: req.body.query.generalSearch,
                                $options: 'i'
                            }
                        }
                    ]
                });
            }
            if (_.isObject(req.body.query) && _.has(req.body.query, 'Status')) {
                and_clauses.push({
                    "status": req.body.query.Status
                });
            }
            conditions['$and'] = and_clauses;


            var sortOperator = {
                "$sort": {}
            };
            if (_.has(req.body, 'sort')) {
                var sortField = req.body.sort.field;
                if (req.body.sort.sort == 'desc') {
                    var sortOrder = -1;
                } else if (req.body.sort.sort == 'asc') {
                    var sortOrder = 1;
                }

                sortOperator["$sort"][sortField] = sortOrder;
            } else {
                sortOperator["$sort"]['_id'] = -1;
            }

            var aggregate = Question.aggregate([{
                    "$lookup": {
                        "from": "books",
                        "localField": "book_id",
                        "foreignField": "_id",
                        "as": "book"
                    },
                },
                {
                    $unwind: "$book"
                },
                {
                    $project: {
                        _id: "$_id",
                        title: "$title",
                        points: "$points",
                        isDeleted: "$isDeleted",
                        question: "$question",
                        question_type: "$question_type",
                        status: "$status",
                        book_name: "$book.title",

                    }
                },
                {
                    $match: conditions
                },
                sortOperator
            ]);

            var options = {
                page: req.body.pagination.page,
                limit: req.body.pagination.perpage
            };
            let allQuestion = await Question.aggregatePaginate(aggregate, options);
            return allQuestion;
        } catch (e) {
            throw (e);
        }
    },

    getById: async (id) => {
        try {
            let question = await Question.findById(id);
            return question ? question : null;
        } catch (e) {
            throw (e);
        }
    },

    getByField: async (params) => {
        try {
            let Question = await Question.findOne(params).exec();
            return Question;
        } catch (e) {
            throw (e);
        }
    },

    getAllByField: async (params) => {
        try {
            let question = await Question.find(params).populate('title').exec();
            return question;
        } catch (e) {
            throw (e);
        }
    },

    delete: async (id) => {
        try {
            let Question = await Question.findById(id);
            if (Question) {
                let QuestionDelete = await Question.remove({
                    _id: id
                }).exec();
                return QuestionDelete;
            } else {
                return null;
            }
        } catch (e) {
            throw (e);
        }
    },

    deleteByField: async (field, fieldValue) => {
        //todo: Implement delete by field
    },


    updateById: async (data, id) => {
        try {
            let question = await Question.findByIdAndUpdate(id, data, {
                new: true
            }).exec();
            if (question) {
                return question;
            } else {
                return null;
            }
        } catch (e) {
            throw (e);
        }
    },

    updateByField: async (field, fieldValue, data) => {
        //todo: update by field
    },

    save: async (data) => {
        try {
            let question = await Question.create(data);
            if (!question) {
                return null;
            }
            return question;
        } catch (e) {
            throw e;
        }
    },

    getAllQuestions: async (id) => {
        try {
            return await Question.aggregate([{
                    $match: {
                        'title': mongoose.Types.ObjectId(id)
                    }
                },
                {
                    "$lookup": {
                        "from": "chapters",
                        "localField": "title",
                        "foreignField": "_id",
                        "as": "chapter_info"
                    },
                },
                {
                    $unwind: "$chapter_info"
                },
                {
                    "$lookup": {
                        "from": "books",
                        "localField": "book_id",
                        "foreignField": "_id",
                        "as": "book_info"
                    },
                },
                {
                    $unwind: "$book_info"
                },
                {
                    $project: {
                        _id: 1,
                        question: 1,
                        question_type: 1,
                        points: 1,
                        book_id: 1,
                        title: 1,
                        book_info: {
                            '_id': 1,
                            'title': 1,
                            'subtitle': 1,
                        },
                        chapter_info: {
                            '_id': 1,
                            'title': 1,
                            'description': 1,
                        }
                    }
                },
            ]);
        } catch (e) {
            throw (e);
        }
    },
};

module.exports = QuestionRepository;