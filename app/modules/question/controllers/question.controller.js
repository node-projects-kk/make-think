const mongoose = require('mongoose');
const questionRepo = require('question/repositories/question.repository');
const bookRepo = require('book/repositories/book.repository');
const chapterRepo = require('chapter/repositories/chapter.repository');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const fs = require('fs');
const errorHandler = require('../../../errorHandler');


class questionController {

    async create(req, res) {
        try {
            const book = await bookRepo.getAllByField({"status":"Active", "isDeleted":"false"});
            res.render('question/views/add.ejs', {
                page_name: 'Question-Management',
                page_title: 'Create New question',
                user: req.user,
                books: book
            });
        } catch (e) {
            throw (e);
        }
    };

    async insert(req, res) {
        try {
            let newquestion = await questionRepo.save(req.body);
            req.flash('success', 'Question added succesfully.');
            res.redirect(namedRouter.urlFor('question.list'));
        } catch (e) {
            const error = errorHandler(e);
            req.flash('error', error.message);
            res.redirect(namedRouter.urlFor('question.create'));
        }
    };

    /*
    // @Method: edit
    // @Description:  Coupon update page
    */
    async edit(req, res) {
        try {
            let result = {};
            let question = await questionRepo.getById(req.params.id);
            let chapterTitle = await chapterRepo.getAllByField({book_id:mongoose.Types.ObjectId(question.book_id)});
            const book = await bookRepo.getAllByField({"status":"Active", "isDeleted":"false"});

            if (!_.isEmpty(question)) {
                result.question_data = question;
                res.render('question/views/edit.ejs', {
                    page_name: 'Question-Management',
                    page_title: 'Update question',
                    user: req.user,
                    books:book,
                    chapterTitle:chapterTitle,
                    response: result
                });
            } else {
                req.flash('error', "Sorry question not found!");
                res.redirect(namedRouter.urlFor('question.list'));
            }
        } catch (e) {
            return res.status(500).send({ message: e.message });
        }
    };

    /* @Method: update
    // @Description: coupon update action
    */
    async update(req, res) {
        try {
            const questionId = req.body.questionId;
            // console.log(req.body)
            let questionUpdate = await questionRepo.updateById(req.body,questionId);
            if (questionUpdate) {
                req.flash('success', "Question Updated Successfully");
                res.redirect(namedRouter.urlFor('question.list'));
            } else {
                // console.log(questionUpdate)
            }
        } catch (e) {
            const error = errorHandler(e);
            req.flash('error', error.message);
            return res.status(500).send({ message: e.message });
        }

    };



    /* @Method: list
    // @Description: To get all the questions from DB
    */
    async list(req, res) {
        try {
            res.render('question/views/list.ejs', {
                page_name: 'Question-Management',
                page_title: 'Question List',
                user: req.user
            });
        } catch (e) {
            return res.status(500).send({ message: e.message });
        }
    };

    async getAll(req, res) {
        try {

            let question = await questionRepo.getAll(req);
            if (_.has(req.body, 'sort')) {
                var sortOrder = req.body.sort.sort;
                var sortField = req.body.sort.field;
            } else {
                var sortOrder = -1;
                var sortField = '_id';
            }
            let meta = { "page": req.body.pagination.page, "pages": question.pageCount, "perpage": req.body.pagination.perpage, "total": question.totalCount, "sort": sortOrder, "field": sortField };
            return { status: 200, meta: meta, data: question.data, message: `Data fetched succesfully.` };
        } catch (e) {
            return { status: 500, data: [], message: e.message };
        }
    }



    /*
    // @Method: status_change
    // @Description: question status change action
    */
    async statusChange(req, res) {
        try {

            let question = await questionRepo.getById(req.params.id);
            if (!_.isEmpty(question)) {
                let questionStatus = (question.isActive == 'Active') ? 'Inactive' : 'Active';
                let questionUpdate = questionRepo.updateById({ 'isActive': questionStatus }, req.body.id);
                req.flash('success', "Question status has changed successfully");
                res.send(questionUpdate);
            } else {
                req.flash('error', "Sorry question was not found");
                res.redirect(namedRouter.urlFor('question.list'));
            }
        } catch (e) {
            return res.status(500).send({ message: e.message });
        }
    };

    /* @Method: delete
    // @Description: question delete
    */
    async delete(req, res) {
        try {
            let questionDelete = await questionRepo.updateById({ "isDeleted": true }, req.params.id)
            if (!_.isEmpty(questionDelete)) {
                req.flash('success', 'Question removed Successfully');
                res.redirect(namedRouter.urlFor('question.list'));
            }
        } catch (e) {
            return res.status(500).send({ message: e.message });
        }
    };
    async getTitleAndDescription(req, res) {
        try {
            
            let chapterDetails = await chapterRepo.getByField({ "book_id": req.params.bookId, "chapter_no": parseInt(req.params.chapterNo) });
            if (!_.isEmpty(chapterDetails)) {
                res.send(chapterDetails)
            }
            else {
                res.send(chapterDetails)
            }
        } catch (e) {
            return res.status(500).send({ message: e.message });
        }
    };
}

module.exports = new questionController();