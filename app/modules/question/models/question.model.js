const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');
const beautifyUnique = require('mongoose-beautiful-unique-validation');

const QuestionSchema = new Schema({
  book_id: { type: Schema.Types.ObjectId, ref: 'Book' },
  title: { type: Schema.Types.ObjectId, ref: 'Chapter' },
  question: { type: String, default: '' },
  question_type: { type: String, default:'descriptive',enum:['descriptive','drawing'] },
  points: { type: Number, default:0},
  status: { type: String, default: "Active", enum: ["Active", "Inactive"] },
  isDeleted: { type: Boolean, default: false, enum: [true, false] }
});

QuestionSchema.plugin(beautifyUnique);
// For pagination
QuestionSchema.plugin(mongooseAggregatePaginate);

// create the model for users and expose it to our app
module.exports = mongoose.model('Question', QuestionSchema);