const mongoose = require('mongoose');
var Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');
const beautifyUnique = require('mongoose-beautiful-unique-validation');
const deleted = [true, false];
const providerstatus = ["parent", "teacher","principal","admin"];

// define the schema for our user model
const coinSchema = mongoose.Schema({	
   	coin_to_user_id:{type: Schema.Types.ObjectId,ref: 'User',default: null},
	coin_from_user_id:{type: Schema.Types.ObjectId,ref: 'User',default: null},
	coin_given_date:{type:Date, default:Date.now()},
	provider_type:{type:String,default:'parent',enum:providerstatus},
	no_of_coins:{type:Number,default:0},
	isDeleted: { type: Boolean, default: false },
	createdAt: { type: Date, default: Date.now() }
});
coinSchema.plugin(beautifyUnique);
// For pagination
coinSchema.plugin(mongooseAggregatePaginate);
coinSchema.plugin(mongoosePaginate);
module.exports = mongoose.model('Coin', coinSchema);