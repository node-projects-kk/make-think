const coinsRepo = require('coins/repositories/coins.repository');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const fs = require('fs');

class coinsController {
    constructor() {
        this.language = [];        
    }

    /*
    // @Method: edit
    // @Description:  Coupon update page
    */
    async edit (req, res){
        try
        {
            let coins = await coinsRepo.getById(req.params.id);
            if (!_.isEmpty(coins)) {

                res.render('coins/views/edit.ejs', {
                    page_name: 'coins-management',
                    page_title: 'Edit coins',
                    user: req.user,
                    response: coins
                });
            } else {
                req.flash('error', "Sorry coins not found!");
                res.redirect(namedRouter.urlFor('coins.list')); 
            }
        } catch(e){
            return res.status(500).send({message: e.message}); 
        }
    };

    /* @Method: update
    // @Description: coupon update action
    */
    async update (req, res){
        try {
            let checkCountry= await coinsRepo.getByField({"coins_name": req.body.coins_name})
        if(checkCountry)
        {
            req.flash('error', 'coins with same name already exists.');
            res.redirect(namedRouter.urlFor('coins.list'));
        }
        else
        {
            let coins = await coinsRepo.getById(req.body.id);
            if (!_.isEmpty(coins)) {
                    let coinsUpdate = await coinsRepo.updateById(req.body, req.body.id)
                    if(coinsUpdate) {
                        req.flash('success', "coins Updated Successfully");
                        res.redirect(namedRouter.urlFor('coins.list'));
                    }
                    
                }else{
                req.flash('error', "coins not found!");
                res.redirect(namedRouter.urlFor('coins.edit', { id: req.body.id }));
            }
        }    
        }catch(e){
            return res.status(500).send({message: e.message});  
        }      
            
    };



    /* @Method: list
    // @Description: To get all the users from DB
    */
    async list (req, res){
            try
            {
                res.render('coins/views/list.ejs', {
                    page_name: 'coins-list',
                    page_title: 'coins List',
                    user: req.user,
                    
                });
        } catch(e){
            return res.status(500).send({message: e.message}); 
        }  
    };

    async create (req, res){
        try
        {
            res.render('coins/views/add.ejs', {
                page_name: 'coins-Create',
                page_title: 'coins Create',
                user: req.user,
            });
    } catch(e){
        return res.status(500).send({message: e.message}); 
    }  
};

async store (req, res){
    try {
        let checkCountry= await coinsRepo.getByField({"coins_name": req.body.coins_name})
        if(checkCountry)
        {
            req.flash('error', 'coins with same name already exists.');
            res.redirect(namedRouter.urlFor('coins.list'));
        }
        else
        {
        let newcoins = await coinsRepo.save(req.body);
        req.flash('success', 'coins created succesfully.');
        res.redirect(namedRouter.urlFor('coins.list'));
        }
    } catch(e) {
        const error = errorHandler(e);
        req.flash('error', error.message);
        //res.status(500).send({message: error.message});
        res.redirect(namedRouter.urlFor('coins.create'));
    }
};

    async getAll (req, res){
        try{
            let coins = await coinsRepo.getAll(req);
            if(_.has(req.body, 'sort')){
                var sortOrder = req.body.sort.sort;
                var sortField = req.body.sort.field;
            }else{
                var sortOrder = -1;
                var sortField = '_id';
            }
         let meta = {"page": req.body.pagination.page, "pages": coins.pageCount, "perpage": req.body.pagination.perpage, "total": coins.totalCount, "sort": sortOrder, "field": sortField};
            
            return {status: 200, meta: meta, data:coins.data, message: `Data fetched succesfully.`};
        } catch(e){
            return {status: 500,data: [],message: e.message};
        }
    }
    /*
    // @Method: status_change
    // @Description: language status change action
    */
    async changeStatus (req, res){
        try {
            let coins = await coinsRepo.getById(req.params.id);
            if(!_.isEmpty(coins)){
                let coinsStatus = (coins.isActive == true) ? false : true;
                let coinsUpdate= await coinsRepo.updateById({isActive: coinsStatus }, req.params.id);
                req.flash('success', "coins status has changed successfully" );
                res.redirect(namedRouter.urlFor('coins.list'));
            } else {
                req.flash('error', "sorry coins not found");
                res.redirect(namedRouter.urlFor('coins.list')); 
            }
        } catch(e){
            return res.status(500).send({message: e.message}); 
        }
    };

    /* @Method: delete
    // @Description: coupon delete
    */
    async destroy (req, res){
        try{
            let languageDelete = await coinsRepo.delete(req.params.id)
            if(!_.isEmpty(languageDelete)){
                req.flash('success','coins Removed Successfully');
                res.redirect(namedRouter.urlFor('coins.list'));
            } 
        }catch(e){
            return res.status(500).send({message: e.message});   
        } 
    };


}

module.exports = new coinsController();