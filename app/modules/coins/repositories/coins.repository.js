const Coin = require('coins/models/coins.model');
const User = require('user/models/user.model');
const mongoose = require('mongoose');
const perPage = config.PAGINATION_PERPAGE;

class CoinRepository {
    constructor() { }

     async getAll(req) {
        try {
            var conditions = {};
            var and_clauses = [];

            and_clauses.push({ "isDeleted": false });

            if (_.isObject(req.body.query) && _.has(req.body.query, 'generalSearch')) {
                //and_clauses.push({"status": /req.body.query.generalSearch/i});
                // and_clauses.push({ 'title': { $regex: req.body.query.generalSearch, $options: 'i' } });
                // and_clauses.push({ 'slug': { $regex: req.body.query.generalSearch, $options: 'i' } });
                // and_clauses.push({ 'desc': { $regex: req.body.query.generalSearch, $options: 'i' } });
                and_clauses.push({
                    $or: [
                        { 'coins_name': { $regex: req.body.query.generalSearch, $options: 'i' } }
                    ]
                });
            }
            if (_.isObject(req.body.query) && _.has(req.body.query, 'Status')) {
                const status = req.body.query.Status == 'true' ? "Active" : "Inactive";
                and_clauses.push({ "status": status });
            }
            conditions['$and'] = and_clauses;

            var sortOperator = { "$sort": {} };
            if (_.has(req.body, 'sort')) {
                var sortField = req.body.sort.field;
                if (req.body.sort.sort == 'desc') {
                    var sortOrder = -1;
                } else if (req.body.sort.sort == 'asc') {
                    var sortOrder = 1;
                }

                sortOperator["$sort"][sortField] = sortOrder;
            } else {
                sortOperator["$sort"]['_id'] = -1;
            }

            var aggregate =  Coin.aggregate([
                { $match: conditions },
                sortOperator
            ]);
            var options = { page: req.body.pagination.page, limit: req.body.pagination.perpage };
            let allFaq = await Coin.aggregatePaginate(aggregate, options);
            return allFaq;
        } catch (e) {
            throw (e);
        }
    }

    async getById(id) {
        try {
            return await Coin.findById(id).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async getByField(params) {
        try {
            return await Coin.findOne(params).exec();
        } catch (error) {
            return error;
        }
    }

    async getAllByField(params) {
        try {
            return await Coin.find(params).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async delete(id) {
        try {
            await Coin.findById(id).lean().exec();
            return await Coin.deleteOne({ _id: id }).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async updateById(data, id) {
        try {
            return await Coin.findByIdAndUpdate(id, data, { new: true, upsert: true })
                .lean().exec();
        } catch (error) {
            return error;
        }
    }

    async updateOneByField(id,data){
        try{
             let update = await Coin.updateOne(
                 {"_id" : id},
                 {$set: { "no_of_coins" : data}}
             );
             if (!update) {
                 return null;
             }
             return update;
        }catch(e){
            return e;
        }
     }

    async save(data) {
        try {
            return await Coin.create(data).lean().exec();
        } catch (error) {
            return error;
        }
    }

    
    
	async coindetails(sids,cm,sm){
		try{
			//console.log(">>",sids); //process.exit();
			
		//	{
		//	$match: {
		//		$and: [
		//			{
		//				'book_to': userId,
		//				startDate: { $gte: date }
		//			}]
		//	}
		//},
		//{
		//	$project: {
		//		startTime: { $dateToString: { format: "%H:%M:%S", date: "$start_date" } },
		//		endTime: { $dateToString: { format: "%H:%M:%S", date: "$end_date" } },
		//		startDate: { $dateToString: { format: "%Y-%m-%d", date: "$start_date" } }
		//	}
		//},
		//	
			
			
			var result = await Coin.aggregate([
							{
								$match:{
									$and:[
										{
											"coin_to_user_id":{ $in: sids },
											"coin_given_date":{ $gte: new Date(sm), $lt: new Date(cm) }
										}
									]
								}
							},
							{
								$lookup: {
									from: "users",
									localField: "coin_to_user_id",
									foreignField: "_id",
									as: "u_details"
								}
							},
							{
							    "$unwind": "$u_details"
							},
							{
								$group:{
									_id: {
										month: { $month: "$coin_given_date" },
										coin_no:"$no_of_coins"
									},
									//coinCount: { $sum: 1 },
									coin_to_user_id:{$addToSet:"$coin_to_user_id"},
									u_details:{$first:"$u_details.first_name"},
								}
							},
							{
								$group: {
									_id:{
										month:"$_id.month",
										cuid:"$coin_to_user_id",
									},
									totalcoin: { $sum: "$_id.coin_no" },
									name:{$first:"$u_details"}
								} 
							},
							{
								$project:{
									_id:"$_id.month",
									totalcoin:1,
									name:1
								}
							},
							{
								$addFields: {
									month: {
										$let: {
											vars: {
												monthsInString: [, 'January', 'February', 'March', 'April', 'May', 'June', 'July','August','September','October','November','December']
											},
											in: {
												$arrayElemAt: ['$$monthsInString', '$_id']
											}
										}
									}
								}
							},
						]).exec();
			
			//console.log("116>>",result); //process.exit();
			return result;
			
		}
		catch(e){
			throw (e);
		}
	}


}

module.exports = new CoinRepository();