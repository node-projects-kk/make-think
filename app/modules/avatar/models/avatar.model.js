const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');
const beautifyUnique = require('mongoose-beautiful-unique-validation');

const AvatarSchema = new Schema({
  title: {
    type: String,
    default: '',
    unique: 'Avatar "{VALUE}" is already exist!'
  },
  purchase_coin: {
    type: Schema.Types.Double,
    default: 0.00
  },
  image_1: {
    type: String,
    default: ''
  },
  image_2: {
    type: String,
    default: ''
  },
  audio:{
    type: String,
    default: ''
  },
  slug: {
    type: String,
    default: ''
  },
  description: {
    type: String,
    default: ''
  },
  question: {
    type: String,
    default: ''
  },
  status: {
    type: String,
    default: "Active",
    enum: ["Active", "Inactive"]
  },
  isDeleted: {
    type: Boolean,
    default: false,
    enum: [true, false]
  }
});

AvatarSchema.plugin(beautifyUnique);
// For pagination
AvatarSchema.plugin(mongooseAggregatePaginate);

// create the model for users and expose it to our app
module.exports = mongoose.model('Avatar', AvatarSchema);