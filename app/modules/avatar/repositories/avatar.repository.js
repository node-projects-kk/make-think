const mongoose = require('mongoose');
const Avatar = require('avatar/models/avatar.model');
const perPage = config.PAGINATION_PERPAGE;

const avatarRepository = {
    getAll: async (req) => {
        // console.log(req.body);
        try {
            var conditions = {};
            var and_clauses = [];

            and_clauses.push({ "isDeleted": false });

            if (_.isObject(req.body.query) && _.has(req.body.query, 'generalSearch')) {
                //and_clauses.push({"status": /req.body.query.generalSearch/i});
                and_clauses.push({ 'title': { $regex: req.body.query.generalSearch, $options: 'i' } });
            }
            if (_.isObject(req.body.query) && _.has(req.body.query, 'Status')) {
                and_clauses.push({ "status": req.body.query.Status });
            }
            conditions['$and'] = and_clauses;

            var sortOperator = { "$sort": {} };
            if (_.has(req.body, 'sort')) {
                var sortField = req.body.sort.field;
                if (req.body.sort.sort == 'desc') {
                    var sortOrder = -1;
                } else if (req.body.sort.sort == 'asc') {
                    var sortOrder = 1;
                }

                sortOperator["$sort"][sortField] = sortOrder;
            } else {
                sortOperator["$sort"]['_id'] = -1;
            }

            var aggregate = Avatar.aggregate([
                { $match: conditions },
                sortOperator
            ]);

            var options = { page: req.body.pagination.page, limit: req.body.pagination.perpage };
            let allAvatar = await Avatar.aggregatePaginate(aggregate, options);
            return allAvatar;
        } catch (e) {
            throw (e);
        }
    },

    getAuthorCount: async (req) => {
        try {

            let avatar = await Avatar.find({ isDeleted: false });
            return avatar;
        } catch (e) {
            throw (e);
        }
    },

    getById: async (id) => {
        try {
            let avatar = await Avatar.findById(id).exec();
            return avatar;
        } catch (e) {
            throw (e);
        }
    },

    getByField: async (params) => {
        try {
            let avatar = await Avatar.findOne(params).exec();
            return avatar;
        } catch (e) {
            throw (e);
        }
    },

    getAllByField: async (params) => {
        try {
            let avatar = await Avatar.find(params).lean().exec();
            return avatar;
        } catch (e) {
            throw (e);
        }
    },

    delete: async (id) => {
        try {
            let avatar = await Avatar.findById(id);
            if (avatar) {
                let avatarDelete = await Avatar.remove({ _id: id }).exec();
                return avatarDelete;
            } else {
                return null;
            }
        } catch (e) {
            throw (e);
        }
    },

    deleteByField: async (field, fieldValue) => {
        //todo: Implement delete by field
    },


    updateById: async (data, id) => {
        try {
            let avatar = await Avatar.findByIdAndUpdate(id, data, { new: true, upsert: true }).exec();
            return avatar;
        } catch (e) {
            throw (e);
        }
    },

    updateByField: async (field, fieldValue, data) => {
        //todo: update by field
    },

    save: async (data) => {
        try {
            let avatar = await Avatar.create(data);
            if (!avatar) {
                return null;
            }
            return avatar;
        } catch (e) {
            throw e;
        }
    },
};

module.exports = avatarRepository;