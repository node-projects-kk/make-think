const mongoose = require('mongoose');
const avatarRepo = require('avatar/repositories/avatar.repository');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const gm = require('gm').subClass({
    imageMagick: true
});
const fs = require('fs');
const errorHandler = require('../../../errorHandler');

class avatarController {
    async create(req, res) {
        try {
            res.render('avatar/views/add.ejs', {
                page_name: 'avatar-management',
                page_title: 'Create New Avatar',
                user: req.user
            });
        } catch (e) {
            throw (e);
        }
    };

	async insert(req, res) {
		try {
			req.body.image = '';
			if (typeof req.fileValidationError != 'undefined') {
				req.flash('error', req.fileValidationError);
				res.redirect(namedRouter.urlFor('avatar.create'));
			}
			else {
				let avatar = await avatarRepo.getByField({'title': req.body.title});
				if (!_.isEmpty(avatar)) {
					req.flash('error', `Avatar "${req.body.title}" is already exist!`);
					res.redirect(namedRouter.urlFor('avatar.create'));
				}
				else {
					if (_.has(req, 'files')) {
						if (req.files.length > 0) {
							req.files.forEach(function (file) {
								if (file.fieldname == 'image_1') {
									gm('public/uploads/avatar/' + file.filename).resize(100).write('public/uploads/avatar/thumb/' + file.filename, function (err) {
										if (err) {
											req.flash('error', err.message);
										}
									});
									req.body.image_1 = file.filename;
								}
								if (file.fieldname == 'image_2') {
									gm('public/uploads/avatar/' + file.filename).resize(100).write('public/uploads/avatar/thumb/' + file.filename, function (err) {
										if (err) {
											req.flash('error', err.message);
										}
									});
									req.body.image_2 = file.filename;
								}
								if(file.fieldname == 'audio'){
									req.body.audio = file.filename;
								}
							});  
							let newavatar = avatarRepo.save(req.body);
							req.flash('success', 'Avatar created succesfully.');
							res.redirect(namedRouter.urlFor('avatar.listing'));
						}
						else {
							req.flash('error', 'Please browse avatars to upload');
							res.redirect(namedRouter.urlFor('avatar.create'));
						}
					}
					else {
						req.flash('error', 'Please browse avatars to upload');
						res.redirect(namedRouter.urlFor('avatar.create'));
					}
				}
			}
		}
		catch (e) {
			const error = errorHandler(e);
			req.flash('error', error.message);
			res.redirect(namedRouter.urlFor('avatar.create'));
		}
	};

    /*
    // @Method: edit
    // @Description:  Avatar update page
    */
    async edit(req, res) {
        try {
            let result = {};
            let avatar = await avatarRepo.getById(req.params.id);
            if (!_.isEmpty(avatar)) {
                result.avatar_data = avatar;
                res.render('avatar/views/edit.ejs', {
                    page_name: 'avatar-management',
                    page_title: 'Update Avatar',
                    user: req.user,
                    response: result
                });
            } else {
                req.flash('error', "Sorry author not found!");
                res.redirect(namedRouter.urlFor('avatar.listing'));
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    /* @Method: update
    // @Description: avatar update action
    */
    async update(req, res) {
        try {
            
            if (typeof req.fileValidationError != 'undefined') {
                req.flash('error', req.fileValidationError);
                res.redirect(namedRouter.urlFor('avatar.edit', {
                    id: req.body.aid
                }));
            } else {
                
                let existAvatar = await avatarRepo.getByField({'title':req.body.title,_id:{$ne:mongoose.Types.ObjectId(req.body.aid)}});
                
                if(_.isEmpty(existAvatar))
                {
                
                    if (_.has(req, 'files')) {
                        if (req.files.length > 0) {
                            let avatar = await avatarRepo.getByField({
                                '_id': req.body.aid
                            });
                            /***********************/
                            req.files.forEach(function (file) {
                                if (file.fieldname == 'image_1') {
                                    if (!_.isEmpty(avatar.image_1)) {
                                        if (fs.existsSync('public/uploads/avatar/' + avatar.image_1)) {
                                            const upl_img = fs.unlinkSync('public/uploads/avatar/' + avatar.image_1);
                                        }
                                        if (fs.existsSync('public/uploads/avatar/thumb/' + avatar.image_1)) {
                                            const upl_thumb_img = fs.unlinkSync('public/uploads/avatar/thumb/' + avatar.image_1);
                                        }
                                    }
                                    gm('public/uploads/avatar/' + file.filename)
                                        .resize(100)
                                        .write('public/uploads/avatar/thumb/' + file.filename, function (err) {
                                            if (err) {
                                                req.flash('error', err.message);
                                            }
                                    });
                                    req.body.image_1 = file.filename;
                                }
                                if (file.fieldname == 'image_2') {
                                    if (!_.isEmpty(avatar.image_2)) {
                                        if (fs.existsSync('public/uploads/avatar/' + avatar.image_2)) {
                                            const upl_img = fs.unlinkSync('public/uploads/avatar/' + avatar.image_2);
                                        }
                                        if (fs.existsSync('./public/uploads/avatar/thumb/' + avatar.image_2)) {
                                            const upl_thumb_img = fs.unlinkSync('public/uploads/avatar/thumb/' + avatar.image_2);
                                        }
                                    }
                                    gm('public/uploads/avatar/' + file.filename)
                                        .resize(100)
                                        .write('./public/uploads/avatar/thumb/' + file.filename, function (err) {
                                            if (err) {
                                                req.flash('error', err.message);
                                            }
                                    });
                                    req.body.image_2 = file.filename;
                                }

                                if(file.fieldname == 'audio'){
                                    if(!_.isEmpty(avatar.audio)){
                                        if(fs.existsSync('./public/upload/avatar_audio/'+ avatar.audio)){
                                             fs.unlinkSync('./public/upload/avatar_audio/'+ avatar.audio)
                                        }
                                    }
									req.body.audio = file.filename;
                                }
                            });
                            /**************************/
                            let avatarUpdate = avatarRepo.updateById(req.body, req.body.aid);
                            req.flash('success', 'Avatar updated succesfully.');
                            res.redirect(namedRouter.urlFor('avatar.listing'));
                           
                        } 
                    } else {
                        let avatarUpdate = avatarRepo.updateById(req.body, req.body.aid);
                        req.flash('success', 'Avatar updated succesfully.');
                        res.redirect(namedRouter.urlFor('avatar.listing'));
                    }
                }
                else
                {
                    req.flash('error', 'Avatar '+req.body.title+'" is already exist!');
                    res.redirect(namedRouter.urlFor('avatar.edit', {
                        id: req.body.aid
                    }));
                }

            }
        } catch (e) {
            const error = errorHandler(e);
            req.flash('error', error.message);
            res.redirect(namedRouter.urlFor('avatar.edit', {
                id: req.body.aid
            }));
        }
    };

    /* @Method: list
    // @Description: To get all the avatars from DB
    */
    async list(req, res) {
        try {
            res.render('avatar/views/list.ejs', {
                page_name: 'avatar-management',
                page_title: 'Avatar List',
                user: req.user
            });
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    async getAll(req, res) {
        try {
            let avatar = await avatarRepo.getAll(req);
            
            if (_.has(req.body, 'sort')) {
                var sortOrder = req.body.sort.sort;
                var sortField = req.body.sort.field;
            } else {
                var sortOrder = -1;
                var sortField = '_id';
            }
            let meta = {
                "page": req.body.pagination.page,
                "pages": avatar.pageCount,
                "perpage": req.body.pagination.perpage,
                "total": avatar.totalCount,
                "sort": sortOrder,
                "field": sortField
            };
            return {
                status: 200,
                meta: meta,
                data: avatar.data,
                message: `Data fetched succesfully.`
            };
        } catch (e) {
            return {
                status: 500,
                data: [],
                message: e.message
            };
        }
    }


    /* @Method: delete
    // @Description: avatar delete
    */
    async delete(req, res) {
        try {
            let avatar = await avatarRepo.getByField({
                '_id': req.params.id
            });
            let avatarDelete = await avatarRepo.delete(req.params.id)
            if (!_.isEmpty(avatarDelete)) {
                if (!_.isEmpty(avatar.image)) {
                    if (fs.existsSync('public/uploads/avatar/' + avatar.image)) {
                        const upl_img = fs.unlinkSync('public/uploads/avatar/' + avatar.image);
                    }
                    if (fs.existsSync('public/uploads/avatar/thumb/' + avatar.image)) {
                        const upl_thumb_img = fs.unlinkSync('public/uploads/avatar/thumb/' + avatar.image);
                    }
                }
                req.flash('success', 'Avatar Removed Successfully');
                res.redirect(namedRouter.urlFor('avatar.listing'));
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };


    /*
    // @Method: verifyAvatarTitle
    // @Description: To verify avatar title
    */
    async verifyAvatarTitle(req, res) {
        try {
            const result = await avatarRepo.getAllByField({
                "title": req.body.title
            });

            if (_.isEmpty(result)) {
                return {
                    "status": 200,
                };
            } else {
                return {
                    "status": 200,
                    "message": "Avatar Title already exists."
                };
            }
        } catch (error) {
            throw error;
        }
    };


}




module.exports = new avatarController();