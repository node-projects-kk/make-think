const mongoose = require('mongoose');
const Activity = require('activities/models/activity_details.model');
const User = require('user/models/user.model');
const moment = require('moment');
const perPage = config.PAGINATION_PERPAGE;

const ActivityDetailsRepository = {
  getAll: async (req) => {

    try {
      var conditions = {};
      var and_clauses = [];

      and_clauses.push({
        "isDeleted": false
      });

      if (_.isObject(req.body.query) && _.has(req.body.query, 'generalSearch')) {
        //and_clauses.push({"status": /req.body.query.generalSearch/i});
        and_clauses.push({
          'title': {
            $regex: req.body.query.generalSearch,
            $options: 'i'
          }
        });
      }
      if (_.isObject(req.body.query) && _.has(req.body.query, 'Status')) {
        and_clauses.push({
          "status": req.body.query.Status
        });
      }
      conditions['$and'] = and_clauses;

      var sortOperator = {
        "$sort": {}
      };
      if (_.has(req.body, 'sort')) {
        var sortField = req.body.sort.field;
        if (req.body.sort.sort == 'desc') {
          var sortOrder = -1;
        } else if (req.body.sort.sort == 'asc') {
          var sortOrder = 1;
        }

        sortOperator["$sort"][sortField] = sortOrder;
      } else {
        sortOperator["$sort"]['_id'] = -1;
      }

      var aggregate = Activity.aggregate([{
          $match: conditions
        },
        sortOperator
      ]);

      var options = {
        page: req.body.pagination.page,
        limit: req.body.pagination.perpage
      };
      let allActivity = await Activity.aggregatePaginate(aggregate, options);
      return allActivity;
    } catch (e) {
      throw (e);
    }
  },

  getActivityCount: async (req) => {
    try {
      let activity = await Activity.find({
        isDeleted: false
      });
      return activity;
    } catch (e) {
      throw (e);
    }
  },

  getById: async (id) => {
    try {
      let activity = await Activity.findById(id).exec();
      return activity;
    } catch (e) {
      throw (e);
    }
  },

  getByField: async (params) => {
    try {
      let activity = await Activity.findOne(params).exec();
      return activity;
    } catch (e) {
      throw (e);
    }
  },

  getAllByField: async (params) => {
    try {
      let activity = await Activity.find(params).exec();
      return activity;
    } catch (e) {
      throw (e);
    }
  },

  delete: async (id) => {
    try {
      let activity = await Activity.findById(id);
      if (activity) {
        let activityDelete = await Activity.remove({
          _id: id
        }).exec();
        return activityDelete;
      } else {
        return null;
      }
    } catch (e) {
      throw (e);
    }
  },

  deleteByField: async (field, fieldValue) => {
    //todo: Implement delete by field
  },


  updateById: async (data, id) => {
    try {
      let activity = await Activity.findByIdAndUpdate(id, data, {
        new: true,
        upsert: true
      }).exec();
      return activity;
    } catch (e) {
      throw (e);
    }
  },

  updateByField: async (field, fieldValue, data) => {
    //todo: update by field
  },

  save: async (data) => {
    try {
      let activity = await Activity.create(data);
      if (!activity) {
        return null;
      }
      return activity;
    } catch (e) {
      throw e;
    }
  },

  getStudentsActivityListByGrade: async (data) => {
    try {
      var conditions = {};
      var and_clauses = [];

      and_clauses.push({
        "isDeleted": false,
      });
      and_clauses.push({
        "user_info.grade": parseInt(data)
      });

      conditions['$and'] = and_clauses;
      let studentsActivity = await Activity.aggregate([{
          "$lookup": {
            "from": "users",
            "localField": "student_id",
            "foreignField": "_id",
            "as": "user_info"
          },
        },
        {
          $unwind: {
            path: "$user_info",
            preserveNullAndEmptyArrays: true
          }
        },
        {
          "$lookup": {
            "from": "chapters",
            "localField": "page_id",
            "foreignField": "pages._id",
            "as": "chapter_info"
          },
        },
        {
          $unwind: {
            path: "$chapter_info",
            preserveNullAndEmptyArrays: true
          }
        },
        {
          "$lookup": {
            "from": "books",
            "localField": "chapter_info.book_id",
            "foreignField": "_id",
            "as": "book_info"
          },
        },
        {
          $unwind: {
            path: "$book_info",
            preserveNullAndEmptyArrays: true
          }
        },
        {
          $match: conditions
        },
        {
          "$sort": {
            _id: 1
          }
        },
      ]).exec();
      return studentsActivity;
    } catch (e) {
      throw (e);
    }
  },

  acitvitiesbyClassId: async (cid, sid) => {
    try {
      var aggregate = await Activity.aggregate([{
          $match: {
            'student_id': {
              $in: sid.map(s => mongoose.Types.ObjectId(s))
            },
            'isDeleted': false
          }
        },
        {
          "$lookup": {
            "from": "users",
            "localField": "student_id",
            "foreignField": "_id",
            "as": "user_info"
          },
        },
        {
          $unwind: "$user_info"
        },
        {
          "$lookup": {
            "from": "chapters",
            "localField": "chapter_id",
            "foreignField": "_id",
            "as": "chapter_info"
          },
        },
        {
          $unwind: "$chapter_info"
        },
        {
          "$lookup": {
            "from": "books",
            "localField": "book_id",
            "foreignField": "_id",
            "as": "book_info"
          },
        },
        {
          $unwind: "$book_info"
        }
      ]);
      return aggregate;
    } catch (e) {
      throw (e);
    }
  },

  acitvityDetails: async (id) => {
    try {
      var aggregate = await Activity.aggregate([{
          "$lookup": {
            "from": "users",
            "localField": "student_id",
            "foreignField": "_id",
            "as": "students"
          },
        },
        {
          $unwind: "$students"
        },
        {
          "$lookup": {
            "from": "chapters",
            "localField": "page_id",
            "foreignField": "pages._id",
            "as": "chapters"
          },
        },
        {
          $unwind: "$chapters"
        },
        {
          "$lookup": {
            "from": "books",
            "localField": "chapters.book_id",
            "foreignField": "_id",
            "as": "books"
          },
        },
        {
          $unwind: "$books"
        },
        {
          $match: {
            _id: mongoose.Types.ObjectId(id)
          }
        },
      ]);
      return aggregate;
    } catch (e) {
      throw (e);
    }
  },

  ParentDashboardActivity: async (id) => {
    try {
      var aggregate = await Activity.aggregate([{
          $match: {
            'student_id': mongoose.Types.ObjectId(id)
          }
        },
        {
          "$lookup": {
            "from": "users",
            "localField": "student_id",
            "foreignField": "_id",
            "as": "userInfo"
          },
        },
        {
          $unwind: "$userInfo"
        },
        {
          "$lookup": {
            "from": "chapters",
            "localField": "chapter_id",
            "foreignField": "_id",
            "as": "chapterInfo"
          },
        },
        {
          $unwind: {
            path: '$chapterInfo',
            preserveNullAndEmptyArrays: true
          }
        },
        {
          "$lookup": {
            "from": "books",
            "localField": "book_id",
            "foreignField": "_id",
            "as": "bookInfo"
          },
        },
        {
          $unwind: {
            path: '$bookInfo',
            preserveNullAndEmptyArrays: true
          }
        },

        /*{
          $group:{
            _id:"$_id",
            chapter_pages:{$first:"$chapter_pages"},
            chapterInfo:{$first:"$chapter_info"},
            bookInfo:{$first:"$book_info"},
            studentName:{$first:{$concat:["$user_info.first_name"," ","$user_info.last_name"]}}
          }
        }*/

        {
          $project: {
            _id: 1,
            chapterInfo: 1,
            bookInfo: 1,
            userInfo: 1,
            createdAt: 1,
            completedAt: 1,
            activityDetails: {
              points_earned: "$points_earned",
              book_id: "$book_id",
              activity_type: "$activity_type",
              book_id: "$book_id",
              student_id: "$student_id",
              chapter_id: "$chapter_id",
              chapter_pages: "$chapter_pages",
            }
          }
        },

      ]);
      return aggregate;
    } catch (e) {
      throw (e);
    }
  },

  objectUpdate: async (data, id, student_id) => {
    try {
      let activity = await Activity.update({
        'student_id': student_id,
        'chapter_pages._id': id
      }, {
        '$set': data,
      }).exec()
      return activity;
    } catch (e) {
      throw (e);
    }
  },

  // acitvitiesOfTeacherStudentbyClassId: async (cid, sid) => {
  //   try {
  //     console.log('teacher', tid);
  //     var aggregate = await Activity.aggregate([{
  //         $match: {
  //           'student_id': {
  //             $in: sid.map(s => mongoose.Types.ObjectId(s))
  //           },
  //           'isDeleted': false
  //         }
  //       },
  //       {
  //         "$lookup": {
  //           "from": "users",
  //           "localField": "student_id",
  //           "foreignField": "_id",
  //           "as": "user_info"
  //         },
  //       },
  //       {
  //         $unwind: {
  //           path: '$user_info',
  //           preserveNullAndEmptyArrays: true
  //         }
  //       },
  //       {
  //         "$lookup": {
  //           "from": "chapters",
  //           "localField": "chapter_id",
  //           "foreignField": "_id",
  //           "as": "chapter_info"
  //         },
  //       },
  //       {
  //         $unwind: "$chapter_info"
  //       },
  //       {
  //         "$lookup": {
  //           "from": "books",
  //           "localField": "book_id",
  //           "foreignField": "_id",
  //           "as": "book_info"
  //         },
  //       },
  //       {
  //         $unwind: "$book_info"
  //       },
  //     ]);
  //     return aggregate;
  //   } catch (e) {
  //     throw (e);
  //   }
  // },




};

module.exports = ActivityDetailsRepository;