const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');
const beautifyUnique = require('mongoose-beautiful-unique-validation');

const ActivityDetailsSchema = new Schema({
  student_id: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    default: null
  },
  book_id: {
    type: Schema.Types.ObjectId,
    ref: 'Book',
    default: null
  },
  chapter_id: {
    type: Schema.Types.ObjectId,
    ref: 'Chapter',
    default: null
  },
  points_earned: {
    type: Schema.Types.Double,
    default: 0.00
  },
  chapter_pages: [{
    status: {
      type: String,
      default: ''
    },
    initialDate: {
      type: Number,
      default: Date.now()
    },
    ongoingDate: {
      type: Number,
      default: Date.now()
    },
    completedDate: {
      type: Number,
      default: Date.now()
    }
  }],
  activity_type: {
    type: String,
    default: 'ongoing',
    enum: ['completed', 'ongoing']
  },
  isDeleted: {
    type: Boolean,
    default: false,
    enum: [true, false]
  },
  createdAt: {
    type: Number,
    default: Date.now()
  },
  completedAt: {
    type: Number,
    default: Date.now()
  }
});

ActivityDetailsSchema.plugin(beautifyUnique);
// For pagination
ActivityDetailsSchema.plugin(mongooseAggregatePaginate);

// create the model for users and expose it to our app
module.exports = mongoose.model('ActivityDetails', ActivityDetailsSchema);