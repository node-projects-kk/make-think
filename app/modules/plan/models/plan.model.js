const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');
const beautifyUnique = require('mongoose-beautiful-unique-validation');

const deletedstatus = [true, false];
const activestatus = ["Active","Inactive"];
const plantimestatus = ["month","year"];

const PlanSchema = new Schema({
	title: {type: String,default: '',},
	slug: {type: String,default: '',},
	price: {type: Schema.Types.Double,default: 0.00},
	content:{type:String,default:''},
	stripe_plan_id:{type:String,default:''},
	plan_time:{type: String,default: "month",enum:plantimestatus },
	child_add_provision:{type:Number,default:0},
	status: {type: String,default: "Active",enum:activestatus },
	isDeleted: {type: Boolean,default: false,enum: deletedstatus},
},{timestamps:true});

PlanSchema.plugin(beautifyUnique);
// For pagination
PlanSchema.plugin(mongooseAggregatePaginate);

// create the model for users and expose it to our app
module.exports = mongoose.model('Plan', PlanSchema);