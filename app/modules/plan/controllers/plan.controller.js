const planRepo = require('plan/repositories/plan.repository');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const gm = require('gm').subClass({
    imageMagick: true
});
const fs = require('fs');
const stripe = require("stripe")(config.stripe_secret_key);
const errorHandler = require('../../../errorHandler');

class planController {
    async create(req, res) {
        try {
            res.render('avatar/views/add.ejs', {
                page_name: 'avatar-management',
                page_title: 'Create New Avatar',
                user: req.user
            });
        } catch (e) {
            throw (e);
        }
    };

    async insert(req, res) {
        try {
            req.body.image = '';
            if (typeof req.fileValidationError != 'undefined') {
                req.flash('error', req.fileValidationError);
                res.redirect(namedRouter.urlFor('avatar.create'));
            } else {
                let avatar = await avatarRepo.getByField({
                    'title': req.body.title
                });
                if (!_.isEmpty(avatar)) {
                    req.flash('error', `Avatar "${req.body.title}" is already exist!`);
                    res.redirect(namedRouter.urlFor('avatar.create'));
                } else {
                    console.log('dsdsdsdsdsd');
                    if (_.has(req, 'files')) {
                        console.log('----1');
                        if (req.files.length > 0) {
                            console.log('----2');
                            if (req.files[0].fieldname == 'image') {
                                console.log('----3');
                                console.log('asas:', req.files);
                                gm('public/uploads/avatar/' + req.files[0].filename)
                                    .resize(100)
                                    .write('public/uploads/avatar/thumb/' + req.files[0].filename, function (err) {
                                        if (err) {
                                            req.flash('error', err.message);
                                            res.redirect(namedRouter.urlFor('avatar.create'));
                                        } else {
                                            req.body.image = req.files[0].filename;
                                            let newavatar = avatarRepo.save(req.body);
                                            req.flash('success', 'Avatar created succesfully.');
                                            res.redirect(namedRouter.urlFor('avatar.listing'));
                                        }
                                    });
                            }
                        } else {
                            req.flash('error', 'Please browse an avatar to upload');
                            res.redirect(namedRouter.urlFor('avatar.create'));
                        }
                    } else {
                        req.flash('error', 'Please browse an avatar to upload');
                        res.redirect(namedRouter.urlFor('avatar.create'));
                    }
                }
            }
        } catch (e) {
            const error = errorHandler(e);
            req.flash('error', error.message);
            res.redirect(namedRouter.urlFor('avatar.create'));
        }
    };

	/*
	// @Method: edit
	// @Description:  Plan update page
	*/
	async edit(req, res) {
		try {
			let result = {};
			let plan = await planRepo.getById(req.params.id);
			if (!_.isEmpty(plan)) {
				result.plan_data = plan;
				res.render('plan/views/edit.ejs', {
					page_name: 'plan-management',
					page_title: 'Update Plan',
					user: req.user,
					response: result
				});
			}
			else {
				req.flash('error', "Sorry plan not found!");
				res.redirect(namedRouter.urlFor('plan.list'));
			}
		}
		catch (e) {
			return res.status(500).send({message: e.message});
		}
	};

	/* @Method: update
	// @Description: plan update action
	*/
	async update(req, res) {
		try {
			const planId = req.body.pid;
			let plan = await planRepo.getByField({'title':req.body.title,_id:{$ne:planId}});
			var slug = req.body.title.toLowerCase();
			req.body.slug=slug.split(' ').join('-');
			if (_.isEmpty(plan)) {
				
				req.body.price = parseFloat(req.body.price);
		
				var splan = await stripe.plans.create({
					amount: req.body.price * 100,
					interval: req.body.plan_time,
					product: {name: req.body.title},
					currency: "usd",
				});
				req.body.stripe_plan_id = splan.id;
				
				let planUpdate = planRepo.updateById(req.body,planId);
				if(planUpdate) {
					req.flash('success', "Plan Updated Successfully");
					res.redirect(namedRouter.urlFor('plan.list'));
				}
			}
			else{
				req.flash('error', "Plan is already availabe!");
				res.redirect(namedRouter.urlFor('plan.edit', { id: planId }));
			}    
		}
		catch (e) {
			const error = errorHandler(e);
			req.flash('error', error.message);
			res.redirect(namedRouter.urlFor('plan.edit', {id: req.body.pid}));
		}
	};

	/* @Method: list
	// @Description: To get all the avatars from DB
	*/
	async list(req, res) {
		try {
			res.render('plan/views/list.ejs', {
				page_name: 'plan-management',
				page_title: 'Plan List',
				user: req.user
			});
		}
		catch (e) {
			return res.status(500).send({message: e.message});
		}
	};

	async getAll(req, res) {
		try {
			let plan = await planRepo.getAll(req);
			if (_.has(req.body, 'sort')) {
				var sortOrder = req.body.sort.sort;
				var sortField = req.body.sort.field;
			}
			else {
				var sortOrder = -1;
				var sortField = '_id';
			}
			let meta = {
				"page": req.body.pagination.page,
				"pages": plan.pageCount,
				"perpage": req.body.pagination.perpage,
				"total": plan.totalCount,
				"sort": sortOrder,
				"field": sortField
			};
			return {	status: 200,	meta: meta,data: plan.data,message: "Data fetched succesfully."};
		}
		catch (e) {
			return {	status: 500,	data: [],message: e.message};
		}
	}


    /* @Method: delete
    // @Description: avatar delete
    */
    async destroy(req, res) {
        try {
            let avatar = await avatarRepo.getByField({
                '_id': req.params.id
            });
            let avatarDelete = await avatarRepo.delete(req.params.id)
            if (!_.isEmpty(avatarDelete)) {
                if (!_.isEmpty(avatar.image)) {
                    if (fs.existsSync('public/uploads/avatar/' + avatar.image)) {
                        const upl_img = fs.unlinkSync('public/uploads/avatar/' + avatar.image);
                    }
                    if (fs.existsSync('public/uploads/avatar/thumb/' + avatar.image)) {
                        const upl_thumb_img = fs.unlinkSync('public/uploads/avatar/thumb/' + avatar.image);
                    }
                }
                req.flash('success', 'Avatar Removed Successfully');
                res.redirect(namedRouter.urlFor('avatar.listing'));
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };


    /*
    // @Method: verifyAvatarTitle
    // @Description: To verify avatar title
    */
    async verifyAvatarTitle(req, res) {
        try {
            const result = await avatarRepo.getAllByField({
                "title": req.body.title
            });

            if (_.isEmpty(result)) {
                return {
                    "status": 200,
                };
            } else {
                return {
                    "status": 200,
                    "message": "Avatar Title already exists."
                };
            }
        } catch (error) {
            throw error;
        }
    };


}




module.exports = new planController();