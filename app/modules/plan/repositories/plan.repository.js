const mongoose = require('mongoose');
const Plan = require('plan/models/plan.model');
const perPage = config.PAGINATION_PERPAGE;

const planRepository = {
	getAll: async (req) => {
		// console.log(req.body);
		try {
			var conditions = {};
			var and_clauses = [];
	
			and_clauses.push({"isDeleted": false});
	
			if (_.isObject(req.body.query) && _.has(req.body.query, 'generalSearch')) {
				//and_clauses.push({"status": /req.body.query.generalSearch/i});
				and_clauses.push({'title': {	$regex: req.body.query.generalSearch,$options: 'i'}});
			}
			if (_.isObject(req.body.query) && _.has(req.body.query, 'Status')) {
				and_clauses.push({"status": req.body.query.Status});
			}
			conditions['$and'] = and_clauses;
	
			var sortOperator = {"$sort": {}};
			if (_.has(req.body, 'sort')) {
				var sortField = req.body.sort.field;
				if (req.body.sort.sort == 'desc') {
					var sortOrder = -1;
				}
				else if (req.body.sort.sort == 'asc') {
					var sortOrder = 1;
				}
	
				sortOperator["$sort"][sortField] = sortOrder;
			}
			else {
				sortOperator["$sort"]['_id'] = -1;
			}
	
			var aggregate = Plan.aggregate([
							{
								$match: conditions
							},
							sortOperator
						]);
	
			var options = {page: req.body.pagination.page,limit: req.body.pagination.perpage};
			let allPlan = await Plan.aggregatePaginate(aggregate, options);
			return allPlan;
		}
		catch (e) {
			throw (e);
		}
	},

    getAuthorCount: async (req) => {
        try {

            let plan = await Plan.find({
                isDeleted: false
            });
            return plan;
        } catch (e) {
            throw (e);
        }
    },

    getById: async (id) => {
        try {
            let plan = await Plan.findById(id).exec();
            return plan;
        } catch (e) {
            throw (e);
        }
    },

    getByField: async (params) => {
        try {
            let plan = await Plan.findOne(params).exec();
            return plan;
        } catch (e) {
            throw (e);
        }
    },

    getAllByField: async (params) => {
        try {
            let plan = await Plan.find(params).exec();
            return plan;
        } catch (e) {
            throw (e);
        }
    },

    delete: async (id) => {
        try {
            let plan = await Plan.findById(id);
            if (plan) {
                let planDelete = await Plan.remove({
                    _id: id
                }).exec();
                return planDelete;
            } else {
                return null;
            }
        } catch (e) {
            throw (e);
        }
    },

    deleteByField: async (field, fieldValue) => {
        //todo: Implement delete by field
    },


    updateById: async (data, id) => {
        try {
            let plan = await Plan.findByIdAndUpdate(id, data, {
                new: true,
                upsert: true
            }).exec();
            return plan;
        } catch (e) {
            throw (e);
        }
    },

    updateByField: async (field, fieldValue, data) => {
        //todo: update by field
    },

    save: async (data) => {
        try {
            let plan = await Plan.create(data);
            if (!plan) {
                return null;
            }
            return plan;
        } catch (e) {
            throw e;
        }
    },

    parentSubscriptionInfo: async (id) => {
        try {
            let users = await Plan.aggregate([{
                $match: {
                    parent_id: mongoose.Types.ObjectId(id),"isDeleted":false
                }
            }]).exec();
            return users;
        } catch (e) {
            throw (e);
        }
    },

};

module.exports = planRepository;