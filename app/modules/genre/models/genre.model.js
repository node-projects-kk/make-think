const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');
const beautifyUnique = require('mongoose-beautiful-unique-validation');

const GenreSchema = new Schema({
  title: { type: String, default: '', unique: 'Genre "{VALUE}" is already exist!' },
  slug: { type: String, default: '' },
  status: { type: String, default: "Active", enum: ["Active", "Inactive"] },
  isDeleted: {type: Boolean, default: false, enum: [true, false]}
});

GenreSchema.plugin(beautifyUnique);
// For pagination
GenreSchema.plugin(mongooseAggregatePaginate);

// create the model for users and expose it to our app
module.exports = mongoose.model('Genre', GenreSchema);