const mongoose = require('mongoose');
const Genre = require('genre/models/genre.model');
const perPage = config.PAGINATION_PERPAGE;

const genreRepository = {
    getAll: async ( req ) => {
        try {
            var conditions = {};
            var and_clauses = [];

            and_clauses.push({"isDeleted": false});
            
            if(_.isObject(req.body.query) && _.has(req.body.query, 'generalSearch')){
                //and_clauses.push({"status": /req.body.query.generalSearch/i});
                and_clauses.push({ 'title': { $regex:  req.body.query.generalSearch, $options: 'i'} });
            }
            if(_.isObject(req.body.query) && _.has(req.body.query, 'Status')){
                and_clauses.push({"status": req.body.query.Status});
            }
            conditions['$and'] = and_clauses;
            
            var sortOperator = { "$sort": { } };
            if(_.has(req.body, 'sort')){
                var sortField = req.body.sort.field;
                if(req.body.sort.sort == 'desc'){
                    var sortOrder = -1;
                }else if(req.body.sort.sort == 'asc'){
                    var sortOrder = 1;
                }
                
                sortOperator["$sort"][sortField] = sortOrder;
            }else{
                sortOperator["$sort"]['_id'] = -1;
            }

            var aggregate = Genre.aggregate([
                {$match: conditions},
                sortOperator
            ]);

            var options = { page : req.body.pagination.page, limit : req.body.pagination.perpage};
            let allGenre = await Genre.aggregatePaginate(aggregate, options);
            return allGenre;
        }catch(e){
            throw(e);
        }
    },

    getGenreCount: async (req) => {
        try {
                      
            let geners = await Genre.find({isDeleted:false});
            return geners;
        } catch(e) {
            throw(e);
        }
     },

    getById: async (id) => {
        try{
            let genre = await Genre.findById(id).exec(); 
            return genre;
        }catch(e){
            throw(e);
        }
    },

    getByField: async (params) => {        
        try{
            let genre =  await Genre.findOne(params).exec(); 
            return genre;    
        }catch(e){
            throw(e);
        }
    },

    getAllByField: async (params) => {         
        try{
            let genre =  await Genre.find(params).exec(); 
            return genre;    
        }catch(e){
            throw(e);
        }
    },
    
    delete: async (id) => {
        try { 
            let genre = await Genre.findById(id);
            if(genre) {
                let genreDelete = await Genre.remove({_id:id}).exec();                
                return genreDelete;
            }else{
                return null;
            }
        } catch(e){
            throw(e);
        }
    },

    deleteByField: async (field, fieldValue) => {
        //todo: Implement delete by field
    },


    updateById: async (data, id) =>{
        try {
            let genre = await Genre.findByIdAndUpdate(id, data, { new: true, upsert: true }).exec();
            return genre;
        } catch(e) {
            throw(e);
        }
    },

    updateByField: async (field, fieldValue, data) => {
        //todo: update by field
    },

    save: async ( data ) => {
        try {
            let genre = await Genre.create(data);
            if (!genre) {
                return null;
            }
            return genre;
        } catch(e) {
            throw e;
        }
    },
};

module.exports = genreRepository;