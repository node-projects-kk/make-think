const genreRepo = require('genre/repositories/genre.repository');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const fs = require('fs');
const errorHandler = require('../../../errorHandler');


class genreController {

    async create (req, res){
        try {
            res.render('genre/views/add.ejs', {
                page_name: 'genre-management',
                page_title: 'Create New Genre',
                user: req.user
            });
        } catch(e) {
            throw(e);
        }
    };

    async insert (req, res){
        try {
            let newGenre = await genreRepo.save(req.body);
            req.flash('success', 'Genre created succesfully.');
            res.redirect(namedRouter.urlFor('genre.listing'));
        } catch(e) {
            const error = errorHandler(e);
            req.flash('error', error.message);
            //res.status(500).send({message: error.message});
            res.redirect(namedRouter.urlFor('genre.create'));
        }
    };

    /*
    // @Method: edit
    // @Description:  Coupon update page
    */
    async edit (req, res){
        try{
            let result = {};
            let genre = await genreRepo.getById(req.params.id);
            if (!_.isEmpty(genre)) {
                result.genre_data = genre;
                res.render('genre/views/edit.ejs', {
                    page_name: 'genre-management',
                    page_title: 'Update Genre',
                    user: req.user,
                    response: result
                });
            } else {
                req.flash('error', "Sorry Genre not found!");
                res.redirect(namedRouter.urlFor('genre.list')); 
            }
        } catch(e){
            return res.status(500).send({message: e.message}); 
        }
    };

    /* @Method: update
    // @Description: coupon update action
    */
    async update (req, res){
        try {
            const genreId = req.body.gid;
            let genreUpdate = await genreRepo.updateById(req.body, genreId);
            if(genreUpdate) {
                req.flash('success', "Genre Updated Successfully");
                res.redirect(namedRouter.urlFor('genre.listing'));
            }else{
                res.redirect(namedRouter.urlFor('genre.edit', { id: genreId }));
            }            
        }catch(e){
            const error = errorHandler(e);
            req.flash('error', error.message);
            res.redirect(namedRouter.urlFor('genre.edit', { id: req.body.gid }));
        }      
            
    };



    /* @Method: list
    // @Description: To get all the genres from DB
    */
    async list (req, res){
        try{
            res.render('genre/views/list.ejs', {
                page_name: 'genre-management',
                page_title: 'Genre List',
                user: req.user
            });
        } catch(e){
            return res.status(500).send({message: e.message}); 
        }  
    };

    async getAll (req, res){
        try{
            let genre = await genreRepo.getAll(req);
            if(_.has(req.body, 'sort')){
                var sortOrder = req.body.sort.sort;
                var sortField = req.body.sort.field;
            }else{
                var sortOrder = -1;
                var sortField = '_id';
            }
            let meta = {"page": req.body.pagination.page, "pages": genre.pageCount, "perpage": req.body.pagination.perpage, "total": genre.totalCount, "sort": sortOrder, "field": sortField};
            return {status: 200, meta: meta, data:genre.data, message: `Data fetched succesfully.`};
        } catch(e){
            return {status: 500,data: [],message: e.message};
        }
    }
   

    
    /*
    // @Method: status_change
    // @Description: coupon status change action
    */
    async statusChange (req, res){
        try {

            let genre = await genreRepo.getById(req.params.id);
            // console.log("fsj;fgsdkgnjksdgs",req.params)

            if(!_.isEmpty(genre)){
                let genreStatus = (genre.isActive == 'Active') ? 'Inactive' : 'Active';
                let genreUpdate = genreRepo.updateById({ 'isActive': genreStatus }, req.body.id);
                req.flash('success', "Genre status has changed successfully" );
                res.send(genreUpdate);
            } else {
                req.flash('error', "sorry genre not found");
                res.redirect(namedRouter.urlFor('genre.listing')); 
            }
        } catch(e){
            return res.status(500).send({message: e.message}); 
        }
    };

    /* @Method: delete
    // @Description: genre delete
    */
    async delete (req, res){
        try{
            let genreDelete = await genreRepo.updateById({"isDeleted": true}, req.params.id)
            if(!_.isEmpty(genreDelete)){
                req.flash('success','Genre Removed Successfully');
                res.redirect(namedRouter.urlFor('genre.listing'));
            } 
        }catch(e){
            return res.status(500).send({message: e.message});   
        } 
    };

}

module.exports = new genreController();