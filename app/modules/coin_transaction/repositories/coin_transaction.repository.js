const mongoose = require('mongoose');
const Coin = require('coin_transaction/models/coin_transaction.model');
const perPage = config.PAGINATION_PERPAGE;

const CoinRepository = {
    getAll: async (req) => {
        // console.log(req.body);
        try {
            var conditions = {};
            var and_clauses = [];

            and_clauses.push({
                "isDeleted": false
            });

            if (_.isObject(req.body.query) && _.has(req.body.query, 'generalSearch')) {
                //and_clauses.push({"status": /req.body.query.generalSearch/i});
                and_clauses.push({
                    'title': {
                        $regex: req.body.query.generalSearch,
                        $options: 'i'
                    }
                });
            }
            if (_.isObject(req.body.query) && _.has(req.body.query, 'Status')) {
                and_clauses.push({
                    "status": req.body.query.Status
                });
            }
            conditions['$and'] = and_clauses;

            var sortOperator = {
                "$sort": {}
            };
            if (_.has(req.body, 'sort')) {
                var sortField = req.body.sort.field;
                if (req.body.sort.sort == 'desc') {
                    var sortOrder = -1;
                } else if (req.body.sort.sort == 'asc') {
                    var sortOrder = 1;
                }

                sortOperator["$sort"][sortField] = sortOrder;
            } else {
                sortOperator["$sort"]['_id'] = -1;
            }

            var aggregate = Coin.aggregate([{
                    $match: conditions
                },
                sortOperator
            ]);

            var options = {
                page: req.body.pagination.page,
                limit: req.body.pagination.perpage
            };
            let allCoin = await Coin.aggregatePaginate(aggregate, options);
            return allCoin;
        } catch (e) {
            throw (e);
        }
    },

    getAuthorCount: async (req) => {
        try {

            let coin = await Coin.find({
                isDeleted: false
            });
            return coin;
        } catch (e) {
            throw (e);
        }
    },

    getById: async (id) => {
        try {
            let coin = await Coin.findById(id).exec();
            return coin;
        } catch (e) {
            throw (e);
        }
    },

    getByField: async (params) => {
        try {
            let coin = await Coin.findOne(params).exec();
            return coin;
        } catch (e) {
            throw (e);
        }
    },

    getAllByField: async (params) => {
        try {
            let coin = await Coin.find(params).exec();
            return coin;
        } catch (e) {
            throw (e);
        }
    },

    delete: async (id) => {
        try {
            let coin = await Coin.findById(id);
            if (coin) {
                let coinDelete = await Coin.remove({
                    _id: id
                }).exec();
                return coinDelete;
            } else {
                return null;
            }
        } catch (e) {
            throw (e);
        }
    },

    deleteByField: async (field, fieldValue) => {
        //todo: Implement delete by field
    },


    updateById: async (data, id) => {
        try {
            let coin = await Coin.findByIdAndUpdate(id, data, {
                new: true,
                upsert: true
            }).exec();
            return coin;
        } catch (e) {
            throw e;
        }
    },

    updateByField: async (field, fieldValue, data) => {
        //todo: update by field
    },

    save: async (data) => {
        try {
            let coin = await Coin.create(data);
            if (!coin) {
                return null;
            }
            return coin;
        } catch (e) {
            throw e;
        }
    },
};

module.exports = CoinRepository;