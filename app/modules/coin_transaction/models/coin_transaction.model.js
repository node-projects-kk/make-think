const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');
const beautifyUnique = require('mongoose-beautiful-unique-validation');

const CoinTransactionSchema = new Schema({
  user_id: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    default: null
  },
  avatar_id: {
    type: Schema.Types.ObjectId,
    ref: 'Avatar',
    default: null
  },
  coins: {
    type: Number,
    default: ''
  },
  purchase_date: {
    type: Number,
    default: Date.now()
  },
  status: {
    type: String,
    default: "Active",
    enum: ["Active", "Inactive"]
  },
  isDeleted: {
    type: Boolean,
    default: false,
    enum: [true, false]
  }
});

CoinTransactionSchema.plugin(beautifyUnique);
// For pagination
CoinTransactionSchema.plugin(mongooseAggregatePaginate);

// create the model for users and expose it to our app
module.exports = mongoose.model('CoinTransaction', CoinTransactionSchema);