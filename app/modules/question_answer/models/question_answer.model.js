const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');
const beautifyUnique = require('mongoose-beautiful-unique-validation');

const QuestionAnswerSchema = new Schema({
  user_id: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  points_earned: {
    type: Schema.Types.Double,
    default: 10.0
  },
  qadetails: [{
    question_id: {
      type: Schema.Types.ObjectId,
      ref: 'Question'
    },
    isCorrect: {
      type: Boolean,
      default: null,
      enum: [true, false]
    },
    answer_type: {
      type: String,
      enum: ['descriptive', 'drawing'],
      default: ''
    },
    answer: {
      type: String,
      default: ''
    },
    _id: false
  }],
  book_id: {
    type: Schema.Types.ObjectId,
    ref: 'Book'
  },
  chapter_id: {
    type: Schema.Types.ObjectId,
    ref: 'Chapter'
  },
  status: {
    type: String,
    default: "Active",
    enum: ["Active", "Inactive"]
  },
  isDeleted: {
    type: Boolean,
    default: false,
    enum: [true, false]
  },
  createdAt: {
    type: Date,
    default: Date.now()
  }
});

QuestionAnswerSchema.plugin(beautifyUnique);
// For pagination
QuestionAnswerSchema.plugin(mongooseAggregatePaginate);

// create the model for users and expose it to our app
module.exports = mongoose.model('UserQuestionAnswer', QuestionAnswerSchema);