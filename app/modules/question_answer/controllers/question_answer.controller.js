const mongoose = require('mongoose');
const answerRepo = require('question_answer/repositories/question_answer.repository');
const bookRepo = require('book/repositories/book.repository');
const chapterRepo = require('chapter/repositories/chapter.repository');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const fs = require('fs');
const errorHandler = require('../../../errorHandler');


class questionAnswerController {

    async studentListAnswer(req, res) {
        try {
            res.render('question_answer/views/list.ejs', {
                page_name: 'answer-list-management',
                page_title: 'Answer Report List',
                user: req.user,
                userid: req.params.id
            });
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    }

    async getAllAnswerChapterWise(req, res) {
        try {
            let answer = await answerRepo.getStudentAnswerByChapter(req);
            if (_.has(req.body, 'sort')) {
                var sortOrder = req.body.sort.sort;
                var sortField = req.body.sort.field;
            } else {
                var sortOrder = -1;
                var sortField = '_id';
            }
            let meta = {
                "page": req.body.pagination.page,
                "pages": answer.pageCount,
                "perpage": req.body.pagination.perpage,
                "total": answer.totalCount,
                "sort": sortOrder,
                "field": sortField
            };
            return {
                status: 200,
                meta: meta,
                data: answer.data,
                message: `Data fetched succesfully.`
            };
        } catch (e) {
            console.log('ererererere', e);
            return {
                status: 500,
                data: [],
                message: e.message
            };
        }
    }

    async studentViewAnswer(req, res) {
        try {
            let answerDetails = await answerRepo.getStudentAnswerDetailsById(req.params.id);
            res.render('question_answer/views/view.ejs', {
                page_name: 'answer-view-management',
                page_title: 'Answer Report View',
                user: req.user,
                userid: req.params.id,
                response: answerDetails
            });
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    }
}

module.exports = new questionAnswerController();