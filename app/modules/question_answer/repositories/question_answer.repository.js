const mongoose = require('mongoose');
const QuestionAnswer = require('question_answer/models/question_answer.model');
const perPage = config.PAGINATION_PERPAGE;

const QuestionAnswerRepository = {
    getStudentAnswerByChapter: async (req) => {
        try {
            var conditions = {};
            const and_clauses = [];

            and_clauses.push({
                "isDeleted": false,
                'user_id': mongoose.Types.ObjectId(req.params.id)
            });
            if (_.isObject(req.body.query) && _.has(req.body.query, 'generalSearch')) {
                and_clauses.push({
                    $or: [{
                            'first_name': {
                                $regex: req.body.query.generalSearch,
                                $options: 'i'
                            }
                        },
                        {
                            'last_name': {
                                $regex: req.body.query.generalSearch,
                                $options: 'i'
                            }
                        },
                        {
                            'question_type': {
                                $regex: req.body.query.generalSearch,
                                $options: 'i'
                            }
                        }
                    ]
                });
            }
            if (_.isObject(req.body.query) && _.has(req.body.query, 'Status')) {
                and_clauses.push({
                    "status": req.body.query.Status
                });
            }
            conditions['$and'] = and_clauses;


            var sortOperator = {
                "$sort": {}
            };
            if (_.has(req.body, 'sort')) {
                var sortField = req.body.sort.field;
                if (req.body.sort.sort == 'desc') {
                    var sortOrder = -1;
                } else if (req.body.sort.sort == 'asc') {
                    var sortOrder = 1;
                }

                sortOperator["$sort"][sortField] = sortOrder;
            } else {
                sortOperator["$sort"]['_id'] = -1;
            }

            var aggregate = QuestionAnswer.aggregate([{
                    $match: conditions
                },
                {
                    "$lookup": {
                        "from": "books",
                        "localField": "book_id",
                        "foreignField": "_id",
                        "as": "book_info"
                    },
                },
                {
                    $unwind: {
                        path: "$book_info",
                        preserveNullAndEmptyArrays: true
                    }
                },
                {
                    "$lookup": {
                        "from": "chapters",
                        "localField": "chapter_id",
                        "foreignField": "_id",
                        "as": "chapter_info"
                    },
                },
                {
                    $unwind: {
                        path: "$chapter_info",
                        preserveNullAndEmptyArrays: true
                    }
                },
                {
                    "$lookup": {
                        "from": "users",
                        "localField": "user_id",
                        "foreignField": "_id",
                        "as": "user_info"
                    },
                },
                {
                    $unwind: {
                        path: "$chapter_info",
                        preserveNullAndEmptyArrays: true
                    }
                },
                {
                    $unwind: "$qadetails"
                },
                {
                    "$lookup": {
                        "from": "questions",
                        "localField": "qadetails.question_id",
                        "foreignField": "_id",
                        "as": "qadetails.question_id"
                    }
                },
                {
                    $group: {
                        _id: {
                            book_id: "$book_id",
                            user_id: "$user_id",
                            chapter_id: "$chapter_id"
                        },
                        answer_id: {
                            $first: '$_id'
                        },
                        book_info: {
                            $first: '$book_info'
                        },
                        chapter_info: {
                            $first: '$chapter_info'
                        },
                        user_info: {
                            $first: '$user_info'
                        },
                        qadetails: {
                            $addToSet: '$qadetails'
                        },
                    }
                },
                sortOperator
            ]);

            var options = {
                page: req.body.pagination.page,
                limit: req.body.pagination.perpage
            };
            let allAnswer = await QuestionAnswer.aggregatePaginate(aggregate, options);
            return allAnswer;
        } catch (e) {
            console.log('ererererererer', e);
            throw (e);
        }
    },

    getStudentAnswerDetailsById: async (id) => {
        try {
            var conditions = {};
            const and_clauses = [];

            and_clauses.push({
                "isDeleted": false,
                '_id': mongoose.Types.ObjectId(id)
            });
            conditions['$and'] = and_clauses;

            return await QuestionAnswer.aggregate([{
                    $match: conditions
                },
                {
                    "$lookup": {
                        "from": "books",
                        "localField": "book_id",
                        "foreignField": "_id",
                        "as": "book_info"
                    },
                },
                {
                    $unwind: {
                        path: "$book_info",
                        preserveNullAndEmptyArrays: true
                    }
                },
                {
                    "$lookup": {
                        "from": "chapters",
                        "localField": "chapter_id",
                        "foreignField": "_id",
                        "as": "chapter_info"
                    },
                },
                {
                    $unwind: {
                        path: "$chapter_info",
                        preserveNullAndEmptyArrays: true
                    }
                },
                {
                    "$lookup": {
                        "from": "users",
                        "localField": "user_id",
                        "foreignField": "_id",
                        "as": "user_info"
                    },
                },
                {
                    $unwind: {
                        path: "$chapter_info",
                        preserveNullAndEmptyArrays: true
                    }
                },
                {
                    $unwind: "$qadetails"
                },
                {
                    "$lookup": {
                        "from": "questions",
                        "localField": "qadetails.question_id",
                        "foreignField": "_id",
                        "as": "qadetails.question_id"
                    }
                },
                {
                    $group: {
                        _id: {
                            book_id: "$book_id",
                            user_id: "$user_id",
                            chapter_id: "$chapter_id"
                        },
                        answer_id: {
                            $first: '$_id'
                        },
                        book_info: {
                            $first: '$book_info'
                        },
                        chapter_info: {
                            $first: '$chapter_info'
                        },
                        user_info: {
                            $first: '$user_info'
                        },
                        qadetails: {
                            $addToSet: '$qadetails'
                        },
                    }
                }
            ]);
        } catch (e) {
            console.log('ererererererer', e);
            throw (e);
        }
    },

    getById: async (id) => {
        try {
            let question = await QuestionAnswer.findById(id);
            return question ? question : null;
        } catch (e) {
            throw (e);
        }
    },

    getByField: async (params) => {
        try {
            let Question = await QuestionAnswer.findOne(params).exec();
            return Question;
        } catch (e) {
            throw (e);
        }
    },

    getAllByField: async (params) => {
        try {
            let Question = await QuestionAnswer.find(params).exec();
            return Question;
        } catch (e) {
            throw (e);
        }
    },

    delete: async (id) => {
        try {
            let Question = await QuestionAnswer.findById(id);
            if (Question) {
                let QuestionDelete = await QuestionAnswer.remove({
                    _id: id
                }).exec();
                return QuestionDelete;
            } else {
                return null;
            }
        } catch (e) {
            throw (e);
        }
    },

    deleteByField: async (field, fieldValue) => {
        //todo: Implement delete by field
    },


    updateById: async (data, id) => {
        try {
            let question = await QuestionAnswer.findByIdAndUpdate(id, data, {
                new: true
            }).exec();
            if (question) {
                return question;
            } else {
                return null;
            }
        } catch (e) {
            throw (e);
        }
    },

    updateByField: async (field, fieldValue, data) => {
        //todo: update by field
    },

    save: async (data) => {
        try {
            let question = await QuestionAnswer.create(data);
            if (!question) {
                return null;
            }
            return question;
        } catch (e) {
            throw e;
        }
    },
};

module.exports = QuestionAnswerRepository;