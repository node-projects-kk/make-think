const roleRepo = require('role/repositories/role.repository');
const userRepo = require('user/repositories/user.repository');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const fs = require('fs');



class roleController {
    constructor() {
        this.language = [];

    }

    /*
    // @Method: edit
    // @Description:  Coupon update page
    */
    async edit(req, res) {
        try {
            let role = await roleRepo.getById(req.params.id);
            if (!_.isEmpty(role)) {

                res.render('role/views/edit.ejs', {
                    page_name: 'role-management',
                    page_title: 'Edit role',
                    user: req.user,
                    response: role
                });
            } else {
                req.flash('error', "Sorry role not found!");
                res.redirect(namedRouter.urlFor('role.list'));
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    /* @Method: update
    // @Description: coupon update action
    */
    async update(req, res) {
        try {
            let checkCountry = await roleRepo.getByField({
                "role_name": req.body.role_name
            })
            if (checkCountry) {
                req.flash('error', 'role with same name already exists.');
                res.redirect(namedRouter.urlFor('role.list'));
            } else {
                let role = await roleRepo.getById(req.body.id);
                if (!_.isEmpty(role)) {
                    let roleUpdate = await roleRepo.updateById(req.body, req.body.id)
                    if (roleUpdate) {
                        req.flash('success', "role Updated Successfully");
                        res.redirect(namedRouter.urlFor('role.list'));
                    }

                } else {
                    req.flash('error', "role not found!");
                    res.redirect(namedRouter.urlFor('role.edit', {
                        id: req.body.id
                    }));
                }
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }

    };



    /* @Method: list
    // @Description: To get all the users from DB
    */
    async list(req, res) {
        try {
            res.render('role/views/list.ejs', {
                page_name: 'role-list',
                page_title: 'role List',
                user: req.user,

            });
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    async create(req, res) {
        try {
            res.render('role/views/add.ejs', {
                page_name: 'role-Create',
                page_title: 'role Create',
                user: req.user,
            });
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    async store(req, res) {
        try {
            let checkCountry = await roleRepo.getByField({
                "role_name": req.body.role_name
            })
            if (checkCountry) {
                req.flash('error', 'role with same name already exists.');
                res.redirect(namedRouter.urlFor('role.list'));
            } else {
                let newrole = await roleRepo.save(req.body);
                req.flash('success', 'role created succesfully.');
                res.redirect(namedRouter.urlFor('role.list'));
            }
        } catch (e) {
            const error = errorHandler(e);
            req.flash('error', error.message);
            //res.status(500).send({message: error.message});
            res.redirect(namedRouter.urlFor('role.create'));
        }
    };

    async getAll(req, res) {
        try {
           
            if (_.has(req.body, 'sort')) {
                var sortOrder = req.body.sort.sort;
                var sortField = req.body.sort.field;
            } else {
                var sortOrder = -1;
                var sortField = '_id';
            }
            let role = await roleRepo.getAll(req);
            let meta = {
                "page": req.body.pagination.page,
                "pages": role.pageCount,
                "perpage": req.body.pagination.perpage,
                "total": role.total,
                "sort": sortOrder,
                "field": sortField
            };
            return {
                status: 200,
                meta: meta,
                data: role.docs,
                message: "Data fetched succesfully."
            };
        } catch (e) {
            return {
                status: 500,
                data: [],
                message: e.message
            };
        }
    }
    /*
    // @Method: status_change
    // @Description: language status change action
    */
    async changeStatus(req, res) {
        try {
            let role = await roleRepo.getById(req.params.id);
            if (!_.isEmpty(role)) {
                let roleStatus = (role.isActive == true) ? false : true;
                let roleUpdate = await roleRepo.updateById({
                    isActive: roleStatus
                }, req.params.id);
                req.flash('success', "role status has changed successfully");
                res.redirect(namedRouter.urlFor('role.list'));
            } else {
                req.flash('error', "sorry role not found");
                res.redirect(namedRouter.urlFor('role.list'));
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    /* @Method: delete
    // @Description: coupon delete
    */
    async destroy(req, res) {
        try {
            let languageDelete = await roleRepo.delete(req.params.id)
            if (!_.isEmpty(languageDelete)) {
                req.flash('success', 'role Removed Successfully');
                res.redirect(namedRouter.urlFor('role.list'));
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    async updateCoins(req,res){
       try{
         var roleId = req.body.id;
         var data = parseFloat(req.body.user_balance);
         var allUsersBasedOnRole = await userRepo.getAllByField({'role': req.body.id, 'isDeleted':false, 'isActive': true});
         var available_balance = allUsersBasedOnRole[0].user_balance.value;
         data += available_balance;
        for(var i=0; i<=allUsersBasedOnRole.length; i++)
        {       
                var updateRoleBasedUserBalance = await userRepo.updateAllByField(roleId,data );
        }
        req.flash('success', 'Coins Update Successfully');
        res.redirect(namedRouter.urlFor('role.list'));
    }
        catch(e){
            return res.status(500).send({
                message: e.message
            });
        }
    }


}

module.exports = new roleController();