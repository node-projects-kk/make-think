const mongoose = require('mongoose');
const Product = require('product/models/product.model');
const perPage = config.PAGINATION_PERPAGE;

const productRepository = {

    getAll: async (req) => {
        try {
            var conditions = {};
            var and_clauses = [];

            and_clauses.push({
                "isDeleted": false
            });

            if (_.isObject(req.body.query) && _.has(req.body.query, 'generalSearch')) {
                and_clauses.push({
                    $or: [{
                            'title': {
                                $regex: req.body.query.generalSearch,
                                $options: 'i'
                            }
                        },
                        {
                            'chapter_num': parseInt(req.body.query.generalSearch),

                        },
                        {
                            'genre_docs.title': {
                                $regex: req.body.query.generalSearch,
                                $options: 'i'
                            }
                        },
                        {
                            'author_docs.title': {
                                $regex: req.body.query.generalSearch,
                                $options: 'i'
                            }
                        }
                    ]
                });
            }
            if (_.isObject(req.body.query) && _.has(req.body.query, 'Status')) {
                and_clauses.push({
                    "status": req.body.query.Status
                });
            }
            conditions['$and'] = and_clauses;
            var sortOperator = {
                "$sort": {}
            };
            if (_.has(req.body, 'sort')) {
                var sortField = req.body.sort.field;
                if (req.body.sort.sort == 'desc') {
                    var sortOrder = -1;
                } else if (req.body.sort.sort == 'asc') {
                    var sortOrder = 1;
                }

                sortOperator["$sort"][sortField] = sortOrder;
            } else {
                sortOperator["$sort"]['_id'] = -1;
            }

            var aggregate = Product.aggregate([{
                    $match: conditions
                },
                sortOperator
            ]);

            var options = {
                page: req.body.pagination.page,
                limit: req.body.pagination.perpage
            };
            let allProduct = await Product.aggregatePaginate(aggregate, options);
            return allProduct;
        } catch (e) {
            throw (e);
        }
    },

    getById: async (id) => {
        let product = await Product.findById(id).exec();
        try {
            if (!product) {
                return null;
            }
            return product;
        } catch (e) {
            return e;
        }
    },

    getByField: async (params) => {
        let product = await Product.findOne(params).exec();
        try {
            if (!product) {
                return null;
            }
            return product;
        } catch (e) {
            return e;
        }
    },

    getAllByField: async (params) => {
        let product = await Product.find(params).exec();
        try {
            if (!product) {
                return null;
            }
            return product;
        } catch (e) {
            return e;
        }
    },

    delete: async (id) => {
        try {
            let product = await Product.findById(id);
            if (product) {
                let productDelete = await Product.remove({
                    _id: id
                }).exec();
                if (!productDelete) {
                    return null;
                }
                return productDelete;
            }
        } catch (e) {
            return e;
        }
    },

    deleteByField: async (field, fieldValue) => {
        //todo: Implement delete by field
    },

    updateById: async (data, id) => {
        try {
            let product = await Product.findByIdAndUpdate(id, data, {
                new: true,
                upsert: true
            }).exec();
            if (!product) {
                return null;
            }
            return product;
        } catch (e) {
            return e;
        }
    },

    updateByField: async (field, fieldValue, data) => {
        //todo: update by field
    },

    save: async (data) => {
        try {
            let product = await Product.create(data);
            if (!product) {
                return null;
            }
            return product;
        } catch (e) {
            throw e;
        }
    },

};

module.exports = productRepository;