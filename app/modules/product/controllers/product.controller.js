const mongoose = require('mongoose');
const productRepo = require('product/repositories/product.repository');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const fs = require('fs');
var gm = require('gm').subClass({
    imageMagick: true
});
const errorHandler = require('../../../errorHandler');

class productController {

    /*
    // @Method: create
    // @Description:  Create Product
    */
    async create(req, res) {
        try {
            res.render('product/views/add.ejs', {
                page_name: 'product-management',
                page_title: 'Create New Product',
                user: req.user,
            });
        } catch (e) {
            throw (e);
        }
    };

    /*
    // @Method: insert
    // @Description:  Insert Product
    */
    async insert(req, res) {
        try {
            req.body.pro_image = '';
            if (typeof req.fileValidationError != 'undefined') {
                req.flash('error', req.fileValidationError);
                res.redirect(namedRouter.urlFor('product.create'));
            } else {
                if (_.has(req, 'files')) {
                    if (req.files.length > 0) {
                        if (req.files[0].fieldname == 'pro_image') {
                            gm('public/uploads/product/' + req.files[0].filename)
                                .resize(100)
                                .write('public/uploads/product/thumb/' + req.files[0].filename, function (err) {
                                    if (err) {
                                        req.flash('error', err.message);
                                        res.redirect(namedRouter.urlFor('product.create'));
                                    }
                                });
                            req.body.pro_image = req.files[0].filename;
                        }
                    }
                }
                let newProduct = await productRepo.save(req.body);
                if (!_.isEmpty(newProduct)) {
                    req.flash('success', 'Product created succesfully.');
                    res.redirect(namedRouter.urlFor('product.listing'));
                } else {
                    req.flash('error', "Sorry some error occur!");
                    res.redirect(namedRouter.urlFor('product.create'));
                }
            }
        } catch (e) {
            req.flash('error', e.message);
            res.redirect(namedRouter.urlFor('product.create'));
        }
    };

    /*
    // @Method: edit
    // @Description:  Product edit page
    */
    async edit(req, res) {
        try {
            let getProductDetail = await productRepo.getById(req.params.id);
            if (!_.isEmpty(getProductDetail)) {
                res.render('product/views/edit.ejs', {
                    page_name: 'product-management',
                    page_title: 'Update Product',
                    user: req.user,
                    response: getProductDetail
                });
            } else {
                req.flash('error', "Sorry Product not found!");
                res.redirect(namedRouter.urlFor('product.listing'));
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    /* @Method: update
    // @Description: Product update action
    */
    async update(req, res) {
        try {
            if (typeof req.fileValidationError != 'undefined') {
                req.flash('error', req.fileValidationError);
                res.redirect(namedRouter.urlFor('product.edit', {
                    id: req.body.pid
                }));
            } else {
                if (_.has(req, 'files')) {
                    if (req.files.length > 0) {
                        let product = await productRepo.getByField({
                            '_id': req.body.pid
                        });
                        if (!_.isEmpty(product.pro_image)) {
                            if (fs.existsSync('public/uploads/product/' + product.pro_image)) {
                                const upl_img = fs.unlinkSync('public/uploads/product/' + product.pro_image);
                            }
                            if (fs.existsSync('public/uploads/product/thumb/' + product.pro_image)) {
                                const upl_thumb_img = fs.unlinkSync('public/uploads/product/thumb/' + product.pro_image);
                            }
                        }
                        if (req.files[0].fieldname == 'pro_image') {
                            gm('public/uploads/product/' + req.files[0].filename)
                                .resize(100)
                                .write('public/uploads/product/thumb/' + req.files[0].filename, function (err) {
                                    if (err) {
                                        req.flash('error', err.message);
                                        res.redirect(namedRouter.urlFor('product.edit', {
                                            id: req.body.pid
                                        }));
                                    }
                                });
                            req.body.pro_image = req.files[0].filename;
                        }
                    }
                }
                let productUpdate = productRepo.updateById(req.body, req.body.pid);
                if (productUpdate) {
                    req.flash('success', 'Product updated succesfully.');
                    res.redirect(namedRouter.urlFor('product.listing'));
                } else {
                    res.redirect(namedRouter.urlFor('product.edit', {
                        id: req.body.pid
                    }));
                }
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    /* @Method: list
    // @Description: To get all the Products from DB
    */
    async list(req, res) {
        try {
            res.render('product/views/list.ejs', {
                page_name: 'product-management',
                page_title: 'Product List',
                user: req.user
            });
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    /* @Method: getAll
    // @Description: To get all the Products details from DB
    */
    async getAll(req, res) {
        try {
            let getProduct = await productRepo.getAll(req);
            if (_.has(req.body, 'sort')) {
                var sortOrder = req.body.sort.sort;
                var sortField = req.body.sort.field;
            } else {
                var sortOrder = -1;
                var sortField = '_id';
            }
            let meta = {
                "page": req.body.pagination.page,
                "pages": getProduct.pageCount,
                "perpage": req.body.pagination.perpage,
                "total": getProduct.totalCount,
                "sort": sortOrder,
                "field": sortField
            };
            return {
                status: 200,
                meta: meta,
                data: getProduct.data,
                message: `Product fetched succesfully.`
            };
        } catch (e) {
            return {
                status: 500,
                data: [],
                message: e.message
            };
        }
    }

    /* @Method: delete
    // @Description: Product Delete
    */
    async delete(req, res) {
        try {
            let productInfo = await productRepo.getByField({
                '_id': req.params.id
            });
            let productDelete = await productRepo.delete(req.params.id)
            if (!_.isEmpty(productDelete)) {
                if (!_.isEmpty(productInfo.pro_image)) {
                    if (fs.existsSync('public/uploads/product/' + productInfo.pro_image)) {
                        const upl_img = fs.unlinkSync('public/uploads/product/' + productInfo.pro_image);
                    }
                    if (fs.existsSync('public/uploads/product/thumb/' + productInfo.pro_image)) {
                        const upl_thumb_img = fs.unlinkSync('public/uploads/product/thumb/' + productInfo.pro_image);
                    }
                }
                req.flash('success', 'Product Removed Successfully');
                res.redirect(namedRouter.urlFor('product.listing'));
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

}

module.exports = new productController();