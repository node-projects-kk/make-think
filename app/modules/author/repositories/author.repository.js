const mongoose = require('mongoose');
const Author = require('author/models/author.model');
const perPage = config.PAGINATION_PERPAGE;

const authorRepository = {
    getAll: async ( req ) => {
        // console.log(req.body);
        try {
            var conditions = {};
            var and_clauses = [];

            and_clauses.push({"isDeleted": false});
            
            if(_.isObject(req.body.query) && _.has(req.body.query, 'generalSearch')){
                //and_clauses.push({"status": /req.body.query.generalSearch/i});
                and_clauses.push({ 'title': { $regex:  req.body.query.generalSearch, $options: 'i'} });
            }
            if(_.isObject(req.body.query) && _.has(req.body.query, 'Status')){
                and_clauses.push({"status": req.body.query.Status});
            }
            conditions['$and'] = and_clauses;
            
            var sortOperator = { "$sort": { } };
            if(_.has(req.body, 'sort')){
                var sortField = req.body.sort.field;
                if(req.body.sort.sort == 'desc'){
                    var sortOrder = -1;
                }else if(req.body.sort.sort == 'asc'){
                    var sortOrder = 1;
                }
                
                sortOperator["$sort"][sortField] = sortOrder;
            }else{
                sortOperator["$sort"]['_id'] = -1;
            }

            var aggregate = Author.aggregate([
                {$match: conditions},
                sortOperator
            ]);

            var options = { page : req.body.pagination.page, limit : req.body.pagination.perpage};
            let allAuthor = await Author.aggregatePaginate(aggregate, options);
            return allAuthor;
        }catch(e){
            throw(e);
        }
    },

    getAuthorCount: async (req) => {
        try {
                      
            let authors = await Author.find({isDeleted:false});
            return authors;
        } catch(e) {
            throw(e);
        }
     },

    getById: async (id) => {
        try{
            let author = await Author.findById(id).exec(); 
            return author;
        }catch(e){
            throw(e);
        }
    },

    getByField: async (params) => {        
        try{
            let author =  await Author.findOne(params).exec(); 
            return author;    
        }catch(e){
            throw(e);
        }
    },

    getAllByField: async (params) => {         
        try{
            let author =  await Author.find(params).exec(); 
            return author;    
        }catch(e){
            throw(e);
        }
    },
    
    delete: async (id) => {
        try { 
            let author = await Author.findById(id);
            if(author) {
                let authorDelete = await Author.remove({_id:id}).exec();                
                return authorDelete;
            }else{
                return null;
            }
        } catch(e){
            throw(e);
        }
    },

    deleteByField: async (field, fieldValue) => {
        //todo: Implement delete by field
    },


    updateById: async (data, id) =>{
        try {
            let author = await Author.findByIdAndUpdate(id, data, { new: true, upsert: true }).exec();
            return author;
        } catch(e) {
            throw(e);
        }
    },

    updateByField: async (field, fieldValue, data) => {
        //todo: update by field
    },

    save: async ( data ) => {
        try {
            let author = await Author.create(data);
            if (!author) {
                return null;
            }
            return author;
        } catch(e) {
            throw e;
        }
    },
};

module.exports = authorRepository;