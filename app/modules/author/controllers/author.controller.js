const authorRepo = require('author/repositories/author.repository');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const fs = require('fs');
const errorHandler = require('../../../errorHandler');


class authorController {

    async create (req, res){
        try {
            res.render('author/views/add.ejs', {
                page_name: 'author-management',
                page_title: 'Create New Author',
                user: req.user
            });
        } catch(e) {
            throw(e);
        }
    };

    async insert (req, res){
        try {
            let newauthor = await authorRepo.save(req.body);
            req.flash('success', 'Author created succesfully.');
            res.redirect(namedRouter.urlFor('author.listing'));
        } catch(e) {
            const error = errorHandler(e);
            req.flash('error', error.message);
            //res.status(500).send({message: error.message});
            res.redirect(namedRouter.urlFor('author.create'));
        }
    };

    /*
    // @Method: edit
    // @Description:  Coupon update page
    */
    async edit (req, res){
        try{
            let result = {};
            let author = await authorRepo.getById(req.params.id);
            if (!_.isEmpty(author)) {
                result.author_data = author;
                res.render('author/views/edit.ejs', {
                    page_name: 'author-management',
                    page_title: 'Update Author',
                    user: req.user,
                    response: result
                });
            } else {
                req.flash('error', "Sorry author not found!");
                res.redirect(namedRouter.urlFor('author.listing')); 
            }
        } catch(e){
            return res.status(500).send({message: e.message}); 
        }
    };

    /* @Method: update
    // @Description: coupon update action
    */
    async update (req, res){
        try {
            const authorId = req.body.gid;
            let authorUpdate = await authorRepo.updateById(req.body, authorId);
            if(authorUpdate) {
                req.flash('success', "Author Updated Successfully");
                res.redirect(namedRouter.urlFor('author.listing'));
            }else{
                res.redirect(namedRouter.urlFor('author.edit', { id: authorId }));
            }            
        }catch(e){
            const error = errorHandler(e);
            req.flash('error', error.message);
            res.redirect(namedRouter.urlFor('author.edit', { id: req.body.gid }));
        }      
            
    };



    /* @Method: list
    // @Description: To get all the authors from DB
    */
    async list (req, res){
        try{
            res.render('author/views/list.ejs', {
                page_name: 'author-management',
                page_title: 'Author List',
                user: req.user
            });
        } catch(e){
            return res.status(500).send({message: e.message}); 
        }  
    };

    async getAll (req, res){
        try{
            let author = await authorRepo.getAll(req);
            if(_.has(req.body, 'sort')){
                var sortOrder = req.body.sort.sort;
                var sortField = req.body.sort.field;
            }else{
                var sortOrder = -1;
                var sortField = '_id';
            }
            let meta = {"page": req.body.pagination.page, "pages": author.pageCount, "perpage": req.body.pagination.perpage, "total": author.totalCount, "sort": sortOrder, "field": sortField};
            return {status: 200, meta: meta, data:author.data, message: `Data fetched succesfully.`};
        } catch(e){
            return {status: 500,data: [],message: e.message};
        }
    }
    

    /* @Method: delete
    // @Description: author delete
    */
    async delete (req, res){
        try{
            let authorDelete = await authorRepo.updateById({"isDeleted": true}, req.params.id)
            if(!_.isEmpty(authorDelete)){
                req.flash('success','Author Removed Successfully');
                res.redirect(namedRouter.urlFor('author.listing'));
            } 
        }catch(e){
            return res.status(500).send({message: e.message});   
        } 
    };

}

module.exports = new authorController();