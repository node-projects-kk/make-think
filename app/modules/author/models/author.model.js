const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');
const beautifyUnique = require('mongoose-beautiful-unique-validation');

const AuthorSchema = new Schema({
  title: { type: String, default: '', unique: 'Author "{VALUE}" is already exist!' },
  slug: { type: String, default: '' },
  status: { type: String, default: "Active", enum: ["Active", "Inactive"] },
  isDeleted: {type: Boolean, default: false, enum: [true, false]}
});

AuthorSchema.plugin(beautifyUnique);
// For pagination
AuthorSchema.plugin(mongooseAggregatePaginate);

// create the model for users and expose it to our app
module.exports = mongoose.model('Author', AuthorSchema);