const countryRepo = require('country/repositories/country.repository');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const fs = require('fs');



class countryController {
    constructor() {
        this.language = [];
        
    }

    /*
    // @Method: edit
    // @Description:  Coupon update page
    */
    async edit (req, res){
        try
        {
            let country = await countryRepo.getById(req.params.id);
            if (!_.isEmpty(country)) {

                res.render('country/views/edit.ejs', {
                    page_name: 'country-management',
                    page_title: 'Edit country',
                    user: req.user,
                    response: country
                });
            } else {
                req.flash('error', "Sorry country not found!");
                res.redirect(namedRouter.urlFor('country.list')); 
            }
        } catch(e){
            return res.status(500).send({message: e.message}); 
        }
    };

    /* @Method: update
    // @Description: coupon update action
    */
    async update (req, res){
        try {
            let checkCountry= await countryRepo.getByField({"country_name": req.body.country_name})
        if(checkCountry)
        {
            req.flash('error', 'country with same name already exists.');
            res.redirect(namedRouter.urlFor('country.list'));
        }
        else
        {
            let country = await countryRepo.getById(req.body.id);
            if (!_.isEmpty(country)) {
                    let countryUpdate = await countryRepo.updateById(req.body, req.body.id)
                    if(countryUpdate) {
                        req.flash('success', "country Updated Successfully");
                        res.redirect(namedRouter.urlFor('country.list'));
                    }
                    
                }else{
                req.flash('error', "country not found!");
                res.redirect(namedRouter.urlFor('country.edit', { id: req.body.id }));
            }
        }    
        }catch(e){
            return res.status(500).send({message: e.message});  
        }      
            
    };



    /* @Method: list
    // @Description: To get all the users from DB
    */
    async list (req, res){
            try
            {
                res.render('country/views/list.ejs', {
                    page_name: 'country-list',
                    page_title: 'country List',
                    user: req.user,
                    
                });
        } catch(e){
            return res.status(500).send({message: e.message}); 
        }  
    };

    async create (req, res){
        try
        {
            res.render('country/views/add.ejs', {
                page_name: 'country-Create',
                page_title: 'country Create',
                user: req.user,
            });
    } catch(e){
        return res.status(500).send({message: e.message}); 
    }  
};

async store (req, res){
    try {
        let checkCountry= await countryRepo.getByField({"country_name": req.body.country_name})
        if(checkCountry)
        {
            req.flash('error', 'country with same name already exists.');
            res.redirect(namedRouter.urlFor('country.list'));
        }
        else
        {
        let newcountry = await countryRepo.save(req.body);
        req.flash('success', 'country created succesfully.');
        res.redirect(namedRouter.urlFor('country.list'));
        }
    } catch(e) {
        const error = errorHandler(e);
        req.flash('error', error.message);
        //res.status(500).send({message: error.message});
        res.redirect(namedRouter.urlFor('country.create'));
    }
};

    async getAll (req, res){
        try{
            let country = await countryRepo.getAll(req);
            if(_.has(req.body, 'sort')){
                var sortOrder = req.body.sort.sort;
                var sortField = req.body.sort.field;
            }else{
                var sortOrder = -1;
                var sortField = '_id';
            }
         let meta = {"page": req.body.pagination.page, "pages": country.pageCount, "perpage": req.body.pagination.perpage, "total": country.totalCount, "sort": sortOrder, "field": sortField};
            
            return {status: 200, meta: meta, data:country.data, message: `Data fetched succesfully.`};
        } catch(e){
            return {status: 500,data: [],message: e.message};
        }
    }
    /*
    // @Method: status_change
    // @Description: language status change action
    */
    async changeStatus (req, res){
        try {
            let country = await countryRepo.getById(req.params.id);
            if(!_.isEmpty(country)){
                let countryStatus = (country.isActive == true) ? false : true;
                let countryUpdate= await countryRepo.updateById({isActive: countryStatus }, req.params.id);
                req.flash('success', "country status has changed successfully" );
                res.redirect(namedRouter.urlFor('country.list'));
            } else {
                req.flash('error', "sorry country not found");
                res.redirect(namedRouter.urlFor('country.list')); 
            }
        } catch(e){
            return res.status(500).send({message: e.message}); 
        }
    };

    /* @Method: delete
    // @Description: coupon delete
    */
    async destroy (req, res){
        try{
            let languageDelete = await countryRepo.delete(req.params.id)
            if(!_.isEmpty(languageDelete)){
                req.flash('success','country Removed Successfully');
                res.redirect(namedRouter.urlFor('country.list'));
            } 
        }catch(e){
            return res.status(500).send({message: e.message});   
        } 
    };


}

module.exports = new countryController();