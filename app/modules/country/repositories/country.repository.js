const Country = require('country/models/country.model');
const perPage = config.PAGINATION_PERPAGE;

class CountryRepository {
    constructor() { }

     async getAll(req) {
        try {
            var conditions = {};
            var and_clauses = [];

            and_clauses.push({ "isDeleted": false });

            if (_.isObject(req.body.query) && _.has(req.body.query, 'generalSearch')) {
                //and_clauses.push({"status": /req.body.query.generalSearch/i});
                // and_clauses.push({ 'title': { $regex: req.body.query.generalSearch, $options: 'i' } });
                // and_clauses.push({ 'slug': { $regex: req.body.query.generalSearch, $options: 'i' } });
                // and_clauses.push({ 'desc': { $regex: req.body.query.generalSearch, $options: 'i' } });
                and_clauses.push({
                    $or: [
                        { 'country_name': { $regex: req.body.query.generalSearch, $options: 'i' } }
                    ]
                });
            }
            if (_.isObject(req.body.query) && _.has(req.body.query, 'Status')) {
                const status = req.body.query.Status == 'true' ? "Active" : "Inactive";
                and_clauses.push({ "status": status });
            }
            conditions['$and'] = and_clauses;

            var sortOperator = { "$sort": {} };
            if (_.has(req.body, 'sort')) {
                var sortField = req.body.sort.field;
                if (req.body.sort.sort == 'desc') {
                    var sortOrder = -1;
                } else if (req.body.sort.sort == 'asc') {
                    var sortOrder = 1;
                }

                sortOperator["$sort"][sortField] = sortOrder;
            } else {
                sortOperator["$sort"]['_id'] = -1;
            }

            var aggregate =  Country.aggregate([
                { $match: conditions },
                sortOperator
            ]);
            var options = { page: req.body.pagination.page, limit: req.body.pagination.perpage };
            let allFaq = await Country.aggregatePaginate(aggregate, options);
            return allFaq;
        } catch (e) {
            throw (e);
        }
    }

    async getById(id) {
        try {
            return await Country.findById(id).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async getByField(params) {
        try {
            return await Country.findOne(params).exec();
        } catch (error) {
            return error;
        }
    }

    async getAllByField(params) {
        try {
            return await Country.find(params).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async delete(id) {
        try {
            await Country.findById(id).lean().exec();
            return await Country.deleteOne({ _id: id }).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async updateById(data, id) {
        try {
            return await Country.findByIdAndUpdate(id, data, { new: true, upsert: true })
                .lean().exec();
        } catch (error) {
            return error;
        }
    }

    async save(data) {
        try {
            return await Country.create(data).lean().exec();
        } catch (error) {
            return error;
        }
    }
}

module.exports = new CountryRepository();