const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');
const beautifyUnique = require('mongoose-beautiful-unique-validation');
// define the schema for our user model
const countrySchema = mongoose.Schema({
    country_name: String,
    isActive: { type: Boolean, default: true },
    isDeleted: { type: Boolean, default: false },
    createdAt: { type: Date, default: Date.now() }
});
countrySchema.plugin(beautifyUnique);
// For pagination
countrySchema.plugin(mongooseAggregatePaginate);
countrySchema.plugin(mongoosePaginate);
module.exports = mongoose.model('Country', countrySchema);