const bookRepo = require('book/repositories/book.repository');
const authorRepo = require('author/repositories/author.repository');
const genreRepo = require('genre/repositories/genre.repository');
const chapterRepo = require('chapter/repositories/chapter.repository');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const fs = require('fs');
var gm = require('gm').subClass({
    imageMagick: true
});
const errorHandler = require('../../../errorHandler');

class chapterController {

    async create(req, res) {
        try {
            const bookDetails = await bookRepo.getById(req.params.id);
            res.render('chapter/views/add.ejs', {
                page_name: 'chapter-management',
                page_title: 'Create New chapter',
                user: req.user,
                book_id: req.params.id,
                books: bookDetails
            });
        } catch (e) {
            throw (e);
        }
    };


    async insert(req, res) {
        try {
            req.body.chapter_icon = '';
            const book_id = req.body.book_id;
            let newbook='';
            if (_.has(req, 'files')) {
                if (req.files.length > 0) {
                    if (req.files[0].fieldname == 'chapter_icon') {
                        gm('public/uploads/chapter/' + req.files[0].filename).resize(100).write('public/uploads/chapter/thumb/' + req.files[0].filename, async function (err) {
                                if (err) {
                                
                                    req.flash('error', err.message);
                                    res.redirect(namedRouter.urlFor('chapter.create', {
                                        id: book_id
                                    }));
                                } else {
                                    
                                    req.body.chapter_icon = req.files[0].filename;
                                   newbook = await chapterRepo.save(req.body);
                                   if (!_.isEmpty(newbook)) {
                                    req.flash('success', 'Chapter created succesfully.');
                                    res.redirect(namedRouter.urlFor('book.getallChapter', {
                                        id: req.body.book_id
                                    }));
                                } else {
                                    req.flash('error', "Sorry some error occur!");
                                    res.redirect(namedRouter.urlFor('chapter.create', {
                                        id: req.body.book_id
                                    }));
                                }

                                }
                            });
                    }
                
                } 
            }
            else
            {
                 newbook = await chapterRepo.save(req.body);
                 if (!_.isEmpty(newbook)) {
                    req.flash('success', 'Chapter created succesfully.');
                    res.redirect(namedRouter.urlFor('book.getallChapter', {
                        id: req.body.book_id
                    }));
                } else {
                    req.flash('error', "Sorry some error occur!");
                    res.redirect(namedRouter.urlFor('chapter.create', {
                        id: req.body.book_id
                    }));
                }
            }

        } catch (e) {
            const error = errorHandler(e);
            req.flash('error', error.message);
            //res.status(500).send({message: error.message});
            res.redirect(namedRouter.urlFor('chapter.create', {
                id: req.body.book_id
            }));
        }
    };

    /*
    // @Method: edit
    // @Description:  Coupon update page
    */
    async edit(req, res) {
        try {
            let result = {};
            const book_id = req.body.book_id;
            let chapter = await chapterRepo.getById(req.params.id);
            let book = await bookRepo.getById(chapter.book_id);
            const allBook = await bookRepo.getAllByField({
                "status": "Active",
                "isDeleted": "false"
            });
            if (!_.isEmpty(chapter)) {
                result.chapter_data = chapter;
                result.book = book;

                res.render('chapter/views/edit.ejs', {
                    page_name: 'chapter-management',
                    page_title: 'Update chapter',
                    user: req.user,
                    response: result,
                    books: allBook
                });
            } else {
                req.flash('error', "Sorry chapter not found!");
                res.redirect(namedRouter.urlFor('book.getallChapter', {
                    id: book_id
                }));
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    /* @Method: update
    // @Description: coupon update action
    */
    async update(req, res) {
        try {
            const book_id = req.body.book_id;
            const Id = req.body.id;
            let chapter = await chapterRepo.getByField({'_id':Id});

            if (req.files.length > 0) {
                if (!_.isEmpty(chapter.chapter_icon)) {
                    if (fs.existsSync('public/uploads/chapter/' + chapter.chapter_icon)) {
                        const upl_img = fs.unlinkSync('public/uploads/chapter/' + chapter.chapter_icon);
                    }
                    if (fs.existsSync('public/uploads/chapter/thumb/' + chapter.chapter_icon)) {
                        const upl_thumb_img = fs.unlinkSync('public/uploads/chapter/thumb/' + chapter.chapter_icon);
                    }
                }
            }

            if (_.has(req, 'files')) {
                if (req.files.length > 0) {
                    if (req.files[0].fieldname == 'chapter_icon') {
                        gm('public/uploads/chapter/' + req.files[0].filename)
                            .resize(100)
                            .write('public/uploads/chapter/thumb/' + req.files[0].filename, function (err) {
                                if (err) {
                                    req.flash('error', err.message);
                                    res.redirect(namedRouter.urlFor('chapter.edit', {
                                        id: Id
                                    }));
                                } else {
                                    req.body.chapter_icon = req.files[0].filename;
                                    chapterRepo.updateById(req.body, Id);
                                    req.flash('success', 'Chapter updated succesfully.');
                                    res.redirect(namedRouter.urlFor('book.getallChapter', {
                                        id: book_id
                                    }));
                                }
                            });
                    }
                } else {
                    let coupon = await chapterRepo.getByField({
                        'title': req.body.title,
                        _id: {
                            $ne: Id
                        },
                        chapter_no: {
                            $ne: req.body.chapter_no
                        }
                    });
                    if (_.isEmpty(coupon)) {
                        let cmsIdUpdate = await chapterRepo.updateById(req.body, Id);
                        if (cmsIdUpdate) {
                            req.flash('success', "Chapter Updated Successfully");
                            res.redirect(namedRouter.urlFor('book.getallChapter', {
                                id: book_id
                            }));
                        }

                    } else {
                        req.flash('error', "Chapter is already availabe!");
                        res.redirect(namedRouter.urlFor('chapter.edit', {
                            id: Id
                        }));
                    }
                }
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    /* @Method: list
    // @Description: To get all the books from DB
    */
    async list(req, res) {
        try {
            res.render('chapter/views/list.ejs', {
                page_name: 'chapter-management',
                page_title: 'Chapter List',
                user: req.user,
                book_id: req.params.id,
            });
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    async getAll(req, res) {
        try {
            let chapter = await chapterRepo.getAll(req);
           

            if (_.has(req.body, 'sort')) {
                var sortOrder = req.body.sort.sort;
                var sortField = req.body.sort.field;
            } else {
                var sortOrder = -1;
                var sortField = '_id';
            }
            let meta = {
                "page": req.body.pagination.page,
                "pages": chapter.pageCount,
                "perpage": req.body.pagination.perpage,
                "total": chapter.totalCount,
                "sort": sortOrder,
                "field": sortField
            };
            return {
                status: 200,
                meta: meta,
                data: chapter.data,
                message: `Data fetched succesfully.`
            };
        } catch (error) {
            // return { status: 500, data: [], message: e.message };
            throw error
        }
    }

    async getAllByBookID(req, res) {
        try {
         
            let chapter = await chapterRepo.getAll(req);
   

            if (_.has(req.body, 'sort')) {
                var sortOrder = req.body.sort.sort;
                var sortField = req.body.sort.field;
            } else {
                var sortOrder = -1;
                var sortField = '_id';
            }
            let meta = {
                "page": req.body.pagination.page,
                "pages": chapter.pageCount,
                "perpage": req.body.pagination.perpage,
                "total": chapter.totalCount,
                "sort": sortOrder,
                "field": sortField
            };
            return {
                status: 200,
                meta: meta,
                data: chapter.data,
                message: `Data fetched succesfully.`
            };
        } catch (error) {
            // return { status: 500, data: [], message: e.message };
            throw error
        }
    }

    /*
    // @Method: status_change
    // @Description: coupon status change action
    */
    async statusChange(req, res) {
        try {
            let coupon = await couponRepo.getById(req.body.id);
            if (!_.isEmpty(coupon)) {
                let couponStatus = (coupon.isActive == true) ? false : true;
                let couponUpdate = couponRepo.updateById({
                    'isActive': couponStatus
                }, req.body.id);
                req.flash('success', "Coupon status has changed successfully");
                res.send(couponUpdate);
            } else {
                req.flash('error', "sorry coupon not found");
                res.redirect(namedRouter.urlFor('coupon.list'));
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    /* @Method: delete
    // @Description: coupon delete
    */
    async destroy(req, res) {
        try {
            const document_id = req.params.id;
            let getChapterDataSet = await chapterRepo.getById(document_id);
            let chapterDelete = await chapterRepo.delete(document_id);
            if (!_.isEmpty(chapterDelete)) {
                if (!_.isEmpty(getChapterDataSet.chapter_icon)) {
                    if (fs.existsSync('public/uploads/chapter/' + getChapterDataSet.chapter_icon)) {
                        const upl_img = fs.unlinkSync('public/uploads/chapter/' + getChapterDataSet.chapter_icon);
                    }
                    if (fs.existsSync('public/uploads/chapter/thumb/' + getChapterDataSet.chapter_icon)) {
                        const upl_thumb_img = fs.unlinkSync('public/uploads/chapter/thumb/' + getChapterDataSet.chapter_icon);
                    }
                }
                req.flash('success', 'Chapter Removed Successfully');
                res.redirect(namedRouter.urlFor('book.getallChapter', {
                    id: getChapterDataSet.book_id
                }));
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };


    /* @Method: list
    // @Description: To get all the pages of chapter from DB
    */
    async getAllPageList(req, res) {
        try {
            res.render('page/views/list.ejs', {
                page_name: 'chapter-page-management',
                page_title: 'Page List',
                user: req.user,
                chapter_id: req.params.id,
            });
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    async getAllPages(req, res) {
        try {
            const pageList = await chapterRepo.getAllPagesByField({
                _id: req.params.id,
                'isDeleted': false
            });
            return {
                status: 200,
                data: pageList,
                message: `Data fetched succesfully.`
            };
        } catch (e) {
            throw e;
        }
    }

    /*
// @Method: verifyChapterNumber
// @Description: User status change action
*/
async verifyChapterNumber(req, res) {
    try {
        const result = await chapterRepo.getAllByField({"book_id": req.body.book_id, "chapter_no": req.body.chapter_no});

        if(_.isEmpty(result))
        {
            return {
                "status": 200,
            };
        }
        else {
            return {
                "status": 200,
                "message": "Chapter No already exists"
            };
        }




    } catch (error) {
        throw error;
    }
};


 /*
// @Method: verifyChapterTitle
// @Description: User status change action
*/
async verifyChapterTitle(req, res) {
    try {
        const result = await chapterRepo.getAllByField({"book_id": req.body.book_id, "title": req.body.title});

        if(_.isEmpty(result))
        {
            return {
                "status": 200,
            };
        }
        else {
            return {
                "status": 200,
                "message": "Chapter Title already exists"
            };
        }




    } catch (error) {
        throw error;
    }
};



}

module.exports = new chapterController();