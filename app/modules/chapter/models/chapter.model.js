var mongoose = require('mongoose');
require('mongoose-double')(mongoose);
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt-nodejs');
var mongooseAggregatePaginate = require('mongoose-aggregate-paginate');

const deleted = [true, false];

var ChapterSchema = new Schema({
  title: {
    type: String,
    default: ''
  },
  description: {
    type: String,
    default: ''
  },
  chapter_no: {
    type: Number,
    default: 0
  },
  chapter_icon: {
    type: String,
    default: ''
  },
  chapter_pages: {
    type: Number,
    default: 0
  },
  book_id: {
    type: Schema.Types.ObjectId,
    ref: 'Book'
  },
  pages: [{
    page_title: {
      type: String,
      default: ''
    },
    page_content: {
      type: String,
      default: ''
    },
    page_number: {
      type: Number,
      default: 0
    },
    _id: {
      type: Schema.ObjectId,
      auto: true
    },
    status: {
      type: String,
      default: "Active",
      enum: ["Active", "Inactive"]
    }
  }],
  isDeleted: {
    type: Boolean,
    default: false,
    enum: deleted
  },
  status: {
    type: String,
    default: "Active",
    enum: ["Active", "Inactive"]
  },
}, {
  timestamp: true
});


// For pagination
ChapterSchema.plugin(mongooseAggregatePaginate);

// create the model for users and expose it to our app
module.exports = mongoose.model('Chapter', ChapterSchema);