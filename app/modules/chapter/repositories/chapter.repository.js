const mongoose = require('mongoose');
const Chapter = require('chapter/models/chapter.model');
const Book = require('book/models/book.model');
const perPage = config.PAGINATION_PERPAGE;

const ChapterRepository = {
    getAll: async (req) => {

        try {
            var conditions = {};
            var and_clauses = [];

            and_clauses.push({
                "isDeleted": false,
                "book_id": mongoose.Types.ObjectId(req.params.id),
            });

            if (_.isObject(req.body.query) && _.has(req.body.query, 'generalSearch')) {
                //and_clauses.push({"status": /req.body.query.generalSearch/i});
                // and_clauses.push({ 'title': { $regex: req.body.query.generalSearch, $options: 'i' } });
                // and_clauses.push({ 'slug': { $regex: req.body.query.generalSearch, $options: 'i' } });
                // and_clauses.push({ 'desc': { $regex: req.body.query.generalSearch, $options: 'i' } });
                and_clauses.push({
                    $or: [{
                            'title': {
                                $regex: req.body.query.generalSearch,
                                $options: 'i'
                            }
                        },
                        {
                            'slug': {
                                $regex: req.body.query.generalSearch,
                                $options: 'i'
                            }
                        },
                        {
                            'description': {
                                $regex: req.body.query.generalSearch,
                                $options: 'i'
                            }
                        },
                        {
                            'chapter_no': parseInt(req.body.query.generalSearch),

                        }
                    ]
                });
            }
            if (_.isObject(req.body.query) && _.has(req.body.query, 'Status')) {
                and_clauses.push({
                    "status": req.body.query.Status
                });
            }
            conditions['$and'] = and_clauses;

            var sortOperator = {
                "$sort": {}
            };
            if (_.has(req.body, 'sort')) {
                var sortField = req.body.sort.field;
                if (req.body.sort.sort == 'desc') {
                    var sortOrder = -1;
                } else if (req.body.sort.sort == 'asc') {
                    var sortOrder = 1;
                }

                sortOperator["$sort"][sortField] = sortOrder;
            } else {
                sortOperator["$sort"]['_id'] = -1;
            }

            var aggregate = Chapter.aggregate([{
                    "$lookup": {
                        "from": "books",
                        "localField": "book_id",
                        "foreignField": "_id",
                        "as": "book"
                    },
                },
                {
                    $unwind: "$book"
                },
                {
                    $project: {
                        _id: "$_id",
                        title: "$title",
                        description: "$description",
                        chapter_no: "$chapter_no",
                        isDeleted: "$isDeleted",
                        status: "$status",
                        book_id: "$book_id",
                        book_name: "$book.title",
                        chapters: "$book.chapter_num"
                    }
                },


                {
                    $match: conditions
                },
                sortOperator
            ]);

            var options = {
                page: req.body.pagination.page,
                limit: req.body.pagination.perpage
            };
            let allCms = await Chapter.aggregatePaginate(aggregate, options);

            return allCms;
        } catch (e) {
            throw (e);
        }
    },

    getById: async (id) => {
        let cms = await Chapter.findById(id).exec();
        try {
            if (!cms) {
                return null;
            }
            return cms;

        } catch (e) {
            return e;
        }
    },

    getByField: async (params) => {
        try {

            let chapter = await Chapter.findOne(params).exec();
            if (!chapter) {
                return null;
            }
            return chapter;

        } catch (e) {
            throw e;
        }
    },

    checkTitle: async (id, title, chapter_num) => {
        let chapter = await Chapter.findOne({
            _id: id,
            title: title,
            chapter_no: chapter_num
        }).exec();
        try {
            if (!chapter) {
                return null;
            }
            return chapter;

        } catch (e) {
            throw e;
        }
    },

    getAllByField: async (params) => {
        let chapter = await Chapter.find(params).exec();
        try {
            if (!chapter) {
                return null;
            }
            return chapter;

        } catch (e) {
            throw e;
        }
    },

    delete: async (id) => {
        try {
            let chapter = await Chapter.findById(id);
            if (chapter) {
                let chapterDelete = await Chapter.remove({
                    _id: id
                }).exec();
                if (!chapterDelete) {
                    return null;
                }
                return chapterDelete;
            }
        } catch (e) {
            throw e;
        }
    },

    deleteByField: async (field, fieldValue) => {
        //todo: Implement delete by field
    },


    updateById: async (data, id) => {
        try {
            let chapter = await Chapter.findByIdAndUpdate(id, data, {
                new: true,
                upsert: true
            }).exec();
            if (!chapter) {
                return null;
            }
            return chapter;
        } catch (e) {
            return e;
        }
    },

    updateByField: async (field, fieldValue, data) => {
        //todo: update by field
    },

    save: async (data) => {
        try {
            let chapter = await Chapter.create(data);
            if (!chapter) {
                return null;
            }
            return chapter;
        } catch (e) {
            throw e;
        }
    },


    getAllPagesByField: async (params) => {
        let pages = await Chapter.find(params).exec();
        try {
            if (!pages) {
                return null;
            }
            return pages;
        } catch (e) {
            return e;
        }
    },

    getBooksChapterAndPageDetails: async (id) => {
        try {
            var conditions = {};
            var and_clauses = [];

            and_clauses.push({
                "isDeleted": false,
                "status": "Active",
                "_id": mongoose.Types.ObjectId(id),
                "chapter_info.status": "Active",
                "chapter_info.isDeleted": false,
                "chapter_info.pages.status": "Active"
            });

            conditions['$and'] = and_clauses;
            let users = await Book.aggregate([{
                    "$lookup": {
                        "from": "chapters",
                        "localField": "_id",
                        "foreignField": "book_id",
                        "as": "chapter_info"
                    },
                },
                {
                    $unwind: {
                        path: "$chapter_info",
                        preserveNullAndEmptyArrays: true
                    }
                },
                {
                    $match: conditions
                },
                {
                    "$lookup": {
                        "from": "genres",
                        "localField": "genre",
                        "foreignField": "_id",
                        "as": "genre_info"
                    },
                },
                {
                    $unwind: {
                        path: "$genre_info",
                        preserveNullAndEmptyArrays: true
                    }
                },
                {
                    "$lookup": {
                        "from": "authors",
                        "localField": "author",
                        "foreignField": "_id",
                        "as": "author_info"
                    },
                },
                {
                    $unwind: {
                        path: "$author_info",
                        preserveNullAndEmptyArrays: true
                    }
                },
                {
                    $group: {
                        _id: "$_id",
                        title: {
                            $first: "$title"
                        },
                        subtitle: {
                            $first: "$subtitle"
                        },
                        grade: {
                            $first: "$grade"
                        },
                        genre: {
                            $first: "$genre_info"
                        },
                        author: {
                            $first: "$author_info"
                        },
                        picture: {
                            $first: "$picture"
                        },
                        total_chapter: {
                            $first: "$chapter_num"
                        },
                        status: {
                            $first: "$status"
                        },
                        isDeleted: {
                            $first: "$isDeleted"
                        },
                        chapter_info: {
                            $addToSet: "$chapter_info"
                        }
                    }
                },
                {
                    "$sort": {
                        _id: 1
                    }
                },
            ]).exec();
            return users;
        } catch (e) {
            throw (e);
        }
    },

};

module.exports = ChapterRepository;