const school = require('school/models/school.model');
const perPage = config.PAGINATION_PERPAGE;

class SchoolRepository {
    constructor() {}

    async getAll(searchQuery, page, cb) {
        try {
            const match = [{
                "status": "Active"
            }];
            if (_.has(searchQuery, "keyword")) {
                if (searchQuery.keyword != '') {
                    const search_string = searchQuery.keyword.trim();
                    match.push({
                        "$or": [{
                                'schoolDisplayName': {
                                    '$regex': search_string,
                                    '$options': 'i'
                                }
                            },
                            {
                                'desc': {
                                    '$regex': search_string,
                                    '$options': 'i'
                                }
                            }
                        ]
                    });
                }
            }

        } catch (error) {
            return error;
        }
    }

    async getById(id) {
        try {
            return await school.findById(id).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async getByField(params) {
        try {
            return await school.findOne(params).exec();
        } catch (error) {
            return error;
        }
    }

    async getAllByField(params) {
        try {
            return await school.find(params).lean().exec();
        } catch (error) {
            throw error;
        }
    }
    /*  
        async delete(id) {
            try {
                await school.findById(id).lean().exec();
                return await school.deleteOne({ _id: id }).lean().exec();
            } catch (error) {
                return error;
            }
        }
       */
    async updateById(data, id) {
        try {
            return await school.findByIdAndUpdate(id, data, {
                    new: true,
                    upsert: true
                })
                .lean().exec();
        } catch (error) {
            throw error;
        }
    }

    async save(data) {
        try {
            return await school.create(data);
        } catch (error) {
            return error;
        }
    }
    async schoolAutocomplete(key) {
        try {
            var conditions = {};
            const match = [{
                "isActive": true
            }];
            match.push({
                "$or": [{
                    'school_name': {
                        '$regex': key,
                        '$options': 'i'
                    }
                }]
            });
            conditions['$and'] = match;
            return await school.aggregate([{
                $match: conditions
            }]);
        } catch (error) {
            throw error;
        }
    }
}

module.exports = new SchoolRepository();