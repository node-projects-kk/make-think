var mongoose = require('mongoose');
require('mongoose-double')(mongoose);
var Schema = mongoose.Schema;
var mongooseAggregatePaginate = require('mongoose-aggregate-paginate');

const deleted = [true, false];

var SchoolSchema = new Schema({
  school_name: { type: String, default: '' },
  isDeleted: { type: Boolean, default: false, enum: deleted },
  isActive: { type: Boolean, default:true, enum:[true,false] },
}, { timestamp: true });



// For pagination
SchoolSchema.plugin(mongooseAggregatePaginate);

module.exports = mongoose.model('School', SchoolSchema);