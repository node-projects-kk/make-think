const bookRepo = require('book/repositories/book.repository');
const chapterRepo = require('chapter/repositories/chapter.repository');
const pageRepo = require('page/repositories/page.repository');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
var mongoose = require('mongoose');
const querystring = require('querystring');
const fs = require('fs');
var gm = require('gm').subClass({
    imageMagick: true
});
const errorHandler = require('../../../errorHandler');

class pageController {

    async create(req, res) {
        try {
            const pageDetails = await pageRepo.getByChapterId(req.params.id);
            res.render('page/views/add.ejs', {
                page_name: 'page-management',
                page_title: 'Create New Page',
                user: req.user,
                chapter_id: req.params.id,
                pagedetail: pageDetails
            });
        } catch (e) {
            throw (e);
        }
    };


    async insert(req, res) {
        try {
            let chapter_id = req.body.chp_id;
            let page_title = req.body.page_title;
            let page_content = req.body.page_content;
            let page_number = req.body.page_number;

            let checkTitleExistence = await pageRepo.checkTitle(chapter_id, page_number);

            let newchapter;

            if (_.isEmpty(checkTitleExistence)) {
                const oldPageData = await pageRepo.getById(chapter_id);
                let newPageData = {
                    'page_title': page_title,
                    'page_content': page_content,
                    'page_number': page_number
                };
                let pagesArray = [];
                pagesArray = oldPageData.pages;
                pagesArray.push(newPageData);
                newchapter = await pageRepo.updateById({
                    pages: pagesArray
                }, chapter_id);

            } else {
                req.flash('error', "Please select different page no!");
                res.redirect(namedRouter.urlFor('page.create', {
                    id: req.body.chp_id
                }));
            }
            if (!_.isEmpty(newchapter)) {
                req.flash('success', 'Page created succesfully.');
                res.redirect(namedRouter.urlFor('chapter.getallPages', {
                    id: req.body.chp_id
                }));
            } else {
                req.flash('error', "Sorry some error occur!");
                res.redirect(namedRouter.urlFor('page.create', {
                    id: req.body.chp_id
                }));
            }

        } catch (e) {
            const error = errorHandler(e);
            req.flash('error', error.message);
            //res.status(500).send({message: error.message});
            res.redirect(namedRouter.urlFor('page.create', {
                id: req.body.chp_id
            }));
        }
    };

    /*
    // @Method: edit
    // @Description:  Coupon update page
    */
    async edit(req, res) {
        try {
            let result = {};
            let chapter_id = req.params.id;
            let chapter = await pageRepo.getByField({
                'pages._id': chapter_id
            });
            let pageNumber = req.params.number;
            let book = await bookRepo.getById(chapter.book_id);

            if (!_.isEmpty(chapter)) {
                result.chapter_data = chapter;
                result.book = book;
                res.render('page/views/edit.ejs', {
                    page_name: 'page-management',
                    page_title: 'Update Page',
                    user: req.user,
                    response: result,
                    pageno: pageNumber

                });
            } else {
                req.flash('error', "Sorry page not found!");
                res.redirect(namedRouter.urlFor('chapter.getallPages', {
                    id: req.params.id
                }));
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    /* @Method: update
    // @Description: coupon update action
    */
    async update(req, res) {
        try {
            const PageId = req.body.id;


            let OldPageDetails = await pageRepo.getById(req.body.cid);
            let page_position = req.body.position;

            OldPageDetails.pages[page_position].page_title = req.body.page_title,
                OldPageDetails.pages[page_position].page_content = req.body.page_content,
                OldPageDetails.pages[page_position].page_number = req.body.page_number

            let pageDataUpdate = await pageRepo.updateById(
                OldPageDetails, req.body.cid);

            if (pageDataUpdate) {
                req.flash('success', "Page Updated Successfully");
                res.redirect(namedRouter.urlFor('chapter.getallPages', {
                    id: req.body.cid
                }));
            } else {
                req.flash('error', "Page not updated due to some issues!");
                res.redirect(namedRouter.urlFor('page.edit', {
                    id: PageId,
                    number: req.body.page_number
                }, ));
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    /* @Method: delete
    // @Description: coupon delete
    */
    async destroy(req, res) {
        try {
            const document_id = req.params.id;
            const pageNumber = req.params.number;
            let getTheDocumentSet = await pageRepo.getByField({
                'pages._id': document_id
            });

            let count = 0;
            for (let i = 0; i < getTheDocumentSet.pages.length; i++) {
                if (pageNumber == getTheDocumentSet.pages[i].page_number) {
                    count = i;
                }
            }

            let chapterId = getTheDocumentSet._id;
            let DeletePageDocument = await pageRepo.PageDelete(chapterId, document_id);
            if (!_.isEmpty(DeletePageDocument)) {
                req.flash('success', 'Page Removed Successfully');
                res.redirect(namedRouter.urlFor('chapter.getallPages', {
                    id: chapterId
                }));
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    /*
// @Method: verifyPageNumber
// @Description: User status change action
*/
    async verifyPageNumber(req, res) {
        try {

            const result = await pageRepo.getByField({
                "book_id": req.body.book_id,
                "chapter_no": req.body.chapter_no
            });
            let count = 0;

            for (var s = 0; s < result.pages.length; s++) {
                if (result.pages[s].page_number == req.body.page_no) {
                    count++;
                }

            }
            if (count == 0) {
                return {
                    "status": 200,
                };
            } else {
                return {
                    "status": 200,
                    "message": "Page No already exists"
                };
            }




        } catch (error) {
            throw error;
        }
    };


    /*
// @Method: verifyPageTitle
// @Description: User status change action
*/
    async verifyPageTitle(req, res) {
        try {
            const result = await pageRepo.getByField({
                "book_id": req.body.book_id,
                "chapter_no": req.body.chapter_no
            });
            let count = 0;
            for (var s = 0; s < result.pages.length; s++) {
                if (result.pages[s].page_title == req.body.page_title) {
                    count++;
                }

            }
            if (count == 0) {
                return {
                    "status": 200,
                };
            } else {
                return {
                    "status": 200,
                    "message": "Page Title already exists"
                };
            }




        } catch (error) {
            throw error;
        }
    };

    async getAllPagesByChapter(req, res) {
        try {

            let chapter = await pageRepo.getAll(req);


            if (_.has(req.body, 'sort')) {
                var sortOrder = req.body.sort.sort;
                var sortField = req.body.sort.field;
            } else {
                var sortOrder = -1;
                var sortField = '_id';
            }
            let meta = {
                "page": req.body.pagination.page,
                "pages": chapter.pageCount,
                "perpage": req.body.pagination.perpage,
                "total": chapter.totalCount,
                "sort": sortOrder,
                "field": sortField
            };
            return {
                status: 200,
                meta: meta,
                data: chapter.data,
                message: `Data fetched succesfully.`
            };
        } catch (error) {
            // return { status: 500, data: [], message: e.message };
            throw error
        }
    }

}

module.exports = new pageController();