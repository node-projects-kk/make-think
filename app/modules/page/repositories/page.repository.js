const mongoose = require('mongoose');
const Chapter = require('chapter/models/chapter.model');
const perPage = config.PAGINATION_PERPAGE;

const PageRepository = {

    getAll: async (req) => {
      
        try {
            var conditions = {};
            var and_clauses = [];

            and_clauses.push({
                "isDeleted": false,
                "_id": mongoose.Types.ObjectId(req.params.id)
            });

            if (_.isObject(req.body.query) && _.has(req.body.query, 'generalSearch')) {
                //and_clauses.push({"status": /req.body.query.generalSearch/i});
                // and_clauses.push({ 'title': { $regex: req.body.query.generalSearch, $options: 'i' } });
                // and_clauses.push({ 'slug': { $regex: req.body.query.generalSearch, $options: 'i' } });
                // and_clauses.push({ 'desc': { $regex: req.body.query.generalSearch, $options: 'i' } });
                and_clauses.push({
                    $or: [{
                            'page_title': {
                                $regex: req.body.query.generalSearch,
                                $options: 'i'
                            }
                        },
                        {
                            'page_content': {
                                $regex: req.body.query.generalSearch,
                                $options: 'i'
                            }
                        },
                        {
                            'page_no': parseInt(req.body.query.generalSearch),

                        }
                    ]
                });
            }
            if (_.isObject(req.body.query) && _.has(req.body.query, 'Status')) {
                and_clauses.push({
                    "status": req.body.query.Status
                });
            }
            conditions['$and'] = and_clauses;

            var sortOperator = {
                "$sort": {}
            };
            if (_.has(req.body, 'sort')) {
                var sortField = req.body.sort.field;
                if (req.body.sort.sort == 'desc') {
                    var sortOrder = -1;
                } else if (req.body.sort.sort == 'asc') {
                    var sortOrder = 1;
                }

                sortOperator["$sort"][sortField] = sortOrder;
            } else {
                sortOperator["$sort"]['_id'] = -1;
            }

            var aggregate = Chapter.aggregate([{
                $unwind:  '$pages'
                },
                {
                    $project: {
                        _id: "$_id",
                        pages: "$pages",
                        book_id: "$book_id",
                        chapter_id: "$pages._id",
                        page_content: "$pages.page_content",
                        page_title: "$pages.page_title",
                        page_number: "$pages.page_number",
                        isDeleted: "$isDeleted",
                        status: "$pages.status"
                    }
                },
                {
                    $match: conditions
                },
                sortOperator
            ]);
        
            var options = {
                page: req.body.pagination.page,
                limit: req.body.pagination.perpage
            };
            let allCms = await Chapter.aggregatePaginate(aggregate, options);
           
            return allCms;
        } catch (e) {
            throw (e);
        }
    },

    getById: async (id) => {
        let cms = await Chapter.findById(id).exec();
        try {
            if (!cms) {
                return null;
            }
            return cms;
        } catch (e) {
            return e;
        }
    },

    getByField: async (params) => {
        try {
            let chapter = await Chapter.findOne(params).exec();
            if (!chapter) {
                return null;
            }
            return chapter;
        } catch (e) {
            throw e;
        }
    },

    checkTitle: async (id, title, pagenumber) => {
        let chapter = await Chapter.findOne({
            _id: id,
            // 'pages.page_title': title,
            'pages.page_number': pagenumber
        }).exec();
        try {
            if (!chapter) {
                return null;
            }
            return chapter;

        } catch (e) {
            throw e;
        }
    },

    getAllByField: async (params) => {
        let chapter = await Chapter.find(params).exec();
        try {
            if (!chapter) {
                return null;
            }
            return chapter;

        } catch (e) {
            throw e;
        }
    },

    delete: async (id) => {
        try {
            let chapter = await Chapter.findById(id);
            if (chapter) {
                let chapterDelete = await Chapter.remove({
                    _id: id
                }).exec();
                if (!chapterDelete) {
                    return null;
                }
                return chapterDelete;
            }
        } catch (e) {
            throw e;
        }
    },

    deleteByField: async (field, fieldValue) => {
        //todo: Implement delete by field
    },


    updateById: async (data, id) => {
        try {
            let chapter = await Chapter.findByIdAndUpdate(id, data, {
                new: true,
                upsert: true
            }).exec();
            if (!chapter) {
                return null;
            }
            return chapter;
        } catch (e) {
            return e;
        }
    },

    updateByField: async (field, fieldValue, data) => {
        //todo: update by field
    },

    save: async (data) => {
        try {
            let chapter = await Chapter.create(data);
            if (!chapter) {
                return null;
            }
            return chapter;
        } catch (e) {
            throw e;
        }
    },


    getByChapterId: async (id) => {
        let chapter = await Chapter.findById(id).populate('book_id').exec();
        try {
            if (!chapter) {
                return null;
            }
            return chapter;
        } catch (e) {
            return e;
        }
    },

    updatePageByField: async (field, fieldValue, data) => {
        try {
            let chapter = await Chapter.findByIdAndUpdate(fieldValue, field, {
                new: true
            });
            if (!chapter) {
                return null;
            }
            return chapter;
        } catch (e) {
            return e;
        }
    },

    checkTitleForPages: async (id, title, pagenumber) => {
        let chapter = await Chapter.findOne({
            'pages._id': id,
            'pages.page_title': title,
            'pages.page_number': pagenumber
        }).exec();
        try {
            if (!chapter) {
                return null;
            }
            return chapter;

        } catch (e) {
            throw e;
        }
    },

    /* PageDelete: async (id, position) => {
        try {
            await Chapter.findById(id).lean().exec();
            return await Chapter.update({
                _id: id
            }, {
                '$pop': {
                    "pages": position
                }
            }).lean().exec();
        } catch (error) {
            throw new Error(error.message);
        }
    }
 */

PageDelete: async (id, page_id) => {
    try {
        return await Chapter.update({
            _id: id
        }, {
            $pull: {
                pages : {
                '_id': page_id
            }
        }
        }).lean().exec();
    } catch (error) {
        throw new Error(error.message);
    }
}


};

module.exports = PageRepository;