const mongoose = require('mongoose');
const classRepo = require('class/repositories/class.repository');
const schoolRepo = require('school/repositories/school.repository');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const fs = require('fs');
const errorHandler = require('../../../errorHandler');


class classController {

	async create(req, res) {
		try {
			let schools = await schoolRepo.getAllByField({"isDeleted": "false","isActive": true});
			let classes = await classRepo.getAllByField({"status": "Active","isDeleted": false});
		
			res.render('class/views/add.ejs', {
				page_name: 'Class-Management',
				page_title: 'Create New Class',
				user: req.user,
				school_info: schools,
				class_info: classes
			});
		}
		catch (e) {
			throw (e);
		}
	};

	async insert(req, res) {
		try {
			let schoolInfo = await schoolRepo.getById(req.body.school_id);
			let checkExist = await classRepo.checkSameData(req.body.class_name, req.body.school_id);
			if (!_.isEmpty(checkExist)) {
				req.flash('error', req.body.class_name + " already exist in "+ schoolInfo.school_name);
				res.redirect(namedRouter.urlFor('class.create'));
			}
			else{
				let newClass = await classRepo.save(req.body);
				req.flash('success', 'Class added succesfully.');
				res.redirect(namedRouter.urlFor('class.list'));
			}
		}
		catch (e) {
			const error = errorHandler(e);
			req.flash('error', error.message);
			res.redirect(namedRouter.urlFor('class.create'));
		}
	};

	async list(req, res) {
		try {
			res.render('class/views/list.ejs', {
				page_name: 'Class-Management',
				page_title: 'Class List',
				user: req.user
			});
		}
		catch (e) {
			return res.status(500).send({message: e.message});
		}
	};

	async getAll(req, res) {
		try {
			let classes = await classRepo.getAll(req);
			if (_.has(req.body, 'sort')) {
				var sortOrder = req.body.sort.sort;
				var sortField = req.body.sort.field;
			}
			else {
				var sortOrder = -1;
				var sortField = '_id';
			}
			let meta = {
				"page": req.body.pagination.page,
				"pages": classes.pageCount,
				"perpage": req.body.pagination.perpage,
				"total": classes.totalCount,
				"sort": sortOrder,
				"field": sortField
			};
		
			return {	status: 200,meta: meta,data: classes.data,message: "Data fetched succesfully."};
		}
		catch (e) {
			return {status: 500,data: [],	message: e.message};
		}
	}

	async viewAllClasses(req, res) {
		try {
			let questionDelete = await questionRepo.updateById({"isDeleted": true}, req.params.id)
			if (!_.isEmpty(questionDelete)) {
				req.flash('success', 'Question removed Successfully');
				res.redirect(namedRouter.urlFor('question.list'));
			}
		}
		catch (e) {
			return res.status(500).send({message: e.message});
		}
	};

	async edit(req, res) {
		try {
			let result = {};
			let schools = await schoolRepo.getAllByField({"isDeleted": "false","isActive": true});
			let classes = await classRepo.getAllByField({"status": "Active","isDeleted": false});
			
			let editresult = await classRepo.getById(req.params.id);
			if (!_.isEmpty(editresult)) {
				res.render('class/views/edit.ejs', {
					page_name: 'Class-Management',
					page_title: 'Edit Class',
					user: req.user,
					school_info: schools,
					class_info: classes,
					response: editresult
				});
			}
			else {
				req.flash('error', "Sorry record not found!");
				res.redirect(namedRouter.urlFor('class.getall'));
			}
		}
		catch (e) {
			return res.status(500).send({message: e.message});
		}
	};

	/* @Method: update
	// @Description: user update action
	*/
	async update(req, res) {
		try {
			const class_Id = req.body.classId;
			let schoolInfo = await schoolRepo.getById(req.body.school_id);
			let checkExist = await classRepo.checkSameDataEdit(req.body.class_name, req.body.school_id,class_Id);
		
			if (!_.isEmpty(checkExist)) {
				req.flash('error', req.body.class_name + " already exist in "+ schoolInfo.school_name);
				res.redirect(namedRouter.urlFor('class.list'));
			}
			else{
				let classUpdate = await classRepo.updateById(req.body, class_Id);
				req.flash('success', 'Class added succesfully.');
				res.redirect(namedRouter.urlFor('class.list'));
			}
		}
		catch (e) {
			return res.status(500).send({message: e.message});
		}
	};
    
	async delete(req, res) {
		try {
			let teacherDelete = await userRepo.updateById({"isDeleted": true}, req.params.id);
			if (!_.isEmpty(teacherDelete)) {
				req.flash('success', 'Teacher Removed Successfully');
				res.redirect(namedRouter.urlFor('teacher.list'));
			}
		}
		catch (e) {
			return res.status(500).send({message: e.message});
		}
	};
}
module.exports = new classController();