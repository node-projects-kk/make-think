const mongoose = require('mongoose');
const Class = require('class/models/class.model');
const perPage = config.PAGINATION_PERPAGE;



const ClassRepository = {
    getAll: async (req) => {
        try {
            var conditions = {};
            const and_clauses = [];

            and_clauses.push({
                "isDeleted": false
            });
            if (_.isObject(req.body.query) && _.has(req.body.query, 'generalSearch')) {
                and_clauses.push({
                    $or: [{
                            'class_name': {
                                $regex: req.body.query.generalSearch,
                                $options: 'i'
                            }
                        },
                        {
                            'school_info.school_name': {
                                $regex: req.body.query.generalSearch,
                                $options: 'i'
                            }
                        },
                    ]
                });
            }
            if (_.isObject(req.body.query) && _.has(req.body.query, 'Status')) {
                and_clauses.push({
                    "status": req.body.query.Status
                });
            }
            conditions['$and'] = and_clauses;

            var sortOperator = {
                "$sort": {}
            };
            if (_.has(req.body, 'sort')) {
                var sortField = req.body.sort.field;
                if (req.body.sort.sort == 'desc') {
                    var sortOrder = -1;
                } else if (req.body.sort.sort == 'asc') {
                    var sortOrder = 1;
                }
                sortOperator["$sort"][sortField] = sortOrder;
            } else {
                sortOperator["$sort"]['_id'] = -1;
            }

            var aggregate = Class.aggregate([{
                    "$lookup": {
                        "from": "schools",
                        "localField": "school_id",
                        "foreignField": "_id",
                        "as": "school_info"
                    },
                },
                {
                    $unwind: "$school_info"
                },
                {
                    $project: {
                        _id: "$_id",
                        class_name: "$class_name",
                        school_info: "$school_info",
                        status: "$status",
                        isDeleted: "$isDeleted",
                    }
                },
                {
                    $match: conditions
                },
                sortOperator
            ]);

            var options = {
                page: req.body.pagination.page,
                limit: req.body.pagination.perpage
            };
            let allClass = await Class.aggregatePaginate(aggregate, options);
            return allClass;
        } catch (e) {
            throw (e);
        }
    },

    getById: async (id) => {
        try {
            let classes = await Class.findById(id);
            return classes ? classes : null;
        } catch (e) {
            throw (e);
        }
    },

    getByField: async (params) => {
        try {
            let classes = await Class.findOne(params).exec();
            return classes;
        } catch (e) {
            throw (e);
        }
    },

    getAllByField: async (params) => {
        try {
            let classes = await Class.find(params).populate('title').exec();
            return classes;
        } catch (e) {
            throw (e);
        }
    },

    delete: async (id) => {
        try {
            let classes = await Class.findById(id);
            if (classes) {
                let classesDelete = await Class.remove({
                    _id: id
                }).exec();
                return classesDelete;
            } else {
                return null;
            }
        } catch (e) {
            throw (e);
        }
    },

    deleteByField: async (field, fieldValue) => {
        //todo: Implement delete by field
    },


    updateById: async (data, id) => {
        try {
            let classes = await Class.findByIdAndUpdate(id, data, {
                new: true
            }).exec();
            if (classes) {
                return classes;
            } else {
                return null;
            }
        } catch (e) {
            throw (e);
        }
    },

    updateByField: async (field, fieldValue, data) => {
        //todo: update by field
    },

    save: async (data) => {
        try {
            let classes = await Class.create(data);
            if (!classes) {
                return null;
            }
            return classes;
        } catch (e) {
            throw e;
        }
    },


    getChildByTearcher: async (tid, sid, page) => {
        try {
            var conditions = {};
            var and_clauses = [];

            and_clauses.push({
                "isDeleted": false,
                "status": "Active"
            });
            and_clauses.push({
                "teacher_id": mongoose.Types.ObjectId(tid),
                "school_id": mongoose.Types.ObjectId(sid)
            });

            conditions['$and'] = and_clauses;
            let classInfo = Class.aggregate([{
                    $match: conditions
                },
                {
                    "$lookup": {
                        "from": "schools",
                        "localField": "school_id",
                        "foreignField": "_id",
                        "as": "school_info"
                    },
                },
                {
                    $unwind: "$school_info"
                },
                {
                    "$lookup": {
                        "from": "users",
                        "localField": "student_id",
                        "foreignField": "_id",
                        "as": "student_id"
                    },
                },
                {
                    "$sort": {
                        _id: 1
                    }
                },
            ]);
            let options = {
                page: page,
                limit: perPage
            }
            return await Class.aggregatePaginate(classInfo, options);
            let classDetails = await Class.aggregatePaginate(classInfo, options);
            if (!classDetails) {
                return null;
            } else {
                return classDetails;
            }
        } catch (e) {
            throw (e);
        }

    },

    deleteChildByTearcher: async (childId, teacherId) => {
        try {
            let result = await Class.update({
                "teacher_id": mongoose.Types.ObjectId(teacherId)
            }, {
                $pull: {
                    "student_id": mongoose.Types.ObjectId(childId)
                }
            }).lean().exec();
            return result;
        } catch (e) {
            throw (e);
        }
    },


    deleteChildFromPrinciple: async (class_id, student_id) => {
        try {
            let result = await Class.update({
                "_id": mongoose.Types.ObjectId(class_id)
            }, {
                $pull: {
                    "student_id": mongoose.Types.ObjectId(student_id)
                }
            }).lean().exec();
            return result;
        } catch (e) {
            throw (e);
        }
    },

    checkSameData: async (cname, student) => {
        try {
            var params = {
                "class_name": cname,
                "school_id": mongoose.Types.ObjectId(student)
            };
            let classes = await Class.findOne(params).exec();
            return classes;
        } catch (e) {
            throw e;
        }
    },

    checkSameDataEdit: async (cname, student, cid) => {
        try {
            var params = {
                "class_name": cname,
                "school_id": mongoose.Types.ObjectId(student),
                '_id': {
                    $ne: mongoose.Types.ObjectId(cid)
                }
            };
            let classes = await Class.findOne(params).exec();
            return classes;
        } catch (e) {
            throw e;
        }
    },

    getChildInfoForParentAndPrinciple: async (sid) => {
        try {
            var conditions = {};
            var and_clauses = [];

            and_clauses.push({
                "isDeleted": false,
                "status": "Active"
            });
            and_clauses.push({
                'student_id': {
                    '$in': [mongoose.Types.ObjectId(sid)]
                }
            });
            conditions['$and'] = and_clauses;
            var aggregate = await Class.aggregate([{
                    $match: conditions
                },
                {
                    "$lookup": {
                        "from": "schools",
                        "localField": "school_id",
                        "foreignField": "_id",
                        "as": "school_info"
                    },
                },
                {
                    $unwind: "$school_info"
                },
                {
                    $unwind: "$student_id"
                },
                {
                    "$lookup": {
                        "from": "users",
                        "localField": "student_id",
                        "foreignField": "_id",
                        "as": "student_info"
                    },
                },
                {
                    $unwind: "$student_info"
                },
                {
                    "$lookup": {
                        "from": "users",
                        "localField": "teacher_id",
                        "foreignField": "_id",
                        "as": "teacher_info"
                    },
                },
                {
                    $unwind: "$teacher_info"
                },
                {
                    $match: {
                        'student_id': mongoose.Types.ObjectId(sid)
                    }
                },
                {
                    $group: {
                        _id: "$student_id",
                        class_name: {
                            $first: "$class_name"
                        },
                        student_info: {
                            $first: "$student_info"
                        },
                        teacher_info: {
                            $first: "$teacher_info"
                        },
                        school_info: {
                            $first: "$school_info"
                        }
                    }
                }
            ]);
            return aggregate;
        } catch (e) {
            throw (e);
        }

    },

    
};

module.exports = ClassRepository;