const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');
const beautifyUnique = require('mongoose-beautiful-unique-validation');

const ClassSchema = new Schema({
	class_name: {type: String,default: ''},
	teacher_id: {type: mongoose.Schema.Types.ObjectId,ref: 'User',default: null},
	school_id: {type: mongoose.Schema.Types.ObjectId,ref: 'School',default: null},
	student_id: [{
		type: mongoose.Schema.Types.ObjectId,
		ref: 'User',
		default: null
	}],
	status: {type: String,default: 'Active',enum: ['Active', 'Inactive']},
	isDeleted: {type: Boolean,default: false,enum: [true, false]}
});

ClassSchema.plugin(beautifyUnique);
// For pagination
ClassSchema.plugin(mongooseAggregatePaginate);

// create the model for users and expose it to our app
module.exports = mongoose.model('Class', ClassSchema);