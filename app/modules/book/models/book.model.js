const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');
const beautifyUnique = require('mongoose-beautiful-unique-validation');

const BookSchema = new Schema({
  title: {
    type: String,
    default: '',
    unique: 'Book with title "{VALUE}" is already exist!'
  },
  subtitle: {
    type: String,
    default: '',
  },
  slug: {
    type: String,
    default: ''
  },
  genre: {
    type: Schema.Types.ObjectId,
    ref: 'Genre',
    default: null
  },
  author: {
    type: Schema.Types.ObjectId,
    ref: 'Author',
    default: null
  },
  picture: {
    type: String,
    default: ''
  },
  chapter_num: {
    type: Number,
    default: 0
  },
  grade: {
    type: Number,
    default: ''
  },
  status: {
    type: String,
    default: "Active",
    enum: ["Active", "Inactive"]
  },
  isDeleted: {
    type: Boolean,
    default: false,
    enum: [true, false]
  }
});

BookSchema.plugin(beautifyUnique);
// For pagination
BookSchema.plugin(mongooseAggregatePaginate);

// create the model for users and expose it to our app
module.exports = mongoose.model('Book', BookSchema);