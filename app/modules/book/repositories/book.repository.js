const mongoose = require('mongoose');
const Book = require('book/models/book.model');
const Chapter = require('chapter/models/chapter.model');
const perPage = config.PAGINATION_PERPAGE;

const bookRepository = {

    getAll: async (req) => {
        try {
            var conditions = {};
            var and_clauses = [];

            and_clauses.push({
                "isDeleted": false
            });

            if (_.isObject(req.body.query) && _.has(req.body.query, 'generalSearch')) {
                //and_clauses.push({ 'title': { $regex:  req.body.query.generalSearch, $options: 'i'} });
                and_clauses.push({
                    $or: [{
                            'title': {
                                $regex: req.body.query.generalSearch,
                                $options: 'i'
                            }
                        },
                        {
                            'chapter_num': parseInt(req.body.query.generalSearch),

                        },
                        {
                            'genre_docs.title': {
                                $regex: req.body.query.generalSearch,
                                $options: 'i'
                            }
                        },
                        {
                            'author_docs.title': {
                                $regex: req.body.query.generalSearch,
                                $options: 'i'
                            }
                        }
                    ]
                });
            }
            if (_.isObject(req.body.query) && _.has(req.body.query, 'Status')) {
                and_clauses.push({
                    "status": req.body.query.Status
                });
            }
            conditions['$and'] = and_clauses;
            // console.log(JSON.stringify(conditions))
            var sortOperator = {
                "$sort": {}
            };
            if (_.has(req.body, 'sort')) {
                var sortField = req.body.sort.field;
                if (req.body.sort.sort == 'desc') {
                    var sortOrder = -1;
                } else if (req.body.sort.sort == 'asc') {
                    var sortOrder = 1;
                }

                sortOperator["$sort"][sortField] = sortOrder;
            } else {
                sortOperator["$sort"]['_id'] = -1;
            }

            var aggregate = Book.aggregate([{
                    $lookup: {
                        from: "genres",
                        localField: "genre",
                        foreignField: "_id",
                        as: "genre_docs"
                    }
                },
                {
                    "$unwind": "$genre_docs"
                },
                {
                    $lookup: {
                        from: "authors",
                        localField: "author",
                        foreignField: "_id",
                        as: "author_docs"
                    }
                },
                {
                    "$unwind": "$author_docs"
                },
                {
                    $match: conditions
                },
                sortOperator
            ]);

            var options = {
                page: req.body.pagination.page,
                limit: req.body.pagination.perpage
            };
            let allBook = await Book.aggregatePaginate(aggregate, options);


            return allBook;
        } catch (e) {
            throw (e);
        }
    },

    getById: async (id) => {
        let cms = await Book.findById(id).exec();
        try {
            if (!cms) {
                return null;
            }
            return cms;

        } catch (e) {
            return e;
        }
    },

    getByField: async (params) => {
        let cms = await Book.findOne(params).exec();
        try {
            if (!cms) {
                return null;
            }
            return cms;

        } catch (e) {
            return e;
        }
    },

    getAllByField: async (params) => {
        let user = await Book.find(params).exec();
        try {
            if (!user) {
                return null;
            }
            return user;

        } catch (e) {
            return e;
        }
    },

    delete: async (id) => {
        try {
            let user = await Book.findById(id);
            if (user) {
                let userDelete = await Book.remove({
                    _id: id
                }).exec();
                if (!userDelete) {
                    return null;
                }
                return userDelete;
            }
        } catch (e) {
            return e;
        }
    },

    deleteByField: async (field, fieldValue) => {
        //todo: Implement delete by field
    },

    updateById: async (data, id) => {
        try {
            let cms = await Book.findByIdAndUpdate(id, data, {
                new: true,
                upsert: true
            }).exec();
            if (!cms) {
                return null;
            }
            return cms;
        } catch (e) {
            return e;
        }
    },

    updateByField: async (field, fieldValue, data) => {
        //todo: update by field
    },

    save: async (data) => {
        try {
            let book = await Book.create(data);
            if (!book) {
                return null;
            }
            return book;
        } catch (e) {
            throw e;
        }
    },

    getAllChapterByField: async (params) => {
        let chapter = await Chapter.find(params).exec();
        try {
            if (!chapter) {
                return null;
            }
            return chapter;
        } catch (e) {
            return e;
        }
    },


    getAllChapterByFieldBackUp: async (req) => {
        try {
            var conditions = {};
            var and_clauses = [];

            and_clauses.push({
                "isDeleted": false
            });

            if (_.isObject(req.body.query) && _.has(req.body.query, 'generalSearch')) {
                and_clauses.push({
                    'title': {
                        $regex: req.body.query.generalSearch,
                        $options: 'i'
                    }
                });
                // and_clauses.push({
                //     $or: [{
                //             'title': {
                //                 $regex: req.body.query.generalSearch,
                //                 $options: 'i'
                //             }
                //         },

                //     ]
                // });
            }
            if (_.isObject(req.body.query) && _.has(req.body.query, 'Status')) {
                and_clauses.push({
                    "status": req.body.query.Status
                });
            }
            conditions['$and'] = and_clauses;
            // console.log(JSON.stringify(conditions))
            var sortOperator = {
                "$sort": {}
            };
            if (_.has(req.body, 'sort')) {
                var sortField = req.body.sort.field;
                if (req.body.sort.sort == 'desc') {
                    var sortOrder = -1;
                } else if (req.body.sort.sort == 'asc') {
                    var sortOrder = 1;
                }
                sortOperator["$sort"][sortField] = sortOrder;
            } else {
                sortOperator["$sort"]['_id'] = -1;
            }

            var aggregate = Chapter.aggregate([{
                    $match: conditions
                },
                sortOperator
            ]);

            var options = {
                page: req.body.pagination.page,
                limit: req.body.pagination.perpage
            };
            let chapter = await Chapter.aggregatePaginate(aggregate, options);
            // console.log('chapter <> 274', chapter);
            return chapter;
        } catch (e) {
            console.log('e <> 277', e);
            throw (e);
        }
    },

    getAllBookByField: async (params) => {
        let user = await Book.find(params).populate('author').populate('genre').sort({
            id: 1
        }).exec();
        try {
            if (!user) {
                return null;
            }
            return user;

        } catch (e) {
            return e;
        }
    },

    getBookWithChapterDetails: async (bookId) => {
        try {
            var aggregate = Book.aggregate([{
                    $lookup: {
                        from: "chapters",
                        localField: "_id",
                        foreignField: "book_id",
                        as: "chapter"
                    }
                },
                {
                    "$unwind": {
                        path: '$chapter',
                        preserveNullAndEmptyArrays: true
                    }
                },
                // {
                //     $lookup: {
                //         from: "activitydetails",
                //         localField: "chapter._id",
                //         foreignField: "chapter_id",
                //         as: "chapter.activity_info"
                //     }
                // },
                // {
                //     "$unwind": {
                //         path: '$chapter.activity_info',
                //         preserveNullAndEmptyArrays: true
                //     }
                // },
                {
                    $match: {
                        "_id": mongoose.Types.ObjectId(bookId)
                    }
                },
                {
                    $group: {
                        _id: "$_id",
                        title: {
                            $first: "$title"
                        },
                        subtitle: {
                            $first: "$subtitle"
                        },
                        chapter_number: {
                            $first: "$chapter_num"
                        },
                        chapters: {
                            $addToSet: "$chapter"
                        }
                    }
                },
                {
                    "$unwind": "$chapters"
                },
                {
                    "$sort": {
                        "chapters.chapter_no": 1
                    }
                },
                {
                    $group: {
                        _id: "$_id",
                        title: {
                            $first: "$title"
                        },
                        subtitle: {
                            $first: "$subtitle"
                        },
                        chapter_number: {
                            $first: "$chapter_number"
                        },
                        chapters: {
                            $push: "$chapters"
                        }
                    }
                },
            ]).exec();
            return aggregate;
        } catch (e) {
            throw (e);
        }
    },

    getAllBookByKeyword: async (keyword) => {
        try {
            var conditions = {};
            var and_clauses = [];

            and_clauses.push({
                "isDeleted": false
            });

            if (keyword) {
                and_clauses.push({
                    $or: [{
                            'title': {
                                $regex: keyword,
                                $options: 'i'
                            }
                        },
                        {
                            'chapter_num': parseInt(keyword),

                        },
                        {
                            'genre_docs.title': {
                                $regex: keyword,
                                $options: 'i'
                            }
                        },
                        {
                            'author_docs.title': {
                                $regex: keyword,
                                $options: 'i'
                            }
                        }
                    ]
                });
            }
            conditions['$and'] = and_clauses;
            // console.log(JSON.stringify(conditions))

            var aggregate = Book.aggregate([{
                    $lookup: {
                        from: "genres",
                        localField: "genre",
                        foreignField: "_id",
                        as: "genre_docs"
                    }
                },
                {
                    "$unwind": "$genre_docs"
                },
                {
                    $lookup: {
                        from: "authors",
                        localField: "author",
                        foreignField: "_id",
                        as: "author_docs"
                    }
                },
                {
                    "$unwind": "$author_docs"
                },
                {
                    $match: conditions
                },
            ]);
            return aggregate;
        } catch (e) {
            throw (e);
        }
    },

    getAllBookByKeyword: async (keyword) => {
        try {
            var conditions = {};
            var and_clauses = [];

            and_clauses.push({
                "isDeleted": false,
                'status': 'Active'
            });

            if (keyword) {
                and_clauses.push({
                    $or: [{
                            'title': {
                                $regex: keyword,
                                $options: 'i'
                            }
                        },
                        {
                            'chapter_num': parseInt(keyword)
                        },
                        {
                            'genre_docs.title': {
                                $regex: keyword,
                                $options: 'i'
                            }
                        },
                        {
                            'author_docs.title': {
                                $regex: keyword,
                                $options: 'i'
                            }
                        },
                    ]
                });
            }
            conditions['$and'] = and_clauses;
            // console.log(JSON.stringify(conditions))

            var aggregate = Book.aggregate([{
                    $lookup: {
                        from: "genres",
                        localField: "genre",
                        foreignField: "_id",
                        as: "genre_docs"
                    }
                },
                {
                    "$unwind": "$genre_docs"
                },
                {
                    $lookup: {
                        from: "authors",
                        localField: "author",
                        foreignField: "_id",
                        as: "author_docs"
                    }
                },
                {
                    "$unwind": "$author_docs"
                },
                {
                    $match: conditions
                },
            ]);
            return aggregate;
        } catch (e) {
            throw (e);
        }
    },

    getBooksFromEachGrade: async () => {
        try {
            var conditions = {};
            var and_clauses = [];

            and_clauses.push({
                'status': 'Active',
                "isDeleted": false
            });

            conditions['$and'] = and_clauses;

            return await Book.aggregate([
                //{$sort:{"formversion":-1}},
                {
                    $lookup: {
                        from: "genres",
                        localField: "genre",
                        foreignField: "_id",
                        as: "genres"
                    }
                },
                {
                    "$unwind": "$genres"
                },
                {
                    $lookup: {
                        from: "authors",
                        localField: "author",
                        foreignField: "_id",
                        as: "authors"
                    }
                },
                {
                    "$unwind": "$authors"
                },
                {
                    $group: {
                        "_id": "$grade",
                        "book": {
                            $first: "$$ROOT"
                        }

                    }
                },
                {
                    $project: {
                        "_id": "$book._id",
                        "title": '$book.title',
                        "subtitle": "$book.subtitle",
                        "slug": "$book.slug",
                        "grade": "$book.grade",
                        "genre": "$book.genres.title",
                        "author": "$book.authors.title",
                        "picture": "$book.picture",
                        "chapter_num": "$book.chapter_num",
                        "status": "$book.status",
                        "isDeleted": "$book.isDeleted"
                    }
                },
                {
                    $match: conditions
                }
            ]).exec();
        } catch (e) {

        }
    },

    getAllBookByGrade: async (keyword) => {
        try {
            var conditions = {};
            var and_clauses = [];

            and_clauses.push({
                "isDeleted": false,
                'status': 'Active'

            });

            if (keyword != 'all') {
                and_clauses.push({
                    $or: [{
                        'grade': parseInt(keyword)
                    }]
                });
            }
            conditions['$and'] = and_clauses;

            var booksByGrade = Book.aggregate([{
                    $lookup: {
                        from: "genres",
                        localField: "genre",
                        foreignField: "_id",
                        as: "genre_docs"
                    }
                },
                {
                    "$unwind": "$genre_docs"
                },
                {
                    $lookup: {
                        from: "authors",
                        localField: "author",
                        foreignField: "_id",
                        as: "author_docs"
                    }
                },
                {
                    "$unwind": "$author_docs"
                },
                {
                    $match: conditions
                },
            ]);
            return booksByGrade;
        } catch (e) {
            throw (e);
        }
    },

};

module.exports = bookRepository;