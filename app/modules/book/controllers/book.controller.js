const mongoose = require('mongoose');
const bookRepo = require('book/repositories/book.repository');
const chapterRepo = require('chapter/repositories/chapter.repository');
const authorRepo = require('author/repositories/author.repository');
const genreRepo = require('genre/repositories/genre.repository');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const fs = require('fs');
var gm = require('gm').subClass({
    imageMagick: true
});
const errorHandler = require('../../../errorHandler');

class bookController {

    async create(req, res) {
        try {
            const activeGenres = await genreRepo.getAllByField({
                "isDeleted": false,
                "status": "Active"
            });
            const activeAuthors = await authorRepo.getAllByField({
                "isDeleted": false,
                "status": "Active"
            });
            res.render('book/views/addbook.ejs', {
                page_name: 'book-management',
                page_title: 'Create New Book',
                user: req.user,
                gneres: activeGenres,
                authors: activeAuthors
            });
        } catch (e) {
            throw (e);
        }
    };

    async insert(req, res) {
        try {
            req.body.picture = '';
            // console.log(req.files[0].fieldname)
            if (typeof req.fileValidationError != 'undefined') {
                req.flash('error', req.fileValidationError);
                res.redirect(namedRouter.urlFor('book.create'));
            } else {
                if (_.has(req, 'files')) {
                    if (req.files.length > 0) {
                        if (req.files[0].fieldname == 'picture') {
                            gm('public/uploads/book/' + req.files[0].filename)
                                .resize(100)
                                .write('public/uploads/book/thumb/' + req.files[0].filename, function (err) {
                                    if (err) {
                                        req.flash('error', err.message);
                                        res.redirect(namedRouter.urlFor('book.create'));
                                    } else {
                                        req.body.picture = req.files[0].filename;
                                        let newbook = bookRepo.save(req.body);
                                        req.flash('success', 'Book created succesfully.');
                                        res.redirect(namedRouter.urlFor('book.listing'));
                                    }
                                });
                        }
                    }
                } else {
                    let newbook = await bookRepo.save(req.body);
                    if (!_.isEmpty(newbook)) {
                        req.flash('success', 'Book created succesfully.');
                        res.redirect(namedRouter.urlFor('book.listing'));
                    } else {
                        req.flash('error', "Sorry some error occur!");
                        res.redirect(namedRouter.urlFor('book.create'));
                    }
                }
            }
        } catch (e) {
            //const error = errorHandler(e);
            // console.log(e)
            req.flash('error', e.message);
            res.redirect(namedRouter.urlFor('book.create'));
        }
    };

    /*
    // @Method: edit
    // @Description:  Coupon update page
    */
    async edit(req, res) {
        try {
            let result = {};
            const activeGenres = await genreRepo.getAllByField({
                "isDeleted": false,
                "status": "Active"
            });
            const activeAuthors = await authorRepo.getAllByField({
                "isDeleted": false,
                "status": "Active"
            });
            let book = await bookRepo.getById(req.params.id);
            // console.log('book 94', book);
            if (!_.isEmpty(book)) {
                result.book_data = book;
                result.genre_data = activeGenres;
                result.author_data = activeAuthors;
                res.render('book/views/edit.ejs', {
                    page_name: 'book-management',
                    page_title: 'Update Book',
                    user: req.user,
                    response: result
                });
            } else {
                req.flash('error', "Sorry book not found!");
                res.redirect(namedRouter.urlFor('book.listing'));
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    /* @Method: update
    // @Description: coupon update action
    */
    async update(req, res) {
        try {
            if (typeof req.fileValidationError != 'undefined') {
                req.flash('error', req.fileValidationError);
                res.redirect(namedRouter.urlFor('book.edit', {
                    id: req.body.bid
                }));
            } else {
                if (_.has(req, 'files')) {
                    if (req.files.length > 0) {

                        let book = await bookRepo.getByField({'_id':req.body.bid});

                         if (!_.isEmpty(book.picture)) {
                            if (fs.existsSync('public/uploads/book/' + book.picture)) {
                             const upl_img = fs.unlinkSync('public/uploads/book/' + book.picture);
                            }
                             if (fs.existsSync('public/uploads/book/thumb/' + book.picture)) {
                                     const upl_thumb_img = fs.unlinkSync('public/uploads/book/thumb/' + book.picture);
                                }
                        }

                        if (req.files[0].fieldname == 'picture') {
                            gm('public/uploads/book/' + req.files[0].filename)
                                .resize(100)
                                .write('public/uploads/book/thumb/' + req.files[0].filename, function (err) {
                                    if (err) {
                                        req.flash('error', err.message);
                                        res.redirect(namedRouter.urlFor('book.edit', {
                                            id: req.body.bid
                                        }));
                                    } else {
                                        req.body.picture = req.files[0].filename;
                                        bookRepo.updateById(req.body, req.body.bid);
                                        req.flash('success', 'Book updated succesfully.');
                                        res.redirect(namedRouter.urlFor('book.listing'));
                                    }
                                });
                        }
                    } else {
                        let bookUpdate = bookRepo.updateById(req.body, req.body.bid);
                        req.flash('success', 'Book updated succesfully.');
                        res.redirect(namedRouter.urlFor('book.listing'));
                    }
                } else {
                    let bookUpdate = bookRepo.updateById(req.body, req.body.bid);
                    req.flash('success', 'Book updated succesfully.');
                    res.redirect(namedRouter.urlFor('book.listing'));
                }
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    /* @Method: list
    // @Description: To get all the books from DB
    */
    async list(req, res) {
        try {
            res.render('book/views/list.ejs', {
                page_name: 'book-management',
                page_title: 'Book List',
                user: req.user
            });
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    async getAll(req, res) {
        try {
            let author = await bookRepo.getAll(req);
            if (_.has(req.body, 'sort')) {
                var sortOrder = req.body.sort.sort;
                var sortField = req.body.sort.field;
            } else {
                var sortOrder = -1;
                var sortField = '_id';
            }
            let meta = {
                "page": req.body.pagination.page,
                "pages": author.pageCount,
                "perpage": req.body.pagination.perpage,
                "total": author.totalCount,
                "sort": sortOrder,
                "field": sortField
            };
            return {
                status: 200,
                meta: meta,
                data: author.data,
                message: `Data fetched succesfully.`
            };
        } catch (e) {
            return {
                status: 500,
                data: [],
                message: e.message
            };
        }
    }

    /* @Method: delete
    // @Description: Book Delete
    */
    async delete(req, res) {
        try {
            let book = await bookRepo.getByField({'_id':req.params.id});
            let bookDelete = await bookRepo.delete(req.params.id)
            if (!_.isEmpty(bookDelete)) {

                

                         if (!_.isEmpty(book.picture)) {
                            if (fs.existsSync('public/uploads/book/' + book.picture)) {
                             const upl_img = fs.unlinkSync('public/uploads/book/' + book.picture);
                            }
                             if (fs.existsSync('public/uploads/book/thumb/' + book.picture)) {
                                     const upl_thumb_img = fs.unlinkSync('public/uploads/book/thumb/' + book.picture);
                                }
                        }

                req.flash('success', 'Book Removed Successfully');
                res.redirect(namedRouter.urlFor('book.listing'));
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    async getChapter(req, res) {
        try {
            let chapter = await chapterRepo.getAllByField({
                book_id: mongoose.Types.ObjectId(req.params.id)
            });
            res.send(chapter);
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    async getChapterById(req, res) {
        try {
            let chapter = await bookRepo.getById(req.params.id);
            res.send(chapter);
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    async getAllChapterBackUp(req, res) {
        try {
            let chapterResult = await bookRepo.getAllChapter(req);
            // console.log('chapterResult 268', chapterResult);
            process.exit();
            if (_.has(req.body, 'sort')) {
                var sortOrder = req.body.sort.sort;
                var sortField = req.body.sort.field;
            } else {
                var sortOrder = -1;
                var sortField = '_id';
            }
            let meta = {
                "page": req.body.pagination.page,
                "pages": author.pageCount,
                "perpage": req.body.pagination.perpage,
                "total": author.totalCount,
                "sort": sortOrder,
                "field": sortField
            };
            return {
                status: 200,
                meta: meta,
                data: author.data,
                message: `Data fetched succesfully.`
            };
        } catch (e) {
            return {
                status: 500,
                data: [],
                message: e.message
            };
        }
    }


    /* @Method: list
    // @Description: To get all the books chapter from DB
    */
    async getAllChapterList(req, res) {
        try {
            res.render('chapter/views/list.ejs', {
                page_name: 'chapter-management',
                page_title: 'Chapter List',
                user: req.user,
                book_id: req.params.id
            });
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    async getAllOth(req, res) {
        try {
            const chapterList = await bookRepo.getAllChapterByField({
                book_id: mongoose.Types.ObjectId(req.params.id),
                'isDeleted': false
            });

            return {
                status: 200,
                data: chapterList,
                message: `Data fetched succesfully.`
            };
        } catch (e) {
            throw e;
        }
    }




}

module.exports = new bookController();