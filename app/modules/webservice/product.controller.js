const productRepo = require('product/repositories/product.repository');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const gm = require('gm').subClass({
  imageMagick: true
});
const fs = require('fs');
const mongoose = require('mongoose');



class productController {
  constructor() {}

  /* @Method: list
  // @Description: To list all the products from DB
  */
  async list(req, res) {
    try {
      let productList = await productRepo.getAllByField({
        "status": "Active",
        "isDeleted": false
      });

      if (!_.isEmpty(productList) && !_.isNull(productList)) {
        return {
          "status": 200,
          data: productList,
          "message": "Products Fetched Successfully."
        };
      } else {
        return {
          status: 201,
          data: [],
          message: 'There are no active Products at this moment.'
        };
      }
    } catch (e) {
      return res.status(500).send({
        message: e.message
      });
    }
  };







}

module.exports = new productController();