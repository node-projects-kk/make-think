const userRepo = require('user/repositories/user.repository');
const paymentRepo = require('payment/repositories/payment.repository');
const roleRepo = require('role/repositories/role.repository');
const userModel = require('user/models/user.model.js');
const express = require('express');
const routeLabel = require('route-label');
const helper = require('../../helper/helper.js');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const gm = require('gm').subClass({
    imageMagick: true
});
const fs = require('fs');
const jwt = require('jsonwebtoken');
const crypto = require("crypto");
//mail send 
const {join} = require('path');
const ejs = require('ejs');
const {readFile} = require('fs');
const {promisify} = require('util');
const readFileAsync = promisify(readFile);

class userController {
    constructor() {
        this.user = [];
    }

    /* @Method: login
    // @Description: User Login
    */
	async login(req, res) {
		try {
			let userData = await userRepo.getUserByField({email: req.body.email});
			let userRole = await roleRepo.getByField({'role': req.body.userType});
		
			if (userData) {
				// principle
				if (userData.role.role == userRole.role) {
					//check is active
					if (userData.isActive == true) {
						var user = new userModel();
						if (!user.validPassword(req.body.password, userData.password)) {
							return {	status: 201,	data: [],message: 'Authentication failed, Wrong Password.'};
						}
						else {
							//check is verified
							if (userData.isVerified == 'Yes') {
								const payload = {	id: userData._id	};
								const token = jwt.sign(payload, config.jwtSecret, {expiresIn: 86400});
								//const updated = await userRepo.updateById(data, userData._id);
								let getSubscriptionInfo = await paymentRepo.parentSubscriptionInfo(userData._id);
								userData.subscription_info = getSubscriptionInfo;
								return {"status": 200,token: token,data: userData,"message": "You have successfully logged in"};
							}
							else {
								return {status: 201,data: userData,message: `Your Account is not verified.`};
							}
						}
					}
					else {
						return {status: 201,data: {},message: `Your Account is not active.`};
					}
				}
				else {
					return {status: 201,data: {},message: `User type is wrong, Please select the correct user type.`};
				}
			}
			else {
				return {status: 201,data: {},	message: `The details you have entered is not correct, Please try again.`};
			}
		}
		catch (e) {
			return {	status: 500,	data: {},message: e.message};
		}
	}


    /* @Method: forgotPassword vehicle owner
    // @Description: To forgotPassword vehicle owner
    */
    async forgotPassword(req, res) {
        try {
            let user = await userRepo.getByField({email: req.body.email});
            if (user) {
                let random_pass = Math.random().toString(36).substr(2, 9);
                const readable_pass = random_pass;
                random_pass = user.generateHash(random_pass);
                const templateData = {
                    firstName: user.first_name,
                    password: readable_pass
                };
                const ejsTemplate = await readFileAsync(join(__dirname, '../../', 'views/templates/email.forgot-password.ejs'));
                const html = ejs.render(ejsTemplate.toString(), {
                    data: templateData
                });
                await transporter.sendMail({
                    from: `Admin<${process.env.MAIL_USERNAME}>`,
                    to: req.body.email,
                    subject: 'Reset Password | Make Thinkers',
                    html: html
                });
                let updatePassword = await userRepo.updateById({
                    password: random_pass
                }, user._id);

                return {
                    status: 200,
                    data: {},
                    "message": 'A email with new password has been sent to your email address.'
                };

            } else {
                return {
                    status: 201,
                    data: {},
                    message: `No user found.`
                };
            }

        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    /* @Method: forgotPassword vehicle owner
    // @Description: To forgotPassword vehicle owner
    */
    async logout(req, res) {
        try {
            let user = await userRepo.getById(req.params.id);
            if (user) {
                //await userRepo.updateById({device_token:'',device_type:''}, user._id);
                return {
                    status: 200,
                    data: {},
                    "message": 'You have successfully logout.'
                };

            } else {
                return {
                    status: 201,
                    data: {},
                    message: `No user found.`
                };
            }

        } catch (e) {
            //return res.status(500).send({message: e.message});
            return {
                status: 500,
                data: {},
                message: e.message
            };
        }
    };

    /* @Method: verifyToken
    // @Description: Verify User Token
    */
    async verifyToken(req, res) {
        try {
            let parent = await userRepo.getById(req.body.user_id);
            if (parent) {
                if (parent.verifyToken == req.body.otp) {
                    let verifyUser = await userRepo.updateById({
                        isVerified: 'Yes'
                    }, req.body.user_id);
                    return {
                        status: 200,
                        data: verifyUser,
                        "message": 'Account Verified Successfully.'
                    };
                } else {
                    return {
                        status: 201,
                        data: [],
                        "message": 'Wrong OTP provided.'
                    };
                }
            } else {
                return {
                    status: 201,
                    data: {},
                    message: `No user found.`
                };
            }

        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };


    /* @Method: resendOtp
    // @Description: resend Otp to vehicle owner email
    */
    async resendOtp(req, res) {
        try {
            let user_id = req.body.id;
            //let parent = await userRepo.getByField({email: req.body.email});
            let parent = await userRepo.getById({
                "_id": user_id
            });
            let email = parent.email;
            if (email) {
                let OTP = Math.floor(1000 + Math.random() * 9000);
                req.body.verifyToken = OTP;
                let parentVerify = await userRepo.updateById({
                    verifyToken: OTP,
                    'isVerified': 'No'
                }, parent._id);
                const templateData = {
                    token: OTP
                };
                const ejsTemplate = await readFileAsync(join(__dirname, '../../', 'views/templates/email.sendVerificationLink.ejs'));
                const html = ejs.render(ejsTemplate.toString(), {
                    data: templateData
                });
                await transporter.sendMail({
                    from: `Admin<${process.env.MAIL_USERNAME}>`,
                    to: email,
                    subject: 'New Parent | Make Thinkers',
                    html: html
                });
                return {
                    status: 200,
                    data: parentVerify,
                    "message": 'A email with new OTP has been sent to your email address.'
                };
            } else {
                return {
                    status: 201,
                    data: {},
                    message: `No user found.`
                };
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    async emailAvailable(req, res) {
        try {
            // let available = helper.isEmailAvailable(req.body.email);
            let emailAvailable = await userRepo.getByField({
                'email': req.body.email
            });
            if (emailAvailable) {
                return {
                    status: 201,
                    data: emailAvailable,
                    message: 'Email already exists. Please choose another email.'
                }
            } else {
                return {
                    status: 200,
                    data: [],
                    message: 'You can use this email.'
                }
            }
        } catch (e) {
            return {
                status: 500,
                data: {},
                "message": e.message
            };
        }


    }

    async getProfile(req, res) {
        try {
            let userData = await userRepo.getById(req.params.id);

            if (!_.isEmpty(userData)) {
                return {
                    status: 200,
                    data: userData,
                    message: 'Profile fetched Successfully'
                }
            } else {
                throw new Error('User not Found')
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }

    }

    async UpdateProfile(req, res) {
        try {
            let userUpdatedData = await userRepo.updateById(req.body, req.body.id);
            if (!_.isEmpty(userUpdatedData)) {
                return {
                    status: 200,
                    data: userUpdatedData,
                    message: 'Profile updated Successfully'
                }
            } else {
                throw new Error('User not Found')
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }

    }

    async changePassword(req, res) {
        try {
            let userData = await userRepo.getById(req.user._id);
            if (!_.isEmpty(userData) && userData.isActive == true && userData.isDeleted == false) {
                if (userData.validPassword(req.body.old_password, userData.password)) {
                    let newPassword = userData.generateHash(req.body.new_password);
                    let updatedUser = await userRepo.updateById({
                        password: newPassword
                    }, req.user._id);
                    if (updatedUser) {
                        return {
                            status: 200,
                            data: updatedUser,
                            message: 'Password Changed Successfully'
                        }
                    }
                } else {
                    return {
                        status: 201,
                        data: [],
                        message: 'You have entered Wrong Old Password'
                    };
                }
            } else {
                return {
                    status: 201,
                    data: [],
                    message: 'User not Found'
                };
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }

    }

}

module.exports = new userController();