const schoolRepo = require('school/repositories/school.repository');
const chapterRepo = require('chapter/repositories/chapter.repository');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const gm = require('gm').subClass({ imageMagick: true });
const fs = require('fs');
const mongoose = require('mongoose');


class schoolController {
    constructor() {
        this.school = {};

    }

    /* @Method: list school us
   // @Description: To get list school from DB
   */
    async list(req, res) {
        try {
            let school = await schoolRepo.getAllByField({ "isActive": true });

            if (school) {
                return { "status": 200, data: school, "message": "Data Fetched Successfully." };
            } else {
                throw new Error(`something went wrong.`)
            }
        } catch (e) {
            return res.status(500).send({ message: e.message });
        }
    };
    async schoolAutocomplete(req, res) {
        try {
            console.log("KEY",req.body.key)
            let school = await schoolRepo.schoolAutocomplete(req.query.school_key);

                return { "status": 200, data: school, "message": "Data Fetched Successfully." };
            
        } catch (e) {
            return res.status(500).send({ message: e.message });
        }
    };
}

module.exports = new schoolController();
