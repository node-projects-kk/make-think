const avatarRepo = require('avatar/repositories/avatar.repository');
const userRepo = require('user/repositories/user.repository');
const roleRepo = require('role/repositories/role.repository');
const coinRepo = require('coin_transaction/repositories/coin_transaction.repository');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const gm = require('gm').subClass({
  imageMagick: true
});
const fs = require('fs');
const mongoose = require('mongoose');
var moment = require('moment');

class avatarController {
  constructor() {}

  /* @Method: avatarList
  // @Description: To list all the Avatars 
  */
  async avatarList(req, res) {
    try {
      let params = {
        "isDeleted": false,
        "status": "Active"
      }
      let avatar = await avatarRepo.getAllByField(params);
      let user_avatar = await userRepo.getById(req.user._id);
      // console.log(user_avatar);
      if (!_.isEmpty(avatar) && !_.isNull(avatar) && !_.isNull(user_avatar)) {
        avatar = await avatar.map(item => {
          item.is_buy = false;
          user_avatar.avatars.map((ind_avt) => {
            if (ind_avt.avatar_id.toString() === item._id.toString()) {
              item.is_buy = true;
            }
          });
          return item;
        })

        return {
          "status": 200,
          data: avatar,
          "message": "Data Fetched Successfully."
        };
      } else {
        return {
          status: 201,
          data: {},
          message: 'There are no data at this moment.'
        };
      }
    } catch (e) {
      return res.status(500).send({
        message: e.message
      });
    }
  }


  /* @Method: avatarDetail
  // @Description: To list all the Avatars 
  */
  async avatarDetail(req, res) {
    try {

      let avatar = await avatarRepo.getById(req.params.id);

      if (!_.isEmpty(avatar) && !_.isNull(avatar)) {
        return {
          "status": 200,
          data: avatar,
          "message": "Avatar Fetched Successfully."
        };
      } else {
        return {
          status: 201,
          data: {},
          message: 'There are no data at this moment.'
        };
      }
    } catch (e) {
      return res.status(500).send({
        message: e.message
      });
    }
  }

  /* @Method: userAvatarPurchase
  // @Description: To purchase Avatar for Student
  */
  async userAvatarPurchase(req, res) {
    try {
      const userDetails = await userRepo.getById(req.user._id);
      const roleDetails = await roleRepo.getById(userDetails.role);
      if (!_.isEmpty(userDetails) && roleDetails.role == 'student') {
        let user_total_balance = 0.00;
        if (userDetails.user_balance != null) {
          user_total_balance = userDetails.user_balance;
        }
        const avatarDetails = await avatarRepo.getById(req.body.avatar_id);
        if (!_.isEmpty(avatarDetails)) {

          let avatar_price = avatarDetails.purchase_coin;
          if (user_total_balance >= avatar_price) {
            req.body.purchase_date = Date.now();
            req.body.user_id = req.user._id;
            req.body.coins = avatar_price;

            let saveCoinTransaction = await coinRepo.save(req.body);
            let user_updated_balance = parseFloat(user_total_balance - avatar_price);
            let updateUserRepo = await userRepo.updateById({
              'user_balance': user_updated_balance,
              'profile_image': avatarDetails.image_1
            }, saveCoinTransaction.user_id);

            if (userDetails.avatars.length > 0) {
              var arr = [];
              for (var i = 0; i < userDetails.avatars.length; i++) {
                arr[i] = userDetails.avatars[i];
                arr[i]['isCurrent'] = false;
              }
              userDetails.avatars = arr;
              let updateExistingAvatar = await userRepo.updateById(userDetails, saveCoinTransaction.user_id);

              let avatarArray = [];
              avatarArray = userDetails.avatars;

              avatarArray.push({
                'avatar_id': avatarDetails._id,
                'isCurrent': true
              });

              let updateNewAvatarInExistingArray = await userRepo.updateById({
                'avatars': avatarArray
              }, saveCoinTransaction.user_id);

            } else {
              let addNewAvatar = [];
              addNewAvatar = {
                'avatar_id': avatarDetails._id,
                'isCurrent': true
              }
              let newAvatarSave = await userRepo.updateById({
                'avatars': addNewAvatar
              }, req.user._id);
            }
            if (saveCoinTransaction) {
              return {
                status: 200,
                data: saveCoinTransaction,
                message: 'You have successfully purchase the Avatar and your balance has been adjusted.'
              }
            } else {
              return {
                status: 201,
                data: [],
                message: 'There are some problem with the transaction, Please try again.'
              }
            }
          } else {
            return {
              status: 201,
              data: [],
              message: 'You dont have sufficient funds for this Avatar. Please try another Avatar.'
            }
          }
        } else {
          return {
            status: 201,
            data: [],
            message: 'We are unable to find the Avatar details, Please try again.'
          }
        }
      } else {
        return {
          status: 201,
          data: [],
          message: 'Only students can perform this action. Please try with students details.'
        }
      }
    } catch (e) {
      console.log('ererererere', e);
      return res.status(500).send({
        message: e.message
      });
    }
  }
}

module.exports = new avatarController();