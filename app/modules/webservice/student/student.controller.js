const userRepo = require('user/repositories/user.repository');
const roleRepo = require('role/repositories/role.repository');
const bookRepo = require('book/repositories/book.repository');
const userModel = require('user/models/user.model.js');
const User = new userModel();
const chapterRepo = require('chapter/repositories/chapter.repository');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const gm = require('gm').subClass({
    imageMagick: true
});
const fs = require('fs');
const jwt = require('jsonwebtoken');
const crypto = require("crypto");
//mail send 
const { join} = require('path');
const ejs = require('ejs');
const { readFile} = require('fs');
const { promisify} = require('util');
const readFileAsync = promisify(readFile);

class studentController {
	constructor() {
		this.student = {};
	}

	/* @Method: store contact us
	// @Description: To store contact us in  DB
	*/
	async store(req, res) {
	    try {
		let studentEmail = await userRepo.getByField({
		    'email': req.body.email
		});
    
		if (studentEmail) {
		    throw new Error("Email is already available.")
		} else {
		    let role = await roleRepo.getByField({
			'role': 'student'
		    });
		    req.body.role = role._id;
    
		    let User = new userModel();
		    req.body.password = User.generateHash(req.body.password);
    
		    let studentStore = await userRepo.save(req.body);
    
		    if (studentStore) {
    
			return {
			    "status": 200,
			    data: studentStore,
			    "message": "Student Successfully regsitered."
			};
    
		    } else {
			throw new Error('something went wrong.');
		    }
		}
	    } catch (e) {
		throw error
	    }
	};


	/* @Method: profile
	// @Description: Parent profile
	*/
	async profile(req, res) {
	}

	async changePassword(req, res) {
		try {
			const userData = await userRepo.getById(req.user._id);
			if (!_.isEmpty(userData) && userData.isActive == true && userData.isDeleted == false) {
				if ((!User.validPassword(req.body.old_password, userData.password))) {
					return { status: 201, data: [], "message": 'Wrong current password' };
				}
				else {
					const newPassword = User.generateHash(req.body.new_password);
					let updatedUser = await userRepo.updateById({password: newPassword}, req.user._id);
					if (updatedUser) {
						return {status: 200,data: updatedUser,message: 'Password Changed Successfully'}
					}
				}	
			}
			else {
				return {status: 201,data: [],	message: 'User not Found'};
			}
		}
		catch (e) {
			return res.status(500).send({message: e.message});
		}
	}

    async studentDashboardBooks(req, res) {
        try {
            var books = await bookRepo.getBooksFromEachGrade();
            //console.log(books);
            if (!_.isEmpty(books)) {
                return {
                    status: 200,
                    data: books,
                    message: 'Books fetched successfully'
                }
            } else {
                return {
                    status: 500,
                    data: [],
                    message: 'Books not Found'
                };
            }

        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    }

    async updateProfile(req, res) {
        try {
            var student_id = req.user._id;
            let student = await userRepo.updateById(req.body, student_id);

            return {
                "status": 200,
                data: student,
                "message": "Profile Updated Successfully"
            };

        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };


    async StudentBookDetails(req, res) {
        try {
            const userDetails = await userRepo.getUserRoleandDetails(req.user._id);
            if (userDetails.role.role == 'student') {
                const getBookDetails = await chapterRepo.getBooksChapterAndPageDetails(req.params.id);
                if (!_.isEmpty(getBookDetails)) {
                    return {
                        status: 200,
                        data: getBookDetails,
                        message: 'Book details fetched successfully.'
                    }
                } else {
                    return {
                        status: 201,
                        data: [],
                        message: 'There are no data at this moment.'
                    }
                }
            } else {
                return {
                    status: 201,
                    data: [],
                    message: 'You are not a Student. Only Students can access this page.'
                }
            }
        } catch (error) {
            return {
                status: 500,
                data: [],
                message: error.message
            }
        }
    }

}

module.exports = new studentController();