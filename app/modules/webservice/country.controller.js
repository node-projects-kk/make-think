const countryRepo = require('country/repositories/country.repository');


class countryController {
    constructor() {
        this.country = {};
    }

    /* @Method: Get country list
   // @Description: ***
   */
    async list(req, res) {
        try {
            let country = await countryRepo.getAllByField({ "isDeleted": false, "isActive": true });
            if (country && country.length > 0) {
                return { "status": 200, data: country, "message": "Data Fetched Successfully." };
            } else {
                return { status: 500, data: {}, message: `something went wrong.` };
            }
        } catch (e) {
            return res.status(500).send({ message: e.message });
        }
    };
}

module.exports = new countryController();
