const bookRepo = require('book/repositories/book.repository');
const chapterRepo = require('chapter/repositories/chapter.repository');
const questionRepo = require('question/repositories/question.repository');
const activityRepo = require('activities/repositories/activity.repository');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const gm = require('gm').subClass({
    imageMagick: true
});
const fs = require('fs');
const mongoose = require('mongoose');


class bookController {
    constructor() {
        this.book = {};
    }

    async list(req, res) {
        try {
            let book = await bookRepo.getAllBookByField({
                "status": "Active"
            });

            if (book) {
                return {
                    "status": 200,
                    data: book,
                    "message": "Data Fetched Successfully."
                };
            } else {
                return {
                    status: 500,
                    data: {},
                    message: `Something went wrong.`
                };
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    async bookDetails(req, res) {
        try {
            const book = await bookRepo.getBookWithChapterDetails(req.params.id);

            let UserHaveAnyActivity = await activityRepo.getAllByField({
                'book_id': req.params.id,
                'student_id': req.user._id
            });

            //checking for if book chapter all completed by student book[0].chapters = 
            let UserCompletedActivity = await activityRepo.getAllByField({
                'book_id': req.params.id,
                'student_id': req.user._id,
                'activity_type': 'completed'
            });

            book[0].chapters = book[0].chapters.map((item, index) => {
                const activity = UserHaveAnyActivity.filter(activity_item => activity_item.chapter_id.toString() == item._id.toString());
                if (activity.length > 0) {
                    item.activity_info = activity[0];
                }
                return item;
            });

            if (book[0].chapters.length > 0 && book[0].chapters.length == UserCompletedActivity.length) {
                return {
                    "status": 200,
                    data: [],
                    "message": "All chapters are completed."
                };
            }
            //end section

            if (UserHaveAnyActivity.length == 0) {
                for (var key in book[0].chapters) {
                    if (key == 0) {
                        book[0].chapters[key].isCurrentChapter = true;
                    } else {
                        book[0].chapters[key].isCurrentChapter = false;
                    }
                }
                if (book) {
                    return {
                        "status": 200,
                        data: book,
                        "message": "Data Fetched Successfully."
                    };
                } else {
                    return {
                        status: 201,
                        data: {},
                        message: 'There are no data at this moment.'
                    };
                }
            } else {
                let lastIndex = (UserHaveAnyActivity.length - 1);
                let lastRecord = UserHaveAnyActivity[lastIndex];
                book[0].chapters.map(async function (item) {
                    if (lastRecord.chapter_id.toString() == item._id.toString() && lastRecord.activity_type == 'ongoing') {
                        item.isCurrentChapter = true;
                    } else {
                        item.isCurrentChapter = false;
                    }
                });
                if (lastRecord.activity_type == 'completed') {
                    book[0].chapters[lastIndex + 1]['isCurrentChapter'] = true;
                }
                return {
                    "status": 200,
                    data: book,
                    "message": "Data Fetched Successfully."
                };
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    async bookByKeyword(req, res) {
        try {
            let book = await bookRepo.getAllBookByKeyword(req.params.keyword);

            if (book.length > 0) {
                return {
                    "status": 200,
                    data: book,
                    "message": "Data Fetched Successfully."
                };
            } else {
                return {
                    status: 201,
                    data: {},
                    message: 'No books available.'
                };
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    async questionByChapter(req, res) {
        try {
            let questions = await questionRepo.getAllQuestions(req.params.chapter_id);
            if (questions) {
                return {
                    "status": 200,
                    data: questions,
                    "message": "Question Fetched Successfully."
                };
            } else {
                return {
                    status: 500,
                    data: {},
                    message: `Something went wrong.`
                };
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    async getBooksByGrade(req, res) {
        try {
            let bookInfo = await bookRepo.getAllBookByGrade(req.params.keyword);
            if (bookInfo.length > 0) {
                return {
                    "status": 200,
                    data: bookInfo,
                    "message": "Data Fetched Successfully."
                };
            } else {
                return {
                    status: 201,
                    data: {},
                    message: 'No books available.'
                };
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

}

module.exports = new bookController();