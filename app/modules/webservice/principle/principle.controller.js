const userRepo = require('user/repositories/user.repository');
const roleRepo = require('role/repositories/role.repository');
const schoolRepo = require('school/repositories/school.repository');
const activityRepo = require('activities/repositories/activity.repository');
const classRepo = require('class/repositories/class.repository');
const paymentRepo = require('payment/repositories/payment.repository');
const userModel = require('user/models/user.model.js');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const gm = require('gm').subClass({
    imageMagick: true
});
const fs = require('fs');
const jwt = require('jsonwebtoken');
const crypto = require("crypto");
const mongoose = require('mongoose');
var stripe = require("stripe")(config.stripe_secret_key);

//mail send 
const {
    join
} = require('path');
const ejs = require('ejs');
const {
    readFile
} = require('fs');
const {
    promisify
} = require('util');
const readFileAsync = promisify(readFile);


class principleController {
    constructor() {
        this.principle = {};
    }

    /* @Method: store contact us
   // @Description: To store contact us in  DB
   */
    async store(req, res) {
        try {
            let email = await userRepo.getByField({
                'email': req.body.email
            });

            if (email) {
                return {
                    "status": 200,
                    data: {},
                    "message": "Email is already available."
                };
            } else {
                let role = await roleRepo.getByField({
                    'role': 'principal'
                });
                req.body.role = role._id;

                let User = new userModel();
                req.body.password = User.generateHash(req.body.password);
                //otp generate
                let OTP = Math.floor(1000 + Math.random() * 9000);
                req.body.verifyToken = OTP;

                if (req.body.school_name) {
                    let school = await schoolRepo.save(req.body);
                    req.body.school = school._id;
                }


                let principleStore = await userRepo.save(req.body);

                if (principleStore) {

                    //verify mail send
                    const templateData = {
                        token: OTP
                    };
                    const ejsTemplate = await readFileAsync(join(__dirname, '../../../', 'views/templates/email.sendVerificationLink.ejs'));
                    const html = ejs.render(ejsTemplate.toString(), {
                        data: templateData
                    });
                    await transporter.sendMail({
                        from: `Admin<${process.env.MAIL_USERNAME}>`,
                        to: req.body.email,
                        subject: 'New Principle | Make Thinkers',
                        html: html
                    });
                    return {
                        "status": 200,
                        data: principleStore,
                        "message": "Verification email sent. Please check your inbox for our verification code."
                    };

                } else {

                    return {
                        status: 500,
                        data: {},
                        message: `something went wrong.`
                    };
                }
            }
        } catch (e) {


            return {
                status: 500,
                data: {},
                message: e.message
            };
        }
    };

    /* @Method: profile
    // @Description: Parent profile
    */
    async profile(req, res) {

    }

    async changePassword(req, res) {
        try {
            let userData = await userRepo.getById(req.user._id);

            if (!_.isEmpty(userData) && userData.isActive == true && userData.isDeleted == false) {
                if (userData.validPassword(req.body.old_password, userData.password)) {
                    let newPassword = userData.generateHash(req.body.new_password);
                    let updatedUser = await userRepo.updateById({
                        password: newPassword
                    }, req.user._id);
                    if (updatedUser) {
                        return {
                            status: 200,
                            data: updatedUser,
                            message: 'Password Changed Successfully'
                        }
                    }
                } else {
                    return {
                        status: 500,
                        data: [],
                        message: 'Wrong Current Password'
                    };
                }
            } else {
                return {
                    status: 500,
                    data: [],
                    message: 'User not Found'
                };
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }

    }

    async editProfilePrincliple(req, res) {
        try {

            let role = await roleRepo.getByField({
                'role': 'principal'
            });
            req.body.role = role._id;


            if (req.body.school_name) {
                let school = await schoolRepo.updateById(req.body, req.body.school_id);
                req.body.school = school._id;
            }


            let principleStore = await userRepo.updateById(req.body, req.body.id);

            return {
                "status": 200,
                data: principleStore,
                "message": "Profile Updated Successfully"
            };


        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
            // return { status: 500, data: {}, message: e.message };
        }
    }

    async getAllGradesAndCount(req, res) {
        try {
            const result = await userRepo.getStudentCountByGrade();
            if (!_.isEmpty(result) && !_.isNull(result)) {
                return {
                    status: 200,
                    data: result,
                    message: 'Data fetched successfully.'
                }
            } else {
                return {
                    status: 201,
                    data: [],
                    message: 'No data found at this moment.'
                }
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    }

    async getStudentsAndTeacherBySchool(req, res) {
        try {
            const getUserDetails = await userRepo.getById(req.user._id);
            const StudentResult = await userRepo.getStudentsBySchool(getUserDetails.school);
            const TeacherResult = await userRepo.getTeachersBySchool(getUserDetails.school);
            let result = {};
            result.student_list = StudentResult;
            result.teacher_list = TeacherResult;
            if (!_.isEmpty(result)) {
                return {
                    status: 200,
                    data: result,
                    message: 'Data fetched successfully.'
                }
            } else {
                return {
                    status: 201,
                    data: [],
                    message: 'No data found at this moment.'
                }
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    }

    async getStudentsActivityList(req, res) {
        try {
            const result = await activityRepo.getStudentsActivityListByGrade(req.params.grade);
            if (!_.isEmpty(result) && !_.isNull(result)) {
                return {
                    status: 200,
                    data: result,
                    message: 'Students activity fetched successfully.'
                }
            } else {
                return {
                    status: 201,
                    data: [],
                    message: 'No Students activity found at this moment.'
                }
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    }

    async getAllClasses(req, res) {
        try {
            let getUserInfo = await userRepo.getById(req.user._id);
            let checkUserType = await roleRepo.getById(getUserInfo.role);
            let userSchoolInfo = await schoolRepo.getById(getUserInfo.school);
            if (checkUserType.role == 'principal' || checkUserType.role == 'teacher') {
                let allClassInfo = await classRepo.getAllByField({
                    'school_id': userSchoolInfo._id,
                    'status': 'Active',
                    'isDeleted': false
                });
                if (allClassInfo.length > 0) {
                    return {
                        status: 200,
                        data: allClassInfo,
                        message: 'All Class fetched successfully.'
                    }
                } else {
                    return {
                        status: 201,
                        data: [],
                        message: 'There are no data at this moment.'
                    }
                }
            } else {
                return {
                    status: 201,
                    data: [],
                    message: 'Sorry, only Principle or Teacher can view this data.'
                }
            }
        } catch (error) {
            console.log('ererererer', error);
            return res.status(500).send({
                message: error.message
            });
        }
    }

    async getAllTeachers(req, res) {
        try {
            let getUserInfo = await userRepo.getById(req.user._id);
            let checkUserType = await roleRepo.getById(getUserInfo.role);
            if (checkUserType.role == 'principal') {
                let getAllTeacher = await userRepo.getTeachersBySchool(getUserInfo.school);
                if (getAllTeacher.length > 0) {
                    return {
                        status: 200,
                        data: getAllTeacher,
                        message: 'All Teacher fetched successfully.'
                    }
                } else {
                    return {
                        status: 201,
                        data: [],
                        message: 'There are no data at this moment.'
                    }
                }
            } else {
                return {
                    status: 201,
                    data: [],
                    message: 'Sorry, only Principle can view this data.'
                }
            }
        } catch (error) {
            console.log('ererererer', e);
            return res.status(500).send({
                message: error.message
            });
        }
    }

    async saveTeacherClassWise(req, res) {
        try {
            let getUserInfo = await userRepo.getById(req.user._id);
            let checkUserType = await roleRepo.getById(getUserInfo.role);
            if (checkUserType.role == 'principal') {
                let saveTeacherByClass = await classRepo.updateById({
                    'teacher_id': req.body.teacher_id
                }, req.body.class_id);
                if (saveTeacherByClass) {
                    return {
                        status: 200,
                        data: saveTeacherByClass,
                        message: 'Teacher assigned successfully.'
                    }
                } else {
                    return {
                        status: 201,
                        data: [],
                        message: 'There are some problem at this moment.'
                    }
                }
            } else {
                return {
                    status: 201,
                    data: [],
                    message: 'Sorry, only Principle can view this data.'
                }
            }
        } catch (error) {
            console.log('ererererer', e);
            return res.status(500).send({
                message: error.message
            });
        }
    }

    async studentTeacherActivities(req, res) {
        try {
            if (req.body.student_id != '') {
                let activities = await activityRepo.acitvitiesbyClassId(req.params.classid, req.body.student_id);
                let teacherInfo = await userRepo.getById(req.body.teacher_id);
                if (!_.isEmpty(activities)) {
                    activities.forEach(function (element) {
                        element.t_info = teacherInfo;
                    });
                    return {
                        "status": 200,
                        data: activities,
                        "message": "Activities fetched successfully."
                    };
                } else {
                    return {
                        status: 201,
                        data: [],
                        message: 'No Students found at this moment.'
                    }
                }
            } else {
                return {
                    status: 201,
                    data: [],
                    message: 'There are no student for this class.'
                }
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    async childDeleteFromPrinciple(req, res) {
        try {
            let princliple_id = req.user._id;
            let class_id = req.body.class_id;
            var student_id = req.body.student_id;

            let userInfo = await userRepo.getById(princliple_id);

            if (!_.isEmpty(userInfo)) {
                var school_id = userInfo.school;
            }
            let classInfo = await classRepo.getById(class_id);

            if (!_.isEmpty(classInfo)) {
                var class_school_id = classInfo.school_id;
            }

            if (_.isEqual(school_id, class_school_id)) {
                var getValFromPayment = await paymentRepo.getByField({
                    "child": mongoose.Types.ObjectId(student_id)
                });
                if (!_.isEmpty(getValFromPayment)) {
                    var subscription_id = getValFromPayment.subscription_id;
                    var payment_id = getValFromPayment._id;
                    var result = await stripe.subscriptions.del(subscription_id);
                    if (result) {
                        var updateVal = {
                            subscription_id: ""
                        };
                        let updateResult = await paymentRepo.updateById(updateVal, payment_id);
                    }
                }
                let deleteChild = await classRepo.deleteChildFromPrinciple(class_id, student_id);
                if (!deleteChild) {
                    return {
                        status: 201,
                        data: [],
                        message: 'Child not deleted'
                    }
                } else {
                    return {
                        status: 200,
                        data: deleteChild,
                        message: 'Child delete successfully.'
                    }
                }
            } else {
                return {
                    status: 200,
                    data: [],
                    message: 'Class and school not matched.'
                }
            }
        } catch (error) {
            return res.status(500).send({
                message: error.message
            });
        }
    }

}

module.exports = new principleController();