const userRepo = require('user/repositories/user.repository');
const roleRepo = require('role/repositories/role.repository');
const activityRepo = require('activities/repositories/activity.repository');
const classRepo = require('class/repositories/class.repository');
const coinRepo = require('coins/repositories/coins.repository');
const schoolRepo = require('school/repositories/school.repository');
const paymentRepo = require('payment/repositories/payment.repository');
const userModel = require('user/models/user.model.js');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const gm = require('gm').subClass({
    imageMagick: true
});
const fs = require('fs');
const jwt = require('jsonwebtoken');
const crypto = require("crypto");
const mongoose = require('mongoose');
const moment = require('moment');

//mail send 
const {
    join
} = require('path');
const ejs = require('ejs');
const {
    readFile
} = require('fs');
const {
    promisify
} = require('util');
const readFileAsync = promisify(readFile);


class teacherController {
    constructor() {
        this.teacher = {};
    }

    /* @Method: store contact us
    // @Description: To store contact us in  DB
    */
    async store(req, res) {
        try {
            let teacherEmail = await userRepo.getByField({
                'email': req.body.email
            });

            if (teacherEmail) {
                throw new Error("Email is already available.");
            } else {
                let role = await roleRepo.getByField({
                    'role': 'teacher'
                });
                req.body.role = role._id;

                let User = new userModel();
                req.body.password = User.generateHash(req.body.password);
                //otp generate
                let OTP = Math.floor(1000 + Math.random() * 9000);
                req.body.verifyToken = OTP;

                let teacherStore = await userRepo.save(req.body);

                if (teacherStore) {

                    //verify mail send
                    const templateData = {
                        token: OTP
                    };
                    const ejsTemplate = await readFileAsync(join(__dirname, '../../../', 'views/templates/email.sendVerificationLink.ejs'));
                    const html = ejs.render(ejsTemplate.toString(), {
                        data: templateData
                    });
                    await transporter.sendMail({
                        from: `Admin<${process.env.MAIL_USERNAME}>`,
                        to: req.body.email,
                        subject: 'New Teacher | Make Thinkers',
                        html: html
                    });


                    return {
                        "status": 200,
                        data: teacherStore,
                        "message": "Verification email sent. Please check your inbox for our verification code."
                    };

                } else {

                    throw new Error(`something went wrong.`);
                }
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
            // return { status: 500, data: {}, message: e.message };
        }
    };

    /* @Method: profile
    // @Description: Parent profile
    */
    async profile(req, res) {};

    async changePassword(req, res) {
        try {
            let userData = await userRepo.getById(req.user._id);

            if (!_.isEmpty(userData) && userData.isActive == true && userData.isDeleted == false) {
                if (userData.validPassword(req.body.old_password, userData.password)) {
                    let newPassword = userData.generateHash(req.body.new_password);
                    let updatedUser = await userRepo.updateById({
                        password: newPassword
                    }, req.user._id);
                    if (updatedUser) {
                        return {
                            status: 200,
                            data: updatedUser,
                            message: 'Password Changed Successfully'
                        }
                    }
                } else {
                    return {
                        status: 500,
                        data: [],
                        message: 'Wrong Current Password'
                    };
                }
            } else {
                return {
                    status: 500,
                    data: [],
                    message: 'User not Found'
                };
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }

    };

    async teacherBySchool(req, res) {
        try {
            let role = await roleRepo.getByField({
                'role': 'teacher'
            });
            let teacherList = await userRepo.getAllByField({
                'school': req.params.id,
                role: role._id
            });
            return {
                status: 200,
                data: teacherList,
                message: 'Teacher list fetched Successfully'
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }

    };

    async editProfileTeacher(req, res) {
        try {

            let teacherStore = await userRepo.updateById(req.body, req.body.id);

            return {
                "status": 200,
                data: teacherStore,
                "message": "Profile Updated Successfully"
            };


        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    async studentsActivities(req, res) {
        try {
            if (req.body.student_id != '') {
                let activities = await activityRepo.acitvitiesbyClassId(req.params.classid, req.body.student_id);
                if (!_.isEmpty(activities)) {
                    return {
                        "status": 200,
                        data: activities,
                        "message": "Activities fetched successfully."
                    };
                } else {
                    return {
                        status: 201,
                        data: [],
                        message: 'No Students found at this moment.'
                    }
                }
            } else {
                return {
                    status: 201,
                    data: [],
                    message: 'There are no student for this class.'
                }
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    async studentActivityDetails(req, res) {
        try {

            let activities = await activityRepo.acitvityDetails(req.params.activity_id);

            return {
                "status": 200,
                data: activities,
                "message": "Activity fetched"
            };


        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    async AllStudentList(req, res) {
        try {
            const getUserDetails = await userRepo.getById(req.user._id);
            const getAllStudents = await userRepo.getStudentsBySchool(getUserDetails.school);
            if (!_.isEmpty(getAllStudents)) {
                return {
                    status: 200,
                    data: getAllStudents,
                    message: 'Students fetched successfully.'
                }
            } else {
                return {
                    status: 200,
                    data: getAllStudents,
                    message: 'There are no data at this moment.'
                }
            }
        } catch (error) {
            return res.status(500).send({
                message: error.message
            });
        }
    }

    async get_class_by_teacher(req, res) {
        try {
            const ClassDetails = await classRepo.getByField({
                teacher_id: req.user._id
            });
            if (!_.isEmpty(ClassDetails)) {
                return {
                    status: 200,
                    data: ClassDetails,
                    message: 'Class fetched successfully.'
                }
            } else {
                return {
                    status: 200,
                    data: ClassDetails,
                    message: 'There are no data at this moment.'
                }
            }
        } catch (error) {
            return res.status(500).send({
                message: error.message
            });
        }
    };

    async saveStudentClassWise(req, res) {
        try {
            let getUserInfo = await userRepo.getById(req.user._id);
            let checkUserType = await roleRepo.getById(getUserInfo.role);
            if (checkUserType.role == 'teacher') {
                let checkHasStudent = await classRepo.getById(req.body.class_id);
                checkHasStudent.student_id.push(req.body.student_id);
                let saveStudentByClass = await classRepo.updateById(checkHasStudent, req.body.class_id);
                let update_teacher_on_student = await userRepo.updateById({
                    teacher_id: req.user._id
                }, req.body.student_id);
                if (saveStudentByClass) {
                    return {
                        status: 200,
                        data: saveStudentByClass,
                        message: 'Student assigned successfully.'
                    }
                } else {
                    return {
                        status: 201,
                        data: [],
                        message: 'There are some problem at this moment.'
                    }
                }
            } else {
                return {
                    status: 201,
                    data: [],
                    message: 'Sorry, only Teacher can view this data.'
                }
            }
        } catch (error) {
            console.log('wererererererer', error);
            return res.status(500).send({
                message: error.message
            });
        }
    }

    async StudentSubmitByTeacher(req, res) {
        try {
            let students = req.body.students_id;
            let teacher_id = req.user._id;
            if (students != '' && students != null) {
                for (var id in students) {
                    let updateUserTeacher = await userRepo.updateById({
                        'teacher_id': teacher_id
                    }, students[id]);
                }
                return {
                    status: 200,
                    data: [],
                    message: 'Students added successfully.'
                }
            } else {
                return {
                    status: 201,
                    data: [],
                    message: 'Before you submit, please select students.'
                }
            }
        } catch (error) {
            return res.status(500).send({
                message: error.message
            });
        }
    }

    async TeacherMyStudentList(req, res) {
        try {
            const getMyStudents = await userRepo.getStudentsByTeacher(req.user._id);
            if (!_.isEmpty(getMyStudents)) {
                return {
                    status: 200,
                    data: getMyStudents,
                    message: 'Your students fetched successfully.'
                }
            } else {
                return {
                    status: 201,
                    data: [],
                    message: 'There are no data at this moment.'
                }
            }
        } catch (error) {
            return res.status(500).send({
                message: error.message
            });
        }
    }

    async childListByTeacher(req, res) {
        try {
            const currentPage = req.query.page || 1;
            const getTeacherInfo = await userRepo.getById(req.user._id);
            const {
                data,
                pageCount,
                totalCount
            } = await classRepo.getChildByTearcher(getTeacherInfo._id, getTeacherInfo.school, currentPage);

            if (!_.isEmpty(data)) {
                return {
                    status: 200,
                    data,
                    current: parseInt(currentPage),
                    pages: pageCount,
                    total: totalCount,
                    message: 'Child fetched successfully.'
                }
            } else {
                return {
                    status: 201,
                    data: [],
                    message: 'There are no data at this moment.'
                }
            }
        } catch (error) {
            return res.status(500).send({
                message: error.message
            });
        }
    }

	async childDeleteByTeacher(req, res) {
		try {
			const childId = req.params.id;
			const teacherId = req.user._id;
			
			var getValFromPayment = await paymentRepo.getByField({"child":mongoose.Types.ObjectId(childId)});
			if (!_.isEmpty(getValFromPayment)) {
				var subscription_id = getValFromPayment.subscription_id;
				var payment_id = getValFromPayment._id;
				var result = await stripe.subscriptions.del(subscription_id);
				if (result) {
					var updateVal = {	subscription_id : ""};
					let updateResult = await paymentRepo.updateById(updateVal,payment_id);
				}
			}
			
			let deleteChild = await classRepo.deleteChildByTearcher(childId, teacherId);
			if (!deleteChild) {
				return {status: 201,data: [],	message: 'Child not deleted'}
			}
			else {
				return {status: 200,data: deleteChild,message: 'Child delete successfully.'}
			}
		}
		catch (error) {
			console.log('ererererer', error);
			return res.status(500).send({message: error.message});
		}
	}

	
	async getPerformanceGraph(req,res){
		try{
			var janArr = [];
			var marArr =[];
			var newObj = {};
			var current_month = moment().format('YYYY-MM-DD');
			var six_month = moment().subtract(12, 'months').format('YYYY-MM-DD');
			const _teacherId = req.user._id;
			const _classId = req.body.class_id;
			const _classDetails = await classRepo.getById(mongoose.Types.ObjectId(_classId));
			if (!_.isEmpty(_classDetails)) {
				const _students = _classDetails.student_id;
				if (!_.isEmpty(_students)) {
					const _noOfStudent = _students.length;
					let coinStudent = await coinRepo.coindetails(_students,current_month,six_month);
					var c = [];
					var monthArr = {};
					var arr1 = [];
					var arr3 =[];
					var totalCoin  = '';
					var getMonth = '';
					var getName = '';
					var finalData = '';
					
					for(var j=0;j<coinStudent.length;j++){
						arr1.push(coinStudent[j].name);
					}
					arr3 = _.uniq(arr1);
					
					for(var i=0;i<coinStudent.length;i++){	
						getName = coinStudent[i].name;
						totalCoin = coinStudent[i].totalcoin; 
						getMonth = coinStudent[i].month;
						if (!monthArr[getMonth]) {
							monthArr[getMonth] = {};
						}
						
						if (_.has(monthArr,getMonth) && !_.isEmpty(monthArr[getMonth])) {
							monthArr[getMonth]['total']  = monthArr[getMonth]['total'] +totalCoin;
						}
						else {
							monthArr[getMonth]['total'] = totalCoin;	
						}
						
						for(var u=0;u<arr3.length;u++){
							monthArr[getMonth][getName] = totalCoin;
							if (!_.has(monthArr[getMonth],arr3[u]) && !_.isEmpty(monthArr[getMonth])) {
								monthArr[getMonth][arr3[u]] = 0;
							}
						}
						monthArr[getMonth]['month'] = getMonth;
					}
					
					finalData = Object.values(monthArr);
					if (finalData) {
						return{status:200,message:"record fetch",data:finalData}
					}
					else{
						return{status:201,message:"record not found",data:[]}
					}
				}
			}
			else{
				return{status:201,message:"User not found",data:{}};
			}
		}
		catch(error){
			console.log('ererererer', error);
			return res.status(500).send({message: error.message});
		}
	}
}

module.exports = new teacherController();