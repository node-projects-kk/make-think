const planRepo = require('plan/repositories/plan.repository');
const userRepo = require('user/repositories/user.repository');
const roleRepo = require('role/repositories/role.repository');
const userModel = require('user/models/user.model.js');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const gm = require('gm').subClass({
  imageMagick: true
});
const fs = require('fs');
const mongoose = require('mongoose');
var stripe = require("stripe")(config.stripe_secret_key);
var moment = require('moment');


class planController {
	constructor() {}

	async planList(req, res) {
		try {
			const plans = await planRepo.getAllByField({"isDeleted":false});
			if (!_.isEmpty(plans)) {
				return {	status: 200,	message: 'Plan list fetched successfully', data:plans}
			}
			else {
				return {	status: 201,	message: "No record found", data: []}
			}
		}
		catch (e) {
			return {status: 500,data: [],	message: e.message}
		}
	};	
}
module.exports = new planController();