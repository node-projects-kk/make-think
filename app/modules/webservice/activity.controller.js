const bookRepo = require('book/repositories/book.repository');
const chapterRepo = require('chapter/repositories/chapter.repository');
const questionRepo = require('question/repositories/question.repository');
const userRepo = require('user/repositories/user.repository');
const coinRepo = require('coins/repositories/coins.repository');
const activityRepo = require('activities/repositories/activity.repository');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const mailer = require('../../helper/mailer.js');
const gm = require('gm').subClass({
  imageMagick: true
});
const fs = require('fs');
const mongoose = require('mongoose');


class activityController {
  constructor() { }

  /* @Method: activityStart
  // @Description: just started
  */
  async activityStart(req, res) {
    try {
      req.body.student_id = req.user._id;
      const getChapterDetails = await chapterRepo.getById(req.body.chapter_id);
      req.body.book_id = getChapterDetails.book_id;
      req.body.chapter_pages = [];
      let temp = {};
      getChapterDetails.pages.map(function (page) {
        temp = {
          _id: page._id,
          status: 'not_started',
          initialdate: Date.now(),
          ongoingdate: Date.now(),
          completeddate: Date.now()
        }
        req.body.chapter_pages.push(temp);
      });
      const ActivityExists = await activityRepo.getByField({
        'student_id': req.body.student_id,
        'chapter_id': req.body.chapter_id
      });

      if (ActivityExists == null) {
        const startActivity = await activityRepo.save(req.body);
        if (startActivity) {
          return {
            status: 200,
            data: startActivity,
            message: 'Started successfully.'
          }
        } else {
          return {
            status: 201,
            data: [],
            message: 'There are some problem at this moment.'
          }
        }
      } else {
        return {
          status: 201,
          data: [],
          message: 'This activity already exits.'
        }
      }
    } catch (e) {
      return res.status(500).send({
        message: e.message
      });
    }
  };

  async activityOngoing(req, res) {
    try {
      const getActivityDetails = await activityRepo.getByField({
        'chapter_id': req.body.chapter_id,
        'student_id': req.user._id
      });
      let ongoingDateTime = Date.now();
      if (req.body.ongoing_page_id != null) {
        req.body.ongoing_page_id.map(async function (pid) {
          const updateOngoingval = await activityRepo.objectUpdate({
            'chapter_pages.$.status': 'ongoing',
            'chapter_pages.$.ongoingDate': ongoingDateTime,
          }, pid, req.user._id);
        });
      }

      let completedDateTime = Date.now();
      if (req.body.complete_page_id != null) {
        req.body.complete_page_id.map(async function (complete_pid) {
          const updateCompleteval = await activityRepo.objectUpdate({
            'chapter_pages.$.status': 'completed',
            'chapter_pages.$.completedDate': completedDateTime,
          }, complete_pid, req.user._id);
        });
      }
      return {
        status: 200,
        data: {},
        message: 'Updated successfully.'
      }
    } catch (error) {
      return res.status(500).send({
        message: error.message
      });
    }
  }

	async activityEnd(req, res) {
		try {
			const getActivityDetails = await activityRepo.getByField({'student_id': req.user._id,'chapter_id': req.body.chapter_id});
			const updateActivity = await activityRepo.updateById({'activity_type': 'completed','completedAt': Date.now()}, getActivityDetails._id);
			if (!_.isEmpty(updateActivity)) {
				return {	status: 200,	data: [],message: 'SUccessfully complete the chapter.'}
			}
			else {
				return {	status: 201,	data: [],message: 'Please complete the Chapter.'}
			}
		}
		catch (error) {
			console.log("126>>",error.message);
			return res.status(500).send({message: error.message});
		}
	}
	
	async provideCoinChild(req,res){
		try{
			const _userId = req.body.user_id;
			const _childId = req.body.child_id;
			const _provideCoin = req.body.coin;
			const _providerType = req.body.provider_type;
			
			let checkUserCoin = await userRepo.getById(mongoose.Types.ObjectId(_userId));
			if (!_.isEmpty(checkUserCoin)) {
				var _coinBalanceUser = parseInt(checkUserCoin.user_balance);
				if (parseInt(_coinBalanceUser) >= parseInt(_provideCoin)) {
					const _remainCoinBalanceUser = parseInt(_coinBalanceUser) -  parseInt(_provideCoin);
					let updateVal = {"user_balance":_remainCoinBalanceUser};
					var updateResultUser = await userRepo.updateById(updateVal,_userId);
					let checkChildCoin = await userRepo.getById(mongoose.Types.ObjectId(_childId));
					if (!_.isEmpty(checkChildCoin)) {
						var _coinBalanceChild = parseInt(checkChildCoin.user_balance);
						var _newCoinBalanceChild = parseInt(_coinBalanceChild) + parseInt(_provideCoin);
						var updateResultChild = await userRepo.updateById({"user_balance":parseInt(_newCoinBalanceChild)},_childId);
						if (updateResultUser && updateResultChild) {
							var _checkCoin = await coinRepo.getByField({"coin_to_user_id":_childId,"coin_from_user_id":_userId});
							if (!_.isEmpty(_checkCoin)) {
								var _id = _checkCoin._id;
								var _getPrevCoin = parseInt(_checkCoin.no_of_coins);
								var _currentCoins = parseInt(_getPrevCoin) + parseInt(_provideCoin);
								var updateCoins = await coinRepo.updateById({"no_of_coins":_currentCoins},_id);
							}
							else{
								var insertObj = {};
								insertObj.coin_to_user_id = _childId;
								insertObj.coin_from_user_id = _userId;
								insertObj.coin_given_date = Date.now();
								insertObj.provider_type = _providerType;
								insertObj.no_of_coins = _provideCoin;
								let insertResult = await coinRepo.save(insertObj);
							}
							return {status:200,message:"Coin is provided to child", data:{"parent":updateResultUser,"child":updateResultChild}}
						}
					}
					else{
						return {status:201,message:"There is no child", data:{}}
					}
				}
				else{
					return {status:201,message:"You don't have sufficient coins to give",data:{}}
				}
			}
			else{
				return {status:201,message:"There is no user", data:{}}
			}
		}
		catch(error){
			console.log(error.message);
			return res.status(500).send({message: error.message});
		}
	}

}

module.exports = new activityController();