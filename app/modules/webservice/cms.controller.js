const cmsRepo = require('cms/repositories/cms.repository');

class cmsController {
    constructor() {
        this.cms = {};        
    }

     /* @Method: store contact us
    // @Description: To store contact us in  DB
    */
    async list (req, res){
        try
        {
            let cms = await cmsRepo.getByField({"slug": req.params.slug});
            if(cms){
                  return { "status": 200,data: cms, "message": "Data Fetched Successfully." };
             }else{
                return {status: 500,data:{},message: `something went wrong.`};
            }
	    }catch(e){
	        return res.status(500).send({message: e.message}); 
	    }
    };
}

module.exports = new cmsController();
