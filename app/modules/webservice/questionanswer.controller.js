const userRepo = require('user/repositories/user.repository');
const roleRepo = require('role/repositories/role.repository');
const QARepo = require('question_answer/repositories/question_answer.repository');
const activityRepo = require('activities/repositories/activity.repository');
const bookRepo = require('book/repositories/book.repository');
const userModel = require('user/models/user.model.js');
const express = require('express');
const routeLabel = require('route-label');
const helper = require('../../helper/helper.js');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const gm = require('gm').subClass({imageMagick: true});
const fs = require('fs');
const jwt = require('jsonwebtoken');
const crypto = require("crypto");
//mail send 
const { join} = require('path');
const ejs = require('ejs');
const { readFile} = require('fs');
const { promisify} = require('util');
const readFileAsync = promisify(readFile);
const mongoose = require('mongoose');
const mailer = require('../../helper/mailer.js');

class questionAnswerController {

	constructor() {}

	async saveUserQA(req, res) {
		try {
			var newArr =[];
			let userId = req.user._id;
			req.body.user_id = userId;
			if (req.files) {
				for (let i = 0; i < req.files.length; i++) {
					for (let j = 0; j < req.body.qadetails.length; j++) {
						if (req.body.qadetails[j].answer_type == 'drawing' && _.isUndefined(req.body.qadetails[j].answer)) {
							req.body.qadetails[j].answer = req.files[i].filename;
							break;
						}
					}
				}
			}
			let userQAData = await QARepo.save(req.body);
			if (!_.isEmpty(userQAData)) {
				
				var _studentId = req.user._id; //console.log("121>>",_studentId);
				var _bookId = req.body.book_id;  //console.log("122>>",_bookId);
				var _chapterId = req.body.chapter_id; //console.log("123>>",_chapterId);
				var _userDetails = await userRepo.getByIdStudent(mongoose.Types.ObjectId(_studentId));
				
				if (!_.isEmpty(_userDetails)) {
					var _parentEmail = _userDetails.parent_id.email;
					var _parentId = _userDetails.parent_id._id;
					var _parentName = _userDetails.parent_id.first_name + (_userDetails.parent_id.last_name !="")?_userDetails.parent_id.last_name:'';
					var _childId = req.user._id;
					var _childName = _userDetails.first_name;
					
					let locals = {
						user_fullname: _parentName,
						parent_id: _parentId,
						site_title: '',
						child_id: _childId,
						child_name: _childName,
						link:process.env.FRONTEND_URL + 'parent-add-coins?parent='+_parentId+'&child='+_childId+'&type=chapter'
					};
					
					let isMailSendForChapter = await mailer.sendMail('Admin<testchapter@yopmail.com>',_parentEmail,`Coin Provided by ${_parentName}`,'provide-coin', locals);
					
					if (isMailSendForChapter) {
						let bookResult = await activityRepo.getAllByField({'chapter_id': req.body.chapter_id,'student_id':req.user._id});
						console.log("bookResult>>73>>", bookResult);
						if (!_.isEmpty(bookResult)) {
							let _bookId = bookResult[0].book_id;
							let bookDetails = await bookRepo.getById(mongoose.Types.ObjectId(_bookId));
							if (!_.isEmpty(bookDetails)) {
								var _noOfChapter = bookDetails.chapter_num;
							}
							for(var i=0;i<bookResult.length;i++){
								newArr.push(bookResult[i].activity_type);
							}
							//console.log("newArr>>82>>", newArr);
							//console.log("newArr.length>>83>>", newArr.length);
							//console.log("_noOfChapter>>84>>", _noOfChapter);
							//console.log("newArr.indexOf('ongoing')>>85>>", newArr.indexOf("ongoing"));
							if (newArr.length == _noOfChapter && newArr.indexOf("ongoing") == -1 ) {  //process.exit();
								let locals = {
									user_fullname: _parentName,
									parent_id: _parentId,
									site_title: '',
									child_id: _childId,
									child_name: _childName,
									link:process.env.FRONTEND_URL + 'parent-add-coins?parent='+_parentId+'&child='+_childId+'&type=book'
								};
								let isMailSendForBook = await mailer.sendMail('Admin<testbook@yopmail.com>',_parentEmail,`Coin Provided by ${_parentName}`,'provide-coin', locals);
								if (isMailSendForBook) {
									return {	status: 200,	data: [],message: 'You have successfully completed the book.'}
								}
							}
						}
						return {	status: 200,	data: [],message: 'You have successfully completed the chapter with question anster.'}
					}
				}
			}
			else {
				return {status: 201,data: [],	message: 'We are unable to save your Answers.'}
			}
		}
		catch (e) {
			console.log('ererererererere', e);
			return res.status(500).send({message: e.message});
		}
	}

	
	async saveUserQAOld(req, res) {
		try {
			let userId = req.user._id;
			req.body.user_id = userId;
			if (req.files) {
				for (let i = 0; i < req.files.length; i++) {
					for (let j = 0; j < req.body.qadetails.length; j++) {
						if (req.body.qadetails[j].answer_type == 'drawing' && _.isUndefined(req.body.qadetails[j].answer)) {
							req.body.qadetails[j].answer = req.files[i].filename;
							break;
						}
					}
				}
			}
			let userQAData = await QARepo.save(req.body);
			if (!_.isEmpty(userQAData)) {
				let findActivtyDocument = await activityRepo.getByField({'book_id': userQAData.book_id,'chapter_id': userQAData.chapter_id,'student_id': userQAData.user_id});
				let updateActivityBalance = await activityRepo.updateById({'points_earned': userQAData.points_earned}, findActivtyDocument._id);
				let checkUserbalance = await userRepo.getById(findActivtyDocument.student_id);
		
				if (checkUserbalance.user_balance == null) {
					let assumeBalance = 0.00;
					let newBalance = assumeBalance + updateActivityBalance.points_earned;
					let updateUserbalance = await userRepo.updateById({'user_balance': newBalance}, checkUserbalance._id);
				}
				else {
					let newBalance = checkUserbalance.user_balance + updateActivityBalance.points_earned;
					let updateUserBalance = await userRepo.updateById({'user_balance': newBalance}, checkUserbalance._id);
				}
				return {	status: 200,	data: userQAData,message: 'Your Answer submitted Successfully.'}
			}
			else {
				return {status: 201,data: [],	message: 'We are unable to save your Answers.'}
			}
		}
		catch (e) {
			console.log('ererererererere', e);
			return res.status(500).send({message: e.message});
		}
	}
}
module.exports = new questionAnswerController();