const userRepo = require('user/repositories/user.repository');
const activityRepo = require('activities/repositories/activity.repository');
const classRepo = require('class/repositories/class.repository');
const paymentRepo = require('payment/repositories/payment.repository');
const roleRepo = require('role/repositories/role.repository');
const userModel = require('user/models/user.model.js');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const gm = require('gm').subClass({imageMagick: true});
const fs = require('fs');
const jwt = require('jsonwebtoken');
const crypto = require("crypto");
const mongoose = require('mongoose');


//mail send 
const {
    join
} = require('path');
const ejs = require('ejs');
const {
    readFile
} = require('fs');
const {
    promisify
} = require('util');
const readFileAsync = promisify(readFile);


class parentController {
    constructor() {
        this.parent = {};

    }

    /* @Method: store contact us
    // @Description: To store contact us in  DB
    */
    async store(req, res) {
        try {
            //console.log("42>>",req.body); //process.exit();
            let parentEmail = await userRepo.getByField({
                'email': req.body.parent_email
            });
            if (parentEmail) {
                throw new Error("Parent email is already available.");
            } else {
                let role = await roleRepo.getByField({
                    'role': 'parent'
                });
                let User = new userModel();
                req.body.password = User.generateHash(req.body.password);
                //otp generate
                let OTP = Math.floor(1000 + Math.random() * 9000);

                let parentObj = {
                    email: req.body.parent_email,
                    first_name: req.body.first_name,
                    last_name: req.body.last_name,
                    registered_as: req.body.registered_as,
                    password: req.body.password,
                    verifyToken: OTP,
                    role: role._id,
                    teacher_id: req.body.teacher_id,
                    school: req.body.school
                };
                let parentStore = await userRepo.save(parentObj);

                if (parentStore) {
                    //verify mail send
                    const templateData = {
                        token: OTP
                    };
                    const ejsTemplate = await readFileAsync(join(__dirname, '../../../', 'views/templates/email.sendVerificationLink.ejs'));
                    const html = ejs.render(ejsTemplate.toString(), {
                        data: templateData
                    });
                    let mail_send = await transporter.sendMail({
                        from: `Admin<${process.env.MAIL_USERNAME}>`,
                        to: req.body.parent_email,
                        subject: 'New Parent | Make Thinkers',
                        html: html
                    });

                    //child store section start
                    // let roleStudent = await roleRepo.getByField({'role': 'student'});
                    // let childObj = {};
                    // let childStore = {};
                    // req.body.child.map(async (data) => {
                    // 	childObj.first_name = data.child_name;
                    // 	childObj.email = data.child_email;
                    // 	childObj.parent_id = parentStore._id;
                    // 	childObj.role = roleStudent._id;
                    // 	childObj.password = User.generateHash(data.child_password);
                    // 	childObj.teacher_id=req.body.teacher_id;
                    // 	childObj.school=req.body.school;
                    // 	childStore = await userRepo.save(childObj);
                    // });

                    // if (childStore) {
                    return {
                        "status": 200,
                        data: parentStore,
                        "message": "Verification email sent. Please check your inbox for our verification code."
                    };
                    // }
                    // else {}
                } else {
                    let deleteParent = userRepo.delete(parentStore._id);
                    return {
                        status: 500,
                        data: {},
                        message: `something went wrong.`
                    };
                }
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    async addChild(req, res) {
        try {
            let roleStudent = await roleRepo.getByField({
                'role': 'student'
            });
            let childStore = 0;
            let childStoreArr = [];
            let duplicateEmails = [];
            let User = new userModel();
            for (var val in req.body.child) {
                const checkEmailExists = await userRepo.getByField({
                    'email': req.body.child[val].child_email,
                    'isDeleted': false
                });
                if (!_.isEmpty(checkEmailExists) || !_.isNull(checkEmailExists)) {
                    duplicateEmails.push(req.body.child[val].child_email);
                }
            }
            ///checking the Array is blank
            if (duplicateEmails.length == 0) {
                req.body.child.forEach(async function (element) {
                    req.body.first_name = element.child_name;
                    req.body.email = element.child_email;
                    req.body.parent_id = req.body.parent_id;
                    req.body.school = element.school_id;
                    req.body.grade = element.grade;
                    req.body.role = roleStudent._id;
                    req.body.password = User.generateHash(element.child_password);
                    req.body.isVerified = 'Yes';
                    await userRepo.save(req.body);
                    childStore = parseInt(childStore) + 1;
                    childStoreArr.push(childStore);
                });
                return {
                    "status": 200,
                    data: [],
                    "message": "You have successfully added Childs."
                };
            } else {
                return {
                    "status": 201,
                    data: duplicateEmails,
                    "message": "The Child email(s) already exists. Please try another and submit once again."
                };
            }
        } catch (e) {
            console.log(e);
            return res.status(500).send({
                message: e.message
            });
        }
    }

    /* @Method: profile
    // @Description: Parent profile
    */
    async profile(req, res) {
        try {
            let user_id = req.user._id;
            let getProfileData = await userRepo.getById(user_id);
            if (!_.isEmpty(getProfileData) && !_.isNull(getProfileData)) {
                return {
                    status: 200,
                    data: getProfileData,
                    message: 'Profile fetched successfully'
                }
            } else {
                return {
                    status: 201,
                    data: [],
                    message: 'Sorry, there are no data right now.'
                }
            }
        } catch (error) {
            throw error;
        }
    }

    async getSubscriptionInfo(req, res) {
        try {
            const userSubscriptionInfo = await userRepo.ParentSubscriptionInfo(req.user._id);
            if (!_.isEmpty(userSubscriptionInfo)) {
                return {
                    status: 200,
                    data: userSubscriptionInfo,
                    message: 'Subscription details fetched successfully.'
                }
            } else {
                return {
                    status: 201,
                    data: [],
                    message: 'No data at this moment.'
                }
            }
        } catch (error) {
            throw error;
        }
    }

    /* @Method: profileUpdate
    // @Description: Parent profile update
    */
    async profileUpdate(req, res) {
        try {
            let user_id = req.user._id;
            let updateProfileData = await userRepo.updateById(req.body, user_id);
            if (!_.isEmpty(updateProfileData) && !_.isNull(updateProfileData)) {
                return {
                    status: 200,
                    data: updateProfileData,
                    message: 'Profile updated successfully'
                }
            } else {
                return {
                    status: 201,
                    data: [],
                    message: 'Sorry, there are no data right now.'
                }
            }
        } catch (error) {
            throw error;
        }
    }

    async changePassword(req, res) {
        try {
            let userData = await userRepo.getById(req.user._id);

            if (!_.isEmpty(userData) && userData.isActive == true && userData.isDeleted == false) {
                if (userData.validPassword(req.body.old_password, userData.password)) {
                    let newPassword = userData.generateHash(req.body.new_password);
                    let updatedUser = await userRepo.updateById({
                        password: newPassword
                    }, req.user._id);
                    if (updatedUser) {
                        return {
                            status: 200,
                            data: updatedUser,
                            message: 'Password Changed Successfully'
                        }
                    }
                } else {
                    return {
                        status: 500,
                        data: [],
                        message: 'Wrong Current Password'
                    };
                }
            } else {
                return {
                    status: 500,
                    data: [],
                    message: 'User not Found'
                };
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }

    }

    async editProfileParent(req, res) {
        try {
            let parentObj = {};
            if (_.has(req.body, "first_name")) {
                parentObj.first_name = req.body.first_name
            }
            if (_.has(req.body, "last_name")) {
                parentObj.last_name = req.body.last_name
            }
            let parentStore = await userRepo.updateById(parentObj, req.body.id);

            if (parentStore) {
                //child store section start
                let childObj = {};
                let childStore = {};
                req.body.child.map(async (data) => {
                    childObj.first_name = data.child_name;
                    childStore = await userRepo.updateById(childObj, data.child_id);
                });

                if (childStore) {
                    return {
                        "status": 200,
                        data: parentStore,
                        "message": "You have successfully regsitered."
                    };
                } else {

                }
            } else {
                throw new Error(`something went wrong.`);
            }

            let principleStore = await userRepo.updateById(req.body, req.body.id);

            return {
                "status": 200,
                data: principleStore,
                "message": "Profile Updated Successfully"
            };


        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
            // return { status: 500, data: {}, message: e.message };
        }
    };

    /* @Method: ParentDashboard
    // @Description: Parent Dashboard
    */
    async ParentDashboard(req, res) {
        try {
            let parent_id = req.user._id;
            const DashboardInfo = await userRepo.ParentDashboardDetails(parent_id);
            if (!_.isEmpty(DashboardInfo) && !_.isNull(DashboardInfo)) {
                return {
                    status: 200,
                    data: DashboardInfo,
                    message: 'Dashboard fetched successfully.'
                }
            } else {
                return {
                    status: 201,
                    data: [],
                    message: 'There are no data at this moment.'
                }
            }
        } catch (error) {
            throw error;
        }
    }

    /* @Method: ParentDashboardActivity
    // @Description: Parent Dashboard Activity
    */
    async ParentDashboardActivity(req, res) {
        try {
            const DashboardActivity = await activityRepo.ParentDashboardActivity(req.params.id);
            if (!_.isEmpty(DashboardActivity)) {
                return {
                    status: 200,
                    data: DashboardActivity,
                    message: 'Dashboard fetched successfully.'
                }
            } else {
                return {
                    status: 201,
                    data: [],
                    message: 'There are no data at this moment.'
                }
            }
        } catch (error) {
            return res.status(500).send({
                message: error.message
            });
        }
    }

    /* @Method: ParentMyChild
    // @Description: Parent My Child
    */
    async ParentMyChild(req, res) {
        try {
            let parent_id = req.user._id;
            const MyChildInfo = await userRepo.ParentMyChildDetails(parent_id);
            if (!_.isEmpty(MyChildInfo) && !_.isNull(MyChildInfo)) {
                return {
                    status: 200,
                    data: MyChildInfo,
                    message: 'My Child fetched successfully.'
                }
            } else {
                return {
                    status: 201,
                    data: [],
                    message: 'There are no data at this moment.'
                }
            }
        } catch (error) {
            return res.status(500).send({
                message: error.message
            });
        }
    }

    /* @Method: ParentMyChildEdit
    // @Description: Parent My Child Edit
    */
    async ParentMyChildEdit(req, res) {
        try {
            const ChildInfo = await userRepo.getById(req.params.id);
            if (!_.isEmpty(ChildInfo) && !_.isNull(ChildInfo)) {
                return {
                    status: 200,
                    data: ChildInfo,
                    message: 'Child fetched successfully.'
                }
            } else {
                return {
                    status: 201,
                    data: [],
                    message: 'There are no data at this moment.'
                }
            }
        } catch (error) {
            throw error;
        }
    }

    /* @Method: ParentMyChildUpdate
    // @Description: Parent My Child Update
    */
    async ParentMyChildUpdate(req, res) {
        try {
            let id = req.body.id;
            const ChildUpdate = await userRepo.updateById(req.body, id);
            if (ChildUpdate) {
                return {
                    status: 200,
                    data: ChildUpdate,
                    message: 'Child updated successfully.'
                }
            } else {
                return {
                    status: 201,
                    data: [],
                    message: 'There are some problem this moment.'
                }
            }
        } catch (error) {
            throw error;
        }
    }

	/* @Method: ParentChildDelete
	// @Description: Parent My Child Delete
	*/
	async ParentChildDelete(req, res) {
		try {
			let id = req.params.id;
			let getChild = await paymentRepo.getByField({"child":mongoose.Types.ObjectId(id)});
			if (!_.isEmpty(getChild)) {
				const _paymentId = getChild._id;
				var deleteChildFrompayment = await paymentRepo.updateById({"isDeleted": true}, _paymentId);
				var deleteChild = await userRepo.updateById({"isDeleted": true}, req.params.id);
			}
			if (deleteChild && deleteChildFrompayment) {
				return {status: 200,data: deleteChild,message: 'Child deleted successfully.'}
			}
			else {
				return {status: 201,data: [],	message: 'Child deletion problem.'}
			}
		}
		catch (error) {
			throw error;
		}
	}

    // async getMyChildren(req, res) {
    //     try {
    //         let userData = await userRepo.getById(req.user._id);
    //         if (!_.isEmpty(userData)) {
    //             let childrenList = await userRepo.getByField({ parent_id: userData._id });
    //             return { "status": 200, data: childrenList, "message": "Children list fetched successfully" };
    //         }
    //         else {
    //             throw new Error("User not found")
    //         }
    //     } catch (e) {
    //         return res.status(500).send({ message: e.message });
    //     }

    // }

    async ParentAndTeacherChildView(req, res) {
        try {
            let getUserInfo = await userRepo.getByIdWithRoleInfo(req.user._id);
            if (getUserInfo.role.role == 'parent' || getUserInfo.role.role == 'principal') {
                let childInfo = await classRepo.getChildInfoForParentAndPrinciple(req.params.id);
                if (!_.isEmpty(childInfo)) {
                    return {
                        status: 200,
                        data: childInfo,
                        message: 'Child information fetched successfully.'
                    }
                } else {
                    return {
                        status: 201,
                        data: [],
                        message: 'There are no data at this moment.'
                    }
                }
            } else {
                return {
                    status: 201,
                    data: [],
                    message: 'Only parent and Principle can view this.'
                }
            }
        } catch (error) {
            return res.status(500).send({
                status: 500,
                message: error.message
            });
        }
    }
}
module.exports = new parentController();