const paymentRepo = require('payment/repositories/payment.repository');
const userRepo = require('user/repositories/user.repository');
const planRepo = require('plan/repositories/plan.repository');
const roleRepo = require('role/repositories/role.repository');
const userModel = require('user/models/user.model.js');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const gm = require('gm').subClass({
  imageMagick: true
});
const fs = require('fs');
const mongoose = require('mongoose');
var stripe = require("stripe")(config.stripe_secret_key);
var moment = require('moment');



class paymentController {
  constructor() {}

  /* @Method: makePayment
  // @Description: subscription payment
  */
	async makePayment(req, res) {
		try {
			console.log('req.body <><>25<><>', req.body);
			const userDetails = await userRepo.getUserRoleandDetails(req.body.parent_id);
			if (userDetails.role.role == 'parent') {
				var card_number = req.body.card_number;
				var expiry_month = parseInt(req.body.exp_month);
				var expiry_year = parseInt(req.body.exp_year);
				var cvv = req.body.cvc;
				var stripeCardResponse = await stripe.tokens.create({
											card: {
												number: card_number,
												exp_month: expiry_month,
												exp_year: expiry_year,
												cvc: cvv,
											}
										});
		
				var stripe_token = stripeCardResponse.id;
				var customer = await stripe.customers.create({
									// source: stripe_token
									email: userDetails.email,
								});
		
				var source = await stripe.customers.createSource(customer.id, {
								source: stripe_token,
							});
		
				req.body.customer_id = source.customer;
		
				if (req.body.payment_type == 'reg') {
					let total_child_count = req.body.child_count;
					if (total_child_count > 0) {
						let additional_child_count = total_child_count - 1;
						let additional_child_price = parseFloat(req.body.additional_price * additional_child_count);
						let single_child_price = parseFloat(req.body.plan_price * 1);
						req.body.total_price = parseFloat(additional_child_price + single_child_price);
					}
				}
				else if (req.body.payment_type == 'additional') {
					let additional_child_count = req.body.child_count;
					if (additional_child_count > 0) {
						let additional_child_price = parseFloat(req.body.additional_price * additional_child_count);
						req.body.total_price = parseFloat(additional_child_price);
					}
				}
		
				if (req.body.package_type == 'monthly') {
					var plan = await stripe.plans.create({
									amount: req.body.total_price * 100,
									interval: "month",
									product: {name: "Regular Plan"},
									currency: "usd",
							});
				}
				else {
					var plan = await stripe.plans.create({
									amount: req.body.total_price * 100,
									interval: "year",
									product: {name: "Premium Plan"},
									currency: "usd",
							});
				}
		
				req.body.plan_id = plan.id
				var payment = await stripe.subscriptions.create({
								customer: source.customer,
								items: [{plan: plan.id,}, ]
							});
		
				req.body.subscription_id = payment.id;
		
				if (!_.isEmpty(payment)) {
					req.body.payment_status = 'Completed';
					req.body.next_billing_date = new Date(payment.current_period_end * 1000);
					const savePayment = await paymentRepo.save(req.body);
					console.log('savePayment <><>103<><>', savePayment);
			
					//send email to user for plan purchase
					var mailOptions = {
						// from: settingObj.Site_Title + '<belasmith00@gmail.com>',
						from: 'Make Thinkers' + '<belasmith00@gmail.com>',
						to: userDetails.email,
						subject: "Plan Successfully Subscribed - Make Thinkers",
						html: 'Hello ' + userDetails.first_name + ' ' + userDetails.last_name + ',<br><br>You have successfully purchased the plan.<br><br>Thank you.'
					};
					await transporter.sendMail(mailOptions);
			
					//send email to admin for plan purchase
					var mailOptions = {
						// from: settingObj.Site_Title + '<belasmith00@gmail.com>',
						from: 'Make Thinkers' + '<belasmith00@gmail.com>',
						to: 'makethinkers@yopmail.com',
						subject: "New Plan Subscribed - Make Thinkers",
						html: 'Hello Admin, <br><br>' + userDetails.first_name + ' ' + userDetails.last_name + ', purchased a plan from your site.<br><br>Thank you.'
					};
					await transporter.sendMail(mailOptions);
			
					return {	status: 200,	data: savePayment,message: 'You have successfully purchased the Plan.'}
				}
				else {
					return {	status: 201,	data: [],message: 'Payment failed. Please try again.'}
				}
			}
			else {
				return {	status: 201,	data: [],message: 'You are not a Parent. Only Parent can subscribe.'}
			}
		}
		catch (e) {
			return {status: 500,data: [],	message: e.message}
		}
	};


	async addChildWithPayment(req,res){
		try{
			//console.log("142>>", req.body); 
			let parent_id = req.body.parent_id;
			let roleStudent = await roleRepo.getByField({'role': 'student'});
			//console.log("145>>", roleStudent);
			let childStore = 0;
			var childAddedLength = 0;
			var countChild = 0;
			let childStoreArr = [];
			let duplicateEmails = [];
			var plan;
			var planArr = [];
			var remainChild;
			for (var val in req.body.child) {
				const checkEmailExists = await userRepo.getByField({'email': req.body.child[val].child_email,'isDeleted': false});
				if (!_.isEmpty(checkEmailExists) || !_.isNull(checkEmailExists)) {
					duplicateEmails.push(req.body.child[val].child_email);
				}
			}
			//console.log("159>>",duplicateEmails);
			if (duplicateEmails.length == 0) { 
				const userDetails = await userRepo.getUserRoleandDetails(parent_id);
				//console.log("162>>", userDetails);
				if (userDetails.role.role == 'parent') { 
					var params = {"parent_id":parent_id,"isDeleted":false}; 
					let checkChild = await userRepo.getAllByField(params);
					//console.log("166>>",checkChild); 
					if (!_.isEmpty(checkChild)) {
						childAddedLength = parseInt(checkChild.length);
					}
					//console.log("childAddedLength>>170>>",childAddedLength); 
					if (parseInt(childAddedLength) == 0) { //console.log("172>>if");  
						var result = await this.makePaymentForChild(req.body,userDetails.email,childAddedLength=0);
						if (!_.isEmpty(result)) {
							return {	status: 200,	data: result,message: 'Successfully added child'}
						}
						else{
							return {	status: 201,	data: [],message: 'Add child failed'}
						}
					}
					else{   //console.log("181>>else");   //process.exit();  
						let checkAlreadyAddedChild = await paymentRepo.getAllByField({"isDeleted":false,"parent_id":mongoose.Types.ObjectId(parent_id)});
						//console.log("183>>checkAlreadyAddedChild>>",checkAlreadyAddedChild);  
						if (!_.isEmpty(checkAlreadyAddedChild)) {
							checkAlreadyAddedChild.map(function(item){
								var pId = item.plan_id;
								if (!_.isNull(pId)) {
									planArr.push(pId.toString());
								}
							});
							//console.log("188>>", planArr); //process.exit();
							//console.log("189>>", req.body.plan_id.toString());
							//console.log("190>>",_.contains(planArr,req.body.plan_id.toString()));
							//console.log("191>>",planArr.indexOf(req.body.plan_id ));
							//console.log("192>>",planArr.includes(req.body.plan_id));
							
							if(_.contains(planArr,req.body.plan_id.toString())){
								//console.log("ss"); process.exit();
								var result = await this.makePaymentForChild(req.body,userDetails.email,checkAlreadyAddedChild);
								if (!_.isEmpty(result)) {
									return {	status: 200,	data: result,message: 'Successfully added child'}
								}
								else{
									return {	status: 201,	data: [],message: 'Add child failed'}
								}
							}
							else{
								//console.log("csac");
								return {	status: 201,"updateSubscription":"needed",data: [],message: 'You have change your subscription plan, If you want to change it your added child plan will be changed.'}
							}	
						}
					}
				}
				else { 
					return {	status: 201,	data: [],message: 'You are not a Parent. Only Parent can subscribe.'}
				}
			}
			else { 
				return {"status": 201,data: duplicateEmails,"message": "The Child email(s) already exists. Please try another and submit once again."};
			}
		}
		catch(e){
			console.log("223>>", e.message);
			return {status: 500,data: [],	message: e.message}
		}
	};
	
	async isInArray(array, search){
		return array.indexOf(search) >= 0;
	}
	
	async makePaymentForChild(params,email,noofhild){
		var card_number = params.card_number;
		var expiry_month = parseInt(params.expiry_month);
		var expiry_year = parseInt(params.expiry_year);
		var cvv = parseInt(params.cvv);
		let User = new userModel();
		let roleStudent = await roleRepo.getByField({'role': 'student'});
		
		var planInfo = await planRepo.getById(mongoose.Types.ObjectId(params.plan_id));
		console.log("257>>",planInfo); //process.exit();
		if (!_.isEmpty(planInfo)) {
			var planId = planInfo._id;
			var planName = planInfo.title;
			var planPrice = JSON.stringify(planInfo.price);
			var stripePlanId = planInfo.stripe_plan_id;
			var planTime = planInfo.plan_time;
		}
		//console.log("255>>",JSON.stringify(planPrice)); process.exit();
		
		var newObj ={};
		var insertPaymentObj ={};
		var insertChildObj ={};
		var resultArr = [];
		
		var stripeCardResponse = await stripe.tokens.create({
									card: {
										number: card_number,
										exp_month: expiry_month,
										exp_year: expiry_year,
										cvc: cvv,
									}
								});
		
		var stripe_token = stripeCardResponse.id;
		var customer = await stripe.customers.create({
							// source: stripe_token
							email: email,
						});
		
		var source = await stripe.customers.createSource(customer.id, {
						source: stripe_token,
					});
			
		var plan;
		
		for(var i=0;i<params.child.length;i++){
			if (noofhild == 0) {
				if (i==0) {
					plan = await stripe.plans.create({
							amount: planPrice * 100,
							interval: planTime,
							product: {name: planName},
							currency: "usd",
						});
					insertPaymentObj.subscription_amount = parseFloat(planPrice);
				}
				else{
					var samount = 5;
					plan = await stripe.plans.create({
							amount: samount * 100,
							interval: planTime,
							product: {name: planName},
							currency: "usd",
					});
					insertPaymentObj.subscription_amount = samount;
				}
			}
			else{
				var samount = 5;
				plan = await stripe.plans.create({
						amount: samount * 100,
						interval: planTime,
						product: {name: planName},
						currency: "usd",
				});
				insertPaymentObj.subscription_amount = samount;
			}
			
			var payment = await stripe.subscriptions.create({
							customer: source.customer,
							items: [{plan: plan.id,}, ]
						});
			
			var todays_date = moment().format("YYYY-MM-DD");
			var nextBillingDate;
			insertPaymentObj.package_type = planName;
			insertPaymentObj.parent_id =  params.parent_id;
			insertPaymentObj.child = null  ;
			insertPaymentObj.customer_id = source.customer;
			insertPaymentObj.stripe_plan_id = stripePlanId;
			insertPaymentObj.plan_id = params.plan_id;
			insertPaymentObj.subscription_id = payment.id;
			insertPaymentObj.subscription_start_date = todays_date;
			if (planTime == "month") {
				var nextBillingDate = moment(todays_date).add(30, 'days').format("YYYY-MM-DD");
			}
			else if (planTime == "year") {
				let nextBillingDate = moment(todays_date).add(365, 'days').format("YYYY-MM-DD");
			}
			insertPaymentObj.next_billing_date = nextBillingDate;
			insertPaymentObj.subscription_plan_time = planTime;
			insertPaymentObj.payment_status = 'Completed';
			
			console.log("328>>",insertPaymentObj); //process.exit();
			
			var savePayment = await paymentRepo.save(insertPaymentObj);
			var paymentId = savePayment._id;
			
			if (savePayment) {
				insertChildObj.first_name = params.child[i].child_name;
				insertChildObj.email = params.child[i].child_email;
				insertChildObj.parent_id = params.parent_id;
				insertChildObj.school = params.child[i].school_id;
				insertChildObj.role = roleStudent._id;
				insertChildObj.password = User.generateHash(params.child[i].child_password);
				insertChildObj.isVerified = 'Yes';
				
				var saveChild = await userRepo.save(insertChildObj);
				var childId = saveChild._id;
				var updateVal = {"child":childId};
				var resultUpdate = await paymentRepo.updateById(updateVal,paymentId);
			}
			resultArr.push({"payment":savePayment,"child":saveChild});
		}			
		newObj.result = resultArr;
		return newObj;
	}
	
	
	async existChildUpgradePayment(req,res){
		try{
			var newObj ={};
			var updatePaymentArr =[];
			var insertChildObj ={};
			var card_number = req.body.card_number;
			var expiry_month = parseInt(req.body.expiry_month);
			var expiry_year = parseInt(req.body.expiry_year);
			var cvv = parseInt(req.body.cvv);
			let parent_id = req.body.parent_id;
			var roleStudent = await roleRepo.getByField({'role': 'student'});
			//console.log("373>>", roleStudent);
			console.log("374>>", req.body); //process.exit();
			let duplicateEmails = [];
			for (var val in req.body.child) {
				const checkEmailExists = await userRepo.getByField({'email': req.body.child[val].child_email,'isDeleted': false});
				if (!_.isEmpty(checkEmailExists) || !_.isNull(checkEmailExists)) {
					duplicateEmails.push(req.body.child[val].child_email);
				}
			}
			//console.log("382>>",duplicateEmails); process.exit();
			if (duplicateEmails.length == 0) {
				const userDetails = await userRepo.getUserRoleandDetails(parent_id);
				console.log("379>>", userDetails); //process.exit();
				if (userDetails.role.role == 'parent') {
					var card_number = req.body.card_number;
					var expiry_month = parseInt(req.body.expiry_month);
					var expiry_year = parseInt(req.body.expiry_year);
					var cvv = parseInt(req.body.cvv);
					let User = new userModel();
					//let roleStudent = await roleRepo.getByField({'role': 'student'});
		
					var planInfo = await planRepo.getById(mongoose.Types.ObjectId(req.body.plan_id));
					console.log("395>>",planInfo); //process.exit();
					if (!_.isEmpty(planInfo)) {
						var planId = planInfo._id;
						var planName = planInfo.title;
						var planPrice = JSON.stringify(planInfo.price);
						var stripePlanId = planInfo.stripe_plan_id;
						var planTime = planInfo.plan_time;
					}
					console.log("403>>",planId,">>",planName,">>",planPrice,">>",stripePlanId,">>",planTime);
					let checkAlreadyAddedChild = await paymentRepo.getAllByField({"isDeleted":false,"parent_id":mongoose.Types.ObjectId(parent_id),"plan_id":mongoose.Types.ObjectId(req.body.prev_plan)});
					console.log("405>>",checkAlreadyAddedChild);
					var noOfAddedChild = ((checkAlreadyAddedChild.length)>0)?checkAlreadyAddedChild.length:0; console.log("noOfAddedChild>>",noOfAddedChild);
					//process.exit();
					
					var todays_date = moment().format("YYYY-MM-DD");
					var nextBillingDate;
					
					for(var j=0;j<checkAlreadyAddedChild.length;j++){
						var _paymentId = checkAlreadyAddedChild[j]._id; console.log("405>>",_paymentId);
						var _packageType = checkAlreadyAddedChild[j].package_type; console.log("406>>",_packageType);
						var _subscriptionAmount = JSON.stringify(checkAlreadyAddedChild[j].subscription_amount); console.log("407>>",_subscriptionAmount);
						var _customerId = checkAlreadyAddedChild[j].customer_id; console.log("408>>",_customerId);
						var _stripePlan = checkAlreadyAddedChild[j].stripe_plan_id; console.log("409>>",_stripePlan);
						var _subscriptionId = checkAlreadyAddedChild[j].subscription_id; console.log("410>>",_subscriptionId);
						var _subscriptionStartDate = checkAlreadyAddedChild[j].subscription_start_date; console.log("411>>",_subscriptionStartDate);
						var _nextBillingDate = checkAlreadyAddedChild[j].next_billing_date; console.log("412>>",_nextBillingDate);
						var _subscriptionPlanTime = checkAlreadyAddedChild[j].subscription_plan_time; console.log("413>>",_subscriptionPlanTime);
						var _pId = checkAlreadyAddedChild[j].plan_id; console.log("414>>",_pId);
						var plan;
						var subscription_amount =0;
						var unsubscribe = stripe.subscriptions.del(_subscriptionId);
						if(unsubscribe){ 
							if (_subscriptionAmount == 10 || _subscriptionAmount == 100) { //console.log("if"); //process.exit();
								plan = await stripe.plans.create({
										amount: planPrice * 100,
										interval: planTime,
										product: {name: planName},
										currency: "usd",
									});
								//console.log("442>>",plan.id);
								subscription_amount = planPrice;
							}
							else{ //console.log("else"); //process.exit();
								var samount = 5;
								plan = await stripe.plans.create({
										amount: samount * 100,
										interval: planTime,
										product: {name: planName},
										currency: "usd",
								});
								//console.log("442>>",plan.id);
								subscription_amount = samount;
							}
							//console.log("442>>",plan.id);
							var stripeCardResponse = await stripe.tokens.create({
														card: {
															number: card_number,
															exp_month: expiry_month,
															exp_year: expiry_year,
															cvc: cvv,
														}
													});
		
							var stripe_token = stripeCardResponse.id; //console.log("452>>",stripe_token);
							var customer = await stripe.customers.create({
											// source: stripe_token
											email: userDetails.email,
										});
							
							//console.log("458>>",customer);
							var source = await stripe.customers.createSource(_customerId, {
											source: stripe_token,
										});
							//console.log("462>>",source);
							var payment = await stripe.subscriptions.create({
											customer: source.customer,
											items: [{plan: plan.id,}, ]
										});
							
							if (planTime == "month") {
								nextBillingDate = moment(todays_date).add(30, 'days').format("YYYY-MM-DD");
							}
							else if (planTime == "year") {
								nextBillingDate = moment(todays_date).add(365, 'days').format("YYYY-MM-DD");
							}

							var updateVal = {
								"package_type":planName,
								"subscription_amount":subscription_amount,
								"stripe_plan_id":stripePlanId,
								"plan_id":mongoose.Types.ObjectId(planId),
								"subscription_id":payment.id,
								"subscription_plan_time":planTime,
								"subscription_start_date":todays_date,
								"next_billing_date":nextBillingDate
							};
							
							//console.log("479>>",updateVal);
							var updateResult = await paymentRepo.updateFields(updateVal,_paymentId);
							updatePaymentArr.push(updateResult);
						}
					}

					var result = await this.makePaymentForChild(req.body,userDetails.email,noOfAddedChild,_customerId);
					
					var responseObj = {};
					var childDetails = await userRepo.getAllByField({"parent_id":req.body.parent_id});
					var paymentDetails = await paymentRepo.getAllByField({"parent_id":req.body.parent_id});
					responseObj.child = childDetails;
					responseObj.payment = paymentDetails;
					
					return {	status: 200,	data: responseObj,message: 'Your package has been upgraded.'}
				}
				else { 
					return {	status: 201,	data: [],message: 'You are not a Parent. Only Parent can subscribe.'}
				}
			}
			else { 
				return {"status": 201,data: duplicateEmails,"message": "The Child email(s) already exists. Please try another and submit once again."};
			}
		}
		catch(e){
			console.log("366>>", e.message);
			return {status: 500,data: [],	message: e.message}
		}
	}
	
	async makePaymentForChildExist(params,email,noofhild){
		var card_number = params.card_number;
		var expiry_month = parseInt(params.expiry_month);
		var expiry_year = parseInt(params.expiry_year);
		var cvv = parseInt(params.cvv);
		let User = new userModel();
		let roleStudent = await roleRepo.getByField({'role': 'student'});
		
		var newObj ={};
		var insertPaymentObj ={};
		var insertChildObj ={};
		var resultArr = [];
		
		var stripeCardResponse = await stripe.tokens.create({
									card: {
										number: card_number,
										exp_month: expiry_month,
										exp_year: expiry_year,
										cvc: cvv,
									}
								});
		
		var stripe_token = stripeCardResponse.id;
		var customer = await stripe.customers.create({
							// source: stripe_token
							email: email,
						});
		
		var source = await stripe.customers.createSource(customer.id, {
						source: stripe_token,
					});
			
		var samount = 0;
		var plan;
		var loop;
		if (noofchild == 0) {
			loop = parseInt(params.child.length);
		}
		else{
			loop = parseInt(noofchild);
		}
		console.log("382>>", loop); 
		for(var i=0;i<loop;i++){
			samount = 5;
			plan = await stripe.plans.create({
				amount: samount * 100,
				interval: "month",
				product: {name: "Regular Plan"},
				currency: "usd",
			});
			
			var payment = await stripe.subscriptions.create({
							customer: source.customer,
							items: [{plan: plan.id,}, ]
						});
			
			insertPaymentObj.package_type = "Default";
			insertPaymentObj.total_price = samount;
			insertPaymentObj.parent_id =  params.parent_id;
			insertPaymentObj.child = null  ;
			insertPaymentObj.subscription_amount = samount;
			insertPaymentObj.customer_id = source.customer;
			insertPaymentObj.plan_id = plan.id;
			insertPaymentObj.subscription_id = payment.id;
			insertPaymentObj.next_billing_date = new Date(payment.current_period_end * 1000);
			insertPaymentObj.payment_status = 'Completed';
			
			var savePayment = await paymentRepo.save(insertPaymentObj);
			var paymentId = savePayment._id;
			
			if (savePayment) {
				insertChildObj.first_name = params.child[i].child_name;
				insertChildObj.email = params.child[i].child_email;
				insertChildObj.parent_id = params.parent_id;
				insertChildObj.school = params.child[i].school_id;
				insertChildObj.role = roleStudent._id;
				insertChildObj.password = User.generateHash(params.child[i].child_password);
				insertChildObj.isVerified = 'Yes';
				
				var saveChild = await userRepo.save(insertChildObj);
				var childId = saveChild._id;
				var updateVal = {"child":childId};
				var resultUpdate = await paymentRepo.updateById(updateVal,paymentId);
			}
			
			resultArr.push({"payment":savePayment,"child":saveChild});
		}
		newObj.result = resultArr;
		return newObj;
	}
}

module.exports = new paymentController();