const mongoose = require('mongoose');
const Payment = require('payment/models/payment.model');
const perPage = config.PAGINATION_PERPAGE;

const paymentRepository = {
    getAll: async (req) => {
        // console.log(req.body);
        try {
            var conditions = {};
            var and_clauses = [];

            and_clauses.push({
                "isDeleted": false
            });

            if (_.isObject(req.body.query) && _.has(req.body.query, 'generalSearch')) {
                //and_clauses.push({"status": /req.body.query.generalSearch/i});
                and_clauses.push({
                    'title': {
                        $regex: req.body.query.generalSearch,
                        $options: 'i'
                    }
                });
            }
            if (_.isObject(req.body.query) && _.has(req.body.query, 'Status')) {
                and_clauses.push({
                    "status": req.body.query.Status
                });
            }
            conditions['$and'] = and_clauses;

            var sortOperator = {
                "$sort": {}
            };
            if (_.has(req.body, 'sort')) {
                var sortField = req.body.sort.field;
                if (req.body.sort.sort == 'desc') {
                    var sortOrder = -1;
                } else if (req.body.sort.sort == 'asc') {
                    var sortOrder = 1;
                }

                sortOperator["$sort"][sortField] = sortOrder;
            } else {
                sortOperator["$sort"]['_id'] = -1;
            }

            var aggregate = Payment.aggregate([{
                    $match: conditions
                },
                sortOperator
            ]);

            var options = {
                page: req.body.pagination.page,
                limit: req.body.pagination.perpage
            };
            let allPayment = await Payment.aggregatePaginate(aggregate, options);
            return allPayment;
        } catch (e) {
            throw (e);
        }
    },

    getAuthorCount: async (req) => {
        try {

            let payment = await Payment.find({
                isDeleted: false
            });
            return payment;
        } catch (e) {
            throw (e);
        }
    },

    getById: async (id) => {
        try {
            let payment = await Payment.findById(id).exec();
            return payment;
        } catch (e) {
            throw (e);
        }
    },

    getByField: async (params) => {
        try {
            let payment = await Payment.findOne(params).exec();
            return payment;
        } catch (e) {
            throw (e);
        }
    },

	getAllByField: async (params) => {
		try {
			let payment = await Payment.find(params).exec();
			return payment;
		}
		catch (e) {
			throw (e);
		}
	},

    delete: async (id) => {
        try {
            let payment = await Payment.findById(id);
            if (payment) {
                let paymentDelete = await Payment.remove({
                    _id: id
                }).exec();
                return paymentDelete;
            } else {
                return null;
            }
        } catch (e) {
            throw (e);
        }
    },

    deleteByField: async (field, fieldValue) => {
        //todo: Implement delete by field
    },


	updateById: async (data, id) => {
		try {
			let payment = await Payment.findByIdAndUpdate(id, data, {new: true,upsert: true}).exec();
			return payment;
		}
		catch (e) {
			throw (e);
		}
	},
	
	updateFields: async (data, id) => {
		try {
			let payment = await Payment.findByIdAndUpdate(id, data, {new: true,upsert: true ,multi: true}).exec();
			return payment;
		}
		catch (e) {
			throw (e);
		}
	},

    updateByField: async (field, fieldValue, data) => {
        //todo: update by field
    },

	save: async (data) => {
		try {
			let payment = await Payment.create(data);
			if (!payment) {
				return null;
			}
			return payment;
		}
		catch (e) {
			throw e;
		}
	},

	parentSubscriptionInfo: async (id) => {
		try {
			let users = await Payment.aggregate([{
							$match: {parent_id: mongoose.Types.ObjectId(id),"isDeleted":false}
						}]).exec();
			return users;
		}
		catch (e) {
			throw (e);
		}
	},

};

module.exports = paymentRepository;