const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');
const beautifyUnique = require('mongoose-beautiful-unique-validation');

const PaymentSchema = new Schema({
	package_type: {type: String,default: '',},
	total_price: {type: Schema.Types.Double,default: 0.00},
	parent_id: {type: Schema.Types.ObjectId,ref: 'User',default: null},
	//child: [
	//	{type: Schema.Types.ObjectId,ref: 'User',default: null},
	//],
	
	child: {	type: Schema.Types.ObjectId,	ref: 'User',default: null},
	subscription_amount: {type: Schema.Types.Double,default: 0.00},
	customer_id: {type: String,default: ''},
	stripe_plan_id: {type: String,default: ''},
	plan_id :{type: Schema.Types.ObjectId,	ref: 'Plan',default: null},
	subscription_id: {type: String,default: ''},
	subscription_start_date:{type: String,default:''},
	next_billing_date: {type: String,default:''},
	subscription_plan_time :{type:String, default:''},
	payment_status: {type: String,default: "Pending",enum: ["Pending", "Completed"]},
	isDeleted: {type: Boolean,default: false,enum: [true, false]},
	createdAt: {type: Date,default: Date.now()}
});

PaymentSchema.plugin(beautifyUnique);
// For pagination
PaymentSchema.plugin(mongooseAggregatePaginate);

// create the model for users and expose it to our app
module.exports = mongoose.model('Payment', PaymentSchema);