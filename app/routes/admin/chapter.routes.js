const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const multer = require('multer');
const chapterController = require('chapter/controllers/chapter.controller');
const pageController = require('page/controllers/page.controller');
const Storage = multer.diskStorage({
    destination: (req, file, callback) => {
        if (file.fieldname === 'chapter_icon') {
            callback(null, "./public/uploads/chapter")
        }
    },
    filename: (req, file, callback) => {
        callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname.replace(/\s/g, '_'));
    }
});

const uploadFile = multer({
    storage: Storage,
    fileFilter: function (req, file, cb) {
        if (file.mimetype !== 'image/jpeg' && file.mimetype !== 'image/jpg' && file.mimetype !== 'image/png') {
            req.fileValidationError = 'Only support jpeg, jpg or png file types.';
            return cb(null, false, new Error('Only support jpeg, jpg or png file types'));
        }
        cb(null, true);
    }
});

const request_param = multer({
    storage: Storage
});


namedRouter.all('/chapter*', auth.authenticate);

/*
// @Route: chapter Listing [Admin]
*/
// chapter Listing Route
// namedRouter.get("chapter.listing", '/chapter/listing', chapterController.list);
// namedRouter.post("chapter.getall", '/chapter/getall', async (req, res) => {
//     try {
//         const success = await chapterController.getAll(req, res);
//         // console.log("GETALL",success)
//         res.send({
//             "meta": success.meta,
//             "data": success.data
//         });
//     } catch (error) {
//         res.status(error.status).send(error);
//     }
// });
namedRouter.get("chapter.edit", '/chapter/edit/:id', chapterController.edit);
namedRouter.post("chapter.update", '/chapter/update', uploadFile.any(), chapterController.update);
// chapter Create Route
namedRouter.get("chapter.create", '/chapter/create/:id', chapterController.create);
namedRouter.get("chapter.delete", '/chapter/delete/:id', chapterController.destroy);
// chapter Insert Route
namedRouter.post("chapter.insert", '/chapter/insert', uploadFile.any(), chapterController.insert);


// Get All Pages Render Route
namedRouter.get("chapter.getallPages", '/chapter/getallPagelist/:id', chapterController.getAllPageList);


// Get All pages Route
namedRouter.post("chapter.getAllOth", '/chapter/getAllOth/:id', async (req, res) => {
    try {
        /* const success = await chapterController.getAllPages(req, res); */
        const success = await pageController.getAllPagesByChapter(req, res);
        res.send({
            "data": success.data
        });
    } catch (error) {
        res.status(error.status).send(error);
    }
});

/* 
// @Route: Chapter number verify
*/
namedRouter.post('verify.chapter.number', '/verify-chapter-number', request_param.any(), async (req, res) => {
	try {
		const success = await chapterController.verifyChapterNumber(req);
		return res.send(success.message);
		
	} catch (error) {
		req.flash('error', error.message);
		return res.redirect(namedRouter.urlFor('chapter.create'));
	}
});

/*
// @Route: Chapter title verify
*/
namedRouter.post('verify.chapter.title', '/verify-chapter-title', request_param.any(), async (req, res) => {
	try {
		const success = await chapterController.verifyChapterTitle(req);
		return res.send(success.message);
		
	} catch (error) {
		req.flash('error', error.message);
		return res.redirect(namedRouter.urlFor('chapter.create'));
	}
});


// Export the express.Router() instance
module.exports = router;