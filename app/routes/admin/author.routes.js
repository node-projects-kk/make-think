const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const multer = require('multer');
const authorController = require('author/controllers/author.controller');

const Storage = multer.diskStorage({
	destination: (req, file, callback) => {
		callback(null, "./public/uploads/book");
	},
	filename: (req, file, callback) => {
		callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname.replace(/\s/g, '_'));
	}
});

const uploadFile = multer({storage: Storage});
const request_param = multer();

namedRouter.all('/author*', auth.authenticate);

// author Listing Route
namedRouter.get("author.listing", '/author/listing', authorController.list);

// author Get All Route
namedRouter.post("author.getall", '/author/getall', async (req, res) => {
    try {
        const success = await authorController.getAll(req, res);
        res.send({"meta": success.meta, "data": success.data});
    } catch (error) {
        res.status(error.status).send(error);
    }
});
// author Create Route
namedRouter.get("author.create", '/author/create', authorController.create);

// author Insert Route
namedRouter.post("author.insert", '/author/insert', request_param.any(), authorController.insert);

// author Edit Route
namedRouter.get("author.edit", "/author/edit/:id", authorController.edit);

// author Update Route
namedRouter.post("author.update", '/author/update', request_param.any(), authorController.update);

// author Delete Route
namedRouter.get("author.delete", "/author/delete/:id", authorController.delete);

// Export the express.Router() instance
module.exports = router;