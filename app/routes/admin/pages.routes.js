const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const multer = require('multer');
const pageController = require('page/controllers/page.controller');

const Storage = multer.diskStorage({
    destination: (req, file, callback) => {
        callback(null, "./public/uploads/page");
    },
    filename: (req, file, callback) => {
        callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname.replace(/\s/g, '_'));
    }
});

const uploadFile = multer({
    storage: Storage
});
const request_param = multer();


namedRouter.all('/page*', auth.authenticate);

/*
// @Route: chapter Listing [Admin]
*/
// page Listing Route
// namedRouter.get("chapter.listing", '/chapter/listing', chapterController.list);
// namedRouter.post("chapter.getall", '/chapter/getall', async (req, res) => {
//     try {
//         const success = await chapterController.getAll(req, res);
//         // console.log("GETALL",success)
//         res.send({
//             "meta": success.meta,
//             "data": success.data
//         });
//     } catch (error) {
//         res.status(error.status).send(error);
//     }
// });

namedRouter.get("page.edit", '/page/edit/:id/:number', pageController.edit);
namedRouter.post("page.update", '/page/update', request_param.any(), pageController.update);

// page Create Route
namedRouter.get("page.create", '/page/create/:id', pageController.create);
namedRouter.get("page.delete", '/page/delete/:id/:number', pageController.destroy);

// page Insert Route
namedRouter.post("page.insert", '/page/insert', request_param.any(), pageController.insert);

/* 
// @Route: Page number verify
*/
namedRouter.post('verify.page.number', '/verify-page-number', request_param.any(), async (req, res) => {
	try {
       
		const success = await pageController.verifyPageNumber(req);
		return res.send(success.message);
		
	} catch (error) {
		req.flash('error', error.message);
		return res.redirect(namedRouter.urlFor('page.create'));
	}
});

/*
// @Route: Page title verify
*/
namedRouter.post('verify.page.title', '/verify-page-title', request_param.any(), async (req, res) => {
	try {
		const success = await pageController.verifyPageTitle(req);
		return res.send(success.message);
		
	} catch (error) {
		req.flash('error', error.message);
		return res.redirect(namedRouter.urlFor('page.create'));
	}
});

// Export the express.Router() instance
module.exports = router;