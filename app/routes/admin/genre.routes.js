const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const multer = require('multer');
const genreController = require('genre/controllers/genre.controller');

const Storage = multer.diskStorage({
	destination: (req, file, callback) => {
		callback(null, "./public/uploads/book");
	},
	filename: (req, file, callback) => {
		callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname.replace(/\s/g, '_'));
	}
});

const uploadFile = multer({storage: Storage});
const request_param = multer();

namedRouter.all('/genre*', auth.authenticate);

// Genre Listing Route
namedRouter.get("genre.listing", '/genre/listing', genreController.list);
// Genre Get All Route
//namedRouter.get("genre.getall", '/genre/getall', genreController.getAll);

namedRouter.post("genre.getall", '/genre/getall', async (req, res) => {
    try {
        const success = await genreController.getAll(req, res);
        res.send({"meta": success.meta, "data": success.data});
    } catch (error) {
        res.status(error.status).send(error);
    }
});
// Genre Create Route
namedRouter.get("genre.create", '/genre/create', genreController.create);

// Genre Insert Route
namedRouter.post("genre.insert", '/genre/insert', request_param.any(), genreController.insert);

// Genre Edit Route
namedRouter.get("genre.edit", "/genre/edit/:id", genreController.edit);

// Genre Update Route
namedRouter.post("genre.update", '/genre/update', request_param.any(), genreController.update);

// Genre Delete Route
namedRouter.get("genre.delete", "/genre/delete/:id", genreController.delete);
namedRouter.get("genre.statusChange", "/genre/statusChange/:id", genreController.statusChange);
// Export the express.Router() instance
module.exports = router;