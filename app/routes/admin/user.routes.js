const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const multer = require('multer');
const userController = require('user/controllers/user.controller');
const AnswerController = require('question_answer/controllers/question_answer.controller');
const coinController = require('coins/controllers/coins.controller');

const Storage = multer.diskStorage({
	destination: (req, file, callback) => {
		if (file.fieldname === 'banner_image') {
			callback(null, "./public/uploads/user/banner")
		} else {
			callback(null, "./public/uploads/user");
		}
	},
	filename: (req, file, callback) => {
		callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname.replace(/\s/g, '_'));
	}
});

const uploadFile = multer({
	storage: Storage
});
const request_param = multer();

// login render route
namedRouter.get('user.login', '/', userController.login);
// login process route
namedRouter.post("user.login.process", '/login', userController.signin);
/*
// @Route: Users Forgotpassowrd [Admin]
*/
namedRouter.post('user.forgotPass.process', '/user/forgotpassword', request_param.any(), async (req, res) => {
	try {
		const success = await userController.forgotPassword(req);
		req.flash('success', success.message);
		return res.redirect(namedRouter.urlFor('user.login'));
	} catch (error) {
		req.flash('error', error.message);
		return res.redirect(namedRouter.urlFor('user.login'));
	}
});

namedRouter.get('user.logout', "/logout", userController.logout);
namedRouter.all('/*', auth.authenticate);

/*
// @Route: Users Dashboard [Admin]
*/
// dashboard route
namedRouter.get("user.dashboard", '/dashboard', userController.dashboard);

namedRouter.get("admin.profile", '/profile/:id', request_param.any(), userController.viewmyprofile);
// admin update profile
namedRouter.post("admin.updateProfile", '/update/profile', request_param.any(), userController.updateprofile);
// admin change Password
namedRouter.get("admin.changepassword", '/change/password', userController.adminChangePassword);
/*
// @Route: Chnage password [Admin] action
*/
namedRouter.post("admin.updateAdminPassword", '/update/admin-password', request_param.any(), userController.adminUpdatePassword);

// Student List
namedRouter.get("student.listing", '/student/listing', userController.studentList);

// Get All Students
namedRouter.post("student.getall", '/student/getall', async (req, res) => {
	try {
		const success = await userController.getAllStudent(req, res);
		res.send({
			"meta": success.meta,
			"data": success.data
		});
	} catch (error) {
		res.status(error.status).send(error);
	}
});

// Student Edit Route
namedRouter.get("student.edit", "/student/edit/:id", userController.studentEdit);

// Student Update Route
namedRouter.post("student.update", '/student/update', request_param.any(), userController.studentUpdate);

// Student Delete Route
namedRouter.get("student.delete", "/student/delete/:id", userController.deleteStudent);

//Student Coins Details
namedRouter.get("student.coinsdetails", '/student/coinsdetails/:id', request_param.any(), userController.studentCoinsDetails);

namedRouter.post("student.cointoadmin",'/student/cointoadmin', request_param.any(), userController.adminCoin);

// student answer list page route
namedRouter.get("student.listanswer", '/student/listanswer/:id', request_param.any(), AnswerController.studentListAnswer);

// Get All students answer Route
namedRouter.post("student.getAllAnswers", '/student/getAllAnswers/:id', request_param.any(), async (req, res) => {
	try {
		const success = await AnswerController.getAllAnswerChapterWise(req, res);
		res.send({
			"data": success.data
		});
	} catch (error) {
		res.status(error.status).send(error);
	}
});

// student answer view page route
namedRouter.get("student.viewanswer", '/student/viewanswer/:id', request_param.any(), AnswerController.studentViewAnswer);



// Export the express.Router() instance
module.exports = router;