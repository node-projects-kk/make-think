const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const questionController = require('question/controllers/question.controller');

const multer = require('multer');
const request_param = multer();

//authentication section of question
namedRouter.all('/question*', auth.authenticate);

// admin question list route
namedRouter.get("question.list", '/question/listing',questionController.list);
namedRouter.get("question.create", '/question/create', questionController.create);
namedRouter.get("question.delete", '/question/delete/:id',questionController.delete);
namedRouter.get("question.edit", '/question/edit/:id',questionController.edit);
namedRouter.post("question.insert", '/question/insert', request_param.any(), questionController.insert);
namedRouter.get("question.getTitleAndDescription", '/question/getTitleAndDescription/:bookId/:chapterNo',questionController.getTitleAndDescription);
namedRouter.post("question.update", '/question/update',request_param.any(),questionController.update);
namedRouter.post("question.getall", '/question/getall', async (req, res) => {
    try {
        const success = await questionController.getAll(req, res);
        res.send({"meta": success.meta, "data": success.data});
    } catch (error) {
        res.status(error.status).send(error);
    }
});

//Export the express.Router() instance
module.exports = router;