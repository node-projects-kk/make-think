const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const multer = require('multer');
const parentsController = require('user/controllers/parents.controller');

const Storage = multer.diskStorage({
	destination: (req, file, callback) => {
		callback(null, "./public/uploads/user");
	},
	filename: (req, file, callback) => {
		callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname.replace(/\s/g, '_'));
	}
});

const uploadFile = multer({storage: Storage});
const request_param = multer();

namedRouter.all('/parents*', auth.authenticate);

// parents Listing Route
namedRouter.get("parents.list", '/parents/list', parentsController.list);
// parents Edit Route
namedRouter.get("parents.edit", "/parents/edit/:id", parentsController.edit);
// Genre Update Route
namedRouter.post("parents.update", '/parents/update', request_param.any(), parentsController.update);
// Genre Delete Route
namedRouter.get("parents.delete", "/parents/delete/:id", parentsController.delete);

// Export the express.Router() instance

namedRouter.post("parents.getall", '/parents/getall', async (req, res) => {
    try {
        const success = await parentsController.getAllParents(req, res);
        res.send({"meta": success.meta, "data": success.data});
    } catch (error) {
        res.status(error.status).send(error);
    }
});
module.exports = router;