const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const multer = require('multer');
const teacherController = require('user/controllers/teacher.controller');

const Storage = multer.diskStorage({
	destination: (req, file, callback) => {
		callback(null, "./public/uploads/user");
	},
	filename: (req, file, callback) => {
		callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname.replace(/\s/g, '_'));
	}
});

const uploadFile = multer({storage: Storage});
const request_param = multer();

namedRouter.all('/teacher*', auth.authenticate);

// teacherController Listing Route
namedRouter.get("teacher.list", '/teacher/list', teacherController.list);
// teacherController Edit Route
namedRouter.get("teacher.edit", "/teacher/edit/:id", teacherController.edit);
// Genre Update Route
namedRouter.post("teacher.update", '/teacher/update', request_param.any(), teacherController.update);
// Genre Delete Route
namedRouter.get("teacher.delete", "/teacher/delete/:id", teacherController.delete);

// Export the express.Router() instance

namedRouter.post("teacher.getall", '/teacher/getall', async (req, res) => {
    try {
        const success = await teacherController.getAllTeacher(req, res);
        res.send({"meta": success.meta, "data": success.data});
    } catch (error) {
        res.status(error.status).send(error);
    }
});
module.exports = router;