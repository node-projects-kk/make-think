const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const multer = require('multer');
const bookController = require('book/controllers/book.controller');
const chapterController = require('chapter/controllers/chapter.controller');
// const Storage = multer.diskStorage({
// 	destination: (req, file, callback) => {
// 		callback(null, "./public/uploads/book");
// 	},
// 	filename: (req, file, callback) => {
// 		callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname.replace(/\s/g, '_'));
// 	}
// });
const Storage = multer.diskStorage({
    destination: function (req, file, callback) {
        // console.log(file.fieldname)
        if (file.fieldname === 'picture') {
            callback(null, "./public/uploads/book")
        }

    },
    filename: function (req, file, callback) {
        callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname.replace(/\s/g, '_'));
    }
});
const uploadFile = multer({
    storage: Storage,
    fileFilter: function (req, file, cb) {
        if (file.mimetype !== 'image/jpeg' && file.mimetype !== 'image/jpg' && file.mimetype !== 'image/png') {
            req.fileValidationError = 'Only support jpeg, jpg or png file types.';
            return cb(null, false, new Error('Only support jpeg, jpg or png file types'));
        }
        cb(null, true);
    }
});
const request_param = multer({
    storage: Storage
});


namedRouter.all('/book*', auth.authenticate);

/*
// @Route: Book Listing [Admin]
*/
// Book Listing Route
namedRouter.get("book.listing", '/book/listing', bookController.list);

// Book Get All Route
namedRouter.post("book.getall", '/book/getall', async (req, res) => {
    try {
        const success = await bookController.getAll(req, res);
        res.send({
            "meta": success.meta,
            "data": success.data
        });
    } catch (error) {
        res.status(error.status).send(error);
    }
});

// Book Create Route
namedRouter.get("book.create", '/book/create', bookController.create);

// Book Insert Route
namedRouter.post("book.insert", '/book/insert', uploadFile.any(), bookController.insert);

// Book Edit Route
namedRouter.get("book.edit", "/book/edit/:id", bookController.edit);

// Book Update Route
namedRouter.post("book.update", '/book/update', uploadFile.any(), bookController.update);

// Book Delete Route
namedRouter.get("book.delete", "/book/delete/:id", bookController.delete);

namedRouter.get("book.chapterBy", '/book/chapterTitle/:id', bookController.getChapter);
namedRouter.get("book.chapterById", '/book/chapterTitleId/:id', bookController.getChapterById);


// Get All Chapter Render Route
namedRouter.get("book.getallChapter", '/book/getallChapterlist/:id', bookController.getAllChapterList);


// Get All Chapter Route
namedRouter.post("book.getAllOth", '/book/getAllOth/:id', async (req, res) => {
    try {
        /* const success = await bookController.getAllOth(req, res); */
        const success = await chapterController.getAllByBookID(req, res);
        res.send({
            "data": success.data
        });
    } catch (error) {
        console.log('error <> 96', error);
        res.status(error.status).send(error);
    }
});



// Export the express.Router() instance
module.exports = router;