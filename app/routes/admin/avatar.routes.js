const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const multer = require('multer');
const avatarController = require('avatar/controllers/avatar.controller');
const avatarRepo = require('avatar/repositories/avatar.repository');

const Storage = multer.diskStorage({

    destination: (req, file, callback) => {
	if(file.fieldname == 'audio'){
		callback(null, "./public/uploads/avatar_audio");    
	}
        if(file.fieldname == 'image_1' || file.fieldname == 'image_2') {
		callback(null, "./public/uploads/avatar");
        }
    },
    filename: (req, file, callback) => {
        callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname.replace(/\s/g, '_'));
    }
});

const uploadFile = multer({
    storage: Storage,
    fileFilter: async function (req, file, cb) {
        
        let avatar = await avatarRepo.getByField({
            'title': req.body.title
        });
            cb(null, true);
       
    }
});
const request_param = multer({
    storage: Storage
});

namedRouter.all('/avatar*', auth.authenticate);

// avatar Listing Route
namedRouter.get("avatar.listing", '/avatar/listing', avatarController.list);

// avatar Get All Route
namedRouter.post("avatar.getall", '/avatar/getall', async (req, res) => {
    try {
        const success = await avatarController.getAll(req, res);
        res.send({
            "meta": success.meta,
            "data": success.data
        });
    } catch (error) {
        res.status(error.status).send(error);
    }
});
// avatar Create Route
namedRouter.get("avatar.create", '/avatar/create', avatarController.create);

// avatar Insert Route
namedRouter.post("avatar.insert", '/avatar/insert', uploadFile.any(), avatarController.insert);

// avatar Edit Route
namedRouter.get("avatar.edit", "/avatar/edit/:id", avatarController.edit);

// avatar Update Route
namedRouter.post("avatar.update", '/avatar/update', uploadFile.any(), avatarController.update);

// avatar Delete Route
namedRouter.get("avatar.delete", "/avatar/delete/:id", avatarController.delete);


/*
// @Route: Avatar title verify
*/
namedRouter.post('verify.avatar.title', '/verify-avatar-title', request_param.any(), async (req, res) => {
    try {
        const success = await avatarController.verifyAvatarTitle(req);
        return res.send(success.message);
    } catch (error) {
        req.flash('error', error.message);
        return res.redirect(namedRouter.urlFor('avatar.create'));
    }
});

// Export the express.Router() instance
module.exports = router;