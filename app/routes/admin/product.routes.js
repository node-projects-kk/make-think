const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const multer = require('multer');
var fs = require('fs');
const productController = require('product/controllers/product.controller');

const Storage = multer.diskStorage({
    destination: function (req, file, cb) {
        if ((file.fieldname).indexOf('pro_image') > -1) {
            // check if directory exists
            if (!fs.existsSync('./public/uploads/product/')) {
                // if not create directory
                fs.mkdirSync('./public/uploads/product/');
            }
            cb(null, './public/uploads/product/')
        }
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '_' + Date.now() + '_' + file.originalname)
    }
});

const uploadFile = multer({
    storage: Storage,
    fileFilter: function (req, file, cb) {
        if (file.mimetype !== 'image/jpeg' && file.mimetype !== 'image/jpg' && file.mimetype !== 'image/png') {
            req.fileValidationError = 'Only support jpeg, jpg or png file types.';
            return cb(null, false, new Error('Only support jpeg, jpg or png file types'));
        }
        cb(null, true);
    }
});

const request_param = multer({
    storage: Storage
});


namedRouter.all('/product*', auth.authenticate);

/*
// @Route: Product Listing [Admin]
*/
// Product Listing Route
namedRouter.get("product.listing", '/product/listing', productController.list);

// Product Get All Route
namedRouter.post("product.getall", '/product/getall', async (req, res) => {
    try {
        const success = await productController.getAll(req, res);
        res.send({
            "meta": success.meta,
            "data": success.data
        });
    } catch (error) {
        res.status(error.status).send(error);
    }
});

// Product Create Route
namedRouter.get("product.create", '/product/create', productController.create);

// Product Insert Route
namedRouter.post("product.insert", '/product/insert', uploadFile.any(), productController.insert);

// Product Edit Route
namedRouter.get("product.edit", "/product/edit/:id", productController.edit);

// Product Update Route
namedRouter.post("product.update", '/product/update', uploadFile.any(), productController.update);

// Product Delete Route
namedRouter.get("product.delete", "/product/delete/:id", productController.delete);


// Export the express.Router() instance
module.exports = router;