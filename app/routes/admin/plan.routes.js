const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const planController = require('plan/controllers/plan.controller');

const multer = require('multer');
const request_param = multer();

//authentication section of plan
namedRouter.all('/plan*', auth.authenticate);

// admin plan list route
namedRouter.get("plan.list", '/plan/list',planController.list);
namedRouter.post("plan.getall", '/plan/getall', async (req, res) => {
    try {
        const success = await planController.getAll(req, res);
        // console.log("GETALL",success)
        res.send({"meta": success.meta, "data": success.data});
    } catch (error) {
        res.status(error.status).send(error);
    }
});
/*@Route:  plan  Edit*/
namedRouter.get("plan.edit", '/plan/edit/:id',planController.edit);
/*@Route:  plan  Edit*/
namedRouter.get("plan.delete", '/plan/delete/:id',planController.destroy);
/*@Route:  plan  Edit*/
namedRouter.post("plan.update", '/plan/update',request_param.any(),planController.update);
/*@Route:  plan  Edit*/
//namedRouter.post("plan.statusChange", '/plan/status-change',request_param.any(),planController.statusChange);


//Export the express.Router() instance
module.exports = router;