const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const classController = require('class/controllers/class.controller');

const multer = require('multer');
const request_param = multer();

//authentication section of language
namedRouter.all('/class*', auth.authenticate);

// admin class routes
namedRouter.post("class.getall", '/class/getall', async (req, res) => {
  try {
    const success = await classController.getAll(req, res);
    res.send({
      "meta": success.meta,
      "data": success.data
    });
  } catch (error) {
    res.status(error.status).send(error);
  }
});
namedRouter.get("class.create", '/class/create', classController.create);
namedRouter.post("class.insert", '/class/insert', request_param.any(),classController.insert);
namedRouter.get("class.list", '/class/list', classController.list);
namedRouter.get("class.edit", "/class/edit/:id", classController.edit);
namedRouter.post("class.update", '/class/update', request_param.any(), classController.update);
namedRouter.get("class.delete", "/class/delete/:id", classController.delete);
namedRouter.get("class.viewAllClass", '/class/viewAllClass/:id', classController.viewAllClasses);


//Export the express.Router() instance
module.exports = router;