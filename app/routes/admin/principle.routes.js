const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const multer = require('multer');
const principleController = require('user/controllers/principle.controller');

const Storage = multer.diskStorage({
	destination: (req, file, callback) => {
		callback(null, "./public/uploads/user");
	},
	filename: (req, file, callback) => {
		callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname.replace(/\s/g, '_'));
	}
});

const uploadFile = multer({storage: Storage});
const request_param = multer();

namedRouter.all('/principle*', auth.authenticate);

// principle Listing Route
namedRouter.get("principle.list", '/principle/list', principleController.list);
// principle Edit Route
namedRouter.get("principle.edit", "/principle/edit/:id", principleController.edit);
// Genre Update Route
namedRouter.post("principle.update", '/principle/update', request_param.any(), principleController.update);
// Genre Delete Route
namedRouter.get("principle.delete", "/principle/delete/:id", principleController.delete);

// Export the express.Router() instance

namedRouter.post("principle.getall", '/principle/getall', async (req, res) => {
    try {
        const success = await principleController.getAllPrinciple(req, res);
        res.send({"meta": success.meta, "data": success.data});
    } catch (error) {
        res.status(error.status).send(error);
    }
});
module.exports = router;