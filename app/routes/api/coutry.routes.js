const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const multer = require('multer');
const CountryController = require('webservice/country.controller');

const request_param = multer();

// namedRouter.all('/country*', auth.authenticateAPI);


/**
 * @api {get} /country/list Get Country List
 * @apiVersion 1.0.0
 * @apiGroup Country
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [
        {
            "_id": "5d316fd8822fa522c7a6edff",
            "country_name": "Australia",
            "isActive": true,
            "isDeleted": false,
            "createdAt": "2019-04-15T08:24:40.827Z",
            "__v": 0
        },
        {
            "_id": "5d316feb822fa522c7a6ef16",
            "country_name": "India",
            "isActive": true,
            "isDeleted": false,
            "createdAt": "2019-04-15T08:24:48.004Z",
            "__v": 0
        }
    ],
    "message": "Data Fetched Successfully."
}
*/
namedRouter.get("api.coutry.list", '/country/list', async (req, res) => {
  try {
    const success = await CountryController.list(req, res);
    res.status(success.status).send(success);
  } catch (error) {
    res.status(error.status).send(error);
  }
});


// Export the express.Router() instance
module.exports = router;