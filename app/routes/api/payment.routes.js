const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const multer = require('multer');
const paymentController = require('webservice/payment.controller');

const request_param = multer();


// namedRouter.all('/subscription*', auth.authenticate);


/**
 * @api {post} /subscription/payment Subscription Payment
 * @apiVersion 1.0.0
 * @apiGroup Payment
 * @apiParam {String} package_type monthly/yearly
 * @apiParam {Objectid} parent_id Parent Id
 * @apiParam {String} child_count Total Child Count
 * @apiParam {Float} additional_price Additional Price
 * @apiParam {Float} plan_price Plan Price
 * @apiParam {Number} card_number Card Number
 * @apiParam {Number} exp_month Card Expiry Month
 * @apiParam {Number} exp_year Card Expiry Year
 * @apiParam {Number} cvc Card CVV
 * @apiParam {String} payment_type During registration send 'reg' and After Login send 'additional'
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "package_type": "monthly",
        "total_price": 25,
        "parent_id": "5d667e763fac9074c866312b",
        "customer_id": "cus_FmKCwg99kbOFYK",
        "plan_id": "plan_FmKCbRlwRfGjf5",
        "subscription_id": "sub_FmKCkeFtIDtXK9",
        "payment_status": "Completed",
        "isDeleted": false,
        "createdAt": "2019-09-09T11:59:41.819Z",
        "_id": "5d763eee4b9ff20d64788910",
        "__v": 0
    },
    "message": "You have successfully purchased the Plan."
}
*/
namedRouter.post("api.subscription.payment", '/subscription/payment', request_param.any(), async (req, res) => {
  try {
    const success = await paymentController.makePayment(req, res);
    res.status(success.status).send(success);
  } catch (error) {
    res.status(error.status).send(error);
  }
});


/**
 * @api {post} /subscription/addchildpayment Subscription Payment and Add Child
 * @apiVersion 1.0.0
 * @apiGroup Payment
 * @apiParam {ObjectId} parent_id Parent Id
 * @apiParam {Array} child [child[0][child_email],child[0][child_name],child[0][school_id],child[0][child_password],child[1][child_email],child[1][child_name],child[1][school_id],child[1][child_password]]
 * @apiParam {ObjectId} plan_id Plan Id
 * @apiParam {Number} card_number Card Number
 * @apiParam {Number} expiry_month Card Expiry Month
 * @apiParam {Number} expiry_year Card Expiry Year
 * @apiParam {Number} cvv Card CVV
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "result": [
            {
                "payment": {
                    "package_type": "Default",
                    "total_price": 5,
                    "parent_id": "5d668a7b58178a28263e21c6",
                    "child": null,
                    "subscription_amount": 5,
                    "customer_id": "cus_G20RqM6iXlps53",
                    "plan_id": "plan_G20RmGPrIdYtNb",
                    "subscription_id": "sub_G20RoRFFkrq5YX",
                    "next_billing_date": "2019-11-21T08:39:40.000Z",
                    "payment_status": "Completed",
                    "isDeleted": false,
                    "createdAt": "2019-10-21T08:39:32.563Z",
                    "_id": "5dad6ecea0455059991ff255",
                    "__v": 0
                },
                "child": {
                    "first_name": "richi",
                    "last_name": "",
                    "email": "baby6@yopmail.com",
                    "registered_as": "",
                    "password": "$2a$08$i0Ews18vA5tQv9lP3XSK2.UkNuJewAUsWBtltDWzoOfViBN7FsGnC",
                    "teacher_id": null,
                    "parent_id": "5d668a7b58178a28263e21c6",
                    "principle_id": null,
                    "school": "5d9deb20d881226a3c6830ce",
                    "avatar_id": null,
                    "profile_image": "",
                    "user_balance": 0,
                    "phone": "",
                    "address": "",
                    "city": "",
                    "state": "",
                    "country": "",
                    "isVerified": "Yes",
                    "grade": null,
                    "verifyToken": null,
                    "deviceToken": "",
                    "deviceType": "",
                    "isDeleted": false,
                    "isActive": true,
                    "_id": "5dad6ecea0455059991ff256",
                    "role": "5cd56d7bc6fff8f0dc8db24b",
                    "avatars": [],
                    "__v": 0
                }
            },
            {
                "payment": {
                    "package_type": "Default",
                    "total_price": 5,
                    "parent_id": "5d668a7b58178a28263e21c6",
                    "child": null,
                    "subscription_amount": 5,
                    "customer_id": "cus_G20RqM6iXlps53",
                    "plan_id": "plan_G20R1BTnQrK0Bd",
                    "subscription_id": "sub_G20RpVHgSnGlvU",
                    "next_billing_date": "2019-11-21T08:39:43.000Z",
                    "payment_status": "Completed",
                    "isDeleted": false,
                    "createdAt": "2019-10-21T08:39:32.563Z",
                    "_id": "5dad6ed1a0455059991ff257",
                    "__v": 0
                },
                "child": {
                    "first_name": "roy",
                    "last_name": "",
                    "email": "baby7@yopmail.com",
                    "registered_as": "",
                    "password": "$2a$08$dED9YUm0OpZ3Q9bR4k4Zje/okJhhz4j.BOdDJ6ct/GZOBypSSuwqS",
                    "teacher_id": null,
                    "parent_id": "5d668a7b58178a28263e21c6",
                    "principle_id": null,
                    "school": "5d9deb20d881226a3c6830ce",
                    "avatar_id": null,
                    "profile_image": "",
                    "user_balance": 0,
                    "phone": "",
                    "address": "",
                    "city": "",
                    "state": "",
                    "country": "",
                    "isVerified": "Yes",
                    "grade": null,
                    "verifyToken": null,
                    "deviceToken": "",
                    "deviceType": "",
                    "isDeleted": false,
                    "isActive": true,
                    "_id": "5dad6ed1a0455059991ff258",
                    "role": "5cd56d7bc6fff8f0dc8db24b",
                    "avatars": [],
                    "__v": 0
                }
            }
        ]
    },
    "message": "Successfully added child"
}
*/
namedRouter.post("api.subscription.addchildpayment", '/subscription/addchildpayment', request_param.any(), async (req, res) => {
	try {
		
		const success = await paymentController.addChildWithPayment(req, res);
		res.status(success.status).send(success);
	}
	catch (error) {
		
		res.status(error.status).send(error);
	}
});

namedRouter.post("api.subscription.upgradepayment", '/subscription/upgradepayment', request_param.any(), async (req, res) => {
	try {
		console.log("csacac");
		const success = await paymentController.existChildUpgradePayment(req, res);
		res.status(success.status).send(success);
	}
	catch (error) {
		console.log("193>>", error.message);
		res.status(error.status).send(error);
	}
});

// Export the express.Router() instance
module.exports = router;