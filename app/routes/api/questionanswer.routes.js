const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const multer = require('multer');
const QAController = require('webservice/questionanswer.controller');
const storage = multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null, './public/uploads/question-answer/');
  },
  filename: function (req, file, callback) {
    callback(null, "answer_" + Date.now() + "_" + file.originalname.replace(/\s/g, '_') + ".png");
  }

});
const request_param = multer();

const uploadImage = multer({
  storage: storage
});

namedRouter.all('/question*', auth.authenticateAPI);


/**
 * @api {post} /question/saveUserQA User Answer Submit
 * @apiVersion 1.0.0
 * @apiGroup Question
 * @apiParam {string} book_id Book Id
 * @apiParam {string} chapter_id Chapter Id
 * @apiParam {array} qadetails (example: qadetails[0][question_id]:5cfa2fae902d713c82f9e57b
  qadetails[0][answer_type]:'descriptive'/'drawing'
  qadetails[0][answer]:Sample Answer 
  qadetails[1][question_id]:5cfe0fd935b73723b076b1e1
  qadetails[0][answer_type]:'descriptive'/'drawing'
  qadetails[1][answer]:Sample Answer 2) 
 * @apiHeader x-access-token User's Access token
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "status": "Active",
        "isDeleted": false,
        "createdAt": "2019-07-26T08:15:19.173Z",
        "_id": "5d3ab6a54e55172be2bff154",
        "book_id": "5cfa0746839f1c081b1e2c74",
        "chapter_id": "5cfa092f8707f41cce5cc46b",
        "qadetails": [
            {
                "answer_type": "descriptive",
                "answer": "just another text answer",
                "question_id": "5cfe0e4535b73723b076b1e0"
            },
            {
                "answer_type": "drawing",
                "answer": "answer_1564128933978_3.png",
                "question_id": "5cfe0fd935b73723b076b1e1"
            },
            {
                "answer_type": "drawing",
                "answer": "answer_1564128933984_8EaQRzerSNSp._UX970_TTW__.jpg",
                "question_id": "5cfe0dc835b73723b076b1df"
            }
        ],
        "user_id": "5d39aa3c08b3f27ee15090f6",
        "__v": 0
    },
    "message": "Your Answer submitted Successfully"
}
*/
namedRouter.post("api.question.saveUserQA", '/question/saveUserQA', uploadImage.any(), async (req, res) => {
	try {
		const success = await QAController.saveUserQA(req, res);
		res.status(success.status).send(success);
	}
	catch (error) {
		res.status(error.status).send(error);
	}
});


// Export the express.Router() instance
module.exports = router;