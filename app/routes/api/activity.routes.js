const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const multer = require('multer');
const ActivityController = require('webservice/activity.controller');
const request_param = multer();

namedRouter.post("api.activityaction.providecoin", '/activityaction/providecoin', request_param.any(), async (req, res) => {
	try {
		const success = await ActivityController.provideCoinChild(req, res);
		res.status(success.status).send(success);
	}
	catch (error) {
		res.status(error.status).send(error);
	}
});

namedRouter.all('/activityaction*', auth.authenticateAPI);

/**
 * @api {post} /activityaction/start Activity Started
 * @apiVersion 1.0.0
 * @apiGroup Activity
 * @apiHeader x-access-token User's Access token
 * @apiParam {ObjectId} chapter_id Chapter Id
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "student_id": "5d7660124439bc7d94880398",
        "book_id": "5cfa1a34779ccd034eb17af0",
        "chapter_id": "5cfa7a5828512408daa170a0",
        "chapter_pages": [
            {
                "_id": "5cfa7a8828512408daa170a1",
                "status": "not_started"
            },
            {
                "_id": "5cfa7b7828512408daa170a8",
                "status": "not_started"
            },
            {
                "_id": "5d81dd71a61b010324c3d190",
                "status": "not_started"
            },
            {
                "_id": "5d81dd94a61b010324c3d197",
                "status": "not_started"
            },
            {
                "_id": "5d81dda2a61b010324c3d19c",
                "status": "not_started"
            },
            {
                "_id": "5d81ddb3a61b010324c3d1a2",
                "status": "not_started"
            }
        ],
        "activity_type": "ongoing",
        "isDeleted": false,
        "createdAt": "2019-09-18T10:25:12.867Z",
        "_id": "5d82060d32137d0678674c3f",
        "__v": 0
    },
    "message": "Started successfully."
}
*/
namedRouter.post("api.activityaction.start", '/activityaction/start', request_param.any(), async (req, res) => {
    try {
        const success = await ActivityController.activityStart(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});


/**
 * @api {post} /activityaction/ongoing Activity Ongoing
 * @apiVersion 1.0.0
 * @apiGroup Activity
 * @apiHeader x-access-token User's Access token
 * @apiParam {ObjectId} chapter_id Chapter Id
 * @apiParam {Array} complete_page_id Pass completed page Id
 * @apiParam {Array} ongoing_page_id Pass completed page Id
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [],
    "message": "Updated successfully."
}
*/
namedRouter.post("api.activityaction.ongoing", '/activityaction/ongoing', request_param.any(), async (req, res) => {
    try {
        const success = await ActivityController.activityOngoing(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});


/**
 * @api {post} /activityaction/end Activity End
 * @apiVersion 1.0.0
 * @apiGroup Activity
 * @apiHeader x-access-token User's Access token
 * @apiParam {ObjectId} chapter_id Chapter Id
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [
        {
            "question": "<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece?</p>\r\n",
            "question_type": "descriptive",
            "points": 2,
            "status": "Active",
            "isDeleted": false,
            "_id": "5d836a014cedf80d0bebb5fa",
            "book_id": "5cfa1a34779ccd034eb17af0",
            "title": "5cfa7a5828512408daa170a0",
            "__v": 0
        },
        {
            "question": "<p>Ipsum is not simply random text. It has roots in a piece?</p>\r\n",
            "question_type": "descriptive",
            "points": 4,
            "status": "Active",
            "isDeleted": false,
            "_id": "5d836a1f4cedf80d0bebb5fb",
            "book_id": "5cfa1a34779ccd034eb17af0",
            "title": "5cfa7a5828512408daa170a0",
            "__v": 0
        },
        {
            "question": "<p>There are many variations of passages of Lorem Ipsum available?</p>\r\n",
            "question_type": "descriptive",
            "points": 8,
            "status": "Active",
            "isDeleted": false,
            "_id": "5d836b27a1087e0d1990810a",
            "book_id": "5cfa1a34779ccd034eb17af0",
            "title": "5cfa7a5828512408daa170a0",
            "__v": 0
        }
    ],
    "message": "Question fetched successfully."
}
*/
namedRouter.post("api.activityaction.end", '/activityaction/end', request_param.any(), async (req, res) => {
    try {
        const success = await ActivityController.activityEnd(req, res);
        res.status(success.status).send(success);
    }
    catch (error) {
        res.status(error.status).send(error);
    }
});






// Export the express.Router() instance
module.exports = router;