const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);


const bookController = require('webservice/book.controller');



/**
 * @api {get} /book Get All Active Books
 * @apiVersion 1.0.0
 * @apiGroup Book
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [
        {
            "title": "Sherlock Holmes",
            "subtitle": "HEAD",
            "slug": "",
            "genre": {
                "title": "Classic",
                "slug": "",
                "status": "Active",
                "isDeleted": false,
                "_id": "5cfa05269f4ec670fb8b4b1f",
                "__v": 0
            },
            "author": {
                "title": "Arthur Conan Doyle",
                "slug": "",
                "status": "Active",
                "isDeleted": false,
                "_id": "5cfa0648839f1c081b1e2c6f",
                "__v": 0
            },
            "picture": "picture_1559904498828_ls.jpg",
            "chapter_num": 5,
            "status": "Active",
            "isDeleted": false,
            "_id": "5cfa0746839f1c081b1e2c74",
            "__v": 0
        },
        {
            "title": "adfasfdafdf",
            "subtitle": "fdafdfdf",
            "slug": "",
            "genre": {
                "title": "Classic",
                "slug": "",
                "status": "Active",
                "isDeleted": false,
                "_id": "5cfa05269f4ec670fb8b4b1f",
                "__v": 0
            },
            "author": {
                "title": "Frank Miller",
                "slug": "",
                "status": "Active",
                "isDeleted": false,
                "_id": "5cfa062a839f1c081b1e2c6e",
                "__v": 0
            },
            "picture": "picture_1559894579683_dummy.png",
            "chapter_num": 10,
            "status": "Active",
            "isDeleted": false,
            "_id": "5cfa1a34779ccd034eb17af0",
            "__v": 0
        }
    ],
    "message": "Data Fetched Successfully."
}
*/

namedRouter.get("api.book", '/book', async (req, res) => {
    try {
        const success = await bookController.list(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

/**
 * @api {get} /book/:keyword Get Active Books By Keyword 
 * @apiVersion 1.0.0
 * @apiGroup Book
 * @apiParam {string} keyword Keyword
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [
        {
            "_id": "5cfa1a34779ccd034eb17af0",
            "title": "adfasfdafdf",
            "subtitle": "fdafdfdf",
            "slug": "",
            "genre": "5cfa05269f4ec670fb8b4b1f",
            "author": "5cfa062a839f1c081b1e2c6e",
            "picture": "picture_1559894579683_dummy.png",
            "chapter_num": 10,
            "status": "Active",
            "isDeleted": false,
            "__v": 0,
            "genre_docs": {
                "_id": "5cfa05269f4ec670fb8b4b1f",
                "title": "Classic",
                "slug": "",
                "status": "Active",
                "isDeleted": false,
                "__v": 0
            },
            "author_docs": {
                "_id": "5cfa062a839f1c081b1e2c6e",
                "title": "Frank Miller",
                "slug": "",
                "status": "Active",
                "isDeleted": false,
                "__v": 0
            }
        }
    ],
    "message": "Data Fetched Successfully."
}
*/

namedRouter.get("api.book.keyword", '/book/:keyword', async (req, res) => {
    try {
        const success = await bookController.bookByKeyword(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

/**
 * @api {get} /getQuestions/:chapter_id Get Questions By Chapter ID
 * @apiVersion 1.0.0
 * @apiGroup Book
 * @apiParam {string} chapter_id Chapter Id
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [
        {
            "_id": "5d84effede7dc014382332c6",
            "question": "<p>Question 1</p>\r\n",
            "question_type": "descriptive",
            "points": 10,
            "book_id": "5cfa1a34779ccd034eb17af0",
            "title": "5cfa7a5828512408daa170a0",
            "chapter_info": {
                "_id": "5cfa7a5828512408daa170a0",
                "title": "Chapter 1 ",
                "description": "desc 1"
            },
            "book_info": {
                "_id": "5cfa1a34779ccd034eb17af0",
                "title": "Sample Book",
                "subtitle": "Sub Title"
            }
        },
        {
            "_id": "5d88967e700cc20b5ac80d12",
            "question": "<p>Question 1A</p>\r\n",
            "question_type": "drawing",
            "points": 10,
            "book_id": "5cfa1a34779ccd034eb17af0",
            "title": "5cfa7a5828512408daa170a0",
            "chapter_info": {
                "_id": "5cfa7a5828512408daa170a0",
                "title": "Chapter 1 ",
                "description": "desc 1"
            },
            "book_info": {
                "_id": "5cfa1a34779ccd034eb17af0",
                "title": "Sample Book",
                "subtitle": "Sub Title"
            }
        }
    ],
    "message": "Question fetched successfully."
}
*/

namedRouter.get("api.book.getQuestions", '/getQuestions/:chapter_id', async (req, res) => {
    try {
        const success = await bookController.questionByChapter(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});


/**
 * @api {get} /grade/:keyword  Books By Grade
 * @apiVersion 1.0.0
 * @apiGroup Book
 * @apiParam {Number} keyword [1 to 8 and 'all']
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [
        {
            "_id": "5cfa0746839f1c081b1e2c74",
            "title": "Sherlock Holmes",
            "subtitle": "HEAD",
            "slug": "",
            "grade": 5,
            "genre": "5cfa05269f4ec670fb8b4b1f",
            "author": "5cfa0648839f1c081b1e2c6f",
            "picture": "picture_1559904498828_ls.jpg",
            "chapter_num": 5,
            "status": "Active",
            "isDeleted": false,
            "__v": 0,
            "genre_docs": {
                "_id": "5cfa05269f4ec670fb8b4b1f",
                "title": "Classic",
                "slug": "",
                "status": "Active",
                "isDeleted": false,
                "__v": 0
            },
            "author_docs": {
                "_id": "5cfa0648839f1c081b1e2c6f",
                "title": "Arthur Conan Doyle",
                "slug": "",
                "status": "Active",
                "isDeleted": false,
                "__v": 0
            }
        }
    ],
    "message": "Data Fetched Successfully."
}
*/
namedRouter.get("api.book.grade", '/grade/:keyword', async (req, res) => {
    try {
        const success = await bookController.getBooksByGrade(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

namedRouter.all('/books*', auth.authenticateAPI);

/**
 * @api {get} /books/details/:id Get Book Details
 * @apiVersion 1.0.0
 * @apiGroup Book
 * @apiParam {string} id Book ID
 * @apiHeader x-access-token User's Access token
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [
        {
            "_id": "5cfa1a34779ccd034eb17af0",
            "title": "adfasfdafdf",
            "subtitle": "fdafdfdf",
            "chapter_number": 10,
            "chapters": [
                {
                    "_id": "5cfa7a5828512408daa170a0",
                    "title": "Title 1 ",
                    "description": "desc 1",
                    "chapter_no": 1,
                    "chapter_icon": "chapter_icon_1559919192278_car_3.jpeg",
                    "chapter_pages": 10,
                    "isDeleted": false,
                    "status": "Active",
                    "book_id": "5cfa1a34779ccd034eb17af0",
                    "pages": [
                        {
                            "page_title": "page title 1 ",
                            "page_content": "<p>page desc&nbsp;1&nbsp;</p>\r\n",
                            "page_number": 1,
                            "_id": "5cfa7a8828512408daa170a1",
                            "status": "Active"
                        },
                        {
                            "page_title": "page title 2",
                            "page_content": "<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>\r\n",
                            "page_number": 2,
                            "_id": "5cfa7b7828512408daa170a8",
                            "status": "Active"
                        }
                    ],
                    "__v": 0,
                    "isCurrentChapter": true
                },
                {
                    "_id": "5cfa7cc4c8cae8241f9935a6",
                    "title": "Title 2",
                    "description": "desc 2",
                    "chapter_no": 2,
                    "chapter_icon": "chapter_icon_1559919812824_car_4.jpeg",
                    "chapter_pages": 5,
                    "isDeleted": false,
                    "status": "Active",
                    "book_id": "5cfa1a34779ccd034eb17af0",
                    "pages": [],
                    "__v": 0,
                    "isCurrentChapter": false
                },
                {
                    "_id": "5cfa7ea0c8cae8241f9935a7",
                    "title": "title 3",
                    "description": "desc 3",
                    "chapter_no": 3,
                    "chapter_icon": "chapter_icon_1559920288681_car_3.jpeg",
                    "chapter_pages": 10,
                    "isDeleted": false,
                    "status": "Active",
                    "book_id": "5cfa1a34779ccd034eb17af0",
                    "pages": [],
                    "__v": 0,
                    "isCurrentChapter": false
                },
                {
                    "_id": "5cfa7ecbc8cae8241f9935a8",
                    "title": "title 4",
                    "description": "desc 4",
                    "chapter_no": 4,
                    "chapter_icon": "chapter_icon_1559920331141_car_5.png",
                    "chapter_pages": 20,
                    "isDeleted": false,
                    "status": "Active",
                    "book_id": "5cfa1a34779ccd034eb17af0",
                    "pages": [],
                    "__v": 0,
                    "isCurrentChapter": false
                },
                {
                    "_id": "5cfa805d23a56b47e0417096",
                    "title": "Title 5",
                    "description": "desc 5 ",
                    "chapter_no": 5,
                    "chapter_icon": "chapter_icon_1559920733521_fl.jpeg",
                    "chapter_pages": 30,
                    "isDeleted": false,
                    "status": "Active",
                    "book_id": "5cfa1a34779ccd034eb17af0",
                    "pages": [],
                    "__v": 0,
                    "isCurrentChapter": false
                }
            ]
        }
    ],
    "message": "Data Fetched Successfully."
}
*/

namedRouter.get("api.books.details", '/books/details/:id', async (req, res) => {
    try {
        const success = await bookController.bookDetails(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});


// Export the express.Router() instance
module.exports = router;