const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const multer = require('multer');
const request_param = multer();

const schoolController = require('webservice/school.controller');



/**
 * @api {get} /school Get All Active schools
 * @apiVersion 1.0.0
 * @apiGroup school
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [
        {
            "title": "Sherlock Holmes",
            "subtitle": "HEAD",
            "slug": "",
            "genre": {
                "title": "Classic",
                "slug": "",
                "status": "Active",
                "isDeleted": false,
                "_id": "5cfa05269f4ec670fb8b4b1f",
                "__v": 0
            },
            "author": {
                "title": "Arthur Conan Doyle",
                "slug": "",
                "status": "Active",
                "isDeleted": false,
                "_id": "5cfa0648839f1c081b1e2c6f",
                "__v": 0
            },
            "picture": "picture_1559904498828_ls.jpg",
            "chapter_num": 5,
            "status": "Active",
            "isDeleted": false,
            "_id": "5cfa0746839f1c081b1e2c74",
            "__v": 0
        },
        {
            "title": "adfasfdafdf",
            "subtitle": "fdafdfdf",
            "slug": "",
            "genre": {
                "title": "Classic",
                "slug": "",
                "status": "Active",
                "isDeleted": false,
                "_id": "5cfa05269f4ec670fb8b4b1f",
                "__v": 0
            },
            "author": {
                "title": "Frank Miller",
                "slug": "",
                "status": "Active",
                "isDeleted": false,
                "_id": "5cfa062a839f1c081b1e2c6e",
                "__v": 0
            },
            "picture": "picture_1559894579683_dummy.png",
            "chapter_num": 10,
            "status": "Active",
            "isDeleted": false,
            "_id": "5cfa1a34779ccd034eb17af0",
            "__v": 0
        }
    ],
    "message": "Data Fetched Successfully."
}
*/

namedRouter.get("api.school", '/school', async (req, res) => {
    try {
        const success = await schoolController.list(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(500).send({ error: error.message });
    }
});
/**
 * @api {get} /school/autoComplete?school_key= School search autoComplete
 * @apiVersion 1.0.0
 * @apiGroup school
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [
        {
            "_id": "5d1373d47c2beb0d6c2af4e8",
            "school_name": "Carmel School",
            "isDeleted": false,
            "isActive": true,
            "__v": 0
        },
        {
            "_id": "5d149edc585379f913ff0457",
            "school_name": "Carnoc School",
            "isDeleted": false,
            "isActive": true,
            "__v": 0
        }
    ],
    "message": "Data Fetched Successfully."
}
*/
namedRouter.get("api.school.autoComplete", '/school/autoComplete', async (req, res) => {
    try {
        const success = await schoolController.schoolAutocomplete(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(500).send({ error: error.message });
    }
});
// Export the express.Router() instance
module.exports = router;
