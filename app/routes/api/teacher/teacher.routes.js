const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);


const multer = require('multer');
const teacherController = require('webservice/teacher/teacher.controller');
const storage = multer.diskStorage({
    destination: function (req, file, callback) {
        callback(null, './public/uploads/teacher/');
    },
    filename: function (req, file, callback) {
        callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname.replace(/\s/g, '_'));
    }

});
const request_param = multer();

const uploadImage = multer({
    storage: storage
});


/**
 * @api {post} /teacher/store Register
 * @apiVersion 1.0.0
 * @apiGroup Teacher
 * @apiParam {string} first_name First Name
 * @apiParam {string} last_name  Last Name
 * @apiParam {string} email Email.
 * @apiParam {id} school School.
 * @apiParam {string} password  Password.
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "first_name": "Teacher ",
        "last_name": "sir",
        "email": "teacherSir@gmail.com",
        "password": "$2a$08$Ni7ZK47Zs3aRtbrVMlmQy.0KTFKsMH5apwcOIY1gYHmfAPB32YfSm",
        "teacher_id": null,
        "parent_id": null,
        "principle_id": null,
        "school_name": "",
        "isVerified": "No",
        "verifyToken": 9869,
        "deviceToken": "",
        "deviceType": "",
        "isDeleted": false,
        "isActive": true,
        "_id": "5ced44553d45955a272c583d",
        "role": "5cd56d27c6fff8f0dc8dae69",
        "__v": 0
    },
    "message": "You have successfully regsitered."
}
*/

namedRouter.post("api.teacher.register", '/teacher/store', request_param.any(), async (req, res) => {
    try {
        const success = await teacherController.store(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(500).send({
            error: error.message
        });
    }
});
/**
 * @api {get} /teacher/BySchool/:id Get teacher by school
 * @apiVersion 1.0.0
 * @apiGroup Teacher
 * @apiParam id school id
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [
        {
            "first_name": "principle",
            "last_name": "smith",
            "email": "principle@gmail.com",
            "password": "$2a$08$AwKHLLRXLA489J0Nfj/Q3O3ZhzA0Z8ZIYFFdNY8Koo3v4/vW28st.",
            "teacher_id": null,
            "parent_id": null,
            "principle_id": null,
            "school": "5d1373d47c2beb0d6c2af4e8",
            "phone": "",
            "address": "",
            "city": "",
            "state": "",
            "country": "",
            "isVerified": "No",
            "verifyToken": null,
            "deviceToken": "",
            "deviceType": "",
            "isDeleted": false,
            "isActive": true,
            "_id": "5cfa065bc1891efa1a543b70",
            "role": "5cd56d27c6fff8f0dc8dae69"
        },
        {
            "first_name": "Archit",
            "last_name": "Test",
            "email": "levivicuye@2emailock.com",
            "password": "$2a$08$hleS5SYneo1I3l2WBVlA3OWIHBaB4c7C34OFfETiDOuEbNfuC982q",
            "teacher_id": null,
            "parent_id": null,
            "principle_id": null,
            "school": "5d1373d47c2beb0d6c2af4e8",
            "phone": "",
            "address": "",
            "city": "",
            "state": "",
            "country": "",
            "isVerified": "No",
            "verifyToken": 5199,
            "deviceToken": "",
            "deviceType": "",
            "isDeleted": false,
            "isActive": true,
            "_id": "5d1373d47c2beb0d6c2af4e9",
            "role": "5cd56d27c6fff8f0dc8dae69",
            "__v": 0
        }
    ],
    "message": "Teacher list fetched Successfully"
}
*/
namedRouter.get("api.teacher.BySchool", '/teacher/BySchool/:id', async (req, res) => {
    try {
        const success = await teacherController.teacherBySchool(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(500).send({
            error: error.message
        });
    }
});
/* auth routes start from here */
namedRouter.all('/teacher*', auth.authenticateAPI);


/**
 * @api {post} /teacher/change/password  Change Password
 * @apiVersion 1.0.0
 * @apiGroup Teacher
 * @apiParam {string} old_password Old password
 * @apiParam {string} new_password New password
 * @apiHeader x-access-token User's Access token
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "first_name": "Teacher ",
        "last_name": "sir",
        "email": "teacherSir@gmail.com",
        "password": "$2a$08$gYO0BRQrD0uEG0wr2CZK5OxrDVdm8fWujDSuTb1sAAiWpkCfpHL6y",
        "teacher_id": null,
        "parent_id": null,
        "principle_id": null,
        "school_name": "",
        "isVerified": "Yes",
        "verifyToken": null,
        "deviceToken": "",
        "deviceType": "",
        "isDeleted": false,
        "isActive": true,
        "_id": "5ced44553d45955a272c583d",
        "role": "5cd56d27c6fff8f0dc8dae69",
        "__v": 0
    },
    "message": "Password Changed Successfully"
}
*/

namedRouter.post("api.teacher.changePassword", '/teacher/change/password', request_param.any(), async (req, res) => {
    try {
        const success = await teacherController.changePassword(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});
/**
 * @api {post} /teacher/edit-profile  Edit profile of teacher
 * @apiVersion 1.0.0
 * @apiGroup Teacher
 * @apiParam {id} id User id
 * @apiParam {string} first_name First name
 * @apiParam {string} last_name Last name
 * @apiParam {id} school School id
 * @apiHeader x-access-token User's Access token
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "first_name": "new TesT",
        "last_name": "Sir",
        "email": "teacher@gmail.com",
        "password": "$2a$08$AwKHLLRXLA489J0Nfj/Q3O3ZhzA0Z8ZIYFFdNY8Koo3v4/vW28st.",
        "teacher_id": null,
        "parent_id": null,
        "principle_id": "5cfa065bc1891efa1a543b70",
        "school": "5d1373d47c2beb0d6c2af4e8",
        "isVerified": "No",
        "verifyToken": null,
        "deviceToken": "",
        "deviceType": "",
        "isDeleted": false,
        "isActive": true,
        "_id": "5cfa0671c1891efa1a543c54",
        "school_name": "Lox Digital",
        "role": "5cd56d27c6fff8f0dc8dae69"
    },
    "message": "Profile Updated Successfully"
}
*/
namedRouter.post("api.teacher.editProfile", '/teacher/edit-profile', request_param.any(), async (req, res) => {
    try {
        const success = await teacherController.editProfileTeacher(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(500).send({
            error: error.message
        });
    }
});

/**
 * @api {post} /teacher/studentsActivities/:classid  Get Student Activites By Class
 * @apiVersion 1.0.0
 * @apiGroup Teacher
 * @apiHeader x-access-token User's Access token
 * @apiParam {ObjectId} classid Pass Class Id in Params
 * @apiParam {Array} student_id Pass Student Id of that class
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [
        {
            "_id": "5d8b30cecc0c2b384fc26014",
            "student_id": "5d7660124439bc7d94880398",
            "book_id": "5cfa1a34779ccd034eb17af0",
            "chapter_id": "5cfa7a5828512408daa170a0",
            "points_earned": 10,
            "activity_type": "completed",
            "isDeleted": false,
            "createdAt": 1569402928910,
            "completedAt": 1569403108694,
            "chapter_pages": [
                {
                    "status": "completed",
                    "initialDate": 1569402928910,
                    "ongoingDate": 1569403103761,
                    "completedDate": 1569403104893,
                    "_id": "5cfa7a8828512408daa170a1"
                },
                {
                    "status": "completed",
                    "initialDate": 1569402928910,
                    "ongoingDate": 1569403103761,
                    "completedDate": 1569403104893,
                    "_id": "5cfa7b7828512408daa170a8"
                },
                {
                    "status": "completed",
                    "initialDate": 1569402928910,
                    "ongoingDate": 1569403104892,
                    "completedDate": 1569403105804,
                    "_id": "5d81dd71a61b010324c3d190"
                },
                {
                    "status": "completed",
                    "initialDate": 1569402928910,
                    "ongoingDate": 1569403104892,
                    "completedDate": 1569403105804,
                    "_id": "5d81dd94a61b010324c3d197"
                },
                {
                    "status": "completed",
                    "initialDate": 1569402928910,
                    "ongoingDate": 1569403105804,
                    "completedDate": 1569403106631,
                    "_id": "5d81dda2a61b010324c3d19c"
                },
                {
                    "status": "completed",
                    "initialDate": 1569402928910,
                    "ongoingDate": 1569403105804,
                    "completedDate": 1569403106631,
                    "_id": "5d81ddb3a61b010324c3d1a2"
                }
            ],
            "__v": 0,
            "user_info": {
                "_id": "5d7660124439bc7d94880398",
                "first_name": "Frank",
                "last_name": "Ross",
                "email": "frank@yopmail.com",
                "registered_as": "",
                "password": "$2a$08$fPoZydp/AOjiWZ3s5q0A9uKSS4or36TzjAQtbZoJ02sE4nt1euMp6",
                "teacher_id": null,
                "parent_id": "5d765fc44439bc7d94880396",
                "principle_id": null,
                "school": null,
                "avatar_id": null,
                "user_balance": 50,
                "phone": "",
                "address": "",
                "city": "",
                "state": "",
                "country": "",
                "isVerified": "Yes",
                "grade": null,
                "verifyToken": null,
                "deviceToken": "",
                "deviceType": "",
                "isDeleted": false,
                "isActive": true,
                "role": "5cd56d7bc6fff8f0dc8db24b",
                "__v": 0
            },
            "chapter_info": {
                "_id": "5cfa7a5828512408daa170a0",
                "title": "Chapter 1 ",
                "description": "desc 1",
                "chapter_no": 1,
                "chapter_icon": "chapter_icon_1559919192278_car_3.jpeg",
                "chapter_pages": 10,
                "isDeleted": false,
                "status": "Active",
                "book_id": "5cfa1a34779ccd034eb17af0",
                "pages": [
                    {
                        "page_title": "page title 1 ",
                        "page_content": "<p>page desc&nbsp;1&nbsp;</p>\r\n",
                        "page_number": 1,
                        "_id": "5cfa7a8828512408daa170a1",
                        "status": "Active"
                    },
                    {
                        "page_title": "page title 2",
                        "page_content": "<p>asdasdfafsadsads</p>\r\n",
                        "page_number": 2,
                        "_id": "5cfa7b7828512408daa170a8",
                        "status": "Active"
                    },
                    {
                        "page_title": "page title 3",
                        "page_content": "<p>sdgfdgfdhrthfdsgffdg</p>\r\n",
                        "page_number": 3,
                        "_id": "5d81dd71a61b010324c3d190",
                        "status": "Active"
                    },
                    {
                        "page_title": "page title 4",
                        "page_content": "<p>dsfdsfdsfdsfdsgreerwewqqweqteqteq</p>\r\n",
                        "page_number": 4,
                        "_id": "5d81dd94a61b010324c3d197",
                        "status": "Active"
                    },
                    {
                        "page_title": "page title 5",
                        "page_content": "<p>sadsadasdasdadaerwerwerwerwerwerwe</p>\r\n",
                        "page_number": 5,
                        "_id": "5d81dda2a61b010324c3d19c",
                        "status": "Active"
                    },
                    {
                        "page_title": "page title 6",
                        "page_content": "<p>asddasdfsfdsgdfgtdhb</p>\r\n",
                        "page_number": 6,
                        "_id": "5d81ddb3a61b010324c3d1a2",
                        "status": "Active"
                    }
                ],
                "__v": 0
            },
            "book_info": {
                "_id": "5cfa1a34779ccd034eb17af0",
                "title": "Sample Book",
                "subtitle": "Sub Title",
                "slug": "",
                "grade": 3,
                "genre": "5cfa05269f4ec670fb8b4b1f",
                "author": "5cfa062a839f1c081b1e2c6e",
                "picture": "picture_1559894579683_dummy.png",
                "chapter_num": 5,
                "status": "Active",
                "isDeleted": false,
                "__v": 0
            }
        }
    ],
    "message": "Activities fetched successfully."
}
*/
namedRouter.post("api.teacher.studentsActivities", '/teacher/studentsActivities/:classid', request_param.any(), async (req, res) => {
    try {
        const success = await teacherController.studentsActivities(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(500).send({
            error: error.message
        });
    }
});

/**
 * @api {get} /teacher/studentActivityDetails/:activity_id  Get student activity details
 * @apiVersion 1.0.0
 * @apiGroup Activity
 * @apiHeader x-access-token User's Access token
 * @apiParam {string} activity_id Activity ID
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [
        {
            "_id": "5d69071287529473b5a8a398",
            "student_id": "5d6685e09d55b31274bca0a6",
            "page_id": "5cfa7a8828512408daa170a1",
            "coins_earned": 45,
            "activity_type": "CC",
            "status": "Active",
            "isDeleted": false,
            "students": {
                "_id": "5d6685e09d55b31274bca0a6",
                "first_name": "luther",
                "last_name": "",
                "email": "luther@yopmail.com",
                "registered_as": "",
                "password": "$2a$08$IcGLXfRUwZG4qJim4/p.teNfSTvBaDFetND22KGZRTwfKxlrpYGkO",
                "teacher_id": null,
                "parent_id": "5d667e763fac9074c866312b",
                "principle_id": null,
                "school": "5d1373d47c2beb0d6c2af4e8",
                "avatar_id": "5d52e019ccd77e0fee01b7ca",
                "user_balance": null,
                "phone": "",
                "address": "",
                "city": "",
                "state": "",
                "country": "",
                "isVerified": "No",
                "grade": 5,
                "verifyToken": null,
                "deviceToken": "",
                "deviceType": "",
                "isDeleted": false,
                "isActive": true,
                "role": "5cd56d7bc6fff8f0dc8db24b",
                "__v": 0
            },
            "chapters": {
                "_id": "5cfa7a5828512408daa170a0",
                "title": "Title 1 ",
                "description": "desc ",
                "chapter_no": 1,
                "chapter_icon": "chapter_icon_1559919192278_car_3.jpeg",
                "chapter_pages": 10,
                "isDeleted": false,
                "status": "Active",
                "book_id": "5cfa1a34779ccd034eb17af0",
                "pages": [
                    {
                        "page_title": "page title 1 ",
                        "page_content": "<p>page desc&nbsp;1&nbsp;</p>\r\n",
                        "page_number": 2,
                        "_id": "5cfa7a8828512408daa170a1",
                        "status": "Active"
                    },
                    {
                        "page_title": "page title 2",
                        "page_content": "<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>\r\n",
                        "page_number": 3,
                        "_id": "5cfa7b7828512408daa170a8",
                        "status": "Active"
                    }
                ],
                "__v": 0
            },
            "books": {
                "_id": "5cfa1a34779ccd034eb17af0",
                "title": "adfasfdafdf",
                "subtitle": "fdafdfdf",
                "slug": "",
                "grade": 3,
                "genre": "5cfa05269f4ec670fb8b4b1f",
                "author": "5cfa062a839f1c081b1e2c6e",
                "picture": "picture_1559894579683_dummy.png",
                "chapter_num": 10,
                "status": "Active",
                "isDeleted": false,
                "__v": 0
            }
        }
    ],
    "message": "Activity fetched"
}
*/
namedRouter.get("api.teacher.studentActivityDetails", '/teacher/studentActivityDetails/:activity_id', request_param.any(), async (req, res) => {
    try {
        const success = await teacherController.studentActivityDetails(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(500).send({
            error: error.message
        });
    }
});


/**
 * @api {get} /teacher/allstudentlist  Get All Student Lists
 * @apiVersion 1.0.0
 * @apiGroup Teacher
 * @apiHeader x-access-token User's Access token
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [
        {
            "_id": "5d77830e0739d20596f54655",
            "first_name": "DAvid Gomes",
            "last_name": "",
            "email": "child10@yopmail.com",
            "avatar_id": {
                "_id": "5d766f30053c406b9979871d",
                "title": "Test Avatar 3",
                "purchase_coin": 5,
                "image_1": "image_1_1568042800610_3.1.png",
                "image_2": "image_2_1568042800641_3.2.png",
                "slug": "",
                "description": "test description",
                "question": "test question",
                "status": "Active",
                "isDeleted": false,
                "__v": 0
            },
            "teacher_id": "5d777b7bdd702c69fdab4615",
            "user_balance": null,
            "role": "student",
            "school_info": {
                "_id": "5d77766ddd702c69fdab460c",
                "school_name": "St Teresa School",
                "isDeleted": false,
                "isActive": true,
                "__v": 0
            },
            "class_info": null
        },
        {
            "_id": "5d779adfc770a40323e8bfa9",
            "first_name": "Child eleven",
            "last_name": "",
            "email": "child11@yopmail.com",
            "avatar_id": null,
            "teacher_id": "5d8080a75957ad7880883cd8",
            "user_balance": null,
            "role": "student",
            "school_info": {
                "_id": "5d77766ddd702c69fdab460c",
                "school_name": "St Teresa School",
                "isDeleted": false,
                "isActive": true,
                "__v": 0
            },
            "class_info": {
                "_id": "5d89de95ff64f15dffcead0f",
                "class_name": "Class 1",
                "teacher_id": "5d777b7bdd702c69fdab4615",
                "student_id": [
                    "5d78ebd606e0e045a4adc436",
                    "5d779adfc770a40323e8bfa9"
                ],
                "status": "Active",
                "isDeleted": false
            }
        },
        {
            "_id": "5d78ebd606e0e045a4adc436",
            "first_name": "Allena Federer",
            "last_name": "",
            "email": "allena@mailinator.com",
            "avatar_id": null,
            "teacher_id": "5d777b7bdd702c69fdab4615",
            "user_balance": null,
            "role": "student",
            "school_info": {
                "_id": "5d77766ddd702c69fdab460c",
                "school_name": "St Teresa School",
                "isDeleted": false,
                "isActive": true,
                "__v": 0
            },
            "class_info": {
                "_id": "5d89de95ff64f15dffcead0f",
                "class_name": "Class 1",
                "teacher_id": "5d777b7bdd702c69fdab4615",
                "student_id": [
                    "5d78ebd606e0e045a4adc436",
                    "5d779adfc770a40323e8bfa9"
                ],
                "status": "Active",
                "isDeleted": false
            }
        },
        {
            "_id": "5d78ebd606e0e045a4adc437",
            "first_name": "Amily Federer",
            "last_name": "",
            "email": "amily@mailinator.com",
            "avatar_id": null,
            "teacher_id": "5d8080a75957ad7880883cd8",
            "user_balance": null,
            "role": "student",
            "school_info": {
                "_id": "5d77766ddd702c69fdab460c",
                "school_name": "St Teresa School",
                "isDeleted": false,
                "isActive": true,
                "__v": 0
            },
            "class_info": null
        },
        {
            "_id": "5d8072905957ad7880883ccf",
            "first_name": "child",
            "last_name": "",
            "email": "child261@yopmail.com",
            "avatar_id": null,
            "teacher_id": "5d777b7bdd702c69fdab4615",
            "user_balance": null,
            "role": "student",
            "school_info": {
                "_id": "5d77766ddd702c69fdab460c",
                "school_name": "St Teresa School",
                "isDeleted": false,
                "isActive": true,
                "__v": 0
            },
            "class_info": null
        },
        {
            "_id": "5d8072915957ad7880883cd0",
            "first_name": "childwe",
            "last_name": "",
            "email": "child262@yopmail.com",
            "avatar_id": null,
            "teacher_id": null,
            "user_balance": null,
            "role": "student",
            "school_info": {
                "_id": "5d77766ddd702c69fdab460c",
                "school_name": "St Teresa School",
                "isDeleted": false,
                "isActive": true,
                "__v": 0
            },
            "class_info": null
        }
    ],
    "message": "Students fetched successfully."
}
*/
namedRouter.get("api.teacher.allstudentlist", '/teacher/allstudentlist', request_param.any(), async (req, res) => {
    try {
        const success = await teacherController.AllStudentList(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(500).send({
            error: error.message
        });
    }
});

/**
 * @api {get} /teacher/allchildlist  Get All child Lists
 * @apiVersion 1.0.0
 * @apiGroup Teacher
 * @apiHeader x-access-token User's Access token
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [
        {
            "_id": "5d89de95ff64f15dffcead0f",
            "class_name": "Class 1",
            "teacher_id": "5d777b7bdd702c69fdab4615",
            "school_id": "5d9deb20d881226a3c6830ce",
            "student_id": [
                {
                    "_id": "5d7660124439bc7d94880398",
                    "first_name": "Frank",
                    "last_name": "Ross",
                    "email": "frank@yopmail.com",
                    "registered_as": "",
                    "password": "$2a$08$fPoZydp/AOjiWZ3s5q0A9uKSS4or36TzjAQtbZoJ02sE4nt1euMp6",
                    "teacher_id": "5d777b7bdd702c69fdab4615",
                    "parent_id": "5d765fc44439bc7d94880396",
                    "principle_id": null,
                    "school": "5d77766ddd702c69fdab460c",
                    "avatar_id": "5d766f30053c406b9979871d",
                    "user_balance": 115,
                    "phone": "",
                    "address": "",
                    "city": "",
                    "state": "",
                    "country": "",
                    "isVerified": "Yes",
                    "grade": null,
                    "verifyToken": null,
                    "deviceToken": "",
                    "deviceType": "",
                    "isDeleted": false,
                    "isActive": true,
                    "role": "5cd56d7bc6fff8f0dc8db24b",
                    "__v": 0,
                    "profile_image": "image_1_1568042800610_3.1.png"
                },
                {
                    "_id": "5d8072905957ad7880883ccf",
                    "first_name": "child",
                    "last_name": "",
                    "email": "child261@yopmail.com",
                    "registered_as": "",
                    "password": "$2a$08$oBHVJ9s4l0ycz3epNgEM/eaxoqMAU.khvtclCBs0XedMy2BL7bprq",
                    "teacher_id": "5d777b7bdd702c69fdab4615",
                    "parent_id": "5d77ac91cfdab029f5a8f236",
                    "principle_id": null,
                    "school": "5d77766ddd702c69fdab460c",
                    "avatar_id": null,
                    "user_balance": null,
                    "phone": "",
                    "address": "",
                    "city": "",
                    "state": "",
                    "country": "",
                    "isVerified": "Yes",
                    "grade": null,
                    "verifyToken": null,
                    "deviceToken": "",
                    "deviceType": "",
                    "isDeleted": false,
                    "isActive": true,
                    "role": "5cd56d7bc6fff8f0dc8db24b",
                    "__v": 0
                }
            ],
            "status": "Active",
            "isDeleted": false,
            "school_info": {
                "_id": "5d9deb20d881226a3c6830ce",
                "school_name": "Sir Ravindra Bhavan",
                "isDeleted": false,
                "isActive": true,
                "__v": 0
            }
        }
    ],
    "current": 1,
    "pages": 1,
    "total": 1,
    "message": "Child fetched successfully."
}
*/
namedRouter.get("api.teacher.allchild", '/teacher/allchildlist', request_param.any(), async (req, res) => {
    try {
        const success = await teacherController.childListByTeacher(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(500).send({
            error: error.message
        });
    }
});

/**
 * @api {get} /teacher/deletechild/:id  Child/Student Delete From Teacher
 * @apiVersion 1.0.0
 * @apiGroup Teacher
 * @apiHeader x-access-token User's Access token
 * @apiParam {ObjectId} :id Teacher Id
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "n": 1,
        "nModified": 1,
        "ok": 1
    },
    "message": "Child delete successfully."
}
*/

namedRouter.get("api.teacher.deletechild", '/teacher/deletechild/:id', request_param.any(), async (req, res) => {
	try {
		const success = await teacherController.childDeleteByTeacher(req, res);
		res.status(success.status).send(success);
	}
	catch (error) {
		res.status(500).send({error: error.message});
	}
});

/**
 * @api {post} /teacher/saveStudentByClass  Save Student By Class
 * @apiVersion 1.0.0
 * @apiGroup Teacher
 * @apiHeader x-access-token User's Access token
 * @apiParam {ObjectId} class_id Pass Class Id
 * @apiParam {ObjectId} student_id Pass Student Id
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "class_name": "Class 1",
        "teacher_id": null,
        "student_id": [
            "5d78ebd606e0e045a4adc436",
            "5d779adfc770a40323e8bfa9"
        ],
        "status": "Active",
        "isDeleted": false,
        "_id": "5d89de95ff64f15dffcead0f"
    },
    "message": "Student assigned successfully."
}
*/
namedRouter.post("api.teacher.saveStudentByClass", '/teacher/saveStudentByClass', request_param.any(), async (req, res) => {
    try {
        const success = await teacherController.saveStudentClassWise(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(500).send({
            error: error.message
        });
    }
});


/**
 * @api {post} /teacher/selectandsubmitstudents  Submit Selected Students
 * @apiVersion 1.0.0
 * @apiGroup Teacher
 * @apiHeader x-access-token User's Access token
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [],        
    "message": "Students added successfully."
}
*/
namedRouter.post("api.teacher.selectstudents", '/teacher/selectandsubmitstudents', request_param.any(), async (req, res) => {
    try {
        const success = await teacherController.StudentSubmitByTeacher(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(500).send({
            error: error.message
        });
    }
});


/**
 * @api {post} /teacher/mystudent  Teacher My Child/Student List
 * @apiVersion 1.0.0
 * @apiGroup Teacher
 * @apiHeader x-access-token User's Access token
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [
        {
            "_id": "5d77a175c770a40323e8bfbd",
            "first_name": "child201",
            "last_name": "",
            "email": "child201@yopmail.com",
            "avatar_id": null,
            "teacher_id": "5d68d6b4a20a5958a61e8ed0",
            "user_balance": null,
            "role": "student"
        },
        {
            "_id": "5d77a2a7c770a40323e8bfc2",
            "first_name": "kk",
            "last_name": "",
            "email": "kk@yopmail.com",
            "avatar_id": null,
            "teacher_id": "5d68d6b4a20a5958a61e8ed0",
            "user_balance": null,
            "role": "student"
        },
        {
            "_id": "5d77a76ccfdab029f5a8f231",
            "first_name": "qqq",
            "last_name": "",
            "email": "qqq@yopmail.com",
            "avatar_id": null,
            "teacher_id": "5d68d6b4a20a5958a61e8ed0",
            "user_balance": null,
            "role": "student"
        }
    ],
    "message": "Your students fetched successfully."
}
*/
namedRouter.get("api.teacher.mystudent", '/teacher/mystudent', request_param.any(), async (req, res) => {
    try {
        const success = await teacherController.TeacherMyStudentList(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(500).send({
            error: error.message
        });
    }
});

/**
 * @api {post} /teacher/class  Teacherwise Class
 * @apiVersion 1.0.0
 * @apiGroup Teacher
 * @apiHeader x-access-token User's Access token
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "class_name": "Class 1",
        "teacher_id": "5d777b7bdd702c69fdab4615",
        "student_id": [
            "5d7660124439bc7d94880398"
        ],
        "status": "Active",
        "isDeleted": false,
        "_id": "5d89de95ff64f15dffcead0f"
    },
    "message": "Class fetched successfully."
  }
*/
namedRouter.get("api.teacher.class", '/teacher/class', async (req, res) => {
    try {
        const success = await teacherController.get_class_by_teacher(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(500).send({
            error: error.message
        });
    }
});


namedRouter.post("api.teacher.graph", '/teacher/graph',request_param.any(), async (req, res) => {
	try {
		const success = await teacherController.getPerformanceGraph(req, res);
		res.status(success.status).send(success);
	}
	catch (error) {
		res.status(500).send({error: error.message});
	}
});

// Export the express.Router() instance
module.exports = router;