const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);


const cmsController = require('webservice/cms.controller');


/**
 * @api {get} /cms/:slug Get CMS By Slug 
 * @apiVersion 1.0.0
 * @apiGroup CMS
 * @apiParam {string} slug CMS slug
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "title": "Terms of Service and Privacy Policy",
        "slug": "terms-of-service-and-privacy-policy",
        "desc": "<p>LOREM IPSUM DESCRIPTION</p>\r\n",
        "content": "this is the content of cms",
        "isDeleted": false,
        "status": "Active",
        "_id": "5cdd8ed8acaf29105ff1a351",
        "__v": 0
    },
    "message": "Data Fetched Successfully."
}
*/

namedRouter.get("api.cms", '/cms/:slug', async (req, res) => {
    try {
        const success = await cmsController.list(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});



// Export the express.Router() instance
module.exports = router;
