const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);


const multer = require('multer');
const studentController = require('webservice/student/student.controller');
const storage = multer.diskStorage({
    destination: function (req, file, callback) {
        callback(null, './public/uploads/student/');
    },
    filename: function (req, file, callback) {
        callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname.replace(/\s/g, '_'));
    }

});
const request_param = multer();

const uploadImage = multer({
    storage: storage
});


/**
 * @api {post} /student/store Register
 * @apiVersion 1.0.0
 * @apiGroup Student
 * @apiParam {string} first_name First Name
 * @apiParam {string} last_name  Last Name
 * @apiParam {string} email Email.
 * @apiParam {string} password  Password.
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "first_name": "sample",
        "last_name": "child",
        "email": "child@yopmail.com",
        "password": "$2a$08$teYCxMKNe3zxiAfL60xK7OEeTTX6TsTBMsxgVpWgdAmR0ZnJMedva",
        "teacher_id": null,
        "parent_id": null,
        "principle_id": null,
        "school": null,
        "isVerified": "No",
        "verifyToken": null,
        "deviceToken": "",
        "deviceType": "",
        "isDeleted": false,
        "isActive": true,
        "_id": "5d14aaa94a453f4bca87d1f4",
        "role": "5cd56d7bc6fff8f0dc8db24b",
        "__v": 0
    },
    "message": "Student Successfully regsitered."
}
*/

namedRouter.post("api.student.register", '/student/store', request_param.any(), async (req, res) => {
    try {
        const success = await studentController.store(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(500).send({
            error: error.message
        });
    }
});

/* auth routes start from here */
namedRouter.all('/student*', auth.authenticateAPI);


/**
 * @api {post} /student/change/password  Change Password
 * @apiVersion 1.0.0
 * @apiGroup Student
 * @apiParam {string} old_password Old password
 * @apiParam {string} new_password New password
 * @apiHeader x-access-token User's Access token
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "first_name": "student ",
        "last_name": "sir",
        "email": "studentSir@gmail.com",
        "password": "$2a$08$gYO0BRQrD0uEG0wr2CZK5OxrDVdm8fWujDSuTb1sAAiWpkCfpHL6y",
        "student_id": null,
        "parent_id": null,
        "principle_id": null,
        "school_name": "",
        "isVerified": "Yes",
        "verifyToken": null,
        "deviceToken": "",
        "deviceType": "",
        "isDeleted": false,
        "isActive": true,
        "_id": "5ced44553d45955a272c583d",
        "role": "5cd56d27c6fff8f0dc8dae69",
        "__v": 0
    },
    "message": "Password Changed Successfully"
}
*/

namedRouter.post("api.student.changePassword", '/student/change/password', request_param.any(), async (req, res) => {
	try {
		const success = await studentController.changePassword(req, res);
		res.status(success.status).send(success);
	}
	catch (error) {
		res.status(error.status).send(error);
	}
});

/**
 * @api {post} /student/update/profile Profile Update
 * @apiVersion 1.0.0
 * @apiGroup Student
 * @apiParam {string} first_name First name
 * @apiParam {string} last_name Last name
 * @apiHeader x-access-token User's Access token
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "first_name": "Jack",
        "last_name": "Smith",
        "email": "jack@yopmail.com",
        "registered_as": "",
        "password": "$2a$08$Ag44BOc.K3mKlXtuN2HdN./8wb6RxPWbJkXYewBejRsm80mYmAqia",
        "teacher_id": null,
        "parent_id": "5d6686b99d55b31274bca0ac",
        "principle_id": null,
        "school": null,
        "avatar_id": null,
        "user_balance": null,
        "phone": "",
        "address": "",
        "city": "",
        "state": "",
        "country": "",
        "isVerified": "Yes",
        "grade": null,
        "verifyToken": null,
        "deviceToken": "",
        "deviceType": "",
        "isDeleted": true,
        "isActive": true,
        "_id": "5d67664a8368ce4558d6ffdb",
        "role": "5cd56d7bc6fff8f0dc8db24b",
        "__v": 0
    },
    "message": "Profile Updated Successfully"
}
*/
namedRouter.post("api.student.update.profile", '/student/update/profile', request_param.any(), async (req, res) => {
    try {
        const success = await studentController.updateProfile(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(500).send({
            error: error.message
        });
    }
});

/**
 * @api {get} /student/dashboard/books Dashboard Books
 * @apiVersion 1.0.0
 * @apiGroup Student
 * @apiHeader x-access-token User's Access token
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [
        {
            "_id": "5d7608de349471144f88da6c",
            "title": "Journeys",
            "subtitle": "Journeys",
            "slug": "",
            "grade": 1,
            "genre": "Comic and Graphic Novel",
            "author": "Houghton Mifflin Harcourt",
            "picture": "picture_1568016606176_610giXgWDTL.jpg",
            "chapter_num": 5,
            "status": "Active",
            "isDeleted": false
        },
        {
            "_id": "5cfa1a34779ccd034eb17af0",
            "title": "adfasfdafdf",
            "subtitle": "fdafdfdf",
            "slug": "",
            "grade": 3,
            "genre": "Classic",
            "author": "Frank Miller",
            "picture": "picture_1559894579683_dummy.png",
            "chapter_num": 10,
            "status": "Active",
            "isDeleted": false
        },
        {
            "_id": "5cfa0746839f1c081b1e2c74",
            "title": "Sherlock Holmes",
            "subtitle": "HEAD",
            "slug": "",
            "grade": 5,
            "genre": "Classic",
            "author": "Arthur Conan Doyle",
            "picture": "picture_1559904498828_ls.jpg",
            "chapter_num": 5,
            "status": "Active",
            "isDeleted": false
        }
    ],
    "message": "Books fetched successfully"
}
*/
namedRouter.get("api.student.dashboard.books", '/student/dashboard/books', request_param.any(), async (req, res) => {
    try {
        const success = await studentController.studentDashboardBooks(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});


/**
 * @api {get} /student/books/details/:id Book Details By Id
 * @apiVersion 1.0.0
 * @apiGroup Student
 * @apiParam {ObjectId} id Pass book id in params
 * @apiHeader x-access-token User's Access token
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [
        {
            "_id": "5cfa0746839f1c081b1e2c74",
            "title": "Sherlock Holmes",
            "subtitle": "HEAD",
            "grade": 5,
            "genre": {
                "_id": "5cfa05269f4ec670fb8b4b1f",
                "title": "Classic",
                "slug": "",
                "status": "Active",
                "isDeleted": false,
                "__v": 0
            },
            "author": {
                "_id": "5cfa0648839f1c081b1e2c6f",
                "title": "Arthur Conan Doyle",
                "slug": "",
                "status": "Active",
                "isDeleted": false,
                "__v": 0
            },
            "picture": "picture_1559904498828_ls.jpg",
            "total_chapter": 5,
            "status": "Active",
            "isDeleted": false,
            "chapter_info": [
                {
                    "_id": "5cfa2bda39e287040c5078a1",
                    "title": "dsadsa",
                    "description": "sadsad",
                    "chapter_no": 4,
                    "chapter_icon": "",
                    "chapter_pages": 10,
                    "isDeleted": false,
                    "status": "Active",
                    "book_id": "5cfa0746839f1c081b1e2c74",
                    "pages": [],
                    "__v": 0
                },
                {
                    "_id": "5cfa266d6cb4965272741809",
                    "title": "Chapter 2",
                    "description": "Test Details.",
                    "chapter_no": 2,
                    "chapter_icon": "",
                    "chapter_pages": 2,
                    "isDeleted": false,
                    "status": "Active",
                    "book_id": "5cfa0746839f1c081b1e2c74",
                    "pages": [],
                    "__v": 0
                },
                {
                    "_id": "5cfa092f8707f41cce5cc46b",
                    "title": "Chapter 1",
                    "description": "test",
                    "chapter_no": 1,
                    "chapter_icon": "chapter_icon_1560164246745_images.png",
                    "chapter_pages": 4,
                    "isDeleted": false,
                    "status": "Active",
                    "book_id": "5cfa0746839f1c081b1e2c74",
                    "pages": [
                        {
                            "page_title": "Sample Title",
                            "page_content": "<p>Sample COntent</p>\r\n",
                            "page_number": 1,
                            "_id": "5cfa257b39e287040c50789c",
                            "status": "Active"
                        }
                    ],
                    "__v": 0
                },
                {
                    "_id": "5cfa07e8839f1c081b1e2c76",
                    "title": "Chapter 3",
                    "description": "testset",
                    "chapter_no": 3,
                    "chapter_icon": "chapter_icon_1559830076250_dymmy2.jpg",
                    "chapter_pages": 3,
                    "isDeleted": false,
                    "status": "Active",
                    "book_id": "5cfa0746839f1c081b1e2c74",
                    "pages": [],
                    "__v": 0
                }
            ]
        }
    ],
    "message": "Book details fetched successfully."
}
*/
namedRouter.get("api.student.books.details", '/student/books/details/:id', request_param.any(), async (req, res) => {
    try {
        const success = await studentController.StudentBookDetails(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});


// Export the express.Router() instance
module.exports = router;