const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const multer = require('multer');
const planController = require('webservice/plan.controller');
const request_param = multer();

/**
 * @api {get} /plan/list Subscription Plan List
 * @apiVersion 1.0.0
 * @apiGroup Plan
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "message": "Plan list fetched successfully",
    "data": [
        {
            "title": "Monthly Plan",
            "slug": "monthly-plan",
            "price": 10,
            "content": "<p>$5 for each additional child</p>",
            "stripe_plan_id": "",
            "plan_time": "Month",
            "child_add_provision": 4,
            "status": "Active",
            "isDeleted": false,
            "_id": "5d403326f5cd7d4a7f6eecc2",
            "createdAt": "2019-05-22T11:10:33.496Z",
            "updatedAt": "2019-06-10T14:31:13.443Z",
            "__v": 0
        },
        {
            "title": "Yearly Plan",
            "slug": "yearly-plan",
            "price": 100,
            "content": "<p>$5 for each additional child</p>",
            "stripe_plan_id": "",
            "plan_time": "Year",
            "child_add_provision": 10,
            "status": "Active",
            "isDeleted": false,
            "_id": "5d403326f5cd7d4a7f6eecc4",
            "createdAt": "2019-05-22T11:10:33.496Z",
            "updatedAt": "2019-06-10T14:31:13.443Z",
            "__v": 0
        }
    ]
}
*/
namedRouter.get("api.plan.list", '/plan/list', async (req, res) => {
	try {
		const success = await planController.planList(req, res);
		res.status(success.status).send(success);
	}
	catch (error) {
		res.status(error.status).send(error);
	}
});

// Export the express.Router() instance
module.exports = router;