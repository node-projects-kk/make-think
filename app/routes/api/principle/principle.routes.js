const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const helper = require('../../../helper/helper.js');

const multer = require('multer');
const principleController = require('webservice/principle/principle.controller');
const storage = multer.diskStorage({
    destination: function (req, file, callback) {
        callback(null, './public/uploads/principle/');
    },
    filename: function (req, file, callback) {
        callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname.replace(/\s/g, '_'));
    }

});
const request_param = multer();

const uploadImage = multer({
    storage: storage
});


/**
 * @api {post} /principle/store Register
 * @apiVersion 1.0.0
 * @apiGroup Principle
 * @apiParam {string} first_name First Name
 * @apiParam {string} last_name  Last Name
 * @apiParam {string} email Email.
 * @apiParam {string} password  Password.
 * @apiParam {string} school_name  School.

 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "first_name": "Principle ",
        "last_name": "sir",
        "email": "principleSir@gmail.com",
        "password": "$2a$08$7Vg98coDmDO1M3OFf6iZseJ9EgyhVvEyoQhZCqHR4wz6u957LqCsq",
        "teacher_id": null,
        "parent_id": null,
        "principle_id": null,
        "school_name": "",
        "isVerified": "No",
        "verifyToken": 1048,
        "deviceToken": "",
        "deviceType": "",
        "isDeleted": false,
        "isActive": true,
        "_id": "5ced489d122e445d98606a59",
        "role": "5cd56d4cc6fff8f0dc8db02f",
        "__v": 0
    },
    "message": "You have successfully regsitered."
}
*/

namedRouter.post("api.principle.register", '/principle/store', request_param.any(), async (req, res) => {
    try {
        const success = await principleController.store(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});


namedRouter.all('/principle*', auth.authenticateAPI);

/**
 * @api {post} /principle/change/password  Change Password
 * @apiVersion 1.0.0
 * @apiGroup Principle
 * @apiParam {string} old_password Old password
 * @apiParam {string} new_password New password
 * @apiHeader x-access-token User's Access token
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "first_name": "principle ",
        "last_name": "sir",
        "email": "principle@gmail.com",
        "password": "$2a$08$gYO0BRQrD0uEG0wr2CZK5OxrDVdm8fWujDSuTb1sAAiWpkCfpHL6y",
        "teacher_id": null,
        "parent_id": null,
        "principle_id": null,
        "school_name": "",
        "isVerified": "Yes",
        "verifyToken": null,
        "deviceToken": "",
        "deviceType": "",
        "isDeleted": false,
        "isActive": true,
        "_id": "5ced44553d45955a272c583d",
        "role": "5cd56d27c6fff8f0dc8dae69",
        "__v": 0
    },
    "message": "Password Changed Successfully"
}
*/
namedRouter.post("api.principle.changePassword", '/principle/change/password', request_param.any(), async (req, res) => {
    try {
        const success = await principleController.changePassword(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

/**
 * @api {post} /principle/edit-profile  Edit profile of principle
 * @apiVersion 1.0.0
 * @apiGroup Principle
 * @apiParam {id} id User id
 * @apiParam {string} first_name First name
 * @apiParam {string} last_name Last name
 * @apiParam {id} school_id School id
 * @apiParam {string} school_name School Name
 * @apiHeader x-access-token User's Access token
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "first_name": "new TesT",
        "last_name": "parent",
        "email": "newparentemail@yop.com",
        "password": "$2a$08$EID4NnPKgR2EA6lrp0WHCOiNt.vVMpkjIxDVMB2Nfe2k9X7rHln/G",
        "teacher_id": null,
        "parent_id": null,
        "principle_id": null,
        "school": "5d1373d47c2beb0d6c2af4e8",
        "isVerified": "Yes",
        "verifyToken": 5102,
        "deviceToken": "",
        "deviceType": "",
        "isDeleted": false,
        "isActive": true,
        "_id": "5d14c27e276b2c71127ffafa",
        "role": "5cd56d4cc6fff8f0dc8db02f",
        "__v": 0
    },
    "message": "Profile Updated Successfully"
}
*/
namedRouter.post("api.principle.editProfile", '/principle/edit-profile', request_param.any(), async (req, res) => {
    try {
        const success = await principleController.editProfilePrincliple(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

/**
 * @api {get} /principle/getallgrades  Get All Grades and Student Count
 * @apiVersion 1.0.0
 * @apiGroup Principle
 * @apiHeader x-access-token User's Access token
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [
        {
            "_id": {
                "grade": 1
            },
            "role": "student",
            "count": 2
        },
        {
            "_id": {
                "grade": 4
            },
            "role": "student",
            "count": 2
        },
        {
            "_id": {
                "grade": 5
            },
            "role": "student",
            "count": 1
        }
    ],
    "message": "Data fetched successfully."
}
*/
namedRouter.get("api.principle.getallgrades", '/principle/getallgrades', request_param.any(), async (req, res) => {
    try {
        const success = await principleController.getAllGradesAndCount(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

/**
 * @api {get} /principle/studentsandteacherbyschool  Get All the Students and Teachers
 * @apiVersion 1.0.0
 * @apiGroup Principle
 * @apiHeader x-access-token User's Access token
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "student_list": [
            {
                "_id": "5d77830e0739d20596f54655",
                "first_name": "DAvid Gomes",
                "last_name": "",
                "email": "child10@yopmail.com",
                "avatar_id": {
                    "_id": "5d766f30053c406b9979871d",
                    "title": "Test Avatar 3",
                    "purchase_coin": 5,
                    "image_1": "image_1_1568042800610_3.1.png",
                    "image_2": "image_2_1568042800641_3.2.png",
                    "slug": "",
                    "description": "test description",
                    "question": "test question",
                    "status": "Active",
                    "isDeleted": false,
                    "__v": 0
                },
                "teacher_id": "5d777b7bdd702c69fdab4615",
                "user_balance": null,
                "role": "student",
                "school_info": {
                    "_id": "5d149edc585379f913ff0457",
                    "school_name": "Carnoc School",
                    "isDeleted": false,
                    "isActive": true,
                    "__v": 0
                },
                "class_info": {
                    "_id": "5d89debbff64f15dffceae90",
                    "class_name": "Class 3",
                    "school_id": "5d77766ddd702c69fdab460c",
                    "teacher_id": "5d9deb52d881226a3c6830d1",
                    "student_id": [
                        "5d77830e0739d20596f54655",
                        "5d779adfc770a40323e8bfa9"
                    ],
                    "status": "Active",
                    "isDeleted": false
                },
                "isDeleted": false,
                "isActive": true
            },
            {
                "_id": "5d779adfc770a40323e8bfa9",
                "first_name": "Child eleven",
                "last_name": "",
                "email": "child11@yopmail.com",
                "avatar_id": null,
                "teacher_id": "5d8080a75957ad7880883cd8",
                "user_balance": null,
                "role": "student",
                "school_info": {
                    "_id": "5d77766ddd702c69fdab460c",
                    "school_name": "St Teresa School",
                    "isDeleted": false,
                    "isActive": true,
                    "__v": 0
                },
                "class_info": {
                    "_id": "5d89debbff64f15dffceae90",
                    "class_name": "Class 3",
                    "school_id": "5d77766ddd702c69fdab460c",
                    "teacher_id": "5d9deb52d881226a3c6830d1",
                    "student_id": [
                        "5d77830e0739d20596f54655",
                        "5d779adfc770a40323e8bfa9"
                    ],
                    "status": "Active",
                    "isDeleted": false
                },
                "isDeleted": false,
                "isActive": true
            }
        ],
        "teacher_list": [
            {
                "_id": "5d9deb52d881226a3c6830d1",
                "first_name": "Paul",
                "last_name": "Molive",
                "email": "paul@yopmail.com",
                "avatar_id": null,
                "user_balance": 0,
                "role": "teacher",
                "school_info": {
                    "_id": "5d77766ddd702c69fdab460c",
                    "school_name": "St Teresa School",
                    "isDeleted": false,
                    "isActive": true,
                    "__v": 0
                },
                "class_info": {
                    "_id": "5d89debbff64f15dffceae90",
                    "class_name": "Class 3",
                    "school_id": "5d77766ddd702c69fdab460c",
                    "teacher_id": "5d9deb52d881226a3c6830d1",
                    "student_id": [
                        "5d77830e0739d20596f54655",
                        "5d779adfc770a40323e8bfa9"
                    ],
                    "status": "Active",
                    "isDeleted": false
                },
                "isDeleted": false,
                "isActive": true
            }
        ]
    },
    "message": "Data fetched successfully."
}
*/
namedRouter.get("api.principle.getstudentsandteacherbyschool", '/principle/studentsandteacherbyschool', request_param.any(), async (req, res) => {
    try {
        const success = await principleController.getStudentsAndTeacherBySchool(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

/**
 * @api {get} /principle/studentsactivitylist/:grade  Get Students Activity
 * @apiVersion 1.0.0
 * @apiGroup Principle
 * @apiParam {Number} grade Pass grade number in params [1 to 8]
 * @apiHeader x-access-token User's Access token
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [
        {
            "_id": "5d6907a887529473b5a8ac2d",
            "student_id": "5d6685df9d55b31274bca0a5",
            "page_id": "5cfa257b39e287040c50789c",
            "coins_earned": 55,
            "activity_type": "CO",
            "status": "Active",
            "isDeleted": false,
            "user_info": {
                "_id": "5d6685df9d55b31274bca0a5",
                "first_name": "martin",
                "last_name": "",
                "email": "martin@yopmail.com",
                "registered_as": "",
                "password": "$2a$08$VgtGd5iG.F0tISA3nW6aWesPkL0XEiigzwZU5M3Ii/l8/zbBXc2km",
                "teacher_id": null,
                "parent_id": "5d667e763fac9074c866312b",
                "principle_id": null,
                "school": "5d2598e357529b1769d87bde",
                "avatar_id": null,
                "user_balance": null,
                "phone": "",
                "address": "",
                "city": "",
                "state": "",
                "country": "",
                "isVerified": "No",
                "grade": 1,
                "verifyToken": null,
                "deviceToken": "",
                "deviceType": "",
                "isDeleted": false,
                "isActive": true,
                "role": "5cd56d7bc6fff8f0dc8db24b",
                "__v": 0
            },
            "chapter_info": {
                "_id": "5cfa092f8707f41cce5cc46b",
                "title": "Chapter 1",
                "description": "test",
                "chapter_no": 1,
                "chapter_icon": "chapter_icon_1560164246745_images.png",
                "chapter_pages": 4,
                "isDeleted": false,
                "status": "Active",
                "book_id": "5cfa0746839f1c081b1e2c74",
                "pages": [
                    {
                        "page_title": "Sample Title",
                        "page_content": "<p>Sample COntent</p>\r\n",
                        "page_number": 1,
                        "_id": "5cfa257b39e287040c50789c",
                        "status": "Active"
                    }
                ],
                "__v": 0
            },
            "book_info": {
                "_id": "5cfa0746839f1c081b1e2c74",
                "title": "Sherlock Holmes",
                "subtitle": "HEAD",
                "slug": "",
                "grade": 5,
                "genre": "5cfa05269f4ec670fb8b4b1f",
                "author": "5cfa0648839f1c081b1e2c6f",
                "picture": "picture_1559904498828_ls.jpg",
                "chapter_num": 5,
                "status": "Active",
                "isDeleted": false,
                "__v": 0
            }
        }
    ],
    "message": "Students activity fetched successfully."
}
*/
namedRouter.get("api.principle.getstudentsactivitylist", '/principle/studentsactivitylist/:grade', request_param.any(), async (req, res) => {
    try {
        const success = await principleController.getStudentsActivityList(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

/**
 * @api {get} /principle/getAllClasses  Get All Classes
 * @apiVersion 1.0.0
 * @apiGroup Principle
 * @apiHeader x-access-token User's Access token
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [
        {
            "class_name": "Class 1",
            "teacher_id": null,
            "student_id": [],
            "status": "Active",
            "isDeleted": false,
            "_id": "5d89de95ff64f15dffcead0f"
        },
        {
            "class_name": "Class 2",
            "teacher_id": null,
            "student_id": [],
            "status": "Active",
            "isDeleted": false,
            "_id": "5d89deb4ff64f15dffceae46"
        },
        {
            "class_name": "Class 3",
            "teacher_id": null,
            "student_id": [],
            "status": "Active",
            "isDeleted": false,
            "_id": "5d89debbff64f15dffceae90"
        },
        {
            "class_name": "Class 4",
            "teacher_id": null,
            "student_id": [],
            "status": "Active",
            "isDeleted": false,
            "_id": "5d89dec1ff64f15dffceaed4"
        },
        {
            "class_name": "Class 5",
            "teacher_id": null,
            "student_id": [],
            "status": "Active",
            "isDeleted": false,
            "_id": "5d89dec9ff64f15dffceaf16"
        },
        {
            "class_name": "Class 6",
            "teacher_id": null,
            "student_id": [],
            "status": "Active",
            "isDeleted": false,
            "_id": "5d89ded0ff64f15dffceaf56"
        },
        {
            "class_name": "Class 7",
            "teacher_id": null,
            "student_id": [],
            "status": "Active",
            "isDeleted": false,
            "_id": "5d89ded6ff64f15dffceaf91"
        },
        {
            "class_name": "Class 8",
            "teacher_id": null,
            "student_id": [],
            "status": "Active",
            "isDeleted": false,
            "_id": "5d89deddff64f15dffceafd2"
        }
    ],
    "message": "All Class fetched successfully."
}
*/
namedRouter.get("api.principle.getAllClasses", '/principle/getAllClasses', request_param.any(), async (req, res) => {
    try {
        const success = await principleController.getAllClasses(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

/**
 * @api {get} /principle/getAllTeachers  Get All Teachers
 * @apiVersion 1.0.0
 * @apiGroup Principle
 * @apiHeader x-access-token User's Access token
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [
        {
            "_id": "5d777b7bdd702c69fdab4615",
            "first_name": "Diana",
            "last_name": "Burnwood",
            "email": "diana@mailinator.com",
            "avatar_id": {
                "_id": "5d766f30053c406b9979871d",
                "title": "Test Avatar 3",
                "purchase_coin": 5,
                "image_1": "image_1_1568042800610_3.1.png",
                "image_2": "image_2_1568042800641_3.2.png",
                "slug": "",
                "description": "test description",
                "question": "test question",
                "status": "Active",
                "isDeleted": false,
                "__v": 0
            },
            "user_balance": null,
            "role": "teacher",
            "school_info": {
                "_id": "5d77766ddd702c69fdab460c",
                "school_name": "St Teresa School",
                "isDeleted": false,
                "isActive": true,
                "__v": 0
            },
            "class_info": {
                "_id": "5d89de95ff64f15dffcead0f",
                "class_name": "Class 1",
                "teacher_id": "5d777b7bdd702c69fdab4615",
                "student_id": [],
                "status": "Active",
                "isDeleted": false
            }
        },
        {
            "_id": "5d777e18dd702c69fdab461d",
            "first_name": "Linda",
            "last_name": "Gomes",
            "email": "linda@mailinator.com",
            "avatar_id": null,
            "user_balance": null,
            "role": "teacher",
            "school_info": {
                "_id": "5d77766ddd702c69fdab460c",
                "school_name": "St Teresa School",
                "isDeleted": false,
                "isActive": true,
                "__v": 0
            },
            "class_info": null
        },
        {
            "_id": "5d8080a75957ad7880883cd8",
            "first_name": "taecher",
            "last_name": "doe",
            "email": "teacherdoe@yopmail.com",
            "avatar_id": null,
            "user_balance": null,
            "role": "teacher",
            "school_info": {
                "_id": "5d77766ddd702c69fdab460c",
                "school_name": "St Teresa School",
                "isDeleted": false,
                "isActive": true,
                "__v": 0
            },
            "class_info": null
        }
    ],
    "message": "All Teacher fetched successfully."
}
*/
namedRouter.get("api.principle.getAllTeachers", '/principle/getAllTeachers', request_param.any(), async (req, res) => {
    try {
        const success = await principleController.getAllTeachers(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

/**
 * @api {post} /principle/saveTeacherByClass  Save Teachers By Class
 * @apiVersion 1.0.0
 * @apiGroup Principle
 * @apiHeader x-access-token User's Access token
 * @apiParam {ObjectId} class_id Pass Class Id
 * @apiParam {ObjectId} teacher_id Pass Teacher Id
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "class_name": "Class 1",
        "teacher_id": "5d777b7bdd702c69fdab4615",
        "student_id": [],
        "status": "Active",
        "isDeleted": false,
        "_id": "5d89de95ff64f15dffcead0f"
    },
    "message": "Teacher assigned successfully."
}
*/
namedRouter.post("api.principle.saveTeacherByClass", '/principle/saveTeacherByClass', request_param.any(), async (req, res) => {
    try {
        const success = await principleController.saveTeacherClassWise(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

/**
 * @api {post} /principle/studentTeacherActivities/:classid  Get Students Activites and teacher information
 * @apiVersion 1.0.0
 * @apiGroup Principle
 * @apiHeader x-access-token User's Access token
 * @apiParam {Array} student_id Pass Students Id
 * @apiParam {ObjectId} class_id Pass Class Id in Params
 * @apiParam {ObjectId} teacher_id Pass Teacher Id
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [
        {
            "_id": "5d8b30cecc0c2b384fc26014",
            "student_id": "5d7660124439bc7d94880398",
            "book_id": "5cfa1a34779ccd034eb17af0",
            "chapter_id": "5cfa7a5828512408daa170a0",
            "points_earned": 10,
            "activity_type": "completed",
            "isDeleted": false,
            "createdAt": 1569402928910,
            "completedAt": 1569403108694,
            "chapter_pages": [
                {
                    "status": "completed",
                    "initialDate": 1569402928910,
                    "ongoingDate": 1569403103761,
                    "completedDate": 1569403104893,
                    "_id": "5cfa7a8828512408daa170a1"
                },
                {
                    "status": "completed",
                    "initialDate": 1569402928910,
                    "ongoingDate": 1569403103761,
                    "completedDate": 1569403104893,
                    "_id": "5cfa7b7828512408daa170a8"
                },
                {
                    "status": "completed",
                    "initialDate": 1569402928910,
                    "ongoingDate": 1569403104892,
                    "completedDate": 1569403105804,
                    "_id": "5d81dd71a61b010324c3d190"
                },
                {
                    "status": "completed",
                    "initialDate": 1569402928910,
                    "ongoingDate": 1569403104892,
                    "completedDate": 1569403105804,
                    "_id": "5d81dd94a61b010324c3d197"
                },
                {
                    "status": "completed",
                    "initialDate": 1569402928910,
                    "ongoingDate": 1569403105804,
                    "completedDate": 1569403106631,
                    "_id": "5d81dda2a61b010324c3d19c"
                },
                {
                    "status": "completed",
                    "initialDate": 1569402928910,
                    "ongoingDate": 1569403105804,
                    "completedDate": 1569403106631,
                    "_id": "5d81ddb3a61b010324c3d1a2"
                }
            ],
            "__v": 0,
            "user_info": {
                "_id": "5d7660124439bc7d94880398",
                "first_name": "Frank",
                "last_name": "Ross",
                "email": "frank@yopmail.com",
                "registered_as": "",
                "password": "$2a$08$fPoZydp/AOjiWZ3s5q0A9uKSS4or36TzjAQtbZoJ02sE4nt1euMp6",
                "teacher_id": null,
                "parent_id": "5d765fc44439bc7d94880396",
                "principle_id": null,
                "school": null,
                "avatar_id": null,
                "user_balance": 50,
                "phone": "",
                "address": "",
                "city": "",
                "state": "",
                "country": "",
                "isVerified": "Yes",
                "grade": null,
                "verifyToken": null,
                "deviceToken": "",
                "deviceType": "",
                "isDeleted": false,
                "isActive": true,
                "role": "5cd56d7bc6fff8f0dc8db24b",
                "__v": 0
            },
            "chapter_info": {
                "_id": "5cfa7a5828512408daa170a0",
                "title": "Chapter 1 ",
                "description": "desc 1",
                "chapter_no": 1,
                "chapter_icon": "chapter_icon_1559919192278_car_3.jpeg",
                "chapter_pages": 10,
                "isDeleted": false,
                "status": "Active",
                "book_id": "5cfa1a34779ccd034eb17af0",
                "pages": [
                    {
                        "page_title": "page title 1 ",
                        "page_content": "<p>page desc&nbsp;1&nbsp;</p>\r\n",
                        "page_number": 1,
                        "_id": "5cfa7a8828512408daa170a1",
                        "status": "Active"
                    },
                    {
                        "page_title": "page title 2",
                        "page_content": "<p>asdasdfafsadsads</p>\r\n",
                        "page_number": 2,
                        "_id": "5cfa7b7828512408daa170a8",
                        "status": "Active"
                    },
                    {
                        "page_title": "page title 3",
                        "page_content": "<p>sdgfdgfdhrthfdsgffdg</p>\r\n",
                        "page_number": 3,
                        "_id": "5d81dd71a61b010324c3d190",
                        "status": "Active"
                    },
                    {
                        "page_title": "page title 4",
                        "page_content": "<p>dsfdsfdsfdsfdsgreerwewqqweqteqteq</p>\r\n",
                        "page_number": 4,
                        "_id": "5d81dd94a61b010324c3d197",
                        "status": "Active"
                    },
                    {
                        "page_title": "page title 5",
                        "page_content": "<p>sadsadasdasdadaerwerwerwerwerwerwe</p>\r\n",
                        "page_number": 5,
                        "_id": "5d81dda2a61b010324c3d19c",
                        "status": "Active"
                    },
                    {
                        "page_title": "page title 6",
                        "page_content": "<p>asddasdfsfdsgdfgtdhb</p>\r\n",
                        "page_number": 6,
                        "_id": "5d81ddb3a61b010324c3d1a2",
                        "status": "Active"
                    }
                ],
                "__v": 0
            },
            "book_info": {
                "_id": "5cfa1a34779ccd034eb17af0",
                "title": "Sample Book",
                "subtitle": "Sub Title",
                "slug": "",
                "grade": 3,
                "genre": "5cfa05269f4ec670fb8b4b1f",
                "author": "5cfa062a839f1c081b1e2c6e",
                "picture": "picture_1559894579683_dummy.png",
                "chapter_num": 5,
                "status": "Active",
                "isDeleted": false,
                "__v": 0
            },
            "t_info": {
                "first_name": "Diana",
                "last_name": "Burnwood",
                "email": "diana@mailinator.com",
                "registered_as": "",
                "password": "$2a$08$t9JLLSBX8yogSi0q0pV.h.cu/cAAs3N5JqNm83aAEJTXKF4qQWJOO",
                "teacher_id": null,
                "parent_id": null,
                "principle_id": null,
                "school": "5d77766ddd702c69fdab460c",
                "avatar_id": "5d766f30053c406b9979871d",
                "user_balance": null,
                "phone": "",
                "address": "",
                "city": "",
                "state": "",
                "country": "",
                "isVerified": "Yes",
                "grade": null,
                "verifyToken": 5629,
                "deviceToken": "",
                "deviceType": "",
                "isDeleted": false,
                "isActive": true,
                "_id": "5d777b7bdd702c69fdab4615",
                "role": "5cd56d27c6fff8f0dc8dae69",
                "__v": 0
            }
        }
    ],
    "message": "Activities fetched successfully."
}
*/
namedRouter.post("api.principle.studentTeacherActivities", '/principle/studentTeacherActivities/:classid', request_param.any(), async (req, res) => {
    try {
        const success = await principleController.studentTeacherActivities(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(500).send({
            error: error.message
        });
    }
});

/**
 * @api {post} /principle/deletechild Child/Student Delete From Principle
 * @apiVersion 1.0.0
 * @apiGroup Principle
 * @apiHeader x-access-token User's Access token
 * @apiParam {Array} student_id Students Id
 * @apiParam {ObjectId} class_id Class Id
 * @apiSuccessExample {json} Success
 *{
    "status": 200,
    "data": {
        "n": 1,
        "nModified": 1,
        "ok": 1
    },
    "message": "Child delete successfully."
}
*/
namedRouter.post("api.principle.deletechild", '/principle/deletechild', request_param.any(), async (req, res) => {
	try {
		const success = await principleController.childDeleteFromPrinciple(req, res);
		res.status(success.status).send(success);
	}
	catch (error) {
		res.status(500).send({error: error.message});
	}
});

// Export the express.Router() instance
module.exports = router;