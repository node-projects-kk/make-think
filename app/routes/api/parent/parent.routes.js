const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const helper = require('../../../helper/helper.js');

const multer = require('multer');
const parentController = require('webservice/parent/parent.controller');
const storage = multer.diskStorage({
    destination: function (req, file, callback) {
        callback(null, './public/uploads/parent/');
    },
    filename: function (req, file, callback) {
        callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname.replace(/\s/g, '_'));
    }

});
const request_param = multer();

const uploadImage = multer({
    storage: storage
});


/**
 * @api {post} /parent/store Register
 * @apiVersion 1.0.0
 * @apiGroup Parent
 * @apiParam {string} first_name First Name
 * @apiParam {string} last_name  Last Name
 * @apiParam {string} parent_email Parent Email
 * @apiParam {string} password  Password
 * @apiParam {string} registered_as  Registered As ("under_a_school", "personal_use")
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "first_name": "Jerald",
        "last_name": "Laury",
        "email": "jerald.laury@gmail.com",
        "registered_as": "under_a_school",
        "password": "$2a$08$mf0931RcnV1LzVACn2UAWODMikN1zId2QMrlJ8uQVyz/acMziY/yu",
        "teacher_id": null,
        "parent_id": null,
        "principle_id": null,
        "school": null,
        "avatar_id": null,
        "user_balance": null,
        "phone": "",
        "address": "",
        "city": "",
        "state": "",
        "country": "",
        "isVerified": "No",
        "grade": null,
        "verifyToken": 3427,
        "deviceToken": "",
        "deviceType": "",
        "isDeleted": false,
        "isActive": true,
        "_id": "5d6668790c4dec406f19a377",
        "role": "5cd56cdac6fff8f0dc8daae7",
        "__v": 0
    },
    "message": "You have successfully regsitered."
}
*/

namedRouter.post("api.parent.register", '/parent/store', request_param.any(), async (req, res) => {
    try {
        const success = await parentController.store(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(500).send({
            error: error.message
        });
    }
});

/**
 * @api {post} /parent/child/add Add Parent Childs
 * @apiVersion 1.0.0
 * @apiGroup Parent
 * @apiParam {Id} parent_id
 * @apiParam {Array} child [{child_name:'',child_email:'',child_password:'',child_confirm_password:''}]
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "first_name": "Parent ",
        "last_name": "sir",
        "email": "parentEmail@gmail.com",
        "password": "$2a$08$RcOWAVF7xgwoPAxnafXRJeM5J2Q.Dw4ytDNsAOli8w9cQQRmWMX1.",
        "teacher_id": null,
        "parent_id": null,
        "principle_id": null,
        "school": "5d1373d47c2beb0d6c2af4e8",
        "isVerified": "No",
        "verifyToken": 3190,
        "deviceToken": "",
        "deviceType": "",
        "isDeleted": false,
        "isActive": true,
        "_id": "5ced1b05c1e1283f30667e9b",
        "role": "5cd56cdac6fff8f0dc8daae7",
        "__v": 0
    },
    "message": "You have successfully regsitered."
}
*/

namedRouter.post("api.parent.child.add", '/parent/child/add', request_param.any(), async (req, res) => {
    try {
        const success = await parentController.addChild(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

namedRouter.all('/parent*', auth.authenticateAPI);

/**
 * @api {get} /parent/profile Get Profile
 * @apiVersion 1.0.0
 * @apiHeader x-access-token User's Access token
 * @apiGroup Parent
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "first_name": "dev",
        "last_name": "banerjee",
        "email": "pr30@gmail.com",
        "password": "$2a$08$E5mex2XseSipbQ7EDYID2uSp50nyIVooDcfBnLEGZf.d2gxoCtFoS",
        "teacher_id": null,
        "parent_id": null,
        "principle_id": null,
        "school": null,
        "phone": "",
        "address": "",
        "city": "",
        "state": "",
        "country": "",
        "isVerified": "Yes",
        "verifyToken": 2322,
        "deviceToken": "",
        "deviceType": "",
        "isDeleted": false,
        "isActive": true,
        "_id": "5d25ce1776d8af24f21cc9fb",
        "role": "5cd56cdac6fff8f0dc8daae7",
        "__v": 0
    },
    "message": "Profile fetched successfully"
}
*/

namedRouter.get("api.parent.profile", '/parent/profile', request_param.any(), async (req, res) => {
    try {
        const success = await parentController.profile(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});


/**
 * @api {get} /parent/getsubscription Get Parent Subscription Info
 * @apiVersion 1.0.0
 * @apiHeader x-access-token User's Access token
 * @apiGroup Parent
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [
        {
            "_id": "5d667e763fac9074c866312b",
            "first_name": "john",
            "last_name": "doe",
            "email": "johndoe@yopmail.com",
            "registered_as": "under_a_school",
            "password": "$2a$08$LJkMCbUmU9mm1itsntcreOl5HmSd9Ca4RzYqvMxFOBnP.fhS7uCbu",
            "teacher_id": null,
            "parent_id": null,
            "principle_id": null,
            "school": null,
            "avatar_id": null,
            "user_balance": null,
            "phone": "123456",
            "address": "usa",
            "city": "usa",
            "state": "usa",
            "country": "USA",
            "isVerified": "Yes",
            "grade": null,
            "verifyToken": 1015,
            "deviceToken": "",
            "deviceType": "",
            "isDeleted": false,
            "isActive": true,
            "role": "5cd56cdac6fff8f0dc8daae7",
            "__v": 0,
            "subscription_info": {
                "_id": "5d7673c4a1adb416db747ce1",
                "package_type": "monthly",
                "total_price": 20,
                "parent_id": "5d667e763fac9074c866312b",
                "customer_id": "cus_FmNpSdFvFpaI9a",
                "plan_id": "plan_FmNpP7QjfJ0i5m",
                "subscription_id": "sub_FmNpmMrS7FGWh3",
                "next_billing_date": "2019-10-09T15:46:10.000Z",
                "payment_status": "Completed",
                "isDeleted": false,
                "createdAt": "2019-09-09T15:45:48.956Z",
                "__v": 0
            }
        }
    ],
    "message": "Subscription details fetched successfully."
}
*/
namedRouter.get("api.parent.subscription", '/parent/getsubscription', request_param.any(), async (req, res) => {
    try {
        const success = await parentController.getSubscriptionInfo(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});


/**
 * @api {post} /parent/profileupdate Profile Update
 * @apiVersion 1.0.0
 * @apiHeader x-access-token User's Access token
 * @apiParam {string} first_name First Name
 * @apiParam {string} last_name  Last Name
 * @apiParam {string} email Email
 * @apiParam {string} phone  Phone
 * @apiParam {string} address  Address
 * @apiParam {string} city  City
 * @apiParam {string} state  State
 * @apiParam {string} country  Country
 * @apiGroup Parent
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "first_name": "dev",
        "last_name": "banerjee",
        "email": "pr30@gmail.com",
        "password": "$2a$08$E5mex2XseSipbQ7EDYID2uSp50nyIVooDcfBnLEGZf.d2gxoCtFoS",
        "teacher_id": null,
        "parent_id": null,
        "principle_id": null,
        "school": null,
        "phone": "7896541230",
        "address": "Sample address, test road, sector 5",
        "city": "kolkata",
        "state": "wb",
        "country": "India",
        "isVerified": "Yes",
        "verifyToken": 2322,
        "deviceToken": "",
        "deviceType": "",
        "isDeleted": false,
        "isActive": true,
        "_id": "5d25ce1776d8af24f21cc9fb",
        "role": "5cd56cdac6fff8f0dc8daae7",
        "__v": 0
    },
    "message": "Profile updated successfully"
}
*/
namedRouter.post("api.parent.profileupdate", '/parent/profileupdate', request_param.any(), async (req, res) => {
    try {
        const success = await parentController.profileUpdate(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});


/**
 * @api {post} /parent/change/password  Change Password
 * @apiVersion 1.0.0
 * @apiGroup Parent
 * @apiParam {string} old_password Old password
 * @apiParam {string} new_password New password
 * @apiHeader x-access-token User's Access token
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "first_name": "Parent",
        "last_name": "sir",
        "email": "parent@gmail.com",
        "password": "$2a$08$gYO0BRQrD0uEG0wr2CZK5OxrDVdm8fWujDSuTb1sAAiWpkCfpHL6y",
        "teacher_id": null,
        "parent_id": null,
        "principle_id": null,
        "school_name": "",
        "isVerified": "Yes",
        "verifyToken": null,
        "deviceToken": "",
        "deviceType": "",
        "isDeleted": false,
        "isActive": true,
        "_id": "5ced44553d45955a272c583d",
        "role": "5cd56d27c6fff8f0dc8dae69",
        "__v": 0
    },
    "message": "Password Changed Successfully"
}
*/

namedRouter.post("api.parent.changePassword", '/parent/change/password', request_param.any(), async (req, res) => {
    try {
        const success = await parentController.changePassword(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});
/**
 * @api {post} /parent/edit-profile Edit profile of parent
 * @apiVersion 1.0.0
 * @apiGroup Parent
 * @apiParam {string} first_name First Name
 * @apiParam {string} last_name  Last Name
 * @apiParam {Array} child [{child_name:'',child_email:''}]
 * @apiHeader x-access-token User's Access token
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "first_name": "New Test",
        "last_name": "Parent",
        "email": "newparentemail@yop.com",
        "password": "$2a$08$EID4NnPKgR2EA6lrp0WHCOiNt.vVMpkjIxDVMB2Nfe2k9X7rHln/G",
        "teacher_id": null,
        "parent_id": null,
        "principle_id": null,
        "school": "5d1373d47c2beb0d6c2af4e8",
        "isVerified": "Yes",
        "verifyToken": 5102,
        "deviceToken": "",
        "deviceType": "",
        "isDeleted": false,
        "isActive": true,
        "_id": "5d14c27e276b2c71127ffafa",
        "role": "5cd56d4cc6fff8f0dc8db02f",
        "__v": 0
    },
    "message": "You have successfully regsitered."
}
*/
namedRouter.post("api.parent.editProfile", '/parent/edit-profile', request_param.any(), async (req, res) => {
    try {
        const success = await parentController.editProfileParent(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(500).send({
            error: error.message
        });
    }
});

/**
 * @api {get} /parent/dashboard Parent Dashboard
 * @apiVersion 1.0.0
 * @apiGroup Parent
 * @apiHeader x-access-token User's Access token
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [
        {
            "_id": "5d668f988368ce4558d6ffd2",
            "first_name": "testParent",
            "last_name": "testParent",
            "email": "testParent@yopmail.com",
            "teacher_id": null,
            "school": null,
            "parent_id": null,
            "principle_id": null,
            "child_details": [
                {
                    "_id": "5d668fe78368ce4558d6ffd4",
                    "first_name": "testChild",
                    "last_name": "",
                    "email": "testChild@yopmail.com",
                    "registered_as": "",
                    "password": "$2a$08$Xye4tSHYloefCRylH7trOeJ9D77lnktuDBJ8DlhJrg9oLfNw7VgKO",
                    "teacher_id": null,
                    "parent_id": "5d668f988368ce4558d6ffd2",
                    "principle_id": null,
                    "school": "5d149edc585379f913ff0457",
                    "avatar_id": "5d52e019ccd77e0fee01b7ca",
                    "user_balance": null,
                    "phone": "",
                    "address": "",
                    "city": "",
                    "state": "",
                    "country": "",
                    "isVerified": "No",
                    "grade": 1,
                    "verifyToken": null,
                    "deviceToken": "",
                    "deviceType": "",
                    "isDeleted": false,
                    "isActive": true,
                    "role": "5cd56d7bc6fff8f0dc8db24b",
                    "__v": 0,
                    "school_details": {
                        "_id": "5d149edc585379f913ff0457",
                        "school_name": "Carnoc School",
                        "isDeleted": false,
                        "isActive": true,
                        "__v": 0
                    },
                    "avatar_details": {
                        "_id": "5d52e019ccd77e0fee01b7ca",
                        "title": "Avatar 2",
                        "image": "image_1565712409771_avatar2.png",
                        "purchase_coin": 40,
                        "slug": "",
                        "status": "Active",
                        "isDeleted": false,
                        "__v": 0
                    }
                }
            ]
        }
    ],
    "message": "Dashboard fetched successfully."
}
*/
namedRouter.get("api.parent.dashboard", '/parent/dashboard', request_param.any(), async (req, res) => {
    try {
        const success = await parentController.ParentDashboard(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});


/**
 * @api {get} /parent/dashboardactivity/:id Parent Dashboard Activity
 * @apiVersion 1.0.0
 * @apiGroup Parent
 * @apiParam {ObjectId} id Pass Student Id in Params
 * @apiHeader x-access-token User's Access token
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [
        {
            "_id": "5d691e29780477ef5675d3ce",
            "student_id": "5d668fe78368ce4558d6ffd4",
            "page_id": "5cfa7b7828512408daa170a8",
            "coins_earned": 70,
            "activity_type": "CS",
            "status": "Active",
            "isDeleted": true,
            "user_info": {
                "_id": "5d668fe78368ce4558d6ffd4",
                "first_name": "testChild",
                "last_name": "",
                "email": "testChild@yopmail.com",
                "registered_as": "",
                "password": "$2a$08$Xye4tSHYloefCRylH7trOeJ9D77lnktuDBJ8DlhJrg9oLfNw7VgKO",
                "teacher_id": null,
                "parent_id": "5d668f988368ce4558d6ffd2",
                "principle_id": null,
                "school": "5d149edc585379f913ff0457",
                "avatar_id": null,
                "user_balance": null,
                "phone": "",
                "address": "",
                "city": "",
                "state": "",
                "country": "",
                "isVerified": "No",
                "grade": 1,
                "verifyToken": null,
                "deviceToken": "",
                "deviceType": "",
                "isDeleted": false,
                "isActive": true,
                "role": "5cd56d7bc6fff8f0dc8db24b",
                "__v": 0
            },
            "chapter_info": {
                "_id": "5cfa7a5828512408daa170a0",
                "title": "Title 1 ",
                "description": "desc ",
                "chapter_no": 1,
                "chapter_icon": "chapter_icon_1559919192278_car_3.jpeg",
                "chapter_pages": 10,
                "isDeleted": false,
                "status": "Active",
                "book_id": "5cfa1a34779ccd034eb17af0",
                "pages": [
                    {
                        "page_title": "page title 1 ",
                        "page_content": "<p>page desc&nbsp;1&nbsp;</p>\r\n",
                        "page_number": 2,
                        "_id": "5cfa7a8828512408daa170a1",
                        "status": "Active"
                    },
                    {
                        "page_title": "page title 2",
                        "page_content": "<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>\r\n",
                        "page_number": 3,
                        "_id": "5cfa7b7828512408daa170a8",
                        "status": "Active"
                    }
                ],
                "__v": 0
            },
            "book_info": {
                "_id": "5cfa1a34779ccd034eb17af0",
                "title": "adfasfdafdf",
                "subtitle": "fdafdfdf",
                "slug": "",
                "grade": 3,
                "genre": "5cfa05269f4ec670fb8b4b1f",
                "author": "5cfa062a839f1c081b1e2c6e",
                "picture": "picture_1559894579683_dummy.png",
                "chapter_num": 10,
                "status": "Active",
                "isDeleted": false,
                "__v": 0
            }
        }
    ],
    "message": "Dashboard fetched successfully."
}
*/
namedRouter.get("api.parent.dashboardactivity", '/parent/dashboardactivity/:id', request_param.any(), async (req, res) => {
    try {
        const success = await parentController.ParentDashboardActivity(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});


/**
 * @api {get} /parent/mychild Parent My Child
 * @apiVersion 1.0.0
 * @apiGroup Parent
 * @apiHeader x-access-token User's Access token
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [
        {
            "_id": "5d5eaf6a7ce0017662965ccd",
            "first_name": "testChild",
            "last_name": "",
            "email": "testChild@yopmail.com",
            "password": "$2a$08$Gfqgeb5SpE/vqPu4XO/8su9LJsYFzqb6/pk49.C5Hqz7XoiAalk4.",
            "teacher_id": "5d5eae4c7ce0017662965cc7",
            "parent_id": "5d5eaf3a7ce0017662965ccb",
            "principle_id": null,
            "school": "5d31989f8b727618daf64356",
            "grade": 2,
            "avatar_id": null,
            "user_balance": null,
            "phone": "",
            "address": "",
            "city": "",
            "state": "",
            "country": "",
            "isVerified": "No",
            "verifyToken": null,
            "deviceToken": "",
            "deviceType": "",
            "isDeleted": false,
            "isActive": true,
            "role": "5cd56d7bc6fff8f0dc8db24b",
            "__v": 0,
            "school_details": {
                "_id": "5d31989f8b727618daf64356",
                "school_name": "kidzee",
                "isDeleted": false,
                "isActive": true,
                "__v": 0
            },
            "teacher_details": {
                "_id": "5d5eae4c7ce0017662965cc7",
                "first_name": "testTeacher",
                "last_name": "testTeacher",
                "email": "testTeacher@yopmail.com",
                "password": "$2a$08$LNHJ5TN2KbAwxQj1yRVwIO4Y7ax4sXCPxnHp1A/CrQ1qZdTlUuVWK",
                "teacher_id": null,
                "parent_id": null,
                "principle_id": null,
                "school": "5d5e9a5507f83077610caacd",
                "avatar_id": null,
                "user_balance": null,
                "phone": "",
                "address": "",
                "city": "",
                "state": "",
                "country": "",
                "isVerified": "Yes",
                "verifyToken": 7920,
                "deviceToken": "",
                "deviceType": "",
                "isDeleted": false,
                "isActive": true,
                "role": "5cd56d27c6fff8f0dc8dae69",
                "__v": 0
            }
        },
        {
            "_id": "5d5eaf6a7ce0017662965cce",
            "first_name": "testChild2",
            "last_name": "testChild2lastName",
            "email": "testChild2@yopmail.com",
            "password": "$2a$08$UmUHNXF/9Q8iTT3ECWRw2.DeVuOREcop8dlzGO5VqhFXWuKBvQvMG",
            "teacher_id": null,
            "parent_id": "5d5eaf3a7ce0017662965ccb",
            "principle_id": null,
            "school": "5d3ab6e5b3dfcb2cace2428c",
            "grade": 5,
            "avatar_id": null,
            "user_balance": null,
            "phone": "12345678",
            "address": "usa",
            "city": "los angeles",
            "state": "miami",
            "country": "USA",
            "isVerified": "No",
            "verifyToken": null,
            "deviceToken": "",
            "deviceType": "",
            "isDeleted": false,
            "isActive": true,
            "role": "5cd56d7bc6fff8f0dc8db24b",
            "__v": 0,
            "school_details": {
                "_id": "5d3ab6e5b3dfcb2cace2428c",
                "school_name": "surendranath",
                "isDeleted": false,
                "isActive": true,
                "__v": 0
            }
        },
        {
            "_id": "5d638419ba29006cc0821c30",
            "first_name": "testChild3",
            "last_name": "",
            "email": "testChild3@yopmail.com",
            "password": "$2a$08$FUT2sGZ1FFbmTG7U/.UwvexOtGiFUxr2C.kcs5ZgmT5VKygUL/FvK",
            "teacher_id": null,
            "parent_id": "5d5eaf3a7ce0017662965ccb",
            "principle_id": null,
            "school": "5d3ef4ff4f0dd721c41e3f58",
            "grade": 6,
            "avatar_id": null,
            "user_balance": null,
            "phone": "",
            "address": "",
            "city": "",
            "state": "",
            "country": "",
            "isVerified": "No",
            "verifyToken": null,
            "deviceToken": "",
            "deviceType": "",
            "isDeleted": false,
            "isActive": true,
            "role": "5cd56d7bc6fff8f0dc8db24b",
            "__v": 0,
            "school_details": {
                "_id": "5d3ef4ff4f0dd721c41e3f58",
                "school_name": "heritage",
                "isDeleted": false,
                "isActive": true,
                "__v": 0
            }
        },
        {
            "_id": "5d63aa1aba29006cc0821c36",
            "first_name": "ad",
            "last_name": "",
            "email": "asd@w.com",
            "password": "$2a$08$eMC5gMnwB.LPbrcPApufy.ZwbPKGdnh5XdgfmFzHQl3vAZxEXj6dq",
            "teacher_id": "5d5ead6f7ce0017662965cc4",
            "parent_id": "5d5eaf3a7ce0017662965ccb",
            "principle_id": null,
            "school": "5d5d1fafd4221d54f859c92e",
            "grade": 8,
            "avatar_id": null,
            "user_balance": null,
            "phone": "",
            "address": "",
            "city": "",
            "state": "",
            "country": "",
            "isVerified": "No",
            "verifyToken": null,
            "deviceToken": "",
            "deviceType": "",
            "isDeleted": false,
            "isActive": true,
            "role": "5cd56d7bc6fff8f0dc8db24b",
            "__v": 0,
            "school_details": {
                "_id": "5d5d1fafd4221d54f859c92e",
                "school_name": "Davids school",
                "isDeleted": false,
                "isActive": true,
                "__v": 0
            }
        },
        {
            "_id": "5d63aa1aba29006cc0821c37",
            "first_name": "skp",
            "last_name": "",
            "email": "sadw@q.com",
            "password": "$2a$08$CAGMdVHx/7q9hZdFiUyRveB1TPK3lTBvNcXSkcukGHay08jd4nUHu",
            "teacher_id": "5d5eae4c7ce0017662965cc7",
            "parent_id": "5d5eaf3a7ce0017662965ccb",
            "principle_id": null,
            "school": "5d5fa5321ad30f4a662f48c1",
            "grade": 7,
            "avatar_id": null,
            "user_balance": null,
            "phone": "",
            "address": "",
            "city": "",
            "state": "",
            "country": "",
            "isVerified": "No",
            "verifyToken": null,
            "deviceToken": "",
            "deviceType": "",
            "isDeleted": false,
            "isActive": true,
            "role": "5cd56d7bc6fff8f0dc8db24b",
            "__v": 0,
            "school_details": {
                "_id": "5d5fa5321ad30f4a662f48c1",
                "school_name": "mckv",
                "isDeleted": false,
                "isActive": true,
                "__v": 0
            },
            "teacher_details": {
                "_id": "5d5eae4c7ce0017662965cc7",
                "first_name": "testTeacher",
                "last_name": "testTeacher",
                "email": "testTeacher@yopmail.com",
                "password": "$2a$08$LNHJ5TN2KbAwxQj1yRVwIO4Y7ax4sXCPxnHp1A/CrQ1qZdTlUuVWK",
                "teacher_id": null,
                "parent_id": null,
                "principle_id": null,
                "school": "5d5e9a5507f83077610caacd",
                "avatar_id": null,
                "user_balance": null,
                "phone": "",
                "address": "",
                "city": "",
                "state": "",
                "country": "",
                "isVerified": "Yes",
                "verifyToken": 7920,
                "deviceToken": "",
                "deviceType": "",
                "isDeleted": false,
                "isActive": true,
                "role": "5cd56d27c6fff8f0dc8dae69",
                "__v": 0
            }
        }
    ],
    "message": "My Child fetched successfully."
}
*/
namedRouter.get("api.parent.mychild", '/parent/mychild', request_param.any(), async (req, res) => {
    try {
        const success = await parentController.ParentMyChild(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});


/**
 * @api {get} /parent/mychildedit/:id Parent My Child Edit
 * @apiVersion 1.0.0
 * @apiGroup Parent
 * @apiHeader x-access-token User's Access token
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "first_name": "child2",
        "last_name": "class2",
        "email": "child2@gmail.com",
        "password": "$2a$08$MyB3pAcUgW.DcMdKvI/Wjutg0.H/2DvFxivlVlDJL9tgK7Sg9/ftK",
        "teacher_id": null,
        "parent_id": "5cfa06b3c1891efa1a543f0b",
        "principle_id": null,
        "school": null,
        "isVerified": "No",
        "verifyToken": null,
        "deviceToken": "",
        "deviceType": "",
        "isDeleted": false,
        "isActive": true,
        "_id": "5d161509585379f9134526c2",
        "school_name": "Test School",
        "role": "5cd56d7bc6fff8f0dc8db24b",
        "__v": 0
    },
    "message": "Child fetched successfully."
}
*/
namedRouter.get("api.parent.mychildedit", '/parent/mychildedit/:id', request_param.any(), async (req, res) => {
    try {
        const success = await parentController.ParentMyChildEdit(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

/**
 * @api {post} /parent/mychildupdate Parent My Child Update
 * @apiVersion 1.0.0
 * @apiGroup Parent
 * @apiHeader x-access-token User's Access token
 * @apiParam {string} id Child Id
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "first_name": "child2",
        "last_name": "class",
        "email": "child2@gmail.com",
        "password": "$2a$08$MyB3pAcUgW.DcMdKvI/Wjutg0.H/2DvFxivlVlDJL9tgK7Sg9/ftK",
        "teacher_id": null,
        "parent_id": "5cfa06b3c1891efa1a543f0b",
        "principle_id": null,
        "school": "5d149eba585379f913ff0178",
        "isVerified": "No",
        "verifyToken": null,
        "deviceToken": "",
        "deviceType": "",
        "isDeleted": false,
        "isActive": true,
        "_id": "5d16356f585379f91347d7af",
        "role": "5cd56d7bc6fff8f0dc8db24b",
        "__v": 0
    },
    "message": "Child updated successfully."
}
*/
namedRouter.post("api.parent.mychildupdate", '/parent/mychildupdate', request_param.any(), async (req, res) => {
    try {
        const success = await parentController.ParentMyChildUpdate(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

/**
 * @api {get} /parent/childDelete/:id Parent Child Delete
 * @apiVersion 1.0.0
 * @apiGroup Parent
 * @apiHeader x-access-token User's Access token
 * @apiParam {string} id Child Id
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "first_name": "testChild",
        "last_name": "",
        "email": "testChild@yopmail.com",
        "password": "$2a$08$/UEq4tGKcMRXKWSbL6tNYO2au9s3A5JANls7lr8/Imm8BPjTE/8Ni",
        "teacher_id": null,
        "parent_id": "5d650bfa9f864b34a26f9be5",
        "principle_id": null,
        "school": "5d5e9a5507f83077610caacd",
        "avatar_id": null,
        "user_balance": null,
        "phone": "",
        "address": "",
        "city": "",
        "state": "",
        "country": "",
        "isVerified": "No",
        "grade": 2,
        "verifyToken": null,
        "deviceToken": "",
        "deviceType": "",
        "isDeleted": true,
        "isActive": true,
        "_id": "5d650c409f864b34a26f9be7",
        "role": "5cd56d7bc6fff8f0dc8db24b",
        "__v": 0
    },
    "message": "Child deleted successfully."
}
*/
namedRouter.get("api.parent.childdelete", '/parent/childDelete/:id', async (req, res) => {
    try {
        const success = await parentController.ParentChildDelete(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

/**
 * @api {get} /parent/childView/:id Parent and Principle Child View
 * @apiVersion 1.0.0
 * @apiGroup Parent
 * @apiHeader x-access-token User's Access token [Parent || Principle]
 * @apiParam {ObjectId} id Child Id
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [
        {
            "_id": "5d78ebd606e0e045a4adc437",
            "class_name": "Class 5",
            "student_info": {
                "_id": "5d78ebd606e0e045a4adc437",
                "first_name": "Amily Federer",
                "last_name": "",
                "email": "amily@mailinator.com",
                "registered_as": "",
                "password": "$2a$08$c0vTcvtNgObaf5.m6ShrCe6db/36iJ0Gje5vfTa3R6ZDMtiZomGpC",
                "teacher_id": "5d8080a75957ad7880883cd8",
                "parent_id": "5d78eb3d06e0e045a4adc434",
                "principle_id": null,
                "school": "5d77766ddd702c69fdab460c",
                "avatar_id": null,
                "user_balance": null,
                "phone": "",
                "address": "",
                "city": "",
                "state": "",
                "country": "",
                "isVerified": "Yes",
                "grade": null,
                "verifyToken": null,
                "deviceToken": "",
                "deviceType": "",
                "isDeleted": false,
                "isActive": true,
                "role": "5cd56d7bc6fff8f0dc8db24b",
                "__v": 0
            },
            "teacher_info": {
                "_id": "5d8080a75957ad7880883cd8",
                "first_name": "taecher",
                "last_name": "doe",
                "email": "teacherdoe@yopmail.com",
                "registered_as": "",
                "password": "$2a$08$YNSs.fdTlAmnPMiq8SdBWuL.IXS6Ny6E8.WhVFtkiTDcm/PPI.Ww6",
                "teacher_id": null,
                "parent_id": null,
                "principle_id": null,
                "school": "5d77766ddd702c69fdab460c",
                "avatar_id": null,
                "user_balance": null,
                "phone": "",
                "address": "",
                "city": "",
                "state": "",
                "country": "",
                "isVerified": "Yes",
                "grade": null,
                "verifyToken": 3478,
                "deviceToken": "",
                "deviceType": "",
                "isDeleted": false,
                "isActive": true,
                "role": "5cd56d27c6fff8f0dc8dae69",
                "__v": 0
            },
            "school_info": {
                "_id": "5d9edc5fd881226a3c6830db",
                "school_name": "testSchool",
                "isDeleted": false,
                "isActive": true,
                "__v": 0
            }
        }
    ],
    "message": "Child information fetched successfully."
}
*/
namedRouter.get("api.parent.childView", '/parent/childView/:id', async (req, res) => {
    try {
        const success = await parentController.ParentAndTeacherChildView(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

// namedRouter.post("api.parent.getMyChildren", '/parent/getMyChildren', request_param.any(), async (req, res) => {
//     try {
//         const success = await parentController.getMyChildren(req, res);
//         res.status(success.status).send(success);
//     } catch (error) {
//         res.status(500).send({ error: error.message });
//     }
// });
// Export the express.Router() instance
module.exports = router;