const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const multer = require('multer');
const userController = require('webservice/user.controller');
const storage = multer.diskStorage({
    destination: function (req, file, callback) {
        callback(null, './public/uploads/parent/');
    },
    filename: function (req, file, callback) {
        callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname.replace(/\s/g, '_'));
    }

});
const request_param = multer();

const uploadImage = multer({
    storage: storage
});



/**
 * @api {post} /user/login Login
 * @apiVersion 1.0.0
 * @apiGroup User
 * @apiParam {string} userType User Type [principal/teacher/student/parent]
 * @apiParam {string} email Email
 * @apiParam {string} password Password
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVjZWQyY2E0YTk1MzkxNDdlN2YzOWZjOCIsImlhdCI6MTU1OTA0ODg0NSwiZXhwIjoxNTU5MTM1MjQ1fQ.t-mi86TxPmtVJ5g1LnG7P2xY7hzlKwpSIN1ib40nhOY",
    "data": {
        "first_name": "Parent ",
        "last_name": "sir",
        "email": "kuldeep.mishra@webskitters.com",
        "password": "$2a$08$uKHFLntNhMOVUmBnUnX3QeYxvWRb/uSskfblgFwl5qTKkS8Qkso4K",
        "teacher_id": null,
        "parent_id": null,
        "principle_id": null,
        "school_name": "",
        "isVerified": "Yes",
        "verifyToken": null,
        "deviceToken": "",
        "deviceType": "",
        "isDeleted": false,
        "isActive": true,
        "_id": "5ced2ca4a9539147e7f39fc8",
        "role": {
            "desc": "Parent of the student.",
            "_id": "5cd56cdac6fff8f0dc8daae7",
            "roleDisplayName": "Parent",
            "role": "parent",
            "id": "5cd56cdac6fff8f0dc8daae7"
        },
        "__v": 0
    },
    "message": "You have successfully logged in"
}

*@apiErrorExample {json} Error
{
    "status": 200,
    "data":{},
    "message": "Authentication failed, Wrong Password"
}
*/

namedRouter.post("api.user.login", '/user/login', request_param.any(), async (req, res) => {
    try {
        const success = await userController.login(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});


/**
 * @api {post} /user/reset-password Forgot Password
 * @apiVersion 1.0.0
 * @apiParam {string} email Email.
 * @apiGroup User


 * @apiSuccessExample {json} Success
 *{
    "status": 200,
    "data": {},
    "message": "A email with new password has been sent to your email address."
}
*@apiErrorExample {json} Error
{
    "status": 200,
    "data": {},
    "message": "No user found."
}
*/



namedRouter.post("api.user.forgotPassword", '/user/reset-password', request_param.any(), async (req, res) => {
    try {
        const success = await userController.forgotPassword(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

/**
 * @api {post} /user/verify Email Otp Verification
 * @apiVersion 1.0.0
 * @apiParam {string} user_id User id.
 * @apiParam {string} otp Otp .
 * @apiGroup User


 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVjZWQxYjA1YzFlMTI4M2YzMDY2N2U5YiIsImlhdCI6MTU1OTA0MzUyNSwiZXhwIjoxNTU5MTI5OTI1fQ.M-6LByjXyA4QR5mDvSJ5BJ_-rdzVbV7e_7uj5LO3Fi8",
    "data": {
        "first_name": "Parent ",
        "last_name": "sir",
        "email": "parentEmail@gmail.com",
        "password": "$2a$08$RcOWAVF7xgwoPAxnafXRJeM5J2Q.Dw4ytDNsAOli8w9cQQRmWMX1.",
        "teacher_id": null,
        "parent_id": null,
        "principle_id": null,
        "school_name": "",
        "isVerified": "Yes",
        "verifyToken": null,
        "deviceToken": "",
        "deviceType": "",
        "isDeleted": false,
        "isActive": true,
        "_id": "5ced1b05c1e1283f30667e9b",
        "role": "5cd56cdac6fff8f0dc8daae7",
        "__v": 0
    },
    "message": "Your account is verified succesfully."
}
*/


namedRouter.post("api.user.verify", '/user/verify', request_param.any(), async (req, res) => {
    try {
        const success = await userController.verifyToken(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});


/**
 * @api {post} /user/resendOtp Resend OTP
 * @apiVersion 1.0.0
 * @apiParam {string} email Email.
 * @apiGroup User
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "first_name": "Parent ",
        "last_name": "sir",
        "email": "kuldeep.mishra@webskitters.com",
        "password": "$2a$08$Sya5fxHAF116siwap4u96OigcAohwSzWtvf4U1N2Qrnz3d4mJp5Zi",
        "teacher_id": null,
        "parent_id": null,
        "principle_id": null,
        "school_name": "",
        "isVerified": "No",
        "verifyToken": 2007,
        "deviceToken": "",
        "deviceType": "",
        "isDeleted": false,
        "isActive": true,
        "_id": "5ced2ca4a9539147e7f39fc8",
        "role": "5cd56cdac6fff8f0dc8daae7",
        "__v": 0
    },
    "message": "A email with new OTP has been sent to your email address."
}
*/

namedRouter.post('api.user.resendOtp', '/user/resendOtp', request_param.any(), async (req, res) => {
    try {
        const success = await userController.resendOtp(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});


/**
 * @api {post} /user/email/available Email Available Check
 * @apiVersion 1.0.0
 * @apiParam {string} email Email.
 * @apiGroup User
 * @apiSuccessExample {json} Success
 *{
    "status": 200,
    "data": true,
    "message": "Email is already exists."
}
* @apiSuccessExample {json} Error
{
    "status": 200,
    "data": false,
    "message": ""
}
*/
namedRouter.post("api.user.emailAvailableCheck", '/user/email/available', request_param.any(), async (req, res) => {
    try {
        const success = await userController.emailAvailable(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});



namedRouter.get("api.user.logout", '/user/logout/:id', request_param.any(), async (req, res) => {
    try {
        const success = await userController.logout(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

/**
 * @api {get} /user/get-profile/:id Get User Profile View(For All Types User)
 * @apiVersion 1.0.0
 * @apiGroup User
 * @apiParam {id} id User id.
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "first_name": "new test",
        "last_name": "parent",
        "email": "newparentemail@yop.com",
        "password": "$2a$08$45VOWXxOp7gPur9OoHleZedLRVYWMtWJug1tORuTiJKVgGZwelnCK",
        "teacher_id": null,
        "parent_id": null,
        "principle_id": null,
        "school": null,
        "isVerified": "Yes",
        "verifyToken": 6911,
        "deviceToken": "",
        "deviceType": "",
        "isDeleted": false,
        "isActive": true,
        "_id": "5d14c27e276b2c71127ffafa",
        "role": "5cd56cdac6fff8f0dc8daae7",
        "__v": 0
    },
    "message": "Profile fetched Successfully"
}
*/
namedRouter.get("api.user.getProfile", '/user/get-profile/:id', async (req, res) => {
    try {
        const success = await userController.getProfile(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(500).send({
            error: error.message
        });
    }
});


/**
 * @api {post} /user/updateProfile Update User Profile
 * @apiVersion 1.0.0
 * @apiGroup User
 * @apiParam {id} id User id.
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "first_name": "child2",
        "last_name": "class2",
        "email": "child2@gmail.com",
        "password": "$2a$08$MyB3pAcUgW.DcMdKvI/Wjutg0.H/2DvFxivlVlDJL9tgK7Sg9/ftK",
        "teacher_id": null,
        "parent_id": "5cfa06b3c1891efa1a543f0b",
        "principle_id": null,
        "school": "5d149eba585379f913ff0178",
        "phone": "",
        "address": "",
        "city": "kolkata",
        "state": "WB",
        "country": "India",
        "isVerified": "Yes",
        "verifyToken": null,
        "deviceToken": "",
        "deviceType": "",
        "isDeleted": false,
        "isActive": true,
        "_id": "5d16356f585379f91347d7af",
        "role": "5cd56d7bc6fff8f0dc8db24b",
        "__v": 0
    },
    "message": "Profile updated Successfully"
}
*/
namedRouter.post("api.user.updateProfile", '/user/updateProfile', request_param.any(), async (req, res) => {
    try {
        const success = await userController.UpdateProfile(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(500).send({
            error: error.message
        });
    }
});

namedRouter.all('/user*', auth.authenticateAPI);

/**
 * @api {post} /user/changePassword User Change Password
 * @apiVersion 1.0.0
 * @apiGroup User
 * @apiParam {string} old_password Old password
 * @apiParam {string} new_password New password
 * @apiHeader x-access-token User's Access token
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "first_name": "child2",
        "last_name": "class2",
        "email": "child2@gmail.com",
        "password": "$2a$08$SB3PqiTpdYarAqstVZTsPeylftJXkKQ/IFpfby9NFC4Ew2rr71UCK",
        "teacher_id": null,
        "parent_id": "5cfa06b3c1891efa1a543f0b",
        "principle_id": null,
        "school": "5d149eba585379f913ff0178",
        "phone": "",
        "address": "",
        "city": "kolkata",
        "state": "WB",
        "country": "India",
        "isVerified": "Yes",
        "verifyToken": null,
        "deviceToken": "",
        "deviceType": "",
        "isDeleted": false,
        "isActive": true,
        "_id": "5d16356f585379f91347d7af",
        "role": "5cd56d7bc6fff8f0dc8db24b",
        "__v": 0
    },
    "message": "Password Changed Successfully"
}
*/
namedRouter.post("api.user.changePassword", '/user/changePassword', request_param.any(), async (req, res) => {
    try {
        const success = await userController.changePassword(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

// Export the express.Router() instance
module.exports = router;