const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const multer = require('multer');
const productController = require('webservice/product.controller');

const request_param = multer();

// namedRouter.all('/product*', auth.authenticateAPI);


/**
 * @api {get} /product/list Product List
 * @apiVersion 1.0.0
 * @apiGroup Product
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [
        {
            "pro_image": "pro_image_1566307997890_amazon.jpg",
            "link": "https://www.amazon.com/gp/product/1946125148/ref=dbs_a_def_rwt_bibl_vppi_i1",
            "status": "Active",
            "isDeleted": false,
            "_id": "5d5bf69d1cd4471586eb9bf5",
            "__v": 0
        },
        {
            "pro_image": "pro_image_1566312480606_amazon_1.jpg",
            "link": "https://www.amazon.com/gp/product/1946125091/ref=dbs_a_def_rwt_bibl_vppi_i2",
            "status": "Active",
            "isDeleted": false,
            "_id": "5d5c08201cd4471586eb9bf6",
            "__v": 0
        }
    ],
    "message": "Products Fetched Successfully."
}
*/
namedRouter.get("api.product.list", '/product/list', request_param.any(), async (req, res) => {
  try {
    const success = await productController.list(req, res);
    res.status(success.status).send(success);
  } catch (error) {
    res.status(error.status).send(error);
  }
});

// Export the express.Router() instance
module.exports = router;