const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const multer = require('multer');
const AvatarController = require('webservice/avatar.controller');
const request_param = multer();


namedRouter.all('/avatar*', auth.authenticateAPI);
/**
 * @api {get} /avatar/list Avatar List
 * @apiVersion 1.0.0
 * @apiGroup Avatar
 * @apiHeader x-access-token User's Access token
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [
       {
            "_id": "5d765be762464e58b6cb5545",
            "title": "Test Avatar 1",
            "purchase_coin": 5,
            "image_1": "image_1_1568037862972_1.1.png",
            "image_2": "image_2_1568037862997_1.2.png",
            "slug": "",
            "description": "Lorem Ipsum",
            "question": "Is Lorem Ipsum is dummy content ?",
            "status": "Active",
            "isDeleted": false,
            "is_buy": true
        },
        {
            "_id": "5d765c8099cf335cc131c44d",
            "title": "Test Avatar 2",
            "purchase_coin": 2,
            "image_1": "image_1_1568038016453_2.1.png",
            "image_2": "image_2_1568038016488_2.2.png",
            "slug": "",
            "description": "Lorem ipsum",
            "question": "Lorem Ipsum dummy content ?",
            "status": "Active",
            "isDeleted": false,
            "is_buy": true
        },
        {
            "_id": "5d766f30053c406b9979871d",
            "title": "Test Avatar 3",
            "purchase_coin": 5,
            "image_1": "image_1_1568042800610_3.1.png",
            "image_2": "image_2_1568042800641_3.2.png",
            "slug": "",
            "description": "test description",
            "question": "test question",
            "status": "Active",
            "isDeleted": false,
            "is_buy": false
        }
    ],
    "message": "Data Fetched Successfully."
}
*/

namedRouter.get("api.avatar.list", '/avatar/list', request_param.any(), async (req, res) => {
  try {
    const success = await AvatarController.avatarList(req, res);
    res.status(success.status).send(success);
  } catch (error) {
    res.status(error.status).send(error);
  }
});

/**
 * @api {get} /avatar/:id Avatar Detail
 * @apiVersion 1.0.0
 * @apiGroup Avatar
 * @apiHeader x-access-token User's Access token
 * @apiParam {string} id Avatar Id
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "title": "Test Avatar 1",
        "purchase_coin": 5,
        "image_1": "image_1_1568037862972_1.1.png",
        "image_2": "image_2_1568037862997_1.2.png",
        "slug": "",
        "description": "Lorem Ipsum",
        "question": "Is Lorem Ipsum is dummy content ?",
        "status": "Active",
        "isDeleted": false,
        "_id": "5d765be762464e58b6cb5545",
        "__v": 0
    },
    "message": "Avatar Fetched Successfully."
}
*/
namedRouter.get("api.avatar.detail", '/avatar/:id', async (req, res) => {
  try {
    const success = await AvatarController.avatarDetail(req, res);
    res.status(success.status).send(success);
  } catch (error) {
    res.status(error.status).send(error);
  }
});



/**
 * @api {post} /avatar/PurchaseAvatar Purchase Avatar for Student
 * @apiVersion 1.0.0
 * @apiParam {string} avatar_id Avatar Id
 * @apiHeader x-access-token User's Access token
 * @apiGroup Avatar
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "user_id": "5d7660124439bc7d94880398",
        "avatar_id": "5d766f30053c406b9979871d",
        "coins": 5,
        "purchase_date": 1569500769995,
        "status": "Active",
        "isDeleted": false,
        "_id": "5d8cae6149e2fe0c8420a910",
        "__v": 0
    },
    "message": "You have successfully purchase the Avatar and your balance has been adjusted."
}
*/
namedRouter.post("api.avatars.PurchaseAvatar", '/avatar/PurchaseAvatar', request_param.any(), async (req, res) => {
  try {
    const success = await AvatarController.userAvatarPurchase(req, res);
    res.status(success.status).send(success);
  } catch (error) {
    res.status(error.status).send(error);
  }
});


// Export the express.Router() instance
module.exports = router;