"use strict";
// Class definition
var KTDatatableBook = function () {
    // Private functions
    var options = {
        // datasource definition
        data: {
            type: 'remote',
            source: {
                read: {
                    url: `http://${window.location.host}/admin/book/getall`,
                },
            },
            pageSize: 10,
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true,
        },

        // layout definition
        layout: {
            scroll: true, // enable/disable datatable scroll both horizontal and
            // vertical when needed.
            height: 500, // datatable's body's fixed height
            footer: false // display/hide footer
        },

        // column sorting
        sortable: true,

        pagination: true,

        // columns definition

        columns: [{
                field: 'title',
                title: 'Title',
                template: function (row) {
                    var user_img = '100_10.jpg';
                    var output = '';
                    output = '<div class="kt-user-card-v2">\
                            <div class="kt-user-card-v2__pic">\
                                <img src="/uploads/book/thumb/' + row.picture + '" alt="photo">\
                            </div>\
                            <div class="kt-user-card-v2__details">\
                                <span class="kt-user-card-v2__name">' + row.title + '</span>\
                            </div>\
                        </div>';
                    return output;
                },
            },
            {
                field: 'grade',
                title: 'Grade',
                width: 100,
                // callback function support for column rendering
                template: function (row) {
                    return row.grade;
                },
            },
            {
                field: 'genre_docs',
                title: 'Genre',
                // callback function support for column rendering
                template: function (row) {
                    return row.genre_docs.title;
                },
            }, {
                field: 'author_docs',
                title: 'Author',
                // callback function support for column rendering
                template: function (row) {
                    return row.author_docs.title;
                },
            }, {
                field: 'chapter_num',
                title: 'Chapters',
                // callback function support for column rendering
                template: function (row) {
                    return row.chapter_num;
                },
            }, {
                field: 'status',
                title: 'Status',
                // callback function support for column rendering
                template: function (row) {
                    var status = {
                        "Active": {
                            'title': 'Active',
                            'class': 'kt-badge--brand'
                        },
                        "Inactive": {
                            'title': 'Inactive',
                            'class': ' kt-badge--danger'
                        },
                    };
                    return '<span class="kt-badge ' + status[row.status].class +
                        ' kt-badge--inline kt-badge--pill">' + status[row.status].title +
                        '</span>';
                },
            }, {
                field: 'Actions',
                title: 'Actions',
                sortable: false,
                width: 110,
                overflow: 'visible',
                textAlign: 'center',
                autoHide: false,
                template: function (row) {
                    return '\
                \<a href="http://' + window.location.host + '/admin/book/getallChapterlist/' + row._id + '" class="btn btn-sm btn-clean btn-icon btn-icon-sm" title="View Chapters">\
                        <i class="flaticon-eye"></i>\
                    </a>\
                    \<a href="http://' + window.location.host + '/admin/book/edit/' + row._id + '" class="btn btn-sm btn-clean btn-icon btn-icon-sm" title="Edit">\
                        <i class="flaticon-edit"></i>\
                    </a>\
                    \<a id="del-' + row._id + '" href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-sm ktDelete" title="Delete">\
                        <i class="flaticon-delete"></i>\
                    </a>\
                ';
                },
            }
        ],
    };

    // console.log('row', row);

    // basic demo
    var bookSelector = function () {

        options.search = {
            input: $('#generalSearch'),
        };

        var datatable = $('#bookRecordSelection').KTDatatable(options);

        $('#kt_form_status').on('change', function () {
            datatable.search($(this).val(), 'Status');
        });

        $('#kt_form_type').on('change', function () {
            datatable.search($(this).val().toLowerCase(), 'Type');
        });

        $('#kt_form_status,#kt_form_type').selectpicker();

        datatable.on(
            'kt-datatable--on-check kt-datatable--on-uncheck kt-datatable--on-layout-updated',
            function (e) {
                var checkedNodes = datatable.rows('.kt-datatable__row--active').nodes();
                var count = checkedNodes.length;
                $('#kt_datatable_selected_number').html(count);
                if (count > 0) {
                    $('#kt_datatable_group_action_form').collapse('show');
                } else {
                    $('#kt_datatable_group_action_form').collapse('hide');
                }
            });

        $('#kt_modal_fetch_id').on('show.bs.modal', function (e) {
            var ids = datatable.rows('.kt-datatable__row--active').
            nodes().
            find('.kt-checkbox--single > [type="checkbox"]').
            map(function (i, chk) {
                return $(chk).val();
            });
            var c = document.createDocumentFragment();
            for (var i = 0; i < ids.length; i++) {
                var li = document.createElement('li');
                li.setAttribute('data-id', ids[i]);
                li.innerHTML = 'Selected record ID: ' + ids[i];
                c.appendChild(li);
            }
            $(e.target).find('.kt-datatable_selected_ids').append(c);
        }).on('hide.bs.modal', function (e) {
            $(e.target).find('.kt-datatable_selected_ids').empty();
        });

        $(document).on('click', '.ktDelete', function () {
            var elemID = $(this).attr('id').replace('del-', '');
            swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true
            }).then(function (result) {
                if (result.value) {
                    window.location.href = `http://${window.location.host}/admin/book/delete/${elemID}`;
                }
            });
        });
    };



    return {
        // public functions
        init: function () {
            bookSelector();
        },
    };
}();

jQuery(document).ready(function () {
    KTDatatableBook.init();
});