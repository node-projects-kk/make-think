"use strict";
// Class definition
var KTDatatablequestion = function () {
    // Private functions

    var options = {
        // datasource definition
        data: {
            type: 'remote',
            source: {
                read: {
                    url: `http://${window.location.host}/admin/question/getall`,
                },
            },
            pageSize: 10,
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true,
        },

        // layout definition
        layout: {
            scroll: true, // enable/disable datatable scroll both horizontal and
            // vertical when needed.
            height: 350, // datatable's body's fixed height
            footer: false // display/hide footer
        },

        // column sorting
        sortable: true,

        pagination: true,

        // columns definition

        columns: [{
            field: 'question',
            title: 'Question',
            template: function (row) {
                if(row.question.length>40)
                {
                  return row.question.substring(0, 40)+'....';
                }
                else
                {
                  return row.question;
                }
             },
        },{
            field: 'question_type',
            title: 'Question Type',
        },{
            field: 'book_name',
            title: 'Book Name',
        },{
            field: 'points',
            title: 'Point',
        },{
            field: 'status',
            title: 'Status',
            // callback function support for column rendering
            template: function (row) {
                var status = {
                    "Active": {
                        'title': 'Active',
                        'class': 'kt-badge--brand'
                    },
                    "Inactive": {
                        'title': 'Inactive',
                        'class': ' kt-badge--danger'
                    },
                };
                return '<span id="status-'+ row._id + '" class="kt-badge ' + status[row.status].class +
                    ' kt-badge--inline kt-badge--pill">' + status[row.status].title +
                    '</span>';
            },
        }, {
            field: 'Actions',
            title: 'Actions',
            sortable: false,
            width: 110,
            overflow: 'visible',
            textAlign: 'left',
            autoHide: false,
            template: function(row) {
	            return '\
                    \<a href="http://'+window.location.host+'/admin/question/edit/'+row._id+'" class="btn btn-sm btn-clean btn-icon btn-icon-sm" title="Edit">\
                        <i class="flaticon-edit"></i>\
                    </a>\
                    \<a id="del-'+row._id+'" href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-sm ktQuestionDelete" title="Delete">\
                        <i class="flaticon-delete"></i>\
                    </a>\
                ';
            },
        }],
    };

    // basic demo
    var questionSelector = function () {

        options.search = {
            input: $('#generalSearch'),
        };

        var datatable = $('#questionRecordSelection').KTDatatable(options);

        $('#kt_form_status').on('change', function () {
            datatable.search($(this).val(), 'Status');
        });

        $('#kt_form_type').on('change', function () {
            datatable.search($(this).val().toLowerCase(), 'Type');
        });

        $('#kt_form_status,#kt_form_type').selectpicker();

        datatable.on(
            'kt-datatable--on-check kt-datatable--on-uncheck kt-datatable--on-layout-updated',
            function (e) {
                var checkedNodes = datatable.rows('.kt-datatable__row--active').nodes();
                var count = checkedNodes.length;
                $('#kt_datatable_selected_number').html(count);
                if (count > 0) {
                    $('#kt_datatable_group_action_form').collapse('show');
                } else {
                    $('#kt_datatable_group_action_form').collapse('hide');
                }
            });

        $('#kt_modal_fetch_id').on('show.bs.modal', function (e) {
            var ids = datatable.rows('.kt-datatable__row--active').
                nodes().
                find('.kt-checkbox--single > [type="checkbox"]').
                map(function (i, chk) {
                    return $(chk).val();
                });
            var c = document.createDocumentFragment();
            for (var i = 0; i < ids.length; i++) {
                var li = document.createElement('li');
                li.setAttribute('data-id', ids[i]);
                li.innerHTML = 'Selected record ID: ' + ids[i];
                c.appendChild(li);
            }
            $(e.target).find('.kt-datatable_selected_ids').append(c);
        }).on('hide.bs.modal', function (e) {
            $(e.target).find('.kt-datatable_selected_ids').empty();
        });

        $(document).on('click', '.ktQuestionDelete', function () {
            var elemID = $(this).attr('id').replace('del-', '');
            swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true
            }).then(function (result) {
                if (result.value) {
                    window.location.href = `http://${window.location.host}/admin/question/delete/${elemID}`;
                }
            });
        });
        // $(document).on('click', '.kt-badge', function () {
        //     // alert("sss")
        //     var elemID = $(this).attr('id').replace('status-', '');;
        //     // console.log("asdsad",elemID)

        //     window.location.href = `http://${window.location.host}/admin/question/statusChange/${elemID}`;
        // });
    };



    return {
        // public functions
        init: function () {
            questionSelector();
        },
    };
}();

jQuery(document).ready(function () {
    if ($('#questionRecordSelection').length > 0) {
        KTDatatablequestion.init();
    }
});