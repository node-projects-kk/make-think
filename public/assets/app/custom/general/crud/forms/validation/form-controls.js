// Class definition

var KTFormControls = function () {
    // Private functions

    var myProfileValidation = function () {
        $("#frmMyProfile").validate({
            // define validation rules
            rules: {
                first_name: {
                    required: true,
                    letterswithbasicpunc: true
                },
                last_name: {
                    required: true,
                    letterswithbasicpunc: true
                }
            },
            messages: {
                first_name: {
                    required: "Please enter your first name",
                    letterswithbasicpunc: "Please enter alphabets only"
                },
                last_name: {
                    required: "Please enter your first name",
                    letterswithbasicpunc: "Please enter alphabets only"
                }
            },
            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                form[0].submit();
            }
        });
    }

    var createBookValidation = function () {
        // alert('hgfd');
        $("#createBookValidation").validate({

            // define validation rules
            rules: {
                title: {
                    required: true,
                    // letterswithbasicpunc: true
                },
                genre: {
                    required: true
                },
                author: {
                    required: true
                },
                grade: {
                    required: true
                },
                picture: {
                    required: true,
                    extension: "jpg|jpeg|png|gif|x-ms-bmp"
                },
                chapter_num: {
                    required: true,
                    digits: true
                }
            },
            messages: {
                title: {
                    required: "Please enter title"
                },
                genre: {
                    required: "Please enter genre"
                },
                author: {
                    required: "Please enter author"
                },
                grade: {
                    required: "Please select grade"
                },
                picture: {
                    required: "Please select picture"
                },
                chapter_num: {
                    required: "Please enter number of chapters",
                    digits: "Please enter digits only"
                },
            },
            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                form[0].submit();
            }
        });
    }

    var editBookValidation = function () {
        // alert('hgfd');
        $("#frmEditBook").validate({

            // define validation rules
            rules: {
                title: {
                    required: true,
                    // letterswithbasicpunc: true
                },
                genre: {
                    required: true
                },
                author: {
                    required: true
                },
                grade: {
                    required: true
                },
                picture: {
                    extension: "jpg|jpeg|png|gif|x-ms-bmp"
                },
                chapter_num: {
                    required: true,
                    digits: true
                }
            },
            messages: {
                title: {
                    required: "Please enter title"
                },
                genre: {
                    required: "Please enter genre"
                },
                author: {
                    required: "Please enter author"
                },
                grade: {
                    required: "Please select grade"
                },
                picture: {
                    required: "Please select picture"
                },
                chapter_num: {
                    required: "Please enter number of chapters",
                    digits: "Please enter digits only"
                },
            },
            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                form[0].submit();
            }
        });
    }

    var CreateChapterValidation = function () {
        $("#CreateChapterValidation").validate({
            // define validation rules
            rules: {
                book_id: {
                    required: true,
                    // letterswithbasicpunc: true
                },
                chapter_no: {
                    required: true,
                    // letterswithbasicpunc: true
                },
                chapter_pages: {
                    required: true,
                },
                chapter_icon: {
                    required: true,
                    extension: "jpg|jpeg|png|gif|x-ms-bmp"
                },
                title: {
                    required: true,
                    // letterswithbasicpunc: true
                },
                description: {
                    required: true,
                    // letterswithbasicpunc: true
                },
            },
            messages: {
                book_id: {
                    required: "Please select book",
                    // letterswithbasicpunc: "Please enter alphabets only"
                },
                chapter_no: {
                    required: "Please select chapter",
                    // letterswithbasicpunc: "Please enter alphabets only"
                },
                chapter_pages: {
                    required: "Please enter pages",
                    // letterswithbasicpunc: "Please enter alphabets only"
                },
                chapter_icon: {
                    required: "Please select images",
                },
                title: {
                    required: "Please enter title",
                    // letterswithbasicpunc: "Please enter alphabets only"
                },
                description: {
                    required: "Please enter title",
                    // letterswithbasicpunc: "Please enter alphabets only"
                },
            },
            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                form[0].submit(); // submit the form

            }
        });
    }

    var EditChapterValidation = function () {
        $("#EditChapterValidation").validate({
            // define validation rules
            rules: {
                book_id: {
                    required: true,
                    // letterswithbasicpunc: true
                },
                chapter_no: {
                    required: true,
                    // letterswithbasicpunc: true
                },
                chapter_pages: {
                    required: true,
                },
                chapter_icon: {
                    /* required: true, */
                    extension: "jpg|jpeg|png|gif|x-ms-bmp"
                },
                title: {
                    required: true,
                    // letterswithbasicpunc: true
                },
                description: {
                    required: true,
                    // letterswithbasicpunc: true
                },
            },
            messages: {
                book_id: {
                    required: "Please select book",
                    // letterswithbasicpunc: "Please enter alphabets only"
                },
                chapter_no: {
                    required: "Please select chapter",
                    // letterswithbasicpunc: "Please enter alphabets only"
                },
                chapter_pages: {
                    required: "Please enter pages",
                    // letterswithbasicpunc: "Please enter alphabets only"
                },
                chapter_icon: {
                    required: "Please select image file only.",
                },
                title: {
                    required: "Please enter title",
                    // letterswithbasicpunc: "Please enter alphabets only"
                },
                description: {
                    required: "Please enter title",
                    // letterswithbasicpunc: "Please enter alphabets only"
                },
            },
            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                form[0].submit(); // submit the form

            }
        });
    }

    var CreatePageValidation = function () {
        $("#CreatePageValidation").validate({
            // define validation rules
            rules: {
                page_number: {
                    required: true,
                },
                page_title: {
                    required: true,
                }
            },
            messages: {
                page_number: {
                    required: "Please select page number",
                    // letterswithbasicpunc: "Please enter alphabets only"
                },
                page_title: {
                    required: "Please enter page title",
                }
            },
            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                form[0].submit(); // submit the form

            }
        });
    }

    var addCountryForm = function () {
        $("#addCountryForm").validate({
            // define validation rules
            rules: {
                country_name: {
                    required: true,
                }
            },
            messages: {
                country_name: {
                    required: "Please country name",
                    // letterswithbasicpunc: "Please enter alphabets only"
                }
            },
            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                form[0].submit(); // submit the form

            }
        });
    }
    var editCountryForm = function () {
        $("#editCountryForm").validate({
            // define validation rules
            rules: {
                country_name: {
                    required: true,
                }
            },
            messages: {
                country_name: {
                    required: "Please country name",
                    // letterswithbasicpunc: "Please enter alphabets only"
                }
            },
            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                form[0].submit(); // submit the form

            }
        });
    }

    var EditPageValidation = function () {
        $("#EditPageValidation").validate({
            // define validation rules
            rules: {
                page_number: {
                    required: true,
                },
                page_title: {
                    required: true,
                },
                page_content: {
                    required: function (textarea) {
                        CKEDITOR.instances[textarea.id].updateElement(); // update textarea
                        var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
                        return editorcontent.length === 0;
                    }
                }
            },
            messages: {
                page_number: {
                    required: "Please select page number",
                    // letterswithbasicpunc: "Please enter alphabets only"
                },
                page_title: {
                    required: "Please enter page title",
                },
                page_title: {
                    required: "Please enter page content",
                }
            },
            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                form[0].submit(); // submit the form

            }
        });
    }

    var createGenreValidation = function () {
        $("#frmCreateGenre").validate({
            // define validation rules
            rules: {
                title: {
                    required: true,
                    letterswithbasicpunc: true
                }
            },
            messages: {
                title: {
                    required: "Please enter title",
                    letterswithbasicpunc: "Please enter alphabets only"
                },
            },
            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                // console.log("form", form)
                form[0].submit(); // submit the form
            }
        });
    }

    var editGenreValidation = function () {
        $("#frmEditGenre").validate({
            // define validation rules
            rules: {
                title: {
                    required: true,
                    letterswithbasicpunc: true
                }
            },
            messages: {
                title: {
                    required: "Please enter title",
                    letterswithbasicpunc: "Please enter alphabets only"
                },
            },
            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                form[0].submit(); // submit the form
            }
        });
    }

    var createAuthorValidation = function () {
        $("#frmCreateAuthor").validate({
            // define validation rules
            rules: {
                title: {
                    required: true,
                    letterswithbasicpunc: true
                }
            },
            messages: {
                title: {
                    required: "Please enter title",
                    letterswithbasicpunc: "Please enter alphabets only"
                },
            },
            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                form[0].submit(); // submit the form
            }
        });
    }

    var editAuthorValidation = function () {
        $("#frmEditAuthor").validate({
            // define validation rules
            rules: {
                title: {
                    required: true,
                    // letterswithbasicpunc: true
                }
            },
            messages: {
                title: {
                    required: "Please enter title",
                    // letterswithbasicpunc: "Please enter alphabets only"
                },
            },
            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                form[0].submit(); // submit the form
            }
        });
    }

    //validtion for principle 
    var editPrincipleValidation = function () {
        $("#frmEditPrinciple").validate({
            rules: {
                first_name: {
                    required: true,
                    letterswithbasicpunc: true
                },
                last_name: {
                    required: true,
                },
                email: {
                    required: true,
                    email: true,
                },
                school_name: {
                    required: true,
                },
            },
            messages: {
                first_name: {
                    required: "Please enter First Name",
                    letterswithbasicpunc: "Please enter alphabets only"
                },
                last_name: {
                    required: "Please enter Last Name",
                },
                email: {
                    required: "Please enter Email",
                },
                school_name: {
                    required: "Please enter School Name",
                },
            },
            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                form[0].submit(); // submit the form
            }
        });
    }

    //validtion for Teacher 
    var editTeacherValidation = function () {
        $("#frmEditTeacher").validate({
            rules: {
                first_name: {
                    required: true,
                    letterswithbasicpunc: true
                },
                last_name: {
                    required: true,
                },
                email: {
                    required: true,
                    email: true,
                },
                school_name: {
                    required: true,
                },
            },
            messages: {
                first_name: {
                    required: "Please enter First Name",
                    letterswithbasicpunc: "Please enter alphabets only"
                },
                last_name: {
                    required: "Please enter Last Name",
                },
                email: {
                    required: "Please enter Email",
                },
                school_name: {
                    required: "Please enter School Name",
                },
            },
            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                form[0].submit(); // submit the form
            }
        });
    }

    var editParentValidation = function () {
        $("#frmEditParent").validate({
            rules: {
                first_name: {
                    required: true,
                    letterswithbasicpunc: true
                },
                last_name: {
                    required: true,
                },
                email: {
                    required: true,
                    email: true,
                },
                school_name: {
                    required: true,
                },
            },
            messages: {
                first_name: {
                    required: "Please enter First Name",
                    letterswithbasicpunc: "Please enter alphabets only"
                },
                last_name: {
                    required: "Please enter Last Name",
                },
                email: {
                    required: "Please enter Email",
                },
                school_name: {
                    required: "Please enter School Name",
                },
            },
            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                form[0].submit(); // submit the form
            }
        });
    }

    var createQuestionValidation = function () {
        $("#frmCreateQuestion").validate({
            // define validation rules
            ignore: [],
            debug: false,
            rules: {
                book_id: {
                    required: true
                },
                chapter_no: {
                    required: true,
                    digits: true
                },
                title: {
                    required: true
                },
                picture: {
                    required: true
                },
                question: {
                    required: function (textarea) {
                        CKEDITOR.instances[textarea.id].updateElement(); // update textarea
                        var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
                        return editorcontent.length === 0;
                    }
                }
            },
            messages: {
                book_id: {
                    required: "Please select a book"
                },
                chapter_no: {
                    required: "Please select chapter no.",
                    digits: "Please enter digits only"
                },
                title: {
                    required: "Please add the title of this chapter"
                },
                description: {
                    required: "Please add the title of this description"
                },
                question: {
                    required: "Please add a question"
                },
            },
            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                form[0].submit();
            }
        });
    }

    var myProfileValidation = function () {
        $("#changePasswordForm").validate({
            // define validation rules
            rules: {
                password: {
                    required: true,
                    // lettersonly: true
                },

                password_confirm: {
                    equalTo: "#password"
                }
            },
            messages: {
                password: {
                    required: "Please enter your password",
                    // lettersonly: "Please enter alphabets only"
                },
                password_confirm: " Enter Confirm Password Same as Password"
            },
            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                form[0].submit();
            }
        });
    }

    var createAvatarValidation = function () {
        $("#frmCreateAvatar").validate({
            // define validation rules
            ignore: [],
            debug: false,
            rules: {
                title: {
                    required: true
                },
                image: {
                    required: true,
                    extension: "jpg|jpeg|png|gif|x-ms-bmp"
                },
                purchase_coin: {
                    required: true
                }
            },
            messages: {
                title: {
                    required: "Please enter Avatar Title"
                },
                image: {
                    required: "Please upload an Image"
                },
                purchase_coin: {
                    required: "Please enter Coin count"
                }
            },
            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                form[0].submit();
            }
        });
    }

    var editAvatarValidation = function () {
        $("#frmEditAvatar").validate({
            // define validation rules
            ignore: [],
            debug: false,
            rules: {
                title: {
                    required: true
                },
                image: {
                    extension: "jpg|jpeg|png|gif|x-ms-bmp"
                },
                purchase_coin: {
                    required: true
                }
            },
            messages: {
                title: {
                    required: "Please enter Avatar Title"
                },
                image: {
                    required: "Please upload an Image"
                },
                purchase_coin: {
                    required: "Please enter Coin count"
                }
            },
            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                form[0].submit();
            }
        });
    }

    var createProductValidation = function () {
        $("#createProductValidation").validate({
            // define validation rules
            rules: {
                link: {
                    required: true,
                },
                pro_image: {
                    required: true,
                    extension: "jpg|jpeg|png"
                },
            },
            messages: {
                link: {
                    required: "Please enter Link",
                    // letterswithbasicpunc: "Please enter alphabets only"
                },
                pro_image: {
                    required: "Please select an image",
                },
            },
            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                form[0].submit(); // submit the form

            }
        });
    }

    var ProductEditValidation = function () {
        $("#ProductEditValidation").validate({
            // define validation rules
            rules: {
                link: {
                    required: true,
                },
                pro_image: {
                    extension: "jpg|jpeg|png"
                },
            },
            messages: {
                link: {
                    required: "Please enter link",
                },
                pro_image: {
                    required: "Please select image file only.",
                },
            },
            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                form[0].submit(); // submit the form

            }
        });
    }


    var chapterAsPerBook = function () {
        var Id = $(".bookId").val();

        $(".bookId").change(function () {
            let Id = $(this).val();
            $('.titleId').html("<option value=''>Select a Chapter</option>")
            $.ajax({
                type: "GET",
                url: `http://${window.location.host}/admin/book/chapterTitle/${Id}`,
                dataType: "json",
                success: function (result) {
                    if (result) {
                        // console.log(result)
                        var html = "<option value=''>Select a Chapter</option>";
                        jQuery.each(result, function (index, item) {
                            html += "<option value=" + item._id + ">" + item.title + "</option>"
                            $('.titleId').html(html);
                        });
                    }

                },
            })
        });
    }

    var chapterAsPerBookId = function () {
        var Id = $("#book_id").val();

        $("#book_id").change(function () {
            let Id = $(this).val();
            // $('.titleId').html("<option value=''>Select a Title</option>")
            $.ajax({
                type: "GET",
                url: `http://${window.location.host}/admin/book/chapterTitleId/${Id}`,
                dataType: "json",
                success: function (result) {
                    if (result) {
                        // console.log(result)
                        var html = "<option value=''>Select a Chapter</option>";
                        for (var i = 1; i <= result.chapter_num; i++) {
                            html += "<option value=" + i + ">" + i + "</option>"
                        }
                        $('#chapter_no').html(html);

                    }
                },
            })
        });
    }

    var titleAsPerBookAndChapter = function () {
        $(".chaperNo").on('change', function () {
            $('.questionTitle').val('');
            $('#description').val('');

            let chapterNo = $(this).val();
            let bookId = $('.bookId').val();

            $.ajax({
                type: "GET",
                url: `http://${window.location.host}/admin/question/getTitleAndDescription/${bookId}/${chapterNo}`,
                dataType: "json",
                success: function (result) {
                    if (result) {
                        $('.questionTitle').val(result.title);
                        $('#description').val(result.description);
                    }
                },
            })
        });
    }

    var blankSpaceNotAllow = function () {
        $("input").on("keypress", function (e) {
            var startPos = e.currentTarget.selectionStart;
            if (e.which === 32 && startPos == 0)
                e.preventDefault();
        })
    }

    return {
        // public functions
        init: function () {
            myProfileValidation();
            CreateChapterValidation();
            EditChapterValidation();
            CreatePageValidation();
            EditPageValidation();
            addCountryForm();
            editCountryForm();
            createBookValidation();
            editBookValidation();
            createGenreValidation();
            editGenreValidation();
            createAuthorValidation();
            editAuthorValidation();
            createQuestionValidation();
            createAvatarValidation();
            editAvatarValidation();
            chapterAsPerBook();
            titleAsPerBookAndChapter();
            editPrincipleValidation();
            chapterAsPerBookId();
            editTeacherValidation();
            editParentValidation();
            blankSpaceNotAllow();
            createProductValidation();
            ProductEditValidation();
        }
    };
}();

jQuery(document).ready(function () {
    KTFormControls.init();
});

//Chapter no validation during add
function checkChapterNo(type) {
    // alert("came");
    var apiBaseURL = `${window.location.origin}/admin`;
    var chapter_no = $("#chapter_no").val(); // value in field email
    var book_id = $("#book_id").val();
    $.ajax({
        type: 'post',
        url: apiBaseURL + "/verify-chapter-number", // put your real file name 
        data: {
            chapter_no: chapter_no,
            book_id: book_id
        },
        success: function (msg) {
            if (msg) {
                document.getElementById('chapter_no').value = '';
                document.getElementById('chapter-no-message').style.display = 'block';
            } else {
                document.getElementById('chapter-no-message').style.display = 'none';
            }
        }
    });
}

//Chapter title validation during add
function checkChapterTitle(type) {
    // alert("came");
    var apiBaseURL = `${window.location.origin}/admin`;
    var title = $("#title").val(); // value in field email
    var book_id = $("#book_id").val();
    $.ajax({
        type: 'post',
        url: apiBaseURL + "/verify-chapter-title", // put your real file name 
        data: {
            title: title,
            book_id: book_id
        },
        success: function (msg) {
            if (msg) {
                document.getElementById('title').value = '';
                document.getElementById('chapter-title-message').style.display = 'block';
            } else {
                document.getElementById('chapter-title-message').style.display = 'none';
            }
        }
    });
}

//Chapter title validation during edit
function checkChapterTitleForEdit(value) {
    // alert("came");
    var apiBaseURL = `${window.location.origin}/admin`;
    var title = $("#title").val(); // value in field email
    var book_id = $("#book_id").val();
    if (title == value) {
        document.getElementById('chapter-title-message').style.display = 'none'
    } else {
        $.ajax({
            type: 'post',
            url: apiBaseURL + "/verify-chapter-title", // put your real file name 
            data: {
                title: title,
                book_id: book_id
            },
            success: function (msg) {
                if (msg) {
                    document.getElementById('title').value = value;
                    document.getElementById('chapter-title-message').style.display = 'block';
                } else {
                    document.getElementById('chapter-title-message').style.display = 'none';
                }
            }
        });
    }
}

//Page no validation during add
function checkPageNo() {
    //alert("came");
    var apiBaseURL = `${window.location.origin}/admin`;
    var page_no = $("#page_number").val(); // value in field email
    var book_id = $("#book_id").val();
    var chapter_no = $("#chapter_no").val();
    $.ajax({
        type: 'post',
        url: apiBaseURL + "/verify-page-number", // put your real file name 
        data: {
            chapter_no: chapter_no,
            page_no: page_no,
            book_id: book_id
        },
        success: function (msg) {
            if (msg) {
                document.getElementById('page_number').value = '';
                document.getElementById('page-no-message').style.display = 'block';
            } else {
                document.getElementById('page-no-message').style.display = 'none';
            }
        }
    });
}

//Page title validation during add
function checkPageTitle() {
    // alert("came");
    var apiBaseURL = `${window.location.origin}/admin`;
    var page_title = $("#page_title").val(); // value in field email
    var book_id = $("#book_id").val();
    var chapter_no = $("#chapter_no").val();
    $.ajax({
        type: 'post',
        url: apiBaseURL + "/verify-page-title", // put your real file name 
        data: {
            page_title: page_title,
            chapter_no: chapter_no,
            book_id: book_id
        },
        success: function (msg) {
            if (msg) {
                document.getElementById('page_title').value = '';
                document.getElementById('page-title-message').style.display = 'block';
            } else {
                document.getElementById('page-title-message').style.display = 'none';
            }
        }
    });
}

//Page title validation during edit
function checkPageTitleForEdit(value) {
    // alert("came");
    var apiBaseURL = `${window.location.origin}/admin`;
    var page_title = $("#page_title").val(); // value in field email
    var book_id = $("#book_id").val();
    var chapter_no = $("#chapter_no").val();
    if (page_title == value) {
        document.getElementById('page-title-message').style.display = 'none'
    } else {
        $.ajax({
            type: 'post',
            url: apiBaseURL + "/verify-page-title", // put your real file name 
            data: {
                page_title: page_title,
                chapter_no: chapter_no,
                book_id: book_id
            },
            success: function (msg) {
                if (msg) {
                    document.getElementById('page_title').value = value;
                    document.getElementById('page-title-message').style.display = 'block';
                } else {
                    document.getElementById('page-title-message').style.display = 'none';
                }
            }
        });
    }
}

//Page no validation during edit
function checkPageNoForEdit(value) {
    //alert("came");
    var apiBaseURL = `${window.location.origin}/admin`;
    var page_no = $("#page_number").val(); // value in field email
    var book_id = $("#book_id").val();
    var chapter_no = $("#chapter_no").val();
    if (page_no == value) {
        document.getElementById('page-title-message').style.display = 'none'
    } else {
        $.ajax({
            type: 'post',
            url: apiBaseURL + "/verify-page-number", // put your real file name 
            data: {
                chapter_no: chapter_no,
                page_no: page_no,
                book_id: book_id
            },
            success: function (msg) {
                if (msg) {
                    document.getElementById('page_number').value = value;
                    document.getElementById('page-no-message').style.display = 'block';
                } else {
                    document.getElementById('page-no-message').style.display = 'none';
                }
            }
        });
    }
}

//Avatar title validation during add
function checkAvatarTitle(type) {
    // alert("came");
    var apiBaseURL = `${window.location.origin}/admin`;
    var title = $("#title").val();
    // alert(title);
    $.ajax({
        type: 'post',
        url: apiBaseURL + "/verify-avatar-title",
        data: {
            title: title,
        },
        success: function (msg) {
            if (msg) {
                document.getElementById('title').value = '';
                toastr.error(msg, 'Error!', {
                    positionClass: 'toast-top-right'
                });
            } else {
                toastr.success('You can use this title.', 'Success!', {
                    positionClass: 'toast-top-right'
                });
            }
        }
    });
}

//Avatar title validation during edit
function checkAvatarTitleForEdit(value) {
    // alert("came");
    var apiBaseURL = `${window.location.origin}/admin`;
    var title = $("#title").val(); // value in field

    if (title == value) {
        document.getElementById('avatar-title-message').style.display = 'none'
    } else {
        $.ajax({
            type: 'post',
            url: apiBaseURL + "/verify-avatar-title", // put your real file name 
            data: {
                title: title,
            },
            success: function (msg) {
                if (msg) {
                    document.getElementById('title').value = value;
                    toastr.error(msg, 'Error!', {
                        positionClass: 'toast-top-right'
                    });
                } else {
                    toastr.success('You can use this title.', 'Success!', {
                        positionClass: 'toast-top-right'
                    });
                }
            }
        });
    }
}